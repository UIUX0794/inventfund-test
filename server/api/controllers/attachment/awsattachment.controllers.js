/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var multer = require('multer');
var file = require('file-system');
var fs = require('fs-extra');
var path = require('path');
var joinPath = require('path.join');
var uuidv4 = require('uuid/v4'); // to generate random number
var async = require('async');
var config = require('../../../config/config');
var aws = require('aws-sdk');
var mongodbCrud = require('../crudmongodb.controllers');
var multerS3 = require('multer-s3');
const JSON = require('circular-json');
var path = require('path');
var fs = require('fs');
var axios = require('axios');

var s3bucket;
var currentDate = new Date();
var bucketName;
var dbName;
var expireTime = config.AWSLINKEXPIRETIME;

// if (config.type == 'DEVELOPMENT') {
// 	aws.config.loadFromPath('./config/devawsconfig.json');
// 	s3bucket = new aws.S3({ params: { Bucket: config.AWSS3bucketNameDev } });
// 	bucketName = config.AWSS3bucketNameDev;
// 	dbName = config.DBNAMEDEV;
// } else if (config.type == 'TESTING') {
// 	aws.config.loadFromPath('./config/testawsconfig.json');
// 	s3bucket = new aws.S3({ params: { Bucket: config.AWSS3bucketNameTest } });
// 	bucketName = config.AWSS3bucketNameTest;
// 	dbName = config.DBNAMEDEV;
// }

if (config.type == 'DEVELOPMENT') {
  aws.config.loadFromPath('./config/devawsconfig.json');
  s3bucket = new aws.S3({ params: { Bucket: config.AWSS3bucketNameDev } });
  bucketName = config.AWSS3bucketNameDev;
  dbName = config.DBNAMEDEV;
} else if (config.type == 'TESTING') {
  aws.config.loadFromPath('./config/testawsconfig.json');
  s3bucket = new aws.S3({ params: { Bucket: config.AWSS3bucketNameTest } });
  bucketName = config.AWSS3bucketNameTest;
  dbName = config.DBNAMETEST;
} else if (config.type == 'UAT') {
  aws.config.loadFromPath('./config/uatawsconfig.json');
  s3bucket = new aws.S3({ params: { Bucket: config.AWSS3bucketNameUat } });
  bucketName = config.AWSS3bucketNameUat;
  dbName = config.DBNAMEUAT;
} else if (config.type == 'PRODUCTION') {
  aws.config.loadFromPath('./config/prodawsconfig.json');
  s3bucket = new aws.S3({ params: { Bucket: config.AWSS3bucketNameProd } });
  bucketName = config.AWSS3bucketNameProd;
  dbName = config.DBNAMEPROD;
}

module.exports.getSignedData = async (req, res) => {
  var jsonObj;
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  // console.log('get signed urls : ' + JSON.stringify(jsonObj));
  var concatPathKey = jsonObj.path + jsonObj.key;
  var params = { Bucket: bucketName, Key: concatPathKey, Expires: expireTime };
  s3bucket.getSignedUrl('getObject', params, function (err, url) {
    // get only object from s3
    // console.log(url);
    if (url) {
      res.status(200).json({ status: 200, data: url });
    } else {
      // console.log('err');
      res.status(400).json({ status: 400, data: 'NA' });
    }
  });
};

module.exports.retrieveAttachmentDataWithSignedUrl = function (req, res) {
  var returnData = {};
  if (req.body) {
    var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    var jsonObj = req;
  }
  jsonObj['collectionName'] = 'attachment';
  mongodbCrud.retrieveAll(jsonObj, function (retieveAttachmentData) {
    // console.log('retieveAttachmentData--->' + retieveAttachmentData);
    // looping through array of keys to get signed url
    if (retieveAttachmentData.status == 200 && retieveAttachmentData.response.length) {
      // var mapDataSigned = retieveAttachmentData.response.map((item) => {
      //   // console('attachment each item : '+JSON.stringify(item))
      //   var concatPathKey = item.path + item.key;
      //   var params = { Bucket: bucketName, Key: concatPathKey, Expires: expireTime };
      //   // get signed url for each key
      //   var signedUrl = s3bucket.getSignedUrl('getObject', params);
      //   // console('signed url assign : '+JSON.stringify(signedUrl))
      //   // adding new key and value to object after getting signed url from s3
      //   item['signedUrl'] = signedUrl;
      //   return item;
      // });

      // final array data
      // console('mapDataSigned : ' + JSON.stringify(mapDataSigned));
      // if (mapDataSigned.length !== 0) {
      //   returnData.status = 200;
      //   returnData.message = 'Success';
      //   returnData.error = 'NA';
      //   returnData.data = mapDataSigned;
      //   if (req.body) {
      //     res.status(200).json(returnData);
      //   } else {
      //     res(returnData);
      //   }
      // } else {
      returnData.status = 200;
      returnData.message = 'Success';
      returnData.error = 'NA';
      returnData.data = retieveAttachmentData.response;
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
      // }
    } else {
      returnData.status = 200;
      returnData.message = 'Sucess';
      returnData.error = 'NA';
      returnData.data = [];
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  });
};

module.exports.getSignedDataFun = (req) => {
  var jsonObj;
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  // console.log('get signed urls : ' + JSON.stringify(jsonObj));
  var concatPathKey = jsonObj.path + jsonObj.key;
  var params = { Bucket: bucketName, Key: concatPathKey, Expires: expireTime };
  return s3bucket.getSignedUrl('getObject', params, function (err, url) {
    // get only object from s3
    // console.log(url);
    if (url) {
      return url;
    } else {
      // console.log('err');
      return null;
    }
  });
};

function base64_encode(file) {
  // read binary data
  var bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString('base64');
}

module.exports.uploadS3WithSignedurl = async (req, res) => {
  // console('req data : '+JSON.stringify(req))
  var fileData = req.files['file'];
  var jsonObj;
  if (req.files['file'].data) {
    jsonObj = JSON.parse(JSON.stringify(req.files['file'].data, null, 3));
  } else {
    jsonObj = req;
  }
  var key = path.parse(fileData['name']).name;
  var params = {
    Bucket: bucketName,
    Key: joinPath('inventFund') + '/' + key, // 'inventFundSigned/data/' + key,
    ContentType: fileData['mimetype'], // This uses the Node mime library
    Expires: expireTime,
  };
  var surl = await s3bucket.getSignedUrl('putObject', params, function (err, surl) {
    if (!err) {
      // console.log('signed url: ' + surl);
      axios({
        url: surl,
        method: 'PUT',
        data: buffer,
        ContentType: fileData['mimetype'],
      })
        .then((result) => {
          // console('result : '+JSON.stringify(result['request']['_redirectable']['_options'].pathname));
          var keyData = result['request']['_redirectable']['_options'].pathname;
          res.status(200).json({ status: 200, data: keyData });
        })
        .catch((err) => {
          // console.log('err : ' + JSON.stringify(err));
          res.status(400).json({ status: 400, data: err });
        });
    } else {
      // console.log('Error signing url ' + err);
    }
  });
};

module.exports.uploadDirectToS3 = (req, res, next) => {
  // await // console.log('surl : '+surl)
  // console.log('direct s3');
  var returnData = {};
  var upload = multer({
    storage: multerS3({
      s3: s3bucket,
      bucket: bucketName,
      metadata: function (req, file, cb) {
        // console.log('req data : ' + JSON.stringify(req));
        // console.log('req file : ' + JSON.stringify(file));
        cb(null, {
          fieldName: joinPath('./inventFund', req.body.TYPE),
        });
      },
      contentType: multerS3.AUTO_CONTENT_TYPE,
      key: (req, file, cb) => {
        var newFileName = file.originalname;
        var fullPath =
          joinPath('./inventFund', req.body.TYPE) +
          '/' +
          newFileName.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/\s]/g, '');
        const fullPathData = JSON.stringify(fullPath);
        // console.log('fullPath : ' + fullPathData);
        cb(null, fullPath);
      },
    }),
  }).any();

  upload(req, res, function (err) {
    // console.log(err);
    if (err) {
      returnData.code = 400;
      returnData.message = 'Error uploading file to AWS S3!';
      returnData.error = err;
      returnData.data = 'NA';
      return res.status(400).json(returnData);
    } else {
      returnData.code = 200;
      returnData.message = 'File uploaded successfully to AWS S3!';
      returnData.error = 'NA';
      // console(res);
      returnData.data = res.socket.parser.incoming.files[0].location;
      var str = res.socket.parser.incoming.files[0].location;
      var splitData = str.split('inventFund')[1];
      // console.log('url key : ' + JSON.stringify(splitData));
      var params = {
        Bucket: bucketName,
        Key: 'inventFund' + splitData,
        Expires: expireTime,
      };
      var signedUrl = s3bucket.getSignedUrl('getObject', params, function (err, url) {
        // console.log('return data : ' + JSON.stringify(url));
        returnData.signedData = url;
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      });
    }
  });
};

// -------------------------------------------------------------------------------------------------
// Upload file in temp location and name in encrypted way
// -------------------------------------------------------------------------------------------------

module.exports.fileUploadTemp = function (req, res, next) {
  var DST = './uploads/temp';
  var upload = multer({
    dest: DST,
  }).any();

  upload(req, res, function (err) {
    if (err) {
      return res.end('Error uploading file.');
    } else {
      var type = req.body.TYPE;
      var email = req.body.EMAIL;
      var id = req.body.ID;

      // console.log('type in fileUpload temp : ' + type);

      req.files.forEach(function (f) {
        var primary_path = f.destination + '/' + f.filename;

        // var secondary_path = joinPath('./uploads', email, type) + '/' + f.filename;

        var secondary_path = joinPath('./uploads', type) + '/' + f.filename;

        // console.log('primary_path : ' + primary_path);
        // console.log('secondary_path : ' + secondary_path);

        fs.move(
          primary_path,
          secondary_path,
          {
            overwrite: true,
          },
          function (err) {
            if (err) {
              return; // console.error(err);
            }
            filePath: String;
            filePath = secondary_path;
            res.end(filePath);
          }
        );
      });
    }
  });
};

// -------------------------------------------------------------------------------------------------
// Upload file in temp location and name in encrypted way
// Call this method in the auth-service and call in ts for uploading the image
// -------------------------------------------------------------------------------------------------

module.exports.fileUploadSingle = function (req, res, next) {
  // console.log('1111111111111111111111111111111111111111111');
  var fileUploadTemp = {};

  var jsonObj;
  var returnData = {};

  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var awsS3FileLocation;
  async.waterfall([
    function (arg1, arg2, callback) {
      // console.log('second callback function arg1 ::::' + arg1);
      // console.log('second callback function arg2 ::::' + arg2);
      fileUploadAwsS3Single({ imageURL: arg2 }, function (error, response) {
        if (response) {
          callback('', {
            fileLocation: response.location,
            localLocation: arg2,
          });
        }
      });
    },
    function (param1, callback) {
      // console.log('param1 in third callback >>> ' + JSON.stringify(param1));
      deleteTempFile({ folderPath: param1.localLocation }, function (err, response) {
        // console.log('response from deleteTempFolder is : ' + JSON.stringify(response));
        if (response.status == 200) {
          awsS3FileLocation = param1.fileLocation;
          callback(null);
        }
      });
    },
    function (callback) {
      attachmentParams = {
        // "db": jsonObj.db,
        db: dbName,
        collectionName: 'asattachment',
        updateData: {
          description: jsonObj.description,
          partyId: jsonObj.partyId,
          type: jsonObj.type,
          awsS3FileLocation: awsS3FileLocation,
          fileName: jsonObj.fileName,
          createdDate: currentDate,
          createdBy: '',
          updatedDate: currentDate,
          updatedBy: '',
        },
      };

      mongodbCrud.createData(attachmentParams, function (attachmentRes) {
        // console.log('attachmentRes--->' + JSON.stringify(attachmentRes));
        if (attachmentRes.status == 200) {
          returnData.code = 200;
          returnData.message = 'File uploaded successfully';
          returnData.error = 'NA';
          returnData.data = attachmentRes.data;
          if (req.body) {
            res.status(200).json(returnData);
          } else {
            res(returnData);
          }
        } else {
          returnData.code = 400;
          returnData.message = 'Error in uploading file in table attachment!';
          returnData.error = attachmentRes.error;
          returnData.data = 'NA';
          // callback(400, returnData);

          if (req.body) {
            res.status(400).json(returnData);
          } else {
            res(returnData);
          }
        }
      });
    },
  ]);
};

// -------------------------------------------------------------------------------------------------
// Upload file in Amazon web service S3 bucket - Single files
// -------------------------------------------------------------------------------------------------
fileUploadAwsS3Single = function (req, callback) {
  var imageURL = req.imageURL;
  var s3Path = imageURL.substring(8);

  var s3FilePath = 'inventFund/' + s3Path;

  // console.log('s3FilePath >>>> ' + s3FilePath);

  s3bucket.createBucket(function () {
    var fileBuffer = fs.readFileSync(imageURL);
    var params = {
      Key: s3FilePath,
      Body: fileBuffer,
    };

    // console.log('params .. ' + params);

    s3bucket.upload(params, function (err, data) {
      if (err) {
        // console.log('Error uploading data: ' + err);
      } else {
        callback('', {
          location: data.Location,
        });
      }
    });
  });
};

// -------------------------------------------------------------------------------------------------
// Deletes file uploaded in temporary location
// -------------------------------------------------------------------------------------------------
deleteTempFile = function (req, callback) {
  // console.log('DELETE TEMP FILE');
  var folderPath = req.folderPath;
  fs.remove(folderPath, function (err) {
    if (err) {
      return; // console.error(err);
    } else {
      // console.log('File deleted successfully!');
      callback('', {
        status: '200',
        message: 'Image in uploads folder deleted successfully!',
      });
    }
  });
};

module.exports.singleAttachment = (req, res) => {
  // console.log('single attachment');
  var returnData = {};
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  // console.log('singleAttachment req : ' + JSON.stringify(jsonObj));
  // res.status(200)
  //     .json(returnData)
  attachmentParams = {
    db: dbName,
    collectionName: jsonObj.collectionName,
    updateData: {
      personId: jsonObj['createData'].id,
      // path: jsonObj['createData'].path,
      // key: jsonObj['createData'].key,
      url: jsonObj['createData'].url,
      type: jsonObj['createData'].type,
      fileType: jsonObj['createData'].fileType ? jsonObj['createData'].fileType : '',
      description: jsonObj['createData'].description ? jsonObj['createData'].description : '',
      createdDate: currentDate,
      createdBy: '',
      updatedDate: currentDate,
      updatedBy: '',
    },
  };

  mongodbCrud.createData(attachmentParams, function (attachmentRes) {
    // console.log('attachmentRes--->' + JSON.stringify(attachmentRes));
    if (attachmentRes.status == 200) {
      returnData.code = 200;
      returnData.message = 'File uploaded successfully';
      returnData.error = 'NA';
      returnData.data = attachmentRes.data;
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    } else {
      returnData.code = 400;
      returnData.message = 'Error in uploading file in table attachment!';
      returnData.error = attachmentRes.error;
      returnData.data = 'NA';
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  });
};

// -------------------------------------------------------------------------------------------------
// Save multiple attachments
// -------------------------------------------------------------------------------------------------
module.exports.saveMultipleAttachment = function (req, res) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var returnData = {};
  // console.log('jsonObj >> ' + JSON.stringify(jsonObj));
  // console.log('jsonObj.imageArray >> ' + JSON.stringify(jsonObj.imageArray));

  //async.forEachLimit is the method it will iterate the array of data.
  //Once u call the userCallback() in the last waterfall the iterator will move to next item.
  //If there is any error in the record sequence pass the error code 400 in userCallback(400),
  //then the control will exit from the loop and stops the iteration and stops the api and throws error response from the API.
  //If record is valid then call userCallback() then control will go to next item in the iterator.
  async.forEachLimit(
    jsonObj.imageArray,
    1,
    function (eachImage, userCallback) {
      async.waterfall(
        [
          function (callback) {
            // console.log('first waterfall function');
            fileUploadAwsS3Single({ imageURL: eachImage.locationLocal }, function (
              error,
              response
            ) {
              if (response) {
                callback('', {
                  fileLocation: response.location,
                  localLocation: eachImage.locationLocal,
                });
              } else if (error) {
                returnData.code = 400;
                returnData.message = 'Error uploading File to AWS S3!';
                returnData.error = error;
                returnData.data = 'NA';
                callback(400);
              }
            });
          },
          function (params, callback) {
            // console.log('second waterfall function');
            deleteTempFile({ folderPath: params.localLocation }, function (err, response) {
              // console.log('response from deleteTempFolder is : ' + JSON.stringify(response));
              if (response.status == 200) {
                awsS3FileLocation = params.fileLocation;
                callback(null);
              }
            });
          },
          function (callback) {
            attachmentParams = {
              //   "db": jsonObj.db,
              db: dbName,
              collectionName: 'asattachment',
              updateData: {
                description: eachImage.description,
                docType: eachImage.docType,
                sourceId: eachImage.sourceId,
                objectId: eachImage.objectId,
                sourceType: eachImage.sourceType,
                partyId: eachImage.partyId,
                type: eachImage.type,
                fileNameExtension: eachImage.fileNameExtension,
                awsS3FileLocation: awsS3FileLocation,
                fileName: eachImage.fileName,
                createdDate: currentDate,
                createdBy: '',
                updatedDate: currentDate,
                updatedBy: '',
              },
            };

            // console.log('attachmentParams--->' + JSON.stringify(attachmentParams));
            mongodbCrud.createData(attachmentParams, function (attachmentRes) {
              // console.log('attachmentRes--->' + JSON.stringify(attachmentRes));
              if (attachmentRes.status == 200) {
                returnData.code = 200;
                returnData.message = 'File uploaded successfully';
                returnData.error = 'NA';
                returnData.data = attachmentRes.data;
                callback(null);
              } else {
                returnData.code = 400;
                returnData.message = 'Error in uploading file in table attachment!';
                returnData.error = attachmentRes.error;
                returnData.data = 'NA';
                callback(400);
              }
            });
          },
        ],
        function (err, result) {
          if (err) {
            // console.log('err in waterfall .....' + err + ' ' + result);
            userCallback(400);
          } else {
            userCallback();
            // console.log('result is success in waterfall');
          }
        }
      );
    },
    function (err) {
      if (err) {
        // console.log('err ' + err);
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      } else {
        // console.log('SUCCESS');
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      }
    }
  );
};

// -------------------------------------------------------------------------------------------------
// Delete attachments from S3 and also from DB
// -------------------------------------------------------------------------------------------------
module.exports.deleteAttachment = function (req, res) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var returnData = {};
  // console.log('jsonObj >> ' + JSON.stringify(jsonObj));
  // console.log('jsonObj.imageArray >> ' + JSON.stringify(jsonObj.imageArray));

  //async.forEachLimit is the method it will iterate the array of data.
  //Once u call the userCallback() in the last waterfall the iterator will move to next item.
  //If there is any error in the record sequence pass the error code 400 in userCallback(400),
  //then the control will exit from the loop and stops the iteration and stops the api and throws error response from the API.
  //If record is valid then call userCallback() then control will go to next item in the iterator.
  async.forEachLimit(
    jsonObj.imageArray,
    1,
    function (eachImage, userCallback) {
      async.waterfall(
        [
          function (callback) {
            // console.log('first waterfall function : ' + eachImage.awsS3FileLocation);
            deleteS3File({ imageURL: eachImage.awsS3FileLocation }, function (error, response) {
              if (response.code == 200) {
                // console.log('return data : ' + JSON.stringify(response));
                returnData.code = 200;
                returnData.message = 'File deleted successfully';
                returnData.error = 'NA';
                returnData.data = response;
                callback(null);
              } else if (error) {
                returnData.code = 400;
                returnData.message = 'Error uploading File to AWS S3!';
                returnData.error = error;
                returnData.data = 'NA';
                callback(400);
              }
            });
          },
        ],
        function (err, result) {
          if (err) {
            // console.log('err in waterfall .....' + err + ' ' + result);
            userCallback(400);
          } else {
            userCallback();
            // console.log('result is success in waterfall');
          }
        }
      );
    },
    function (err) {
      if (err) {
        // console.log('err ' + err);
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      } else {
        // console.log('SUCCESS');
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      }
    }
  );
};

// -------------------------------------------------------------------------------------------------
// File deletion from AWS - S3
// -------------------------------------------------------------------------------------------------
deleteS3File = function (req, callback) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var imageURL = jsonObj.imageURL;
  // console.log('imageURL - : ' + imageURL);
  // for key need to take string from inventFund onwards
  var params = {
    Bucket: bucketName,
    Key: imageURL.substring(imageURL.indexOf('inventFund')),
  };

  var returnData = {};

  s3bucket.deleteObject(params, function (err, data) {
    // console.log('deleteS3File - error : ' + JSON.stringify(err));
    if (data) {
      // console.log('deleteS3File - success : ' + JSON.stringify(data));
      returnData.code = 200;
      returnData.message = 'Success - Selected file is deleted successfully!';
      returnData.data = 'NA';
      returnData.error = 'NA';
      callback('', returnData);
      // if (req.body) {
      //   res
      //     .status(200)
      //     .json(returnData)
      // } else {
      //   res(returnData);
      // }
    } else {
      // console.log('deleteS3File - error : ' + JSON.stringify(err));
      returnData.code = 400;
      returnData.message = 'Error in deleting file.';
      returnData.data = 'NA';
      returnData.error = err;

      // console.log('deleteS3File - data: ' + JSON.stringify(data));
      // if (req.body) {
      //   res
      //     .status(400)
      //     .json(returnData)
      // } else {
      //   res(returnData);
      // }

      callback('', returnData);
    }
  });
};

module.exports.synchronousLoopWithWaterfall = function (req, res) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var testData = [1, 2, -5, 5, 10];
  // var testData = [1,2,5,10,-5];

  var returnData = {};
  //async.forEachLimit is the method it will iterate the array of data.
  //Once u call the userCallback() in the last waterfall the iterator will move to next item.
  //If there is any error in the record sequence pass the error code 400 in userCallback(400),
  //then the control will exit from the loop and stops the iteration and stops the api and throws error response from the API.
  //If record is valid then call userCallback() then control will go to next item in the iterator.
  async.forEachLimit(
    testData,
    1,
    function (eachItem, userCallback) {
      async.waterfall(
        [
          function (callback) {
            // console.log('first waterfall function');
            returnData.code = '200';
            returnData.message = 'Success!';
            callback(null);
          },
          function (callback) {
            // console.log('second waterfall function');
            if (eachItem > 0) {
              returnData.code = '200';
              returnData.message = 'Success!';
              callback(null);
            } else {
              // console.log('error in record sequence.....');
              returnData.code = '400';
              returnData.message = 'Negative Number in sequence!';
              callback('400', returnData);
            }
          },
        ],
        function (err, result) {
          if (err) {
            // console.log('err in waterfall .....' + err);
            userCallback(400);
          } else {
            userCallback();
            // console.log('result is success in waterfall' + result);
          }
        }
      );
    },
    function (err) {
      if (err) {
        // console.log('err ' + err);
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      } else {
        // console.log('SUCCESS');

        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      }
    }
  );
};

// -------------------------------------------------------------------------------------------------
// Save multiple attachments
// -------------------------------------------------------------------------------------------------

module.exports.updateAttachment = function (req, res) {
  var updateId;
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  var updateData = jsonObj;
  var returnData = {};
  async.waterfall(
    [
      function (callback) {
        // console.log('first waterfall function : ' + JSON.stringify(jsonObj));
        var params = {
          //   "db": jsonObj.db,
          db: dbName,
          collectionName: 'attachment',
          queryStr: {
            $and: [
              {
                personId: { $eq: jsonObj['createData'].id },
                type: { $eq: 'PROFILE' },
              },
            ],
          },
        };
        mongodbCrud.retrieveAll(params, (res) => {
          // console.log('data retrived : ' + JSON.stringify(res));
          if (res.status == 200 && res.response.length != 0) {
            updateId = res.response[0]._id;
            // console.log(res.response[0].path + res.response[0].key);
            // urlDelete = res.response[0].path + res.response[0].key;
            urlDelete = res.response[0].url;
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'ERROR!';
            returnData.data = 'NA';
            returnData.error = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        // console.log('first waterfall function : ' + urlDelete);
        if (urlDelete) {
          deleteS3File({ imageURL: urlDelete }, function (error, response) {
            // console.log('delete object from s3 : ' + JSON.stringify(response));
            if (response.code == 200) {
              callback(null);
            } else if (error) {
              returnData.code = 400;
              returnData.message = 'Error uploading File to AWS S3!';
              returnData.error = error;
              returnData.data = 'NA';
              callback(400);
            }
          });
        } else {
          callback(null);
        }
      },
      function (callback) {
        // console('data for update : ' + JSON.stringify(updateId));
        attachmentDeleteParams = {
          db: dbName,
          collectionName: 'attachment',
          pkId: updateId,
        };

        // console('attachmentParams--->' + JSON.stringify(attachmentDeleteParams));
        mongodbCrud.deleteData(attachmentDeleteParams, function (attachmentRes) {
          // console('attachmentRes delete--->' + JSON.stringify(attachmentRes));
          if (attachmentRes.status == 200) {
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in uploading file in table attachment!';
            returnData.error = attachmentRes.error;
            returnData.data = 'NA';
            callback(400);
          }
        });
      },
      function (callback) {
        // console('data for update : ' + JSON.stringify(updateId));
        // console('data for update : ' + JSON.stringify(jsonObj));
        // console('json update after delete : ' + JSON.stringify(updateData));
        attachmentParams = {
          db: dbName,
          collectionName: updateData.collectionName,
          updateData: {
            personId: updateData['createData'].id,
            // path: updateData['createData'].path,
            // key: updateData['createData'].key,
            url: updateData['createData'].url,
            type: updateData['createData'].type,
            createdDate: currentDate,
            createdBy: '',
            updatedDate: currentDate,
            updatedBy: '',
          },
        };

        mongodbCrud.createData(attachmentParams, function (attachmentRes) {
          // console.log('attachmentRes--->' + JSON.stringify(attachmentRes));
          if (attachmentRes.status == 200) {
            returnData.code = 200;
            returnData.message = 'File uploaded successfully';
            returnData.error = 'NA';
            returnData.data = attachmentRes.data;
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in uploading file in table attachment!';
            returnData.error = attachmentRes.error;
            returnData.data = 'NA';
            callback(400);
          }
        });
      },
    ],
    function (err, result) {
      if (err) {
        // console.log('err ' + err);
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      } else {
        // console.log('SUCCESS');
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      }
    }
  );
};

module.exports.deleteAttachmentSingle = function (req, res) {
  // {"_id":"5ebe41c0153e0013d444fce4","personId":"5eb910cedad6633afcc6b1ce",
  // "url":"https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFund/HOST/100/IFFIXER/SHOWCASE/5eb910cedad6633afcc6b1ce/SPACE.JPG",
  // "type":"SHOWCASE","fileType":"image/jpeg","description":"hello.","createdDate":"2020-05-15T04:04:17.139Z","createdBy":"","updatedDate":"2020-05-15T04:04:17.139Z","updatedBy":""}
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  // console.log('delete single attach : ' + JSON.stringify(jsonObj));
  var urlDelete = jsonObj['data'].url;
  var deleteAttachment = jsonObj;
  var returnData = {};

  async.waterfall(
    [
      function (callback) {
        deleteS3File({ imageURL: urlDelete }, function (error, response) {
          // console.log(JSON.stringify(response));
          if (response.code == 200) {
            callback(null);
          } else if (error) {
            returnData.code = 400;
            returnData.message = 'Error uploading File to AWS S3!';
            returnData.error = error;
            returnData.data = 'NA';
            callback(400);
          }
        });
      },
      function (callback) {
        // console.log('delete single attach data base : ' + JSON.stringify(jsonObj));
        var obj = {
          collectionName: deleteAttachment.collectionName,
          pkId: deleteAttachment['data']._id,
        };
        // console.log('delete single attach params constrction : ' + JSON.stringify(obj));
        mongodbCrud.deleteData(obj, function (resAttachment) {
          // console.log('delete image in database : ' + JSON.stringify(resAttachment));
          if (resAttachment.status == 200) {
            returnData.code = 200;
            returnData.message = 'sucess!';
            returnData.data = 'NA';
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error uploading File to AWS S3!';
            returnData.error = 'error';
            returnData.data = 'NA';
            callback(400);
          }
        });
      },
    ],
    function (err, result) {
      if (err) {
        // console.log('err ' + err);
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      } else {
        // console.log('SUCCESS');
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      }
    }
  );
};

module.exports.getSignedUrlWithId = function (req, res) {
  var updateId;
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  var updateData = jsonObj;
  var returnData = {};
  async.waterfall(
    [
      function (callback) {
        // console.log('first waterfall function : ' + JSON.stringify(jsonObj));
        var params = {
          db: dbName,
          collectionName: 'attachment',
          queryStr: {
            $and: [
              {
                personId: { $eq: jsonObj.personId },
                type: { $eq: jsonObj.type },
              },
            ],
          },
        };
        mongodbCrud.retrieveAll(params, (res) => {
          if (res.response.length) {
            var obj = res.response[0];
            // console.log('obj data : ' + JSON.stringify(obj));
            // console.log('condition 1 : ' + ('path' in obj && 'key' in obj));
            if ('path' in obj && 'key' in obj) {
              // console.log('condition 2 : ' + (obj.path !== null && obj.key !== null));
              if (obj.path !== null && obj.key !== null) {
                var concatPathKey = obj.path + obj.key;
                var params = { Bucket: bucketName, Key: concatPathKey, Expires: expireTime };
                var signedUrl = s3bucket.getSignedUrl('getObject', params);
                // console.log('data retrived : ' + JSON.stringify(res));
                if (signedUrl) {
                  returnData.code = 200;
                  returnData.data = signedUrl;
                  callback(200, returnData);
                } else {
                  returnData.code = 400;
                  returnData.data = 'error signedurl !';
                  callback(400, returnData);
                }
              } else {
                returnData.code = 400;
                returnData.data = 'error path and key null !';
                callback(400, returnData);
              }
            } else if ('url' in obj) {
              returnData.code = 200;
              returnData.data = obj.url;
              callback(200, returnData);
            } else {
              returnData.code = 400;
              returnData.data = 'error path and key dosent exist !';
              callback(400, returnData);
            }
          } else {
            returnData.code = 400;
            returnData.data = 'error response empty !';
            callback(400, returnData);
          }
        });
      },
    ],
    function (err, result) {
      if (err) {
        // console.log('err ' + err);
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      } else {
        // console.log('SUCCESS');
        if (req.body) {
          res.status(200).json(returnData);
        } else {
          res(returnData);
        }
      }
    }
  );
};
