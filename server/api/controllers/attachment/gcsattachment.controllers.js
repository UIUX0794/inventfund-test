/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var multer = require('multer');
var file = require('file-system');
var fs = require('fs-extra');
var path = require('path');
var joinPath = require('path.join');
var uuidv4 = require('uuid/v4'); // to generate random number
var async = require("async");
var config=require('../../../config/config');
var gcsBucketName;
var S3BucketName;
var bucketName;
if (config.type == 'DEVELOPMENT') {
   gcsBucketName = config.GCSbucketNameDev;
} else if (config.type == 'TEST') {
    gcsBucketName = config.GCSbucketNameTest;
} else if  (config.type == 'PRODUCTION') {
    gcsBucketName = config.GCSbucketNameProd;
}

var mongodbCrud = require('../crudmongodb.controllers');

// Imports the Google Cloud client library
const {Storage} = require('@google-cloud/storage');

var currentDate = new Date();


// -------------------------------------------------------------------------------------------------
// Upload file in temp location and name in encrypted way
// -------------------------------------------------------------------------------------------------
module.exports.fileUploadTemp = function (req, res, next) {
  var DST = './uploads/temp';
  var upload = multer({
    dest: DST
  }).any();

  upload(req, res, function (err) {
    if (err) {
      return res.end("Error uploading file.");
    } else {
      var type = req.body.TYPE;
      var email = req.body.EMAIL;
      var id = req.body.ID;

      console.log("type in fileUpload temp : " + type);

      req.files.forEach(function (f) {
        var primary_path = f.destination + '/' + f.filename;

        // var secondary_path = joinPath('./uploads', email, type) + '/' + f.filename;

        var secondary_path = joinPath('./uploads', type) + '/' + f.filename;

        console.log("primary_path : " + primary_path);
        console.log("secondary_path : " + secondary_path);
        
        fs.move(primary_path, secondary_path, {
          overwrite: true
        }, function (err) {
          if (err) {
            return console.error(err)
          }
          filePath: String;
          filePath = secondary_path;
          res.end(filePath)
        });
      });
    }
  });
}



// -------------------------------------------------------------------------------------------------
// Upload file in temp location and name in encrypted way
// Call this method in the auth-service and call in ts for uploading the image
// -------------------------------------------------------------------------------------------------
module.exports.fileUploadSingle = function (req, res, next) {
    console.log("1111111111111111111111111111111111111111111" );
    var fileUploadTemp = {};

    var jsonObj;
    var returnData = {};

    if (req.body) {
      jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
      jsonObj = req;
    }

    var gcsFileLocation;
    async.waterfall(
      [
        function(arg1,arg2,callback){
          console.log("second callback function arg1 ::::" +arg1);
          console.log("second callback function arg2 ::::" +arg2);
          fileUploadGcsSingle({'imageURL':arg2},
              function (error, response) {
                if(response){
                  callback('', {'fileLocation':response.location,'localLocation':arg2});
                }              
                              
              });
        }, function(param1,callback){
          console.log("param1 in third callback >>> " + JSON.stringify(param1));
          deleteTempFile({'folderPath':param1.localLocation},function(err,response){
            console.log("response from deleteTempFolder is : " +JSON.stringify(response));
            if(response.status == 200){
              gcsFileLocation = param1.fileLocation;
              callback(null);
            }            
          })
        }, function(callback){
          attachmentParams = {
            "db": config.AUTOPORTALDBNAME,
            "collectionName": "asattachment",
            "updateData": {
              "description": jsonObj.description,
              "partyId":jsonObj.partyId,
              "gcsFileLocation":gcsFileLocation,
              "fileName":jsonObj.fileName,
              "createdDate": currentDate,
              "createdBy": "",
              "updatedDate": currentDate,
              "updatedBy": ""
            }
          }
    
          mongodbCrud.createData(attachmentParams, function (attachmentRes) {
            console.log('attachmentRes--->'+JSON.stringify(attachmentRes))
            if (attachmentRes.status == 200) {
              returnData.code = 200;
              returnData.message = "File uploaded successfully";
              returnData.error = "NA";
              returnData.data = attachmentRes.data;
              if (req.body) {
                res.status(200).json(returnData)
              } else {
                res(returnData)
              }
            }
            else {
              returnData.code = 400;
              returnData.message = "Error in uploading file in table attachment!";
              returnData.error = attachmentRes.error;
              returnData.data = "NA";
              // callback(400, returnData);

              if (req.body) {
                res.status(400).json(returnData)
              } else {
                res(returnData)
              }
            }
          })
        }
    ])
}

// -------------------------------------------------------------------------------------------------
// Upload file in Google Cloud Storage(GCS) - Single files
// -------------------------------------------------------------------------------------------------
fileUploadGcsSingle = function (req, callback) {
    var imageURL = req.imageURL;
    var gcsPath = imageURL.substring(8);
    
    console.log("gcsPath >>>> " +gcsPath);
    // Creates a client
    const storage = new Storage();

    var BUCKET_NAME = gcsBucketName //'auto-webapp-test'
    // https://googlecloudplatform.github.io/google-cloud-node/#/docs/google-cloud/0.39.0/storage/bucket
    var myBucket = storage.bucket(BUCKET_NAME)
    let filename = imageURL // "D:\\work\\traning\\nodejs\\gcp\\test.txt"
    //let filename = "/uploads/test.txt";
    console.log("File Name is " + filename );

    // var gcsFilePath = "customer/" + gcsPath;

    var gcsFilePath = gcsPath;

    console.log("gcsFilePath : " +gcsFilePath);
    storage.bucket(BUCKET_NAME).upload(filename, {
        // Support for HTTP requests made with `Accept-Encoding: gzip`
        gzip: true,
        destination: gcsFilePath,
        // By setting the option `destination`, you can change the name of the
        // object you are uploading to a bucket.
        metadata: {
            // Enable long-lived HTTP caching headers
            // Use only if the contents of the file will never change
            // (If the contents will change, use cacheControl: 'no-cache')
            cacheControl: 'public, max-age=31536000',
        },
    }).then(
      console.log("SUCCESSS"),
      callback('', {
      "location": `https://storage.googleapis.com/${BUCKET_NAME}/${gcsFilePath}`,
    }))
    .catch(console.log("error :"));

    // get public url for file
    var getPublicThumbnailUrlForItem = gcsFilePath => {
      return `https://storage.googleapis.com/${BUCKET_NAME}/${gcsFilePath}`
    }

    
    
}


// -------------------------------------------------------------------------------------------------
// Deletes file uploaded in temporary location
// -------------------------------------------------------------------------------------------------
deleteTempFile = function (req, callback) {
  console.log("DELETE TEMP FILE");
  var folderPath = req.folderPath;
  fs.remove(folderPath, function (err) {
    if (err) {
      return console.error(err) 
    } else {  
      console.log("File deleted successfully!");    
      callback('', {
        "status": '200',
        "message":'Image in uploads folder deleted successfully!'
      });
    }
  });
}



// -------------------------------------------------------------------------------------------------
// Save multiple attachments
// -------------------------------------------------------------------------------------------------
module.exports.saveMultipleAttachment = function (req, res) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var returnData = {};
  console.log("jsonObj >> " + JSON.stringify(jsonObj));
  console.log("jsonObj.imageArray >> " + JSON.stringify(jsonObj.imageArray));

  //async.forEachLimit is the method it will iterate the array of data.
  //Once u call the userCallback() in the last waterfall the iterator will move to next item.
  //If there is any error in the record sequence pass the error code 400 in userCallback(400),
  //then the control will exit from the loop and stops the iteration and stops the api and throws error response from the API.
  //If record is valid then call userCallback() then control will go to next item in the iterator.
  async.forEachLimit(jsonObj.imageArray,1,function(eachImage, userCallback){
    async.waterfall([
      function(callback){
        console.log("first waterfall function");
        fileUploadGcsSingle({'imageURL':eachImage.locationLocal},
          function (error, response) {
            if(response){
              callback('', {'fileLocation':response.location,'localLocation':eachImage.locationLocal});
            } else if(error){
              returnData.code = 400;
              returnData.message = "Error uploading File to GCS!";
              returnData.error = error;
              returnData.data = "NA";
              callback(400);
            }             
        });
      },function(params,callback){
        console.log("second waterfall function");
        // if(eachImage > 0){
        //   callback(null);
        // } else {
        //   console.log("error in record sequence.....");
        //   callback("400","Negative Number in sequence");
        // }
        deleteTempFile({'folderPath':params.localLocation},function(err,response){
          console.log("response from deleteTempFolder is : " +JSON.stringify(response));
          if(response.status == 200){
            gcsFileLocation = params.fileLocation;
            callback(null);
          }            
        })
      }, function(callback){
        attachmentParams = {
          "db": config.AUTOPORTALDBNAME,
          "collectionName": "asattachment",
          "updateData": {
            "description": eachImage.description,
            "partyId":eachImage.partyId,
            "gcsFileLocation":gcsFileLocation,
            "fileName":eachImage.fileName,
            "createdDate": currentDate,
            "createdBy": "",
            "updatedDate": currentDate,
            "updatedBy": ""
          }
        }
  
        mongodbCrud.createData(attachmentParams, function (attachmentRes) {
          console.log('attachmentRes--->'+JSON.stringify(attachmentRes))
          if (attachmentRes.status == 200) {
            returnData.code = 200;
            returnData.message = "File uploaded successfully";
            returnData.error = "NA";
            returnData.data = attachmentRes.data;
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = "Error in uploading file in table attachment!";
            returnData.error = attachmentRes.error;
            returnData.data = "NA";
            callback(400);            
          }
        })
      }
    ], function(err,result){
      if(err){        
        console.log("err in waterfall ....." + err + " " + result);
        userCallback(400);
      } else{
        userCallback();
        console.log("result is success in waterfall");
      }
    })
  },function(err){
    if(err){
      console.log("err " +err);
      if (req.body) {
        res
          .status(200)
          .json(returnData)
      } else {
        res(returnData);
      }
    } else {
      console.log("SUCCESS");
      if (req.body) {
        res
          .status(200)
          .json(returnData)
      } else {
        res(returnData);
      }
    }
  })
}



module.exports.synchronousLoopWithWaterfall = function (req, res) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var testData = [1,2,-5,5,10,];
  // var testData = [1,2,5,10,-5];

  
  var returnData = {};
  //async.forEachLimit is the method it will iterate the array of data.
  //Once u call the userCallback() in the last waterfall the iterator will move to next item.
  //If there is any error in the record sequence pass the error code 400 in userCallback(400),
  //then the control will exit from the loop and stops the iteration and stops the api and throws error response from the API.
  //If record is valid then call userCallback() then control will go to next item in the iterator.
  async.forEachLimit(testData,1,function(eachItem, userCallback){
    async.waterfall([
      function(callback){
        console.log("first waterfall function");
        returnData.code = "200";
        returnData.message = "Success!"
        callback(null);
      },function(callback){
        console.log("second waterfall function");
        if(eachItem > 0){
          returnData.code = "200";
          returnData.message = "Success!"
          callback(null);
        } else {
          console.log("error in record sequence.....");
          returnData.code = "400";
          returnData.message = "Negative Number in sequence!"
          callback("400",returnData);
        }
      }
    ], function(err,result){
      if(err){        
        console.log("err in waterfall ....." + err);
        userCallback(400);
      } else{
        userCallback();
        console.log("result is success in waterfall" + result);
      }
    })
  },function(err){
    if(err){
      console.log("err " +err);
      if (req.body) {
        res
          .status(200)
          .json(returnData)
      } else {
        res(returnData);
      }
    } else {
      console.log("SUCCESS");

      if (req.body) {
        res
          .status(200)
          .json(returnData)
      } else {
        res(returnData);
      }
    }
  })
}
