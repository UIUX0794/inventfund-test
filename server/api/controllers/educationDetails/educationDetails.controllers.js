/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var mongodbCrud = require('../crudmongodb.controllers');
var config = require('../../../config/config');
var async = require("async");
var ObjectId = require('mongodb').ObjectID;
const mortgageCalculate = require('mortgage-calculate');
var Loan = require('loanjs').Loan;
var moment = require('moment');
moment().format();

var currentDate = new Date();


module.exports.createEducationDetails = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = jsonObj.collectionName

      mongodbCrud.createData(jsonObj, function (createEducationDetails) {
        console.log('createEducationDetails--->'+JSON.stringify(createEducationDetails))
        if (createEducationDetails.status == 200) {
          returnData.status = createEducationDetails.status;
          returnData.message = createEducationDetails.message;
          returnData.error = createEducationDetails.error;
          returnData.data = createEducationDetails.data;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = createEducationDetails.status;
          returnData.message = createEducationDetails.message;
          returnData.error = createEducationDetails.error;
          returnData.data = createEducationDetails.data;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })

}


module.exports.updateEducationDetails = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = jsonObj.collectionName

      mongodbCrud.updateData(jsonObj, function (updateEducationDetails) {
        // console.log('updateEducationDetails--->'+JSON.stringify(updateEducationDetails))
        if (updateEducationDetails.status == 200) {
          returnData.status = updateEducationDetails.status;
          returnData.message = updateEducationDetails.message;
          returnData.error = updateEducationDetails.error;
          returnData.data = updateEducationDetails.data;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = updateEducationDetails.status;
          returnData.message = updateEducationDetails.message;
          returnData.error = updateEducationDetails.error;
          returnData.data = updateEducationDetails.data;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })
}

module.exports.retrieveEducationDetailsOne = function (req, res) {
  var returnData = {};
  if (req.body) {
      var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
      var jsonObj = req;
  }

  jsonObj["collectionName"] = jsonObj.collectionName

    mongodbCrud.retrieveOne(jsonObj, function (retieveEducationDetailsOne) {
      console.log('retieveEducationDetailsOne--->'+JSON.stringify(retieveEducationDetailsOne))
      if (retieveEducationDetailsOne.status == 200) {
        returnData.status = retieveEducationDetailsOne.status;
        returnData.message = retieveEducationDetailsOne.message;
        returnData.error = retieveEducationDetailsOne.error;
        returnData.response = retieveEducationDetailsOne.response;
        if (req.body) {
          res.status(200).json(returnData)
        } else {
          res(returnData)
        }
      }
      else {
        returnData.status = retieveEducationDetailsOne.status;
        returnData.message = retieveEducationDetailsOne.message;
        returnData.error = retieveEducationDetailsOne.error;
        returnData.response = retieveEducationDetailsOne.response;

        if (req.body) {
          res.status(400).json(returnData)
        } else {
          res(returnData)
        }
      }
    })

}


module.exports.retrieveEducationDetails = function (req, res) {
  var returnData = {};
  if (req.body) {
      var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
      var jsonObj = req;
  }

  jsonObj["collectionName"] = jsonObj.collectionName
  console.log('json obj in retrieve profile summary:'+JSON.stringify(jsonObj))
    mongodbCrud.retrieveAll(jsonObj, function (retrieveEducationDetails) {
      console.log('retrieveEducationDetails--->'+JSON.stringify(retrieveEducationDetails))
      if (retrieveEducationDetails.status == 200) {
        returnData.status = retrieveEducationDetails.status;
        returnData.message = retrieveEducationDetails.message;
        returnData.error = retrieveEducationDetails.error;
        returnData.response = retrieveEducationDetails.response;
        if (req.body) {
          res.status(200).json(returnData)
        } else {
          res(returnData)
        }
      }
      else {
        returnData.status = retrieveEducationDetails.status;
        returnData.message = retrieveEducationDetails.message;
        returnData.error = retrieveEducationDetails.error;
        returnData.response = retrieveEducationDetails.response;

        if (req.body) {
          res.status(400).json(returnData)
        } else {
          res(returnData)
        }
      }
    })

}


module.exports.deleteEducationDetails = function (req, res) {
  var returnData = {};
  if (req.body) {
      var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
      var jsonObj = req;
  }

  jsonObj["collectionName"] = jsonObj.collectionName
  console.log("delete EducationDetails : "+JSON.stringify(jsonObj))
    mongodbCrud.deleteData(jsonObj, function (deleteEducationDetailsRes) {
      console.log('deleteEducationDetailsRes--->'+JSON.stringify(deleteEducationDetailsRes))
      if (deleteEducationDetailsRes.status == 200) {
        returnData.status = deleteEducationDetailsRes.status;
        returnData.message = deleteEducationDetailsRes.message;
        returnData.error = deleteEducationDetailsRes.error;
        returnData.response = deleteEducationDetailsRes.response;
        if (req.body) {
          res.status(200).json(returnData)
        } else {
          res(returnData)
        }
      }
      else {
        returnData.status = deleteEducationDetailsRes.status;
        returnData.message = deleteEducationDetailsRes.message;
        returnData.error = deleteEducationDetailsRes.error;
        returnData.response = deleteEducationDetailsRes.response;

        if (req.body) {
          res.status(400).json(returnData)
        } else {
          res(returnData)
        }
      }
    })

}