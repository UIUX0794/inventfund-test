const nodemailer = require("nodemailer");
var config = require('../../../config/config');

var multer = require('multer');
var file = require('file-system');
var fs = require('fs-extra');
var path = require('path');
var joinPath = require('path.join');

var smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'support.unigovern@crecientech.com',
        pass: 'crecientech@123'       
    },
    tls: { rejectUnauthorized: false }
});



//----------------------------------------------------------------------------------
// Purpose: To Send the Email to the individuals or a list of person.
// Pass this following json to this method to send the email 		
//  { "subject": "String - send your own structured subject",
//     "body": "String - pass the body to be sent in the email. It can also include the html tags",
//     "to": ["emailId1@gmail.com","emailId2@gmail.com"] - 
//               Array:Pass the email address to be sent.May be individual email or multiple email
//   }
//-------------------------------------------------------------------------------------
module.exports.sendMail = function (req, res) {
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    var to = jsonObj.to;
    var subject = jsonObj.subject;
    var body = jsonObj.emailBody;

    var mailConfig = {
        from: '"Invent Funds"' + '<' + config.usernameForEmail + '>',
        to: to,
        subject: subject,
        //body : body,
        html: body,

    };

    smtpTransport.sendMail(mailConfig, (error, response) => {
        error ? console.log("error : " + error) : console.log("response : " + JSON.stringify(response));
        smtpTransport.close();

        if (error) {
            if (req.body) {
                res.status(400).json({ "code": "400", "error": error });
            } else {
                res({
                    "code": "200",
                    "error": error
                })
            }


        } else {
            if (req.body) {
                res
                    .status(200)
                    .json({
                        "code": "200",
                        "message": "Email sent successfully!",
                        "response": response
                    })
            } else {
                res({
                    "code": "200",
                    "message": "Email sent successfully!",
                    "response": response
                })
            }
        }
    });

}

module.exports.fileUploadTemp = function (req, res, next) {
    var DST = './uploads/temp';
    var upload = multer({ dest: DST }).any();
    // console.log('fileUploadTemp');

    upload(req, res, function (err) {
        // console.log('req.files[0].path: ' + req.files[0].path);
        if (err) {
            // console.log(err);
            return res.end("Error uploading file.");
        } else {
            var type = req.body.TYPE;
            var email = req.body.EMAIL;
            var id = req.body.ID;
            req.files.forEach(function (f) {
                var primary_path = f.destination + '/' + f.filename;
                // console.log('fileUploadTemp - primary_path: ' + primary_path);

                var secondary_path = joinPath('./uploads', type) + '/' + f.filename;
                // console.log('fileUploadTemp - secondary_path: ' + secondary_path);
                // console.log('fileUploadTemp - f.filename: ' + f.filename);

                fs.move(primary_path, secondary_path, { overwrite: true }, function (err) {
                    if (err) {
                        return console.error(err)
                    }
                    //console.log('final file path: ' + secondary_path + '/' + f.filename);
                    filePath: String;
                    filePath = secondary_path;
                    // console.log('fileUploadTemp - filePath ' + filePath);
                    res.end(filePath)
                });
            });
        }
    });
}

