/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var mongodbCrud = require('../crudmongodb.controllers');
var config = require('../../../config/config');
var async = require('async');
var ObjectId = require('mongodb').ObjectID;
const mortgageCalculate = require('mortgage-calculate');
var Loan = require('loanjs').Loan;
var moment = require('moment');
moment().format();

var currentDate = new Date();
var screenbuilderDbName;
//var domainUrl = "http://localhost:4200/";
// if (config.type == 'DEVELOPMENT') {
//     dbName = config.DBNAMEDEV;
//     screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV
// }

if (config.type == 'DEVELOPMENT') {
	dbName = config.DBNAMEDEV;
	screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV;
} else if (config.type == 'TESTING') {
	dbName = config.DBNAMETEST;
	screenbuilderDbName = config.SCREENBUILDERDBNAMETEST;
} else if (config.type == 'UAT') {
	dbName = config.DBNAMEUAT;
	screenbuilderDbName = config.SCREENBUILDERDBNAMEUAT;
} else if (config.type == 'PRODUCTION') {
	dbName = config.DBNAMEPROD;
	screenbuilderDbName = config.SCREENBUILDERDBNAMEPROD;
}

module.exports.createProfessionalDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;

	mongodbCrud.createData(jsonObj, function (createprofessionalDetails) {
		console.log(
			'createprofessionalDetails--->' +
				JSON.stringify(createprofessionalDetails)
		);
		if (createprofessionalDetails.status == 200) {
			returnData.status = createprofessionalDetails.status;
			returnData.message = createprofessionalDetails.message;
			returnData.error = createprofessionalDetails.error;
			returnData.data = createprofessionalDetails.data;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = createprofessionalDetails.status;
			returnData.message = createprofessionalDetails.message;
			returnData.error = createprofessionalDetails.error;
			returnData.data = createprofessionalDetails.data;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.updateProfessionalDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;

	mongodbCrud.updateData(jsonObj, function (updateProfessionalDetails) {
		// console.log('updateProfessionalDetails--->'+JSON.stringify(updateProfessionalDetails))
		if (updateProfessionalDetails.status == 200) {
			returnData.status = updateProfessionalDetails.status;
			returnData.message = updateProfessionalDetails.message;
			returnData.error = updateProfessionalDetails.error;
			returnData.data = updateProfessionalDetails.data;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = updateProfessionalDetails.status;
			returnData.message = updateProfessionalDetails.message;
			returnData.error = updateProfessionalDetails.error;
			returnData.data = updateProfessionalDetails.data;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.retrieveProfessionalDetailsOne = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;

	mongodbCrud.retrieveOne(jsonObj, function (
		retieveProfessionalDetailsDataOne
	) {
		console.log(
			'retieveProfessionalDetailsDataOne--->' +
				JSON.stringify(retieveProfessionalDetailsDataOne)
		);
		if (retieveProfessionalDetailsDataOne.status == 200) {
			returnData.status = retieveProfessionalDetailsDataOne.status;
			returnData.message = retieveProfessionalDetailsDataOne.message;
			returnData.error = retieveProfessionalDetailsDataOne.error;
			returnData.response = retieveProfessionalDetailsDataOne.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retieveProfessionalDetailsDataOne.status;
			returnData.message = retieveProfessionalDetailsDataOne.message;
			returnData.error = retieveProfessionalDetailsDataOne.error;
			returnData.response = retieveProfessionalDetailsDataOne.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.retrieveProfessionalDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;
	console.log(
		'json obj in retrieve profile summary:' + JSON.stringify(jsonObj)
	);
	mongodbCrud.retrieveAll(jsonObj, function (retrieveProfessionalDetails) {
		console.log(
			'retrieveProfessionalDetails--->' +
				JSON.stringify(retrieveProfessionalDetails)
		);
		if (retrieveProfessionalDetails.status == 200) {
			returnData.status = retrieveProfessionalDetails.status;
			returnData.message = retrieveProfessionalDetails.message;
			returnData.error = retrieveProfessionalDetails.error;
			returnData.response = retrieveProfessionalDetails.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retrieveProfessionalDetails.status;
			returnData.message = retrieveProfessionalDetails.message;
			returnData.error = retrieveProfessionalDetails.error;
			returnData.response = retrieveProfessionalDetails.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

// -------------------------------------------------------------------------------------------------
// getScreenBuilder is the method to be called from front end to get the screen input
// fileds and this will populate the select drop down values from lookUp collection.
// Pass the collection name and db name to which you need the screen builder input fields.
// -------------------------------------------------------------------------------------------------
module.exports.getScreenBuildergrouped = function (req, res, next) {
	console.log('getScreenBuilder method start :');

	var jsonObj;
	var returnData = {};

	if (req.body) {
		jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		jsonObj = req;
	}

	console.log('jsonObj : ' + JSON.stringify(jsonObj));

	var collectionResponse;
	var lookUpResponse;
	async.waterfall(
		[
			function (callback) {
				var retrieveData = {
					//   "db":jsonObj.db,
					db: screenbuilderDbName,
					collectionName: jsonObj.collectionName,
					sortQuery: { order: 1 },
				};

				mongodbCrud.retrieveAllScreenbuilder(retrieveData, function (
					retrieveRes
				) {
					console.log(
						'resposne from db is : ' + JSON.stringify(retrieveRes)
					);
					collectionResponse = retrieveRes.response;
					if (collectionResponse) {
						callback(null);
					}
				});
			},
			function (callback) {
				var retrieveData = {
					db: screenbuilderDbName,
					collectionName: 'lookUp',
				};

				mongodbCrud.retrieveAllScreenbuilder(retrieveData, function (
					retrieveRes
				) {
					console.log(
						'resposne from lookUp db is : ' +
							JSON.stringify(retrieveRes)
					);
					lookUpResponse = retrieveRes.response;
					if (lookUpResponse) {
						callback(null);
					}
				});
			},
			function (callback) {
				console.log(
					'collectionResponse : in 3rd waterfall > ' +
						JSON.stringify(collectionResponse)
				);
				console.log(
					'lookUpResponse : in 3rd waterfall > ' +
						JSON.stringify(lookUpResponse)
				);

				console.log(
					'response look up : ' + JSON.stringify(lookUpResponse)
				);
				collectionResponse.map((eachCollection, index) => {
					if (eachCollection.hasOwnProperty('fieldGroup')) {
						eachCollection.fieldGroup.map((item, index) => {
							if (item.type == 'select') {
								lookUpResponse.map((each, index) => {
									if (each.lookUpKey == item.key) {
										console.log(
											'eachLookUp.lookUpKey : ' +
												item.length
										);
										console.log(
											'eachLookUp.lookUpKey : ' +
												each.lookUpKey
										);
										console.log(
											'eachCollection.key : ' + each.key
										);
										var optionsMap = {};
										optionsMap.value = each.value;
										optionsMap.label = each.label;
										item.templateOptions.options.push(
											optionsMap
										);
										optionsMap = {
											value: '',
											label: '',
										};
									}
								});
								console.log(
									'collection data fieldGroup : ' +
										index +
										' ' +
										JSON.stringify(item)
								);
							}
						});
					} else {
					}
					if (index == collectionResponse.length - 1) {
						console.log(
							'collectionResponse after adding is :: ' +
								JSON.stringify(collectionResponse)
						);
						returnData.code = 200;
						returnData.message =
							'Lookup for select input added successfully!';
						returnData.data = collectionResponse;
						returnData.error = 'NA';
						callback(null);
					}
				});
			},
		],
		function (err, result) {
			if (err) {
				console.log('err ' + err);
				if (req.body) {
					res.status(200).json(returnData);
				} else {
					res(returnData);
				}
			} else {
				console.log('SUCCESS');
				if (req.body) {
					res.status(200).json(returnData);
				} else {
					res(returnData);
				}
			}
		}
	);
};

module.exports.retrieveLookupData = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = 'lookUp';

	mongodbCrud.retrieveAll(jsonObj, function (retieveLookupData) {
		console.log(
			'retieveLookupData--->' + JSON.stringify(retieveLookupData)
		);
		if (retieveLookupData.status == 200) {
			returnData.status = retieveLookupData.status;
			returnData.message = retieveLookupData.message;
			returnData.error = retieveLookupData.error;
			returnData.response = retieveLookupData.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retieveLookupData.status;
			returnData.message = retieveLookupData.message;
			returnData.error = retieveLookupData.error;
			returnData.response = retieveLookupData.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.deleteProfessionalDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = 'fixerProfessionalDetails';
	console.log('delete ProfessionalDetails : ' + JSON.stringify(jsonObj));
	mongodbCrud.deleteData(jsonObj, function (deleteProfessionalDetails) {
		console.log(
			'deleteProfessionalDetails--->' +
				JSON.stringify(deleteProfessionalDetails)
		);
		if (deleteProfessionalDetails.status == 200) {
			returnData.status = deleteProfessionalDetails.status;
			returnData.message = deleteProfessionalDetails.message;
			returnData.error = deleteProfessionalDetails.error;
			returnData.response = deleteProfessionalDetails.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = deleteProfessionalDetails.status;
			returnData.message = deleteProfessionalDetails.message;
			returnData.error = deleteProfessionalDetails.error;
			returnData.response = deleteProfessionalDetails.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};
