/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var mongodbCrud = require('../crudmongodb.controllers');
var config = require('../../../config/config');
var async = require("async");
var ObjectId = require('mongodb').ObjectID;
const mortgageCalculate = require('mortgage-calculate');
var Loan = require('loanjs').Loan;
var moment = require('moment');
moment().format();

var currentDate = new Date();


module.exports.createProfile = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = "profile"

      mongodbCrud.createData(jsonObj, function (createprofile) {
        // console.log('createprofile--->'+JSON.stringify(createprofile))
        if (createprofile.status == 200) {
          returnData.status = createprofile.status;
          returnData.message = createprofile.message;
          returnData.error = createprofile.error;
          returnData.data = createprofile.data;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = createprofile.status;
          returnData.message = createprofile.message;
          returnData.error = createprofile.error;
          returnData.data = createprofile.data;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })

}


module.exports.updateprofile = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = "person"

      mongodbCrud.updateData(jsonObj, function (updateProfile) {
        // console.log('updateProfile--->'+JSON.stringify(updateProfile))
        if (updateProfile.status == 200) {
          returnData.status = updateProfile.status;
          returnData.message = updateProfile.message;
          returnData.error = updateProfile.error;
          returnData.data = updateProfile.data;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = updateProfile.status;
          returnData.message = updateProfile.message;
          returnData.error = updateProfile.error;
          returnData.data = updateProfile.data;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })
}

module.exports.retrieveProfileData = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = "person"

      mongodbCrud.retrieveOne(jsonObj, function (retieveProfile) {
        console.log('retieveProfile--->'+JSON.stringify(retieveProfile))
        if (retieveProfile.status == 200) {
          returnData.status = retieveProfile.status;
          returnData.message = retieveProfile.message;
          returnData.error = retieveProfile.error;
          returnData.response = retieveProfile.response;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = retieveProfile.status;
          returnData.message = retieveProfile.message;
          returnData.error = retieveProfile.error;
          returnData.response = retieveProfile.response;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })

}