/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var mongodbCrud = require('../crudmongodb.controllers');
var config = require('../../../config/config');
var async = require("async");
var ObjectId = require('mongodb').ObjectID;
const mortgageCalculate = require('mortgage-calculate');
var Loan = require('loanjs').Loan;
var moment = require('moment');
moment().format();
// var LP = require('linkedin-public-profile-parser');
var currentDate = new Date();
const scrapedin = require('scrapedin')
const options = {
  email: 'jyothsna@crecientech.com',
  password: 'Jyo@1990'
}

module.exports.createProfileSummary = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = jsonObj.collectionName

      mongodbCrud.createData(jsonObj, function (createprofileSummary) {
        console.log('createprofileSummary--->'+JSON.stringify(createprofileSummary))
        if (createprofileSummary.status == 200) {
          returnData.status = createprofileSummary.status;
          returnData.message = createprofileSummary.message;
          returnData.error = createprofileSummary.error;
          returnData.data = createprofileSummary.data;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = createprofileSummary.status;
          returnData.message = createprofileSummary.message;
          returnData.error = createprofileSummary.error;
          returnData.data = createprofileSummary.data;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })

}


module.exports.updateProfileSummary = function (req, res) {
    var returnData = {};
    if (req.body) {
        var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
    } else {
        var jsonObj = req;
    }

    jsonObj["collectionName"] = jsonObj.collectionName

      mongodbCrud.updateData(jsonObj, function (updateProfileSummary) {
        // console.log('updateProfileSummary--->'+JSON.stringify(updateProfileSummary))
        if (updateProfileSummary.status == 200) {
          returnData.status = updateProfileSummary.status;
          returnData.message = updateProfileSummary.message;
          returnData.error = updateProfileSummary.error;
          returnData.data = updateProfileSummary.data;
          if (req.body) {
            res.status(200).json(returnData)
          } else {
            res(returnData)
          }
        }
        else {
          returnData.status = updateProfileSummary.status;
          returnData.message = updateProfileSummary.message;
          returnData.error = updateProfileSummary.error;
          returnData.data = updateProfileSummary.data;

          if (req.body) {
            res.status(400).json(returnData)
          } else {
            res(returnData)
          }
        }
      })
}

module.exports.retrieveProfileSummaryOne = function (req, res) {
  var returnData = {};
  if (req.body) {
      var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
      var jsonObj = req;
  }

  jsonObj["collectionName"] = jsonObj.collectionName

    mongodbCrud.retrieveOne(jsonObj, function (retieveProfileSummaryDataOne) {
      console.log('retieveProfileSummaryDataOne--->'+JSON.stringify(retieveProfileSummaryDataOne))
      if (retieveProfileSummaryDataOne.status == 200) {
        returnData.status = retieveProfileSummaryDataOne.status;
        returnData.message = retieveProfileSummaryDataOne.message;
        returnData.error = retieveProfileSummaryDataOne.error;
        returnData.response = retieveProfileSummaryDataOne.response;
        if (req.body) {
          res.status(200).json(returnData)
        } else {
          res(returnData)
        }
      }
      else {
        returnData.status = retieveProfileSummaryDataOne.status;
        returnData.message = retieveProfileSummaryDataOne.message;
        returnData.error = retieveProfileSummaryDataOne.error;
        returnData.response = retieveProfileSummaryDataOne.response;

        if (req.body) {
          res.status(400).json(returnData)
        } else {
          res(returnData)
        }
      }
    })

}


module.exports.retrieveProfileSummary = function (req, res) {
  var returnData = {};
  if (req.body) {
      var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
      var jsonObj = req;
  }

  jsonObj["collectionName"] = jsonObj.collectionName
  console.log('json obj in retrieve profile summary:'+JSON.stringify(jsonObj))
    mongodbCrud.retrieveAll(jsonObj, function (retrieveProfileSummary) {
      console.log('retrieveProfileSummary--->'+JSON.stringify(retrieveProfileSummary))
      if (retrieveProfileSummary.status == 200) {
        returnData.status = retrieveProfileSummary.status;
        returnData.message = retrieveProfileSummary.message;
        returnData.error = retrieveProfileSummary.error;
        returnData.response = retrieveProfileSummary.response;
        if (req.body) {
          res.status(200).json(returnData)
        } else {
          res(returnData)
        }
      }
      else {
        returnData.status = retrieveProfileSummary.status;
        returnData.message = retrieveProfileSummary.message;
        returnData.error = retrieveProfileSummary.error;
        returnData.response = retrieveProfileSummary.response;

        if (req.body) {
          res.status(400).json(returnData)
        } else {
          res(returnData)
        }
      }
    })

}


module.exports.retrieveLinkedInPublicProfileDetails =async function (req,res) {
  // var url = 'https://www.linkedin.com/in/nagarajachar';
  // LP(url, function(err, data){
  //   console.log('LinkedIn Profile parser output:'+JSON.stringify(data, null, 2)); 
  // })
  
  console.log('inside profile scrapper');
  const profileScraper = await scrapedin(options)
  const profile = await profileScraper('https://www.linkedin.com/in/crecientech-employee-7859ba1a2/')
  console.log('Profile obtained: '+JSON.stringify(profile))

  var returnData={};

  // var profileObj = new linkedInProfileSummary();

  // var profile = profileObj.getProfile();


  if(profile){
    returnData.profile = profile;
    res.json(returnData);
  }
  else{
    returnData.error="Unable to retrieve profile"
    res.json(returnData);
  }


}

module.exports.retrieveAttachmentData = function (req, res) {
  var returnData = {};
  if (req.body) {
      var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
      var jsonObj = req;
  }

  jsonObj["collectionName"] = "attachment"

    mongodbCrud.retrieveAll(jsonObj, function (retieveAttachmentData) {
      console.log('retieveAttachmentData--->'+JSON.stringify(retieveAttachmentData))
      if (retieveAttachmentData.status == 200) {
        returnData.status = retieveAttachmentData.status;
        returnData.message = retieveAttachmentData.message;
        returnData.error = retieveAttachmentData.error;
        returnData.response = retieveAttachmentData.response;
        if (req.body) {
          res.status(200).json(returnData)
        } else {
          res(returnData)
        }
      }
      else {
        returnData.status = retieveAttachmentData.status;
        returnData.message = retieveAttachmentData.message;
        returnData.error = retieveAttachmentData.error;
        returnData.response = retieveAttachmentData.response;

        if (req.body) {
          res.status(400).json(returnData)
        } else {
          res(returnData)
        }
      }
    })

}