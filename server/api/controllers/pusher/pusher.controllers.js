var mongodbCrud = require('../crudmongodb.controllers');
var config = require('../../../config/config');
var async = require('async');
var ObjectId = require('mongodb').ObjectID;
var dbconn = require('../../data/mongonative-connection.js');
const Pusher = require('pusher');
var jsSHA = require('jssha');
var moment = require('moment');
moment().format();

var dbName;

if (config.type == 'DEVELOPMENT') {
  domainUrl = config.SERVERURLDEVREDIRECT;
  dbName = config.DBNAMEDEV;
  screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV;
} else if (config.type == 'TESTING') {
  domainUrl = config.SERVERURLTEST;
  dbName = config.DBNAMETEST;
  screenbuilderDbName = config.SCREENBUILDERDBNAMETEST;
} else if (config.type == 'UAT') {
  domainUrl = config.SERVERURLUAT;
  dbName = config.DBNAMEUAT;
  screenbuilderDbName = config.SCREENBUILDERDBNAMEUAT;
} else if (config.type == 'PRODUCTION') {
  domainUrl = config.SERVERURLPROD;
  dbName = config.DBNAMEPROD;
  screenbuilderDbName = config.SCREENBUILDERDBNAMEPROD;
}

const pusher = new Pusher({
  appId: '1009016',
  key: 'cb99f8ff9973327701ca',
  secret: 'e0989e6287529a9fb215',
  cluster: 'ap2',
});

module.exports.pusher = function (req, res) {
  const socketId = req.body.socket_id;
  const channel = req.body.channel_name;
  console.log('Socket Id ' + req.body.socket_id);
  console.log('channel Id ' + req.body.channel_name);
  const auth = pusher.authenticate(socketId, channel);
  res.send(auth);
};

module.exports.createChat = function (req, res) {
  var returnData = {};
  if (req.body) {
    var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    var jsonObj = req;
  }
  console.log(jsonObj);
  jsonObj['collectionName'] = jsonObj.collectionName;

  mongodbCrud.createData(jsonObj, function (createChatData) {
    console.log('create chat data : ' + JSON.stringify(createChatData));
    if (createChatData.status == 200) {
      returnData.status = createChatData.status;
      returnData.message = createChatData.message;
      returnData.error = createChatData.error;
      returnData.data = createChatData.data;
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    } else {
      returnData.status = createChatData.status;
      returnData.message = createChatData.message;
      returnData.error = createChatData.error;
      returnData.data = createChatData.data;
      if (req.body) {
        res.status(400).json(returnData);
      } else {
        res(returnData);
      }
    }
  });
};

module.exports.createChannel = function (req, res) {
  var returnData = {};
  var duplicateChannelNone = '';
  if (req.body) {
    var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    var jsonObj = req;
  }
  async.waterfall(
    [
      function (callback) {
        var params = {
          db: dbName,
          collectionName: 'contactsConnected',
          queryStr: {
            senderId: {
              $in: [jsonObj.senderId, jsonObj.reciverId],
            },
            reciverId: {
              $in: [jsonObj.senderId, jsonObj.reciverId],
            },
          },
        };
        mongodbCrud.retrieveAll(params, function (res) {
          console.log('response : ' + JSON.stringify(res));
          if (res.status === '200' && res.response.length !== 0) {
            returnData.code = 400;
            returnData.message = 'Duplicate channel!';
            returnData.error = 'NA';
            returnData.data = 'NA';
            callback(400, returnData);
          } else if (res.status === '400') {
            returnData.code = 400;
            returnData.message = 'retrive channel error!';
            returnData.error = 'NA';
            returnData.data = 'NA';
            callback(400, returnData);
          } else {
            duplicateChannelNone = 'notDuplicate';
            callback(null);
          }
        });
      },
      function (callback) {
        if (duplicateChannelNone === 'notDuplicate') {
          var getuniqueChannel = new uniqueChannel();
          var uniqueValue = getuniqueChannel.getUniqueCode();
          console.log('unique channel : ' + JSON.stringify(uniqueValue));
          var params = {
            updateData: {
              senderId: jsonObj.senderId,
              reciverId: jsonObj.reciverId,
              status: 'offline',
              chanelId: uniqueValue,
              channelInitiator: jsonObj.senderId,
              ideaId: '',
              timeOfChat: new Date(),
              createdDate: new Date(),
              createdBy: jsonObj.senderId,
              updatedDate: new Date(),
              updatedDate: jsonObj.senderId,
            },
            collectionName: 'contactsConnected',
          };
          mongodbCrud.createData(params, function (createChannel) {
            if (createChannel.status === '200') {
              returnData.status = createChannel.status;
              returnData.message = createChannel.message;
              returnData.error = createChannel.error;
              returnData.data = createChannel.data;
              callback(200, returnData);
            } else {
              returnData.code = 400;
              returnData.message = 'channel create error!';
              returnData.error = 'NA';
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        } else {
          returnData.code = 400;
          returnData.message = 'channel create error!';
          returnData.error = 'NA';
          returnData.data = 'NA';
          callback(400, returnData);
        }
      },
    ],
    function (err, result) {
      if (err !== 200) {
        if (req.body) {
          res.status(400).json(result);
        } else {
          res(result);
        }
      } else {
        if (req.body) {
          res.status(200).json(result);
        } else {
          res(result);
        }
      }
    }
  );
};

module.exports.retriveContacts = function (req, res) {
  var jsonObj;
  var returnData = {};

  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  console.log('req data : ' + JSON.stringify(jsonObj));
  var personId = jsonObj.personId;
  var reciverId = jsonObj.reciverId;
  var senderId = jsonObj.senderId;

  var db = dbconn.get(dbName);
  var locCollection = db.collection('contactsConnected');
  locCollection
    .aggregate([
      {
        $match: {
          $or: [{ senderId: { $eq: senderId } }, { reciverId: { $eq: reciverId } }],
        },
      },
      { $addFields: { senderId: { $toObjectId: '$senderId' } } },
      { $addFields: { reciverId: { $toObjectId: '$reciverId' } } },
      { $addFields: { senderIdString: { $toString: '$senderId' } } },
      { $addFields: { reciverIdString: { $toString: '$reciverId' } } },
      { $addFields: { chanelId: '$chanelId' } },
      { $addFields: { timeOfChat: '$timeOfChat' } },
      {
        $lookup: {
          from: 'person',
          let: { reciverid_id: '$reciverId', senderId_id: '$senderId' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $or: [
                        { $eq: ['$_id', '$$reciverid_id'] },
                        { $eq: ['$_id', '$$senderId_id'] },
                      ],
                    },
                    {
                      $ne: ['$_id', ObjectId(personId)],
                    },
                  ],
                },
              },
            },
            { $project: { firstName: 1, lastName: 1, status: 1, _id: 1 } },
          ],
          as: 'personData',
        },
      },
      {
        $lookup: {
          from: 'attachment',
          let: { reciverid_id: '$reciverIdString', senderId_id: '$senderIdString' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $or: [
                        { $eq: ['$personId', '$$reciverid_id'] },
                        { $eq: ['$personId', '$$senderId_id'] },
                      ],
                    },
                    {
                      $eq: ['$type', 'PROFILE'],
                    },
                    {
                      $ne: ['$personId', personId],
                    },
                  ],
                },
              },
            },
            { $project: { path: 1, _id: 1 } },
          ],
          as: 'attachmentData',
        },
      },
      {
        $lookup: {
          from: 'chatMessages',
          let: { reciverid_id: '$reciverIdString', senderId_id: '$senderIdString' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    {
                      $or: [
                        {
                          $and: [
                            { $eq: ['$reciverId', '$$reciverid_id'] },
                            { $eq: ['$senderId', '$$senderId_id'] },
                          ],
                        },
                        {
                          $and: [
                            { $eq: ['$senderId', '$$reciverid_id'] },
                            { $eq: ['$reciverId', '$$senderId_id'] },
                          ],
                        },
                      ],
                    },
                    {
                      $and: [
                        {
                          $eq: ['$readStatus', 'unRead'],
                        },
                        {
                          $ne: ['$senderId', personId],
                        },
                      ],
                    },
                  ],
                },
              },
            },
            { $count: 'readStatus' },
          ],
          as: 'chatMessagesData',
        },
      },
      {
        $project: {
          _id: 1,
          senderId: 1,
          reciverId: 1,
          status: 1,
          chanelId: 1,
          channelInitiator: 1,
          ideaId: 1,
          timeOfChat: 1,
          personData: {
            firstName: 1,
            lastName: 1,
            status: 1,
          },
          attachmentData: {
            path: 1,
          },
          chatMessagesData: {
            readStatus: 1,
          },
        },
      },
      { $sort: { timeOfChat: 1 } },
    ])
    .toArray(function (err, response) {
      if (err) {
        if (req.body) {
          res.status(500).json({
            status: '500',
            message: 'Error retrieving data',
            error: err,
            response: response,
          });
        } else {
          res.status(500).json({
            status: '500',
            message: 'Error retrieving data',
            error: err,
            response: response,
          });
        }
      } else {
        res.status(200).json({
          status: '200',
          message: 'Successfull...',
          error: 'NA',
          response: response,
        });
      }
    });
};

module.exports.retriveAllMessages = function (req, res) {
  var returnData = {};
  if (req.body) {
    var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    var jsonObj = req;
  }
  var params = {
    db: dbName,
    collectionName: 'chatMessages',
    queryStr:
      // {
      //   senderId: {
      //     $in: [jsonObj.senderId, jsonObj.reciverId],
      //   },
      //   reciverId: {
      //     $in: [jsonObj.senderId, jsonObj.reciverId],
      //   },
      // },
      {
        $or: [
          {
            $and: [
              {
                reciverId: { $eq: jsonObj.reciverId },
              },
              {
                senderId: { $eq: jsonObj.senderId },
              },
            ],
          },
          {
            $and: [
              {
                senderId: { $eq: jsonObj.reciverId },
              },
              {
                reciverId: { $eq: jsonObj.senderId },
              },
            ],
          },
        ],
      },
  };
  mongodbCrud.retrieveAll(params, function (response) {
    // console.log(JSON.stringify(response));
    if (response.status === '200') {
      returnData.status = response.status;
      returnData.message = response.message;
      returnData.error = response.error;
      returnData.data = response.response;
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    } else {
      returnData.code = 400;
      returnData.message = 'channel create error!';
      returnData.error = 'NA';
      returnData.data = 'NA';
      if (req.body) {
        res.status(400).json(returnData);
      } else {
        res(returnData);
      }
    }
  });
};

module.exports.updateMessageStatus = function (req, res) {
  var returnData = {};
  if (req.body) {
    var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    var jsonObj = req;
  }
  var senderId = jsonObj.senderId;
  var reciverId = jsonObj.reciverId;

  var db = dbconn.get(dbName);
  var locCollection = db.collection('chatMessages');
  locCollection.updateMany(
    {
      $and: [
        {
          senderId: {
            $eq: senderId,
          },
          reciverId: {
            $eq: reciverId,
          },
        },
      ],
    },
    {
      $set: {
        readStatus: 'read',
      },
    },
    function (err, response) {
      returnData.status = '200';
      returnData.message = 'Sucessfully updated status!';
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

module.exports.updateChannelId = function (req, res) {
  var returnData = {};
  if (req.body) {
    var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    var jsonObj = req;
  }
  var senderId = jsonObj.senderId;
  var reciverId = jsonObj.reciverId;
  var channelIdActive = jsonObj.channelId;

  var db = dbconn.get(dbName);
  var locCollection = db.collection('person');
  locCollection.updateMany(
    {
      _id: { $in: [ObjectId(senderId)] },
    },
    { $set: { channelIdActive: channelIdActive } },
    function (err, response) {
      returnData.status = '200';
      returnData.message = 'Sucessfully updated channelId!';
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

module.exports.activeChannelCount = function (req, res) {
  var jsonObj;
  var returnData = {};

  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  console.log('channel count : ' + JSON.stringify(jsonObj));
  var channelId = jsonObj.channelId;
  var reciverId = jsonObj.reciverId;
  var senderId = jsonObj.senderId;

  var db = dbconn.get(dbName);
  var locCollection = db.collection('person');
  locCollection
    .aggregate([
      {
        $match: {
          $and: [
            {
              _id: {
                $in: [ObjectId(senderId), ObjectId(reciverId)],
              },
            },
            {
              channelIdActive: { $eq: channelId },
            },
          ],
        },
      },
      {
        $count: 'channelIdActive',
      },
    ])
    .toArray(function (err, response) {
      if (err) {
        if (req.body) {
          res.status(500).json({
            status: '500',
            message: 'Error retrieving data',
            error: err,
            response: response,
          });
        } else {
          res.status(500).json({
            status: '500',
            message: 'Error retrieving data',
            error: err,
            response: response,
          });
        }
      } else {
        console.log('response : ' + JSON.stringify(response));
        console.log('response : ' + JSON.stringify(Object.keys(response).length));
        console.log('response : ' + JSON.stringify(response[0].channelIdActive));
        console.log('response : ' + response[0].channelIdActive === 2 ? 'read' : 'unRead');
        if (Object.keys(response).length) {
          res.status(200).json({
            status: '200',
            message: 'Successfull...',
            error: 'NA',
            response: response[0].channelIdActive === 2 ? 'read' : 'unRead',
          });
        } else {
          res.status(200).json({
            status: '200',
            message: 'Successfull...',
            error: 'NA',
            response: 'unRead',
          });
        }
      }
    });
};

uniqueChannel = function () {
  var hex2dec = function (s) {
    return parseInt(s, 16);
  };
  this.getUniqueCode = function () {
    try {
      var currentDateTime = moment().format('YYYYMMDDHHmmssSSS');
      var shaObj = new jsSHA('SHA-512', 'TEXT');
      shaObj.update(currentDateTime);
      var hash = shaObj.getHash('HEX');
      var offset = hex2dec(hash.substring(hash.length - 1));
      var otp = (hex2dec(hash.substr(offset * 2, 8)) & hex2dec('7fffffff')) + '';
      otp = otp.substr(otp.length - 5, 5);
    } catch (error) {
      throw error;
    }
    return otp;
  };
};
