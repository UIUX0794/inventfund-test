/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var mongodbCrud = require('../crudmongodb.controllers');
var config = require('../../../config/config');
var async = require('async');
var ObjectId = require('mongodb').ObjectID;
const mortgageCalculate = require('mortgage-calculate');
var Loan = require('loanjs').Loan;
var moment = require('moment');
moment().format();

var currentDate = new Date();
var screenbuilderDbName;
//var domainUrl = "http://localhost:4200/";
// if (config.type == 'DEVELOPMENT') {
//     dbName = config.DBNAMEDEV;
//     screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV
// }

if (config.type == 'DEVELOPMENT') {
	dbName = config.DBNAMEDEV;
	screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV;
} else if (config.type == 'TESTING') {
	dbName = config.DBNAMETEST;
	screenbuilderDbName = config.SCREENBUILDERDBNAMETEST;
} else if (config.type == 'UAT') {
	dbName = config.DBNAMEUAT;
	screenbuilderDbName = config.SCREENBUILDERDBNAMEUAT;
} else if (config.type == 'PRODUCTION') {
	dbName = config.DBNAMEPROD;
	screenbuilderDbName = config.SCREENBUILDERDBNAMEPROD;
}

module.exports.createSkillsDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;

	mongodbCrud.createData(jsonObj, function (createSkillsDetails) {
		console.log(
			'createSkillsDetails--->' + JSON.stringify(createSkillsDetails)
		);
		if (createSkillsDetails.status == 200) {
			returnData.status = createSkillsDetails.status;
			returnData.message = createSkillsDetails.message;
			returnData.error = createSkillsDetails.error;
			returnData.data = createSkillsDetails.data;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = createSkillsDetails.status;
			returnData.message = createSkillsDetails.message;
			returnData.error = createSkillsDetails.error;
			returnData.data = createSkillsDetails.data;
			SS;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.updateSkillsDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;

	mongodbCrud.updateData(jsonObj, function (updateSkillsDetails) {
		// console.log('updateSkillsDetails--->'+JSON.stringify(updateSkillsDetails))
		if (updateSkillsDetails.status == 200) {
			returnData.status = updateSkillsDetails.status;
			returnData.message = updateSkillsDetails.message;
			returnData.error = updateSkillsDetails.error;
			returnData.data = updateSkillsDetails.data;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = updateSkillsDetails.status;
			returnData.message = updateSkillsDetails.message;
			returnData.error = updateSkillsDetails.error;
			returnData.data = updateSkillsDetails.data;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.retrieveSkillsDetailsOne = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;

	mongodbCrud.retrieveOne(jsonObj, function (retieveSkillsDetailsDataOne) {
		console.log(
			'retieveSkillsDetailsDataOne--->' +
				JSON.stringify(retieveSkillsDetailsDataOne)
		);
		if (retieveSkillsDetailsDataOne.status == 200) {
			returnData.status = retieveSkillsDetailsDataOne.status;
			returnData.message = retieveSkillsDetailsDataOne.message;
			returnData.error = retieveSkillsDetailsDataOne.error;
			returnData.response = retieveSkillsDetailsDataOne.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retieveSkillsDetailsDataOne.status;
			returnData.message = retieveSkillsDetailsDataOne.message;
			returnData.error = retieveSkillsDetailsDataOne.error;
			returnData.response = retieveSkillsDetailsDataOne.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.deleteSkillsDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = 'fixerSkillDetails';
	console.log('delete EducationDetails : ' + JSON.stringify(jsonObj));
	mongodbCrud.deleteData(jsonObj, function (deleteSkillsDetailsRes) {
		console.log(
			'deleteSkillsDetailsRes--->' +
				JSON.stringify(deleteSkillsDetailsRes)
		);
		if (deleteSkillsDetailsRes.status == 200) {
			returnData.status = deleteSkillsDetailsRes.status;
			returnData.message = deleteSkillsDetailsRes.message;
			returnData.error = deleteSkillsDetailsRes.error;
			returnData.response = deleteSkillsDetailsRes.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = deleteSkillsDetailsRes.status;
			returnData.message = deleteSkillsDetailsRes.message;
			returnData.error = deleteSkillsDetailsRes.error;
			returnData.response = deleteSkillsDetailsRes.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.retrieveSkillsDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = jsonObj.collectionName;
	console.log(
		'json obj in retrieve profile summary:' + JSON.stringify(jsonObj)
	);
	mongodbCrud.retrieveAll(jsonObj, function (retrieveSkillsDetails) {
		console.log(
			'retrieveSkillsDetails--->' + JSON.stringify(retrieveSkillsDetails)
		);
		if (retrieveSkillsDetails.status == 200) {
			returnData.status = retrieveSkillsDetails.status;
			returnData.message = retrieveSkillsDetails.message;
			returnData.error = retrieveSkillsDetails.error;
			returnData.response = retrieveSkillsDetails.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retrieveSkillsDetails.status;
			returnData.message = retrieveSkillsDetails.message;
			returnData.error = retrieveSkillsDetails.error;
			returnData.response = retrieveSkillsDetails.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

// -------------------------------------------------------------------------------------------------
// getScreenBuilder is the method to be called from front end to get the screen input
// fileds and this will populate the select drop down values from lookUp collection.
// Pass the collection name and db name to which you need the screen builder input fields.
// -------------------------------------------------------------------------------------------------
module.exports.getScreenBuildergrouped = function (req, res, next) {
	console.log('getScreenBuilder method start :');

	var jsonObj;
	var returnData = {};

	if (req.body) {
		jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		jsonObj = req;
	}

	console.log('jsonObj : ' + JSON.stringify(jsonObj));

	var collectionResponse;
	var lookUpResponse;
	async.waterfall(
		[
			function (callback) {
				var retrieveData = {
					//   "db":jsonObj.db,
					db: screenbuilderDbName,
					collectionName: jsonObj.collectionName,
					sortQuery: { order: 1 },
				};

				mongodbCrud.retrieveAllScreenbuilder(retrieveData, function (
					retrieveRes
				) {
					console.log(
						'resposne from db is : ' + JSON.stringify(retrieveRes)
					);
					collectionResponse = retrieveRes.response;
					if (collectionResponse) {
						callback(null);
					}
				});
			},
			function (callback) {
				var retrieveData = {
					db: screenbuilderDbName,
					collectionName: 'lookUp',
				};

				mongodbCrud.retrieveAllScreenbuilder(retrieveData, function (
					retrieveRes
				) {
					console.log(
						'resposne from lookUp db is : ' +
							JSON.stringify(retrieveRes)
					);
					lookUpResponse = retrieveRes.response;
					if (lookUpResponse) {
						callback(null);
					}
				});
			},
			function (callback) {
				console.log(
					'collectionResponse : in 3rd waterfall > ' +
						JSON.stringify(collectionResponse)
				);
				console.log(
					'lookUpResponse : in 3rd waterfall > ' +
						JSON.stringify(lookUpResponse)
				);

				console.log(
					'response look up : ' + JSON.stringify(lookUpResponse)
				);
				collectionResponse.map((eachCollection, index) => {
					if (eachCollection.hasOwnProperty('fieldGroup')) {
						eachCollection.fieldGroup.map((item, index) => {
							if (item.type == 'select') {
								lookUpResponse.map((each, index) => {
									if (each.lookUpKey == item.key) {
										console.log(
											'eachLookUp.lookUpKey : ' +
												item.length
										);
										console.log(
											'eachLookUp.lookUpKey : ' +
												each.lookUpKey
										);
										console.log(
											'eachCollection.key : ' + each.key
										);
										var optionsMap = {};
										optionsMap.value = each.value;
										optionsMap.label = each.label;
										item.templateOptions.options.push(
											optionsMap
										);
										optionsMap = {
											value: '',
											label: '',
										};
									}
								});
								console.log(
									'collection data fieldGroup : ' +
										index +
										' ' +
										JSON.stringify(item)
								);
							}
						});
					} else {
					}
					if (index == collectionResponse.length - 1) {
						console.log(
							'collectionResponse after adding is :: ' +
								JSON.stringify(collectionResponse)
						);
						returnData.code = 200;
						returnData.message =
							'Lookup for select input added successfully!';
						returnData.data = collectionResponse;
						returnData.error = 'NA';
						callback(null);
					}
				});
			},
		],
		function (err, result) {
			if (err) {
				console.log('err ' + err);
				if (req.body) {
					res.status(200).json(returnData);
				} else {
					res(returnData);
				}
			} else {
				console.log('SUCCESS');
				if (req.body) {
					res.status(200).json(returnData);
				} else {
					res(returnData);
				}
			}
		}
	);
};

module.exports.retrieveLookupData = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = 'lookUp';

	mongodbCrud.retrieveAll(jsonObj, function (retieveLookupData) {
		console.log(
			'retieveLookupData--->' + JSON.stringify(retieveLookupData)
		);
		if (retieveLookupData.status == 200) {
			returnData.status = retieveLookupData.status;
			returnData.message = retieveLookupData.message;
			returnData.error = retieveLookupData.error;
			returnData.response = retieveLookupData.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retieveLookupData.status;
			returnData.message = retieveLookupData.message;
			returnData.error = retieveLookupData.error;
			returnData.response = retieveLookupData.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};

module.exports.retrieveLookUpDetails = function (req, res) {
	var returnData = {};
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}

	jsonObj['collectionName'] = 'lookUp';
	console.log(
		'json obj in retrieve profile summary:' + JSON.stringify(jsonObj)
	);
	mongodbCrud.retrieveAllScreenbuilder(jsonObj, function (
		retrieveLookUpDetails
	) {
		console.log(
			'retrieveLookUpDetails--->' + JSON.stringify(retrieveLookUpDetails)
		);
		if (retrieveLookUpDetails.status == 200) {
			returnData.status = retrieveLookUpDetails.status;
			returnData.message = retrieveLookUpDetails.message;
			returnData.error = retrieveLookUpDetails.error;
			returnData.response = retrieveLookUpDetails.response;
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		} else {
			returnData.status = retrieveLookUpDetails.status;
			returnData.message = retrieveLookUpDetails.message;
			returnData.error = retrieveLookUpDetails.error;
			returnData.response = retrieveLookUpDetails.response;

			if (req.body) {
				res.status(400).json(returnData);
			} else {
				res(returnData);
			}
		}
	});
};
