const express = require('express');
const app = express();
app.use(passport.initialize());


passport.use(new GoogleStrategy({
    clientID: '518112019515-lk75gpa98pi65el6vqmbtld6ovatgsm5.apps.googleusercontent.com',
    clientSecret: 'B43ACwj10Zz1nUf_qjMYDkKX',
    callbackURL: 'http://localhost:4200/linkedInLogin'
},
(accessToken, refreshToken, profile, done) => {
    done(null, profile); // passes the profile data to serializeUser
}
));


app.get('/auth/google', passport.authenticate('google', {
    scope: ['profile'] // Used to specify the required data
}));

app.get('/auth/google/callback', passport.authenticate('google'), (req, res) => {
    res.redirect('http://localhost:4200/linkedInLogin');
});