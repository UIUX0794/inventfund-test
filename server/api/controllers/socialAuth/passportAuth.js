var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var config = require('../../../config/config');
var passport = require('passport');
var axios = require('axios');
var querystring = require('querystring');
var async = require('async');
var http = require('http');
var request = require('request');
var https = require('https');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

const JSON = require('circular-json');

var domainUrl;
// var redirectUrl;
// var apiRedirectUrl;

if (config.type == 'DEVELOPMENT') {
	var domainUrl = config.SERVERURLDEV;
	var redirectUrl =
		config.REDIRECTURLLINKEDINAUTHDEV;
	var redirectUrlChangePassword = config.REDIRECTURLLINKEDINCHANGEPASSWORDDEV;
	var apiRedirectUrl = config.SERVERURLDEV;
} else if (config.type == 'TESTING') {
	var domainUrl = config.SERVERURLTEST;
	var redirectUrl =
		config.REDIRECTURLLINKEDINAUTHTEST;
	var apiRedirectUrl = config.SERVERURLTEST;
	var redirectUrlChangePassword = config.REDIRECTURLLINKEDINCHANGEPASSWORDTEST;
} else if (config.type == 'UAT') {
	var domainUrl = config.SERVERURLUAT;
	var redirectUrl = config.REDIRECTURLLINKEDINAUTHUAT;
	var redirectUrlChangePassword = config.REDIRECTURLLINKEDINCHANGEPASSWORDUAT;
	var apiRedirectUrl = config.SERVERURLUAT;
} else if (config.type == 'PRODUCTION') {
	var domainUrl = config.SERVERURLPROD;
	var redirectUrl =
		config.REDIRECTURLLINKEDINAUTHPROD;
	var apiRedirectUrl = config.SERVERURLPROD;
	var redirectUrlChangePassword = config.REDIRECTURLLINKEDINCHANGEPASSWORDPROD;
}

// var signUpRedirect = 'sign-up-roles';
// var loginRedirect = 'profile/settings';

// if (config.type == 'DEVELOPMENT') {
// 	var redirectUrl = `http://localhost:4200/#/auth/linkedInLogin?email=`;
// } else {
// 	var redirectUrl = `http://localhost:4200/#/auth/linkedInLogin?email=`;
// }

module.exports.passportLinkedIn = function () {
	passport.use(
		new LinkedInStrategy(
			{
				clientID: '81ctgyj14vau28',
				clientSecret: 'SVZdgCb92Ph9OVbG',
				// callbackURL: domainUrl+"/login",
				callbackURL: 'http://127.0.0.1:4200/login',
				scope: ['r_emailaddress', 'r_liteprofile'],
			},
			function (accessToken, refreshToken, profile, done) {
				process.nextTick(function () {
					return done(null, profile);
				});
			}
		)
	);
};

module.exports.passportAuth = function () {
	passport.authenticate('linkedin', { state: 'SOME STATE' }),
		function (req, res) {
			// The request will be redirected to LinkedIn for authentication, so this
			// function will not be called.
			if (req.body) {
				var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
			} else {
				var jsonObj = req;
			}
			console.log('req 1:' + jsonObj);
			// console.log('res:'+res)
		};
};

module.exports.passportAuthRedirect = function (req, res) {
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}
	console.log('req 2:' + jsonObj);

	passport.authenticate('linkedin', { failureRedirect: '/login' }),
		function (req, res) {
			// Successful authentication
			if (req.body) {
				var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
			} else {
				var jsonObj = req;
			}
			console.log('req 3:' + jsonObj);

			res.json(req.user);
		};
};

module.exports.requestToken = function (req, res) {
	var jsonObj;
	var returnData = {};

	console.log('in request token-----' + redirectUrl);
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}
	console.log(
		'code in backend route:' +
		JSON.stringify(
			`https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=${ req.query.code }&redirect_uri=http://localhost:4200/linkedInLogin&client_id=81ctgyj14vau28&client_secret=SVZdgCb92Ph9OVbG`
		)
	);

	async.waterfall(
		[
			function (callback) {
				// http.get({
				//   url:`https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=${req.query.code}&redirect_uri=http://localhost:4000/linkedInLogin&client_id=81ctgyj14vau28&client_secret=SVZdgCb92Ph9OVbG`
				// }, (res) => {
				//   // Do stuff with response
				//   console.log("response get data : "+JSON.stringify(res))
				// });
				axios({
					// url: `https://www.linkedin.com/oauth/v2/accessToken`,
					// method: 'get',
					// data: querystring.stringify({
					//   grant_type: 'authorization_code',
					//   code: jsonObj.query.code,
					//   redirect_uri: "http://localhost:4000/linkedInLogin",
					//   client_id: '81ctgyj14vau28',
					//   client_secret: 'SVZdgCb92Ph9OVbG',
					url: `https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=${ req.query.code }&redirect_uri=http://localhost:4200/auth/linkedInLogin&client_id=81ctgyj14vau28&client_secret=SVZdgCb92Ph9OVbG`,
					// }),
				})
					.then((res) => {
						const json = JSON.stringify(res);
						console.log('original file : ' + json);
						// console.log('res-->'+res)
						callback(null, res);
					})
					.catch((err) => {
						const json = JSON.stringify(err);
						console.log('original file : ' + json);
						returnData.code = 400;
						returnData.message = 'Error';
						returnData.error = err;
						returnData.data = 'NA';
						callback(400, returnData);
					});
			},
			function (resData, callback) {
				console.log('in w2:' + JSON.stringify(resData));
				var token = resData.data.access_token;
				console.log('token--', token);
				axios({
					url: ` https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))`,
					method: 'get',
					headers: {
						Authorization: `Bearer ${ token }`,
					},
				})
					.then((res) => {
						console.log('in w2 res-->' + res);
						callback(
							null,
							res.data.elements[0]['handle~'].emailAddress
						);
					})
					.catch((err) => {
						console.log('Error in w2:', err);
						returnData.code = 400;
						returnData.message = 'Error';
						returnData.error = err;
						returnData.data = 'NA';
						callback(400, returnData);
					});
			},
			function (emailData, callback) {
				console.log('in test function--', JSON.stringify(emailData));
				returnData.code = 200;
				returnData.message = 'Profile details obtained successfully';
				returnData.error = 'NA';
				returnData.data = emailData;
				callback(null);
			},
		],
		function (err, result) {
			if (req.body) {
				res.status(200).json(returnData);
			} else {
				res(returnData);
			}
		}
	);
};

async function getAccessTokenFromCode(code) {
	const { data } = await axios({
		url: `https://www.linkedin.com/oauth/v2/accessToken`,
		method: 'post',
		data: querystring.stringify({
			grant_type: 'authorization_code',
			code: code,
			redirect_uri: redirectUrl,
			client_id: '81ctgyj14vau28',
			client_secret: 'SVZdgCb92Ph9OVbG',
		}),
	});
	console.log(data);
	return data.access_token;
}

async function getProfile(token) {
	const { data } = await axios({
		url: ` https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))`,
		method: 'get',
		headers: {
			Authorization: `Bearer ${ token }`,
		},
	});
	console.log(data);
	return data;
}

module.exports.passportGoogle = function (req, res) {
	// console.log('inside 1 ')
	//   passport.use(new GoogleStrategy({
	//     clientID: '518112019515-lk75gpa98pi65el6vqmbtld6ovatgsm5.apps.googleusercontent.com',
	//     clientSecret: 'B43ACwj10Zz1nUf_qjMYDkKX',
	//     callbackURL: 'http://localhost:8000/auth/google/callback'
	// },
	// (accessToken, refreshToken, profile, done) => {
	//   console.log('inside 1.1 ')
	//     done(null, profile); // passes the profile data to serializeUser
	// }
	// ));

	console.log('inside 1 ');
	passport.use(
		new GoogleStrategy(
			{
				clientID:
					'518112019515-lk75gpa98pi65el6vqmbtld6ovatgsm5.apps.googleusercontent.com',
				clientSecret: 'B43ACwj10Zz1nUf_qjMYDkKX',
				callbackURL: 'http://localhost:8000/auth/google/callback',
				scope: ['profile'],
			},
			function (accessToken, refreshToken, profile, done) {
				process.nextTick(function () {
					console.log('inside 1.1 ');
					return done(null, profile);
				});
			}
		)
	);
	// console.log('inside 2 ')
	//   passport.authenticate('google', {
	//     scope: ['profile'] // Used to specify the required data
	// })
};

module.exports.passportGoogleRedirect = function (req, res) {
	passport.authenticate('google/callback', {
		scope: ['profile'], // Used to specify the required data
	});
};

// module.exports.newPassportGoogle = function googleSignInCallback(req, res, next) {

//   passport.use(new GoogleStrategy({
//     clientID: '518112019515-lk75gpa98pi65el6vqmbtld6ovatgsm5.apps.googleusercontent.com',
//     clientSecret: 'B43ACwj10Zz1nUf_qjMYDkKX',
//     callbackURL: '/auth/google/redirect'
// }
// ,
// (accessToken, refreshToken, profile, done) => {
//   console.log('passport callback function fired:');
//   console.log(profile);
// }
// ));
//   // passport = req._passport.instance;
// 	passport.authenticate('google',{ scope: ['profile']},function(err, user, info) {
// 		if(err) {
// 			return next(err);
// 		}

// 	})(req,res,next);
// };

passport.use(
	new GoogleStrategy(
		{
			clientID:
				'518112019515-lk75gpa98pi65el6vqmbtld6ovatgsm5.apps.googleusercontent.com',
			clientSecret: 'B43ACwj10Zz1nUf_qjMYDkKX',
			callbackURL: '/auth/google/redirect',
		},
		(accessToken, refreshToken, profile, done) => {
			console.log('passport callback function fired:');
			console.log(profile);
		}
	)
);

module.exports.getToken = function (req, res) {
	var data = JSON.stringify(req);
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}
	console.log('req from api : ' + JSON.stringify(jsonObj));
	const json = JSON.stringify(
		res['socket'].parser['incoming'].query['code']
	).replace(/^"(.*)"$/, '$1');
	var URL = `https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=${ json }&redirect_uri=${ apiRedirectUrl }api/auth/getToken&client_id=81ctgyj14vau28&client_secret=SVZdgCb92Ph9OVbG`;
	console.log('url to get access token : ' + URL);
	axios(URL)
		.then(function (response) {
			var token = response.data.access_token;
			axios({
				url: ` https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))`,
				method: 'get',
				headers: {
					Authorization: `Bearer ${ token }`,
				},
			})
				.then((result) => {
					console.log(
						'email address : ',
						result.data.elements[0]['handle~'].emailAddress
					);
					var email = result.data.elements[0]['handle~'].emailAddress;
					res.redirect(redirectUrl + email);
				})
				.catch((err) => {
					console.log('err :>> ', err);
					res.redirect('');
				});
		})
		.catch(function (error) {
			console.log('Error: ' + error);
			res.redirect('');
		});
};

module.exports.getTokenChangePassword = function (req, res) {
	var data = JSON.stringify(req);
	if (req.body) {
		var jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
	} else {
		var jsonObj = req;
	}
	console.log('req from api : ' + JSON.stringify(jsonObj));
	const json = JSON.stringify(
		res['socket'].parser['incoming'].query['code']
	).replace(/^"(.*)"$/, '$1');
	var URL = `https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code=${ json }&redirect_uri=${ apiRedirectUrl }api/auth/getTokenChangePassword&client_id=81ctgyj14vau28&client_secret=SVZdgCb92Ph9OVbG`;
	console.log('url to get access token : ' + URL);
	axios(URL)
		.then(function (response) {
			var token = response.data.access_token;
			axios({
				url: ` https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))`,
				method: 'get',
				headers: {
					Authorization: `Bearer ${ token }`,
				},
			})
				.then((result) => {
					console.log(
						'email address : ',
						result.data.elements[0]['handle~'].emailAddress
					);
					var email = result.data.elements[0]['handle~'].emailAddress;
					res.redirect(redirectUrlChangePassword + email + '&setActive=2');
				})
				.catch((err) => {
					console.log('err :>> ', err);
					res.redirect('');
				});
		})
		.catch(function (error) {
			console.log('Error: ' + error);
			res.redirect('');
		});
};
