/* ----------------------------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------------------------*/
var mongodbCrud = require('../../controllers/crudmongodb.controllers');
var btoa = require('btoa');
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
const express = require('express');
var config = require('../../../config/config');
// var dbconn = require('../data/mongonative-connection.js');
var dbconn = require('../../data/mongonative-connection.js');
var async = require('async');
// var secret = require('../../data/config.js').secretVariable.secret;
var secret = require('../../data/config.js').secretVariable;
var emailCtrl = require('../../controllers/email/email.controllers.js');
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');
moment().format();

var currentDate = new Date();
var emailSubject = 'Invitation to Invent Funds';

var domainUrl;
var dbName;
var screenbuilderDbName;

// if (config.type == 'DEVELOPMENT') {
//     domainUrl = config.SERVERURLDEV;
//     dbName = config.DBNAMEDEV;
//     screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV
// }

// if (config.type == 'DEVELOPMENT') {
//   // console.log('In dev')

//   domainUrl = config.SERVERURLDEV
//   dbName = config.DBNAMEDEV
//   screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV
// } else if (config.type == 'TESTING') {
//   // console.log('In testing')
//   domainUrl = config.SERVERURLTEST
//   dbName = config.DBNAMETEST
//   screenbuilderDbName = config.SCREENBUILDERDBNAMETEST
// }

if (config.type == 'DEVELOPMENT') {
  domainUrl = config.SERVERURLDEVREDIRECT;
  dbName = config.DBNAMEDEV;
  screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV;
} else if (config.type == 'TESTING') {
  domainUrl = config.SERVERURLTEST;
  dbName = config.DBNAMETEST;
  screenbuilderDbName = config.SCREENBUILDERDBNAMETEST;
} else if (config.type == 'UAT') {
  domainUrl = config.SERVERURLUAT;
  dbName = config.DBNAMEUAT;
  screenbuilderDbName = config.SCREENBUILDERDBNAMEUAT;
} else if (config.type == 'PRODUCTION') {
  domainUrl = config.SERVERURLPROD;
  dbName = config.DBNAMEPROD;
  screenbuilderDbName = config.SCREENBUILDERDBNAMEPROD;
}

// domainUrl = config.SERVERURLDEV;
// dbName = config.DBNAMEDEV;
// screenbuilderDbName = config.SCREENBUILDERDBNAMEDEV

//This method used to compare the password (from frontend end and hash stored in backend)
comparePassword = function (candidatePassword, hashpassword, callback) {
  // console.log(" >>>>>> " + candidatePassword)
  // console.log(" >>>>>> " + hashpassword)
  bcrypt.compare(candidatePassword, hashpassword, (err, isMatch) => {
    if (err) {
      //  console.log("err" + err)
      callback(err, null);
    } else {
      //  console.log(" comparePassword - success: " + isMatch);
      callback(null, isMatch);
    }
  });
};

/* ----------------------------------------------------------------------------------
**Purpose: API for Mongo DB SignUp
-------------------------------------------------------------------------------------*/

module.exports.mongoSignUp = function (req, res) {
  // var moduleName = "SignUp";
  // var moduleInfo = "Creating user records";

  var jsonObj;
  var returnData = {};

  var hash;

  var partyId;
  var tempPassword;
  var personId = '';

  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  // console.log('sign up json object : ' + JSON.stringify(jsonObj));
  var userName = jsonObj.emailId;

  var roleId;
  var userId;
  returnData.passWordType = jsonObj.type !== 'Social' ? true : false;
  async.waterfall(
    [
      function (callback) {
        //retrieve data from user table based on the userName and check the emailId exists or not
        //jsonObj.db
        userParams = {
          db: dbName,
          collectionName: 'user',
          queryStr: { userName: { $eq: userName } },
        };
        //Check emailId exists in system
        mongodbCrud.retrieveAll(userParams, function (response) {
          // console.log('data response user : ' + JSON.stringify(response));
          if (response.response.length > 0) {
            // console.log('**************Duplicate Email!********************');
            returnData.code = 400;
            returnData.message = 'Duplicate Email!';
            returnData.error = response.error;
            returnData.data = 'NA';
            callback(400, returnData);
          } else {
            // console.log('*****************new user*************************');
            callback(null);
          }
        });
      },
      function (callback) {
        //retrieve data from tblParty based on the partyCode and get the related partyId
        // console.log("party code--->"+jsonObj.partyCode );
        partyParams = {
          db: dbName,
          collectionName: 'party',
          queryStr: { partyCode: { $eq: jsonObj.partyCode } },
        };
        mongodbCrud.retrieveAll(partyParams, function (partyRes) {
          if (partyRes.response.length > 0) {
            if (partyRes.status == 200) {
              partyId = partyRes.response[0]._id;

              callback(null);
            } else {
              returnData.code = 400;
              returnData.message = 'Error in retrieving data from party table!';
              returnData.error = partyRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          } else {
            // console.log('*****************new party*************************');
            callback(null);
          }
        });
      },
      function (callback) {
        if (partyId) {
          callback(null);
        } else {
          partyParams = {
            db: dbName,
            collectionName: 'party',
            updateData: {
              orgId: jsonObj.orgId,
              partyCode: jsonObj.partyCode,
              partyType: jsonObj.partyType,
              partyName: jsonObj.partyName,
              partyDescription: jsonObj.partyDescription,
              createdDate: currentDate,
              createdBy: '',
              updatedDate: currentDate,
              updatedBy: '',
            },
          };

          mongodbCrud.createData(partyParams, function (partyRes) {
            // console.log('partyres--->'+JSON.stringify(partyRes))
            if (partyRes.status == 200) {
              //    partyId = partyRes.data[0];
              partyId = partyRes.data[0]._id;
              //    console.log('party id ------>'+partyId);
              callback(null);
            } else {
              returnData.code = 400;
              returnData.message = 'Error in creating data in table party!';
              returnData.error = partyRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        }
      },
      function (callback) {
        if (partyId) {
          var personParams = {
            db: dbName,
            collectionName: 'person',
            updateData: {
              partyId: partyId,
              personType: jsonObj.personType,
              name: jsonObj.name,
              firstName: jsonObj.firstName,
              lastName: jsonObj.lastName,
              name: jsonObj.name,
              phoneNumber: jsonObj.phoneNumber,
              createdDate: currentDate,
              createdBy: '',
              updatedDate: currentDate,
              updatedBy: '',
            },
          };
          mongodbCrud.createData(personParams, function (personRes) {
            // console.log('response from personParams' + JSON.stringify(personRes));

            if (personRes.status == 200) {
              personId = personRes.data[0]._id;
              // console.log('person id -->'+personId);
              if (jsonObj.logIn == 'true') {
                callback(null);
              } else {
                returnData.code = 200;
                returnData.message = 'Profile created successfully!';
                returnData.error = 'NA';
                returnData.data = 'NA';
                if (req.body) {
                  res.status(200).json(returnData);
                } else {
                  res(returnData);
                }
              }
            } else {
              returnData.code = 400;
              returnData.message = 'Error in creating data in table person!';
              returnData.error = personRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        } else {
          callback(null);
        }
      },
      function (callback) {
        // attachmentParams = {
        // "db": jsonObj.db,
        //     "db": dbName,
        //     "collectionName": "asattachment",
        //     "updateData": {
        //         "description": "",
        //         "partyId": jsonObj.partyId,
        //         "type": 'PROFILE',
        //         "awsS3FileLocation": jsonObj.photoUrl,
        //         "createdDate": currentDate,
        //         "createdBy": "",
        //         "updatedDate": currentDate,
        //         "updatedBy": ""
        //     }
        // }

        attachmentParams = {
          db: dbName,
          collectionName: 'attachment',
          updateData: {
            personId: personId.toString(),
            url: jsonObj.photoUrl,
            type: 'PROFILE',
            createdDate: currentDate,
            createdBy: '',
            updatedDate: currentDate,
            updatedBy: '',
          },
        };

        mongodbCrud.createData(attachmentParams, function (attachmentRes) {
          //     console.log('attachmentRes--->' + JSON.stringify(attachmentRes))
          if (attachmentRes.status == 200) {
            returnData.url = attachmentRes.data[0].url;
            // console.log('user id --->'+userId);
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in creating data in table attachment!';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        if (jsonObj.logIn == 'true') {
          if (jsonObj.passwordRequired == 'Y') {
            // Generate a salt
            var salt = bcrypt.genSaltSync(10);
            // Hash the password with the salt
            hash = bcrypt.hashSync(req.body.password, salt);
            // console.log("hash---->" + hash)
          } else {
            hash = null;
          }

          tempPassword = getTempPassword();
          //console.log("tempPassword : " +tempPassword);

          var paramUser = {
            db: dbName,
            collectionName: 'user',
            updateData: {
              //   "id": null,
              personId: personId.toString(),
              partyId: partyId,
              userName: jsonObj.emailId,
              passWord: '',
              tempPassword: '',
              userStatus: 'Inactive',
              signUpType: jsonObj.type,
              signUpProvider: jsonObj.provider,
              createdDate: currentDate,
              createdBy: personId,
              updatedDate: currentDate,
              updatedBy: personId,
            },
          };
          mongodbCrud.createData(paramUser, function (usersRes) {
            // console.log('User response password : ' + JSON.stringify(usersRes));
            if (usersRes.status == 200) {
              userId = usersRes.data[0]._id;
              returnData.signUpProvider = usersRes.data[0].signUpProvider;
              // console.log('user id --->'+userId);
              callback(null);
            } else {
              returnData.code = 400;
              returnData.message = 'Error in creating data in table users!';
              returnData.error = usersRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        }
      },

      function (callback) {
        if (jsonObj.logIn == 'true') {
          var paramUserRoles = {
            db: dbName,
            collectionName: 'userrole',
            updateData: {
              userId: userId,
              roleId: roleId,
              createdDate: currentDate,
              createdBy: personId,
              updatedDate: currentDate,
              updatedBy: personId,
            },
          };
          mongodbCrud.createData(paramUserRoles, function (usersRoleRes) {
            // console.log('social or custom');
            if (usersRoleRes.status == 200) {
              // console.log('data for show and hide password');
              if (jsonObj.type == 'Social') {
                returnData.passWordType = false;
                returnData.code = 200;
                returnData.personId = personId;
                returnData.message = 'Profile created successfully!';
                returnData.error = 'NA';
                returnData.data = 'NA';
                res.status(200).json(returnData);
              } else {
                returnData.passWordType = true;
                callback(null);
              }
            } else {
              returnData.code = 400;
              returnData.message = 'Error in creating data in table users!';
              returnData.error = usersRoleRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        }
      },
      function (callback) {
        if (jsonObj.logIn == 'true') {
          //email sending api calling
          var encryptedData = {
            email: jsonObj.emailId,
            personId: personId,
            partyType: jsonObj.partyType,
          };
          // var hashedJson = window.btoa(JSON.stringify(encryptedData));
          var hashedJson = personId;
          // var salt = bcrypt.genSaltSync(10);
          //  // Hash the data with the salt
          // var hashedJson = bcrypt.hashSync(encryptedData, salt)
          // emailJson.to.push(jsonObj.emailId);
          var body =
            ` 
                <div class="container" style="max-width: 430px; padding-top: 0">
                <div>
                    <br>
                    <h1 style="color: white;background: #d55822;padding: 10px;font-size: 14px;">
                  Invent Funds</h1>
                </div>
                    <div class="row" >
                        <div class="col-lg-12" style="padding-bottom: 10px;">
                            Dear Sir/Madam,<br>
                        </div>
                        <div>
                            <div class="col-lg-12">
                            Your email Id ` +
            jsonObj.emailId +
            ` has been registered with Invent Funds System  
                            </div>
                           
                            <div class="col-lg-12" style="padding-top: 10px;">
                           To proceed with the registration,please click on the button below.
                                               
                        </div>
                        
                        <div class="col-lg-12" style="padding-top:30px;">
                        <div>Click on the button to reset your password.</div>
                            <div>
                            <a href = "` +
            domainUrl +
            `#/auth/sign-up-details/` +
            hashedJson +
            `">
                                <button class='btn' 
                                style="cursor: pointer;padding: 10px;color:white;background-color:#2E324D; font-size: 20px;margin-top: 10px;">
                                Proceed with Sign-Up</button>
                            </a>
                            
                            </div>
                          
                        
                        <div class="col-lg-12" style="padding-top:30px">
                        Yours truly,<br>
                        Team Invent Funds<br>
                        Regards.<br><br>
                        </div>
                    </div>
                </div>`;

          var emailJson = {
            to: jsonObj.emailId,
            subject: emailSubject,
            emailBody: body,
          };
          //console.log("emailJsons : " +JSON.stringify(emailJson));

          callback('', emailJson);
        }
      },
      function (emailJson, callback) {
        if (jsonObj.logIn == 'true') {
          emailCtrl.sendMail(emailJson, function (emailRes) {
            returnData.code = 200;
            returnData.message = 'Profile created successfully!';
            returnData.error = 'NA';
            returnData.data = 'NA';
            returnData.tempPassword = tempPassword;
            if (req.body) {
              res.status(200).json(returnData);
            } else {
              res(returnData);
            }
          });
        }
      },
    ],
    function (err, result) {
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );

  function done() {
    return res.status(200).json({
      code: '200',
      message: 'successfully signup',
      error: '',
      data: returnData,
    });
  }

  //This method generates some unique password with length of 8 and returns the temporary password.
  function getTempPassword() {
    var tpassword = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 8; i++) {
      tpassword += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return tpassword;
  }
};

/* ----------------------------------------------------------------------------------
**Purpose: API for Mongo DB Change Password
-------------------------------------------------------------------------------------*/
module.exports.mongoChangePassword = function (req, res) {
  var moduleName = 'changePassword';
  var moduleInfo = 'Changing new password';

  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var returnData = {};
  var userRecord;
  var personRecord;
  var token;
  var userResData;

  // console.log("jsonObj in changePassword method : " + JSON.stringify(jsonObj));

  async.waterfall(
    [
      function (callback) {
        //retrieve data from user table based on the userName and check the emailId exists or not

        // console.log('jsonObj.userName:' + jsonObj.userName);

        userParams = {
          db: dbName,
          collectionName: 'user',
          queryStr: { userName: { $eq: jsonObj.userName } },
        };
        //Check emailId exists in system
        mongodbCrud.retrieveAll(userParams, function (usersRes) {
          // console.log('response usersRes' + JSON.stringify(usersRes));
          userRecord = usersRes.response[0];
          if (usersRes.response.length > 0) {
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Invalid Email!';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },

      function (callback) {
        var personParams = {
          collectionName: 'person',
          pkId: ObjectId(userRecord.personId),
          updateData: {
            phoneNumber: jsonObj.phoneNumber,
          },
        };
        mongodbCrud.updateData(personParams, function (personResp) {
          // console.log('person table phoneNumber update response ->' + JSON.stringify(personResp));
          if (personResp.status == '200') {
            // userResData = userRes.data[0];
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating person phone number';
            returnData.error = 'NA';
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },

      function (callback) {
        // console.log('user record : ' + JSON.stringify(userRecord));
        if (userRecord.password !== null && jsonObj.oldPassword !== 'Forgot Password') {
          comparePassword(jsonObj.oldPassword, userRecord.passWord, function (err, isMatch) {
            // console.log("is match data : "+JSON.stringify(isMatch))
            // console.log("is match data : "+JSON.stringify(err))
            if (isMatch) {
              var salt = bcrypt.genSaltSync(10);
              // Hash the password with the salt
              var hash = bcrypt.hashSync(jsonObj.newPassword, salt);

              var userParams = {
                collectionName: 'user',
                pkId: userRecord._id,
                updateData: {
                  userName: userRecord.userName,
                  passWord: hash,
                  phoneNumber: jsonObj.phoneNumber,
                },
              };
              mongodbCrud.updateData(userParams, function (userRes) {
                // console.log('if match --> userRes : ' + JSON.stringify(userRes));
                //Generating token
                token = jwt.sign(
                  {
                    userName: jsonObj.userName,
                    personId: userRecord.personId,
                    // roleCode:userRecord.roleCode
                  },
                  secret.secret,
                  {
                    expiresIn: 7200,
                  }
                );
                // console.log("token generated:" + token);

                if (userRes.status == '200') {
                  //userResData = userRes.data[0];
                  returnData.code = 200;
                  returnData.message = 'updating password sucess!';
                  returnData.error = 'NA';
                  returnData.data = 'NA';
                  callback(200, returnData);
                } else {
                  returnData.code = 400;
                  returnData.message = 'Error in updating password';
                  returnData.error = 'NA';
                  returnData.data = 'NA';
                  callback(400, returnData);
                }
              });
            } else {
              returnData.code = 400;
              returnData.message = 'Error in updating password';
              returnData.error = 'NA';
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        } else {
          var salt = bcrypt.genSaltSync(10);
          // Hash the password with the salt
          var hash = bcrypt.hashSync(jsonObj.newPassword, salt);

          var userParams = {
            db: dbName,
            collectionName: 'user',
            pkId: userRecord._id,
            updateData: {
              userName: userRecord.userName,
              passWord: hash,
              // "userStatus":"Active"
            },
          };
          mongodbCrud.updateData(userParams, function (userRes) {
            //    console.log("userRes : " + JSON.stringify(userRes));
            //Generating token
            token = jwt.sign(
              {
                userName: jsonObj.userName,
                personId: userRecord.personId,
                // roleCode:userRecord.roleCode
              },
              secret.secret,
              {
                expiresIn: 7200,
              }
            );
            // console.log("token generated:" + token);

            if (userRes.status == '200') {
              //userResData = userRes.data[0];
              returnData.code = 200;
              returnData.message = 'Password Updated Successfully';
              returnData.token = token;
              returnData.partyId = userRecord.partyId;
              returnData.personId = userRecord.personId;
              returnData.userId = userRecord._id;
              returnData.personEmail = userRecord.userName;
              callback(null);
            } else {
              returnData.code = 400;
              returnData.message = 'Error in updating password!';
              returnData.error = usersRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        }
      },
    ],
    function (err, result) {
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

/* ----------------------------------------------------------------------------------
**Purpose: API for Mongo DB Change Password
-------------------------------------------------------------------------------------*/
module.exports.mongoResetPassword = function (req, res) {
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var returnData = {};
  var userRecord;
  var personRecord;
  var token;
  var userResData;

  // console.log('jsonObj in changePassword method reset password: ' + JSON.stringify(jsonObj));

  async.waterfall(
    [
      function (callback) {
        //retrieve data from user table based on the userName and check the emailId exists or not

        // console.log('jsonObj.userName:' + jsonObj);

        userParams = {
          db: dbName,
          collectionName: 'user',
          queryStr: { userName: { $eq: jsonObj.userName } },
        };
        //Check emailId exists in system
        mongodbCrud.retrieveAll(userParams, function (usersRes) {
          // console.log('response usersRes' + JSON.stringify(usersRes));
          if (usersRes.response.length > 0) {
            userRecord = usersRes.response[0];
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Invalid Email!';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        var salt = bcrypt.genSaltSync(10);
        // Hash the password with the salt
        var hash = bcrypt.hashSync(jsonObj.newPassword, salt);
        var userParams = {
          db: dbName,
          collectionName: 'user',
          pkId: userRecord._id,
          updateData: {
            userName: userRecord.userName,
            passWord: hash,
          },
        };
        mongodbCrud.updateData(userParams, function (userRes) {
          if (userRes.status == '200') {
            returnData.code = 200;
            returnData.message = 'Password Updated Successfully';
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating password!';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
    ],
    function (err, result) {
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

/* -----------------------------------------------------------------------------------
**Purpose: API for Mongo DB SignIn
-------------------------------------------------------------------------------------*/
module.exports.mongoSignIn = function (req, res) {
  // console.log('signIn');
  var jsonObj;
  var returnData = {};

  var userRecord;
  var personRecord;
  var orgRecord;
  var hostRecord;
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  var token = '';

  // console.log('json object : ' + JSON.stringify(jsonObj));

  async.waterfall(
    [
      function (callback) {
        params = {
          db: dbName,
          collectionName: jsonObj.collectionName,
          queryStr: { userName: { $eq: jsonObj.userName } },
        };
        //This method to check email is exists or not
        mongodbCrud.retrieveAll(params, function (response) {
          // console.log('user table response--->' + JSON.stringify(response));
          //var userRes = response.response[0];
          // console.log('user data : ' + JSON.stringify(response));
          if (response.response.length == 0) {
            //  console.log("Invalid Email!");
            returnData.code = 400;
            returnData.message = 'Invalid Email!';
            returnData.error = 'NA';
            returnData.data = 'NA';
            callback(400, returnData);
          } else if (response.response[0].userStatus == 'Inactive') {
            // console.log('status of user if : ' + JSON.stringify(response.response[0].userStatus));
            returnData.code = 400;
            returnData.message = 'Inactive!';
            returnData.error = 'NA';
            returnData.data = 'NA';
            callback(400, returnData);
          } else {
            // console.log('status of user else : ' + JSON.stringify(response.response[0].userStatus));
            userRecord = response.response[0];

            if (userRecord.signUpType == 'Social') {
              if (jsonObj.password == '') {
                // console.log('data for check password : ' + JSON.stringify(userRecord));
                returnData.code = 200;
                returnData.signUpType = userRecord.signUpType;
                returnData.signUpProvider = userRecord.signUpProvider;
                returnData.passWordType = userRecord.passWord !== '' ? true : false;
                returnData.message = 'Social Sign Up';
                // returnData.personEmail = userRecord.userName;
                returnData.partyId = userRecord.partyId;
                returnData.personId = userRecord.personId;
                // returnData.signUpType = userRecord.signUpType;
                returnData.userId = userRecord.id;
                returnData.personEmail = jsonObj.userName;
                returnData.data = 'NA';
                callback(null);
              } else {
                var resPassword = userRecord.passWord;
                //Comparing password
                comparePassword(jsonObj.password, resPassword, function (err, isMatch) {
                  if (err) {
                    returnData.code = 400;
                    returnData.message = 'Invalid password!';
                    returnData.error = err;
                    returnData.data = 'NA';
                    returnData.provider = userRecord.signUpProvider;
                    callback(400, returnData);
                  } else if (isMatch) {
                    returnData.passWordType = userRecord.passWord !== '' ? true : false;
                    returnData.partyId = userRecord.partyId;
                    returnData.personId = userRecord.personId;
                    returnData.signUpType = 'Custom';
                    returnData.signUpChangePassword = userRecord.signUpType;
                    returnData.userId = userRecord.id;
                    returnData.personEmail = jsonObj.userName;
                    // console.log('return data email:' + JSON.stringify(returnData));
                    callback(null);
                  } else {
                    returnData.code = 400;
                    returnData.message = 'Invalid password!';
                    returnData.error = err;
                    returnData.data = 'NA';
                    returnData.provider = userRecord.signUpProvider;
                    callback(400, returnData);
                  }
                });
              }
            } else {
              var resPassword = userRecord.passWord;
              //Comparing password
              comparePassword(jsonObj.password, resPassword, function (err, isMatch) {
                if (err) {
                  returnData.code = 400;
                  returnData.message = 'Invalid password!';
                  returnData.error = err;
                  returnData.data = 'NA';
                  returnData.provider = userRecord.signUpProvider;
                  callback(400, returnData);
                } else if (isMatch) {
                  returnData.passWordType = userRecord.passWord !== '' ? true : false;
                  returnData.partyId = userRecord.partyId;
                  returnData.personId = userRecord.personId;
                  returnData.signUpType = userRecord.signUpType;
                  returnData.userId = userRecord.id;
                  returnData.personEmail = jsonObj.userName;
                  // console.log('return data email:' + JSON.stringify(returnData));
                  callback(null);
                } else {
                  returnData.code = 400;
                  returnData.message = 'Invalid password!';
                  returnData.error = err;
                  returnData.data = 'NA';
                  returnData.provider = userRecord.signUpProvider;
                  callback(400, returnData);
                }
              });
            }
          }
        });
      },
      function (callback) {
        var personParams = {
          db: dbName,
          collectionName: 'person',
          _id: ObjectId(userRecord.personId),
          // "queryStr": { "_id": { "$eq": userRecord.personId } }
        };
        mongodbCrud.retrieveOne(personParams, function (personRes) {
          // console.log('person table response ----> : ' + JSON.stringify(personRes));
          if (personRes.status == 200) {
            personRecord = personRes.response[0];
            returnData.personType = personRes.response[0].personType;

            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in retrieving data in person table';
            returnData.error = err;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        var userParams = {
          collectionName: 'person',
          pkId: ObjectId(userRecord.personId),
          updateData: {
            status: 'active',
          },
        };
        mongodbCrud.updateData(userParams, function (userRes) {
          if (userRes.status == '200') {
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating user status';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },

      function (callback) {
        // console.log('person table response ----> : ' + JSON.stringify(userRecord));
        var personParams = {
          db: dbName,
          collectionName: 'attachment',
          // "_id": ObjectId(userRecord.personId)
          queryStr: {
            $and: [
              {
                personId: { $eq: userRecord.personId },
                type: { $eq: 'PROFILE' },
              },
            ],
          },
        };
        mongodbCrud.retrieveAll(personParams, function (personRes) {
          // console.log('person table response ----> : ' + JSON.stringify(personRes));
          if (personRes.status == 200 && personRes.response.length != 0) {
            // console.log('data to display profile image : ' + JSON.stringify(personRes.response[0]));
            const attachmentData = personRes.response[0];
            //   console.log('attachment key if');
            returnData.url = attachmentData.url;
            returnData.path = null;
            returnData.key = null;
            callback(null);
          } else {
            returnData.url = null;
            returnData.path = null;
            returnData.key = null;
            callback(null);
          }
        });
      },
      function (callback) {
        var rolesParams = {
          db: dbName,
          collectionName: 'role',
          queryStr: { roleName: { $eq: personRecord.personType } },
        };
        mongodbCrud.retrieveAll(rolesParams, function (rolesRes) {
          // console.log('roles res--->> ' + JSON.stringify(rolesRes));
          if (rolesRes.status == 200) {
            token = jwt.sign(
              {
                userName: jsonObj.userName,
                personId: userRecord.personId,
                roleCode: rolesRes.response[0].roleCode,
              },
              secret.secret,
              {
                expiresIn: 7200,
              }
            );

            var secretKey = secret.secret;
            jwt.verify(token, secretKey, function (error, decoded) {
              // console.log('decoded in sign in method' + JSON.stringify(decoded));
            });

            // console.log('role code --->> ' + JSON.stringify(rolesRes.response[0].roleCode));
            returnData.token = token;
            returnData.roleId = rolesRes.response[0]._id;
            returnData.roleCode = rolesRes.response[0].roleCode;
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in retrieving data in role table';
            returnData.error = err;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        var partyParams = {
          db: dbName,
          collectionName: 'party',
          queryStr: { _id: { $eq: userRecord.partyId } },
        };
        mongodbCrud.retrieveAll(partyParams, function (partyRes) {
          if (partyRes.status == 200) {
            // console.log('response of party-->' + JSON.stringify(partyRes));
            returnData.partyCode = partyRes.response[0].partyCode;
            returnData.orgId = partyRes.response[0].orgId;
            orgRecord = partyRes.response[0].orgId;
            if (req.body) {
              returnData.code = 200;
              callback(null, returnData);
            } else {
              returnData.code = 200;
              callback(null, returnData);
            }
          } else {
            returnData.code = 400;
            returnData.message = 'Error in retrieving data in party table';
            returnData.error = err;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
    ],
    function (err, result) {
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

//This method used to compare the password (from frontend end and hash stored in backend)
comparePassword = function (candidatePassword, hashpassword, callback) {
  // console.log(' >>>>>> ' + candidatePassword);
  // console.log(' >>>>>> ' + hashpassword);
  bcrypt.compare(candidatePassword, hashpassword, (err, isMatch) => {
    if (err) {
      // console.log('err : ' + JSON.stringify(err));
      callback(err, null);
    } else {
      // console.log(' comparePassword - success: ' + JSON.stringify(isMatch));
      callback(null, isMatch);
    }
  });
};

/* -----------------------------------------------------------------------------------
**Purpose: API for updating role and organization details
-------------------------------------------------------------------------------------*/

module.exports.updateOrgAndRole = function (req, res) {
  var jsonObj;
  var returnData = {};
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  var personId = jsonObj.personId;
  var role = jsonObj.roleName;
  var personTypeName;
  async.waterfall(
    [
      function (callback) {
        //retrieve data from user table based on the userName and check the emailId exists or not
        // console.log('jsonObj ' + JSON.stringify(jsonObj));
        //jsonObj.db
        userParams = {
          db: dbName,
          collectionName: 'user',
          queryStr: { personId: { $eq: personId } },
        };
        // console.log('personId>>>>' + JSON.stringify(personId));
        //Check emailId exists in system
        mongodbCrud.retrieveAll(userParams, function (usersRes) {
          // console.log('response usersRes' + JSON.stringify(usersRes));
          userRecord = usersRes.response[0];
          if (usersRes.response.length > 0) {
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Invalid Email!';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        var userParams = {
          collectionName: 'user',
          pkId: userRecord._id,
          updateData: {
            userStatus: 'Active',
          },
        };
        mongodbCrud.updateData(userParams, function (userRes) {
          // console.log('User table data update response->' + JSON.stringify(userRes));
          if (userRes.status == '200') {
            userResData = userRes.data[0];
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating user status';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        orgParams = {
          db: dbName,
          collectionName: 'organization',
          queryStr: { shortName: { $eq: role } },
        };
        mongodbCrud.retrieveAll(orgParams, function (orgRes) {
          // console.log('response from orgParams-->' + JSON.stringify(orgRes));
          if (orgRes.response.length > 0) {
            if (orgRes.status == 200) {
              orgData = orgRes.response[0];
              personTypeName = orgData.description;
              // console.log("org code --->" + orgCode);
              // callback(null, partyRes.data[0].id)
              callback(null, orgData);
            } else {
              returnData.code = 400;
              returnData.message = 'Error in retrieving data from org table!';
              returnData.error = partyRes.error;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          }
        });
      },
      function (data, callback) {
        // console.log('org data->' + JSON.stringify(data));
        var partyParams = {
          collectionName: 'party',
          // "queryStr": { "_id": { "$eq": userRecord.partyId.str } },
          pkId: userRecord.partyId,
          updateData: {
            orgId: data._id.toString(),
            partyCode: data.orgCode,
            partyType: data.shortName,
            partyDescription: data.description,
          },
        };
        mongodbCrud.updateData(partyParams, function (partyRes) {
          // console.log('party table data update response->' + JSON.stringify(partyRes));
          if (partyRes.status == '200') {
            partyResData = partyRes.data[0];
            callback(null, data);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating user status';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (orgDataInPerson, callback) {
        // console.log('orgData in person function' + JSON.stringify(orgDataInPerson));
        var personParams = {
          collectionName: 'person',
          // "queryStr": { "_id": { "$eq": userRecord.personId } },
          pkId: userRecord.personId,
          updateData: {
            personType: orgDataInPerson.description,
          },
        };
        mongodbCrud.updateData(personParams, function (personRes) {
          // console.log('person data update response->' + JSON.stringify(personRes));
          if (personRes.status == '200') {
            personResData = personRes.data;
            callback(null, personResData);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating person table';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (personData, callback) {
        // console.log('personResData:' + JSON.stringify(personData));
        var rolesParams = {
          collectionName: 'role',
          queryStr: { roleName: { $eq: personTypeName } },
        };
        mongodbCrud.retrieveAll(rolesParams, function (rolesRes) {
          // console.log('rolesRes----->>> : ' + JSON.stringify(rolesRes));
          if (rolesRes.status == 200) {
            rolesResData = rolesRes.response[0];
            returnData.roleId = rolesRes.response[0]._id;
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in retrieving data in role table';
            returnData.error = err;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        userParams = {
          collectionName: 'userrole',
          queryStr: { userId: { $eq: ObjectId(userRecord._id) } },
        };
        mongodbCrud.retrieveAll(userParams, function (response) {
          // console.log('response of userparams' + JSON.stringify(response));
          if (response.status == 200) {
            userRolesData = response.response[0];
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in retrieving data in userrole table';
            returnData.error = err;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        var userRoleParams = {
          collectionName: 'userrole',
          // "queryStr": { "_id": { "$eq": ObjectId(userRecord._id) } },
          pkId: ObjectId(userRolesData._id),
          updateData: {
            roleId: rolesResData._id.toString(),
          },
        };
        mongodbCrud.updateData(userRoleParams, function (userRoleRes) {
          // console.log('User table data update response->' + JSON.stringify(userRoleRes));
          if (userRoleRes.status == '200') {
            returnData.code = 200;
            returnData.status = 'Updated successfully';
            userRoleResData = userRoleRes.data[0];
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Error in updating user role status';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
    ],
    function (err, result) {
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

module.exports.getToken = function (req, res) {
  // console.log('db name : ' + JSON.stringify(dbName));
  var token;
  var jsonObj;
  var returnData = {};
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  var personId = jsonObj.personId;
  var db = dbconn.get(dbName);
  var locCollection = db.collection('person');
  locCollection
    .aggregate([
      { $match: { _id: ObjectId(personId) } },
      { $addFields: { personId: { $toString: '$_id' } } },
      { $addFields: { personType: '$personType' } },
      { $addFields: { partyId: { $toObjectId: '$partyId' } } },
      {
        $lookup: {
          from: 'party',
          localField: 'partyId',
          foreignField: '_id',
          as: 'partyDetails',
        },
      },
      {
        $lookup: {
          from: 'user',
          localField: 'personId',
          foreignField: 'personId',
          as: 'userDetails',
        },
      },
      {
        $lookup: {
          from: 'role',
          localField: 'personType',
          foreignField: 'roleName',
          as: 'roleDetails',
        },
      },
      {
        $lookup: {
          from: 'attachment',
          localField: 'personId',
          foreignField: 'personId',
          as: 'asAttachment',
        },
      },
      // { $unwind: "$userDetails" },
      // { $unwind: "$roleDetails" }
    ])
    .toArray(function (err, response) {
      if (err) {
        if (req.body) {
          res.status(500).json({
            status: '500',
            message: 'Error retrieving data',
            error: err,
            response: response,
          });
        } else {
          res.status(500).json({
            status: '500',
            message: 'Error retrieving data',
            error: err,
            response: response,
          });
        }
      } else {
        // console.log('reponse data for token : ' + JSON.stringify(response));
        if (response.length != 0) {
          var data = response[0];
          token = jwt.sign(
            {
              userName: data.userDetails[0].userName,
              personId: data.personId,
              roleCode: data.roleDetails[0].roleCode,
            },
            secret.secret,
            {
              expiresIn: 7200,
            }
          );
          returnData.token = token;
          if (data.roleDetails.length != 0) {
            returnData.roleId = data.roleDetails[0].roleId;
            returnData.roleCode = data.roleDetails[0].roleCode;
          } else {
            returnData.roleId = '';
            returnData.roleCode = '';
          }
          returnData.partyId = data.partyId;
          returnData.personId = data.personId;
          if (data.roleDetails.length != 0) {
            returnData.signUpProvider = data.userDetails[0].signUpProvider;
            returnData.personEmail = data.userDetails[0].userName;
            returnData.passWordType = data.userDetails[0].passWord !== '' ? true : false;
          } else {
            returnData.signUpProvider = '';
            returnData.personEmail = '';
            returnData.passWordType = false;
          }
          returnData.personType = data.personType;
          console.log('attachment signup goole : ' + JSON.stringify(data.asAttachment));
          if (data.asAttachment.length != 0) {
            // console.log('attachment signup goole : ' + JSON.stringify(data.asAttachment));
            returnData.url = data.asAttachment[0].url;
            returnData.path = data.asAttachment[0].path;
            returnData.key = data.asAttachment[0].key;
          } else {
            returnData.url = null;
            returnData.path = null;
            returnData.key = null;
          }
          if (data.partyDetails.length != 0) {
            returnData.orgId = data.partyDetails[0].orgId;
            returnData.partyCode = data.partyDetails[0].partyCode;
          } else {
            returnData.orgId = '';
            returnData.partyCode = '';
          }
        }
        res.status(200).json({
          status: '200',
          message: 'Successfull...',
          error: 'NA',
          response: returnData,
        });
      }
    });
};

module.exports.compareCurrentPassword = function (req, res) {
  var userRecord;
  var returnData = {};
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }

  async.waterfall(
    [
      function (callback) {
        //retrieve data from user table based on the userName and check the emailId exists or not
        // console.log('jsonObj ' + JSON.stringify(jsonObj));
        //jsonObj.db
        userParams = {
          db: dbName,
          collectionName: 'user',
          queryStr: { userName: { $eq: jsonObj.userName } },
        };
        // console.log('personId>>>>' + JSON.stringify(jsonObj.userName));
        //Check emailId exists in system
        mongodbCrud.retrieveAll(userParams, function (usersRes) {
          // console.log('response usersRes' + JSON.stringify(usersRes));
          if (usersRes.response.length > 0) {
            userRecord = usersRes.response[0];
            callback(null);
          } else {
            returnData.code = 400;
            returnData.message = 'Invalid Email!';
            returnData.error = usersRes.error;
            returnData.data = 'NA';
            callback(400, returnData);
          }
        });
      },
      function (callback) {
        // console.log('user record : ' + JSON.stringify(userRecord));
        if (userRecord.passWord == '') {
          returnData.code = 200;
          returnData.message = 'Sucess!';
          returnData.error = 'NA';
          returnData.data = 'NA';
          callback(200, returnData);
        } else {
          comparePassword(jsonObj.currentPassword, userRecord.passWord, (err, ismatch) => {
            // console.log('is match password : ' + JSON.stringify(ismatch));
            if (ismatch) {
              returnData.code = 200;
              returnData.message = 'Sucess!';
              returnData.error = err;
              returnData.data = 'NA';
              callback(200, returnData);
            } else {
              returnData.code = 400;
              returnData.message = 'Error!';
              returnData.error = err;
              returnData.data = 'NA';
              callback(400, returnData);
            }
          });
        }
      },
    ],
    function (err, result) {
      if (req.body) {
        res.status(200).json(returnData);
      } else {
        res(returnData);
      }
    }
  );
};

module.exports.verifyToken = function (req, res) {
  var returnData = {};
  if (req.body) {
    jsonObj = JSON.parse(JSON.stringify(req.body, null, 3));
  } else {
    jsonObj = req;
  }
  // console.log('json obj : ' + JSON.stringify(jsonObj));
  jwt.verify(jsonObj.token, secret.secret, function (err, decoded) {
    if (err) {
      returnData.err = err;
      returnData.code = '400';
      res.status(200).json(returnData);
    } else {
      returnData.decoded = decoded;
      returnData.code = '200';
      res.status(200).json(returnData);
    }
  });
};
