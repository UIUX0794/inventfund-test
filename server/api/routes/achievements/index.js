/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlAchievements = require('../../controllers/achievements/achievements.controllers')

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
  .route('/createAchievementsDetails')
  .post(ctrlAchievements.createAchievementsDetails);

router
  .route('/updateAchievementsDetails')
  .post(ctrlAchievements.updateAchievementsDetails)

router
  .route('/retrieveAchievementsDetails')
  .post(ctrlAchievements.retrieveAchievementsDetails)

router
  .route('/retrieveAchievementsDetailsOne')
  .post(ctrlAchievements.retrieveAchievementsDetailsOne)

router
  .route('/deleteAchiDetails')
  .post(ctrlAchievements.deleteAchiDetails)

router
  .route('/retrieveLookupData')
  .post(ctrlAchievements.retrieveLookupData)  

router
  .route('/getScreenBuildergrouped')
  .post(ctrlAchievements.getScreenBuildergrouped)    
  
module.exports = router;
