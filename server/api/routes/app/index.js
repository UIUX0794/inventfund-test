/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();

/* GET home page. */
// router.get('/*', function (req, res, next) {
//   res.render('./index.html', { title: 'Express' })
// })

// /* GET home page. */
router.get('/', function (req, res, next) {
	res.render('dist/index.html', { title: 'Express' });
});
// router.get("*", function (req, res, next) {
//   res.sendfile("./dist/index.html");
//   // res.render("dist/index.html", { title: "Express" });
//   });

module.exports = router;
