/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
// var ctrlAWSS3Attachment = require('../../../controllers/generic/attachment/awsattachment.controllers');
// var ctrlGcsAttachment = require('../../../controllers/generic/attachment/gcsattachment.controllers');

var ctrlAWSS3Attachment = require('../../controllers/attachment/awsattachment.controllers');
var ctrlGcsAttachment = require('../../controllers/attachment/gcsattachment.controllers');

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
    .route('/fileUploadSingle')
    .post(ctrlAWSS3Attachment.fileUploadSingle);

router
    .route('/fileUploadTemp')
    .post(ctrlAWSS3Attachment.fileUploadTemp);

router
    .route('/uploadDirectToS3')
    .post(ctrlAWSS3Attachment.uploadDirectToS3)
router
    .route('/saveMultipleAttachment')
    .post(ctrlAWSS3Attachment.saveMultipleAttachment);

router
    .route('/deleteAttachment')
    .post(ctrlAWSS3Attachment.deleteAttachment);

router
    .route('/singleAttachment')
    .post(ctrlAWSS3Attachment.singleAttachment)

router
    .route('/updateAttachment')
    .post(ctrlAWSS3Attachment.updateAttachment)

router
    .route('/deleteAttachmentSingle')
    .post(ctrlAWSS3Attachment.deleteAttachmentSingle)

router
    .route('/uploadS3WithSignedurl')
    .post(ctrlAWSS3Attachment.uploadS3WithSignedurl)

router
    .route('/getSignedData')
    .post(ctrlAWSS3Attachment.getSignedData)

//----GCS Attachment router----
// router
//     .route('/fileUploadSingle')
//     .post(ctrlGcsAttachment.fileUploadSingle);

// router
//     .route('/fileUploadTemp')
//     .post(ctrlGcsAttachment.fileUploadTemp);

// router
//     .route('/saveMultipleAttachment')
//     .post(ctrlGcsAttachment.saveMultipleAttachment);

// router
//     .route('/imageUploadSingle')
//     .post(ctrlAttachment.imageUploadSingle);

// router
//     .route('/deleteS3File')
//     .get(ctrlAttachment.deleteS3File);

router
  .route('/retrieveAttachmentDataWithSignedUrl')
  .post(ctrlAWSS3Attachment.retrieveAttachmentDataWithSignedUrl) 
  
router
    .route('/getSignedUrlWithId')
    .post(ctrlAWSS3Attachment.getSignedUrlWithId)


module.exports = router;
