/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlEducationDetails = require('../../controllers/educationDetails/educationDetails.controllers');

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
  .route('/createEducationDetails')
  .post(ctrlEducationDetails.createEducationDetails);

router
  .route('/updateEducationDetails')
  .post(ctrlEducationDetails.updateEducationDetails)

router
  .route('/retrieveEducationDetails')
  .post(ctrlEducationDetails.retrieveEducationDetails)

router
  .route('/retrieveEducationDetailsOne')
  .post(ctrlEducationDetails.retrieveEducationDetailsOne)  

router
  .route('/deleteEducationDetails')
  .post(ctrlEducationDetails.deleteEducationDetails)  

module.exports = router;
