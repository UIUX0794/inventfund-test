
var express = require('express');
var router = express.Router();

var ctrlEmail = require('../../controllers/email/email.controllers');

router
  .route('/sendMail')
  .post(ctrlEmail.sendMail);


module.exports = router;
