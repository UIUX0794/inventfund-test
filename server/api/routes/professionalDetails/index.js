/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlProfessionalDetails = require('../../controllers/professionalDetails/professionalDetails.controllers')

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
  .route('/createProfessionalDetails')
  .post(ctrlProfessionalDetails.createProfessionalDetails);

router
  .route('/updateProfessionalDetails')
  .post(ctrlProfessionalDetails.updateProfessionalDetails)

router
  .route('/retrieveProfessionalDetails')
  .post(ctrlProfessionalDetails.retrieveProfessionalDetails)

  router
  .route('/retrieveProfessionalDetailsOne')
  .post(ctrlProfessionalDetails.retrieveProfessionalDetailsOne)

router
  .route('/retrieveLookupData')
  .post(ctrlProfessionalDetails.retrieveLookupData)  

  router
  .route('/getScreenBuildergrouped')
  .post(ctrlProfessionalDetails.getScreenBuildergrouped)    
  
  router
  .route('/deleteProfessionalDetails')
  .post(ctrlProfessionalDetails.deleteProfessionalDetails)    

module.exports = router;
