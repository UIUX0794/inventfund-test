/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlProfile = require('../../controllers/profileData/profile.controllers');

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
  .route('/createProfile')
  .post(ctrlProfile.createProfile);

router
  .route('/updateprofile')
  .post(ctrlProfile.updateprofile)

router
  .route('/retrieveProfileData')
  .post(ctrlProfile.retrieveProfileData)

module.exports = router;
