/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlProfileSummary = require('../../controllers/profileSummary/profileSumary.controllers');

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
  .route('/createProfileSummary')
  .post(ctrlProfileSummary.createProfileSummary);

router
  .route('/updateProfileSummary')
  .post(ctrlProfileSummary.updateProfileSummary)

router
  .route('/retrieveProfileSummary')
  .post(ctrlProfileSummary.retrieveProfileSummary)

router
  .route('/retrieveProfileSummaryOne')
  .post(ctrlProfileSummary.retrieveProfileSummaryOne)  


router
  .route('/retrieveLinkedInPublicProfileDetails')
  .post(ctrlProfileSummary.retrieveLinkedInPublicProfileDetails)

router
  .route('/retrieveAttachmentData')
  .post(ctrlProfileSummary.retrieveAttachmentData)  
  
  
  
module.exports = router;
