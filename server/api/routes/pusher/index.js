var express = require('express');
var router = express.Router();
var pusherController = require('../../controllers/pusher/pusher.controllers');

router.route('/pusher').post(pusherController.pusher);

router.route('/createChat').post(pusherController.createChat);

router.route('/createChannel').post(pusherController.createChannel);

router.route('/retriveContacts').post(pusherController.retriveContacts);

router.route('/retriveAllMessages').post(pusherController.retriveAllMessages);

router.route('/updateMessageStatus').post(pusherController.updateMessageStatus);

router.route('/updateChannelId').post(pusherController.updateChannelId);

router.route('/activeChannelCount').post(pusherController.activeChannelCount);

module.exports = router;
