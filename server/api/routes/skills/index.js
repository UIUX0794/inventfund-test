/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlSkills = require('../../controllers/skills/skills.controllers')

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

router
  .route('/createSkillsDetails')
  .post(ctrlSkills.createSkillsDetails);

router
  .route('/updateSkillsDetails')
  .post(ctrlSkills.updateSkillsDetails)

router
  .route('/retrieveSkillsDetails')
  .post(ctrlSkills.retrieveSkillsDetails)

router
  .route('/retrieveSkillsDetailsOne')
  .post(ctrlSkills.retrieveSkillsDetailsOne)
 
router
  .route('/retrieveLookUpDetails')
  .post(ctrlSkills.retrieveLookUpDetails)

router
  .route('/deleteAchiDetails')
  .post(ctrlSkills.deleteSkillsDetails)

router
  .route('/retrieveLookupData')
  .post(ctrlSkills.retrieveLookupData)  

router
  .route('/getScreenBuildergrouped')
  .post(ctrlSkills.getScreenBuildergrouped)    
  
module.exports = router;
