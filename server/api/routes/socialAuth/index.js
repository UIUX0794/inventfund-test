var express = require('express');
var router = express.Router();
const passport = require('passport');

var ctrlPassportAuth = require('../../controllers/socialAuth/passportAuth');

router
  .route('/linkedin')
  .get(ctrlPassportAuth.passportAuth);

router
  .route('/linkedin/callback')
  .get(ctrlPassportAuth.passportAuthRedirect);


router
  .route('/requestToken')
  .get(ctrlPassportAuth.requestToken);


router
  .route("/getToken")
  .get(ctrlPassportAuth.getToken);

router
.route("/getTokenChangePassword")
.get(ctrlPassportAuth.getTokenChangePassword);


// router
// .route('/google')
// .get(ctrlPassportAuth.newPassportGoogle);


// router
// .route('/google/redirect')
// .get(ctrlPassportAuth.passportGoogleRedirect);



//Google login
router.get('/google', passport.authenticate('google', {
  scope: ['profile']
}));

// callback route for google to redirect to
router.get('/google/redirect', (req, res) => {
  res.send('you reached the redirect URI');
});

module.exports = router;