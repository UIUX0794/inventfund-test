/* -----------------------------------------------------------------
**Variable declaration
-------------------------------------------------------------------*/
var express = require('express');
var router = express.Router();
var ctrlUser = require('../../controllers/user/user.controllers.js');

/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */

//Used during Mongodb signUp 
router
  .route('/mongoSignUp')
  .post(ctrlUser.mongoSignUp);

//Used during Mongo Db signIn
router
  .route('/mongoSignIn')
  .post(ctrlUser.mongoSignIn)


//Used during MongoDb change password
router
  .route('/mongoChangePassword')
  .post(ctrlUser.mongoChangePassword)
  
//Used during MongoDb reset password
router
.route('/mongoResetPassword')
.post(ctrlUser.mongoResetPassword)

//Used to update role and org
router
  .route('/updateOrgAndRole')
  .post(ctrlUser.updateOrgAndRole)

//Used to update role and org
router
  .route('/getToken')
  .post(ctrlUser.getToken)

//Used to compare current password
router
  .route('/comparePassword')
  .post(ctrlUser.compareCurrentPassword)


//Used during MongoDb reset password
router
  .route('/verifyToken')
  .post(ctrlUser.verifyToken)

module.exports = router;
