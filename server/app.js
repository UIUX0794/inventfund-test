/* ---------------------------------------------------------------
** Variable declaration
------------------------------------------------------------------*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// var api = require('./api/routes/crudmongodb/index')

//passport related
var passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20');
var cors = require('cors');

/* ---------------------------------------------------------------
** Giving Connection for mongonative and mysql database
------------------------------------------------------------------*/
// require('./server/api/data/mongonative-connection.js').openDynamoDB();
require('./api/data/mongonative-connection.js').open();
//require('./api/data/mysql-connection.js')

/* ---------------------------------------------------------------
** ** Declarations of routes
------------------------------------------------------------------*/
var appRoutes = require('./api/routes/app');
var appUser = require('./api/routes/user');
var appAuth = require('./api/routes/socialAuth');
var appAttachment = require('./api/routes/attachment');
var appProfile = require('./api/routes/profileData');
var appProfileSummary = require('./api/routes/profileSummary');
var appProfessionalDetails = require('./api/routes/professionalDetails');
var appEducationDetails = require('./api/routes/educationDetails');
var appAchiDetails = require('./api/routes/achievements');
var appSkillsDetails = require('./api/routes/skills');
var pusher = require('./api/routes/pusher');

// mongoDb
var appCrudMongoDb = require('./api/routes/crudmongodb');

/* ---------------------------------------------------------------
** Express Setting
---------------------------------------------------------------- */
var app = express();
// app.engine('html', require('ejs').renderFile)
// app.set('view engine', 'html');
// app.set('views', 'dist')
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'dist')));
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));
app.use(express.static(path.join(__dirname, 'public')));
// This is required if angular and backend code are
// in different servers
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
  );
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  next();
});
/* ---------------------------------------------------------------
** Set Routes
---------------------------------------------------------------- */
// app.use(cors())
app.options('*', cors());
// app.options('*', (req, res) => { res.set('Access-Control-Allow-Origin', '*'); res.send('ok'); }); app.use((req, res) => { res.set('Access-Control-Allow-Origin', '*'); });

app.use('/', appRoutes);
app.use('/api/mongoDb', appCrudMongoDb);
app.use('/api/user', appUser);
app.use('/api/auth', appAuth);
app.use('/api/attachment', appAttachment);
app.use('/api/profileData', appProfile);
app.use('/api/profileSummary', appProfileSummary);
app.use('/api/professionalDetails', appProfessionalDetails);
app.use('/api/achievementsDetails', appAchiDetails);
app.use('/api/skillsDetails', appSkillsDetails);
app.use('/api/educationDetails', appEducationDetails);
app.use('/api/chatPusher', pusher);

// mongoDb routes
// app.use('/api/mongoDb', appCrudMongoDb);

/* ---------------------------------------------------------------
** Error handling
---------------------------------------------------------------- */
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.send('ok');
});

module.exports = app;
// module.exports = passport;
