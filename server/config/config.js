module.exports = {
  // set Type = "UAT" or "DEVELOPMENT" or "PRODUCTION"

  // set type DEVELOPMENT , TESTING , UAT , PRODUCTION

  type: 'DEVELOPMENT',

  AWSLINKEXPIRETIME: 30,

  //Credentials for sending emails
  usernameForEmail: 'support.unigovern@crecientech.com',
  password: 'crecientech@123',

  //DEVELOPMENT

  SERVERURLDEV: 'http://inventfunds-dev.crecientech.com/',
  SERVERURLDEVREDIRECT: 'http://inventfunds-dev.crecientech.com/',
  DBSERVERURLDEV: 'mongodb://admin:invenfundsadmin@52.72.82.77:27017',
  DBNAMEDEV: 'inventfunds-test',
  SCREENBUILDERDBNAMEDEV: 'screenbuilder-inventfunds-test',
  AWSS3bucketNameDev: 'invent-fund-dev',
  REDIRECTURLLINKEDINAUTHDEV: 'http://inventfunds-dev.crecientech.com/#/auth/linkedInLogin?email=',
  REDIRECTURLLINKEDINCHANGEPASSWORDDEV:
    'http://inventfunds-dev.crecientech.com/#/profile/settings?email=',

  //TESTING

  // mongodb://admin:invenfundsadmin@52.72.82.77:27017/?authSource=inventfunds-test&readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false

  SERVERURLTEST: 'http://inventfunds-dev.crecientech.com/',
  SERVERURLTESTREDIRECT: 'http://inventfunds-dev.crecientech.com/',
  DBSERVERURLTEST: 'mongodb://admin:invenfundsadmin@52.72.82.77:27017',
  DBNAMETEST: 'inventfunds-test',
  SCREENBUILDERDBNAMETEST: 'screenbuilder-inventfunds-test',
  AWSS3bucketNameTest: 'inventfunds-test',
  REDIRECTURLLINKEDINAUTHTEST: 'http://inventfunds-dev.crecientech.com/#/auth/linkedInLogin?email=',
  REDIRECTURLLINKEDINCHANGEPASSWORDTEST:
    'http://inventfunds-dev.crecientech.com/#/profile/settings?email=',

  // SERVERURLTEST: 'http://localhost:4000/',
  // SERVERURLTESTREDIRECT: 'http://localhost:4000/',
  // DBSERVERURLTEST: 'mongodb://admin:invenfundsadmin@52.72.82.77:27017/?authSource=inventfunds-test&readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false',
  // DBNAMETEST: 'inventFunds',
  // SCREENBUILDERDBNAMETEST: 'screenbuilder-inventFunds',
  // AWSS3bucketNameTest: 'invent-fund-dev',

  //UAT

  SERVERURLUAT: 'https://inventfunds-demo.crecientech.com/',
  SERVERURLUATDIRECT: 'http://localhost:4000/',
  DBSERVERURLUAT: 'mongodb://admin:invenfundsadmin@35.170.237.229:27017',
  DBNAMEUAT: 'invent-funds-test',
  SCREENBUILDERDBNAMEUAT: 'screenbuilder-invent-funds-test',
  AWSS3bucketNameUat: 'invent-funds-test',
  REDIRECTURLLINKEDINAUTHUAT:
    'https://inventfunds-demo.crecientech.com/#/auth/linkedInLogin?email=',
  REDIRECTURLLINKEDINCHANGEPASSWORDUAT:
    'https://inventfunds-demo.crecientech.com/#/profile/settings?email=',

  //PRODUCTION

  SERVERURLPROD: 'https://inventfunds-demo.crecientech.com/',
  SERVERURLPRODREDIRECT: 'http://localhost:4000/',
  DBSERVERURLPROD: 'mongodb://admin:invenfundsadmin@35.170.237.229:27017',
  DBNAMEPROD: 'invent-funds-prod',
  SCREENBUILDERDBNAMEPROD: 'screenbuilder-invent-funds-test',
  AWSS3bucketNameProd: 'invent-funds-test',
  REDIRECTURLLINKEDINAUTHPROD:
    'https://inventfunds-demo.crecientech.com/#/auth/linkedInLogin?email=',
  REDIRECTURLLINKEDINCHANGEPASSWORDPROD:
    'https://inventfunds-demo.crecientech.com/#/profile/settings?email=',
};
