(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-auth-auth-module"],{

/***/ "./src/app/auth/auth-callback/auth-callback.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/auth/auth-callback/auth-callback.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- auth callback reference route for linkedin login -->\r\n"

/***/ }),

/***/ "./src/app/auth/auth-callback/auth-callback.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/auth/auth-callback/auth-callback.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvYXV0aC1jYWxsYmFjay9hdXRoLWNhbGxiYWNrLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/auth/auth-callback/auth-callback.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/auth/auth-callback/auth-callback.component.ts ***!
  \***************************************************************/
/*! exports provided: AuthCallbackComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthCallbackComponent", function() { return AuthCallbackComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");





var AuthCallbackComponent = /** @class */ (function () {
    /**
     *
     * @param inventFundsApiService apo service to get data from database
     * @param activatedRoute to get the current activated route
     * @param router navigate between diffrent routes
     * @param appState get required data from appstate
     */
    function AuthCallbackComponent(inventFundsApiService, activatedRoute, router, appState) {
        var _this = this;
        this.inventFundsApiService = inventFundsApiService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.appState = appState;
        this.page = sessionStorage.getItem("page");
        // console.log("page type from session storage : " + JSON.stringify(this.page));
        this.activatedRoute.queryParams.subscribe(function (res) {
            // console.log("queryParams:" + JSON.stringify(res));
            if (res) {
                _this.navigate(res, _this.page);
            }
        });
    }
    // ng onInit
    AuthCallbackComponent.prototype.ngOnInit = function () { };
    // dippends on the page type navigate to login or signup
    /**
     *
     * @param queryData response from query params
     * @param pageType login or signup from session storage
     */
    AuthCallbackComponent.prototype.navigate = function (queryData, pageType) {
        // console.log("query data : " + JSON.stringify(queryData));
        // console.log("page type : " + JSON.stringify(pageType));
        if (pageType == "login") {
            this.navigateToProfile(queryData);
        }
        else if (pageType == "signup") {
            this.navigateToSignupRole(queryData);
        }
        else if (pageType == "resetPassword") {
            console.log("reset password : " + JSON.stringify(queryData));
            this.navigateToresetPassword(queryData);
        }
    };
    // if page type login navigate to login
    AuthCallbackComponent.prototype.navigateToProfile = function (email) {
        if (email) {
            this.router.navigate(["auth/login"], {
                queryParams: { email: email.email }
            });
        }
    };
    // if page type signup navigate to signup
    AuthCallbackComponent.prototype.navigateToSignupRole = function (email) {
        console.log("data for signup : " + JSON.stringify(email));
        if (email) {
            this.router.navigate(["auth/sign-up"], {
                queryParams: { email: email.email }
            });
        }
    };
    // if page type signup navigate to signup
    AuthCallbackComponent.prototype.navigateToresetPassword = function (email) {
        console.log("data for signup : " + JSON.stringify(email));
        if (email) {
            var enc = window.btoa(email.email);
            this.router.navigate(['/profile/settings'], {
                queryParams: { email: enc, setActive: 2 }
            });
        }
    };
    AuthCallbackComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-auth-callback",
            template: __webpack_require__(/*! ./auth-callback.component.html */ "./src/app/auth/auth-callback/auth-callback.component.html"),
            styles: [__webpack_require__(/*! ./auth-callback.component.scss */ "./src/app/auth/auth-callback/auth-callback.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_2__["InventFundsApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_app_state_service__WEBPACK_IMPORTED_MODULE_4__["AppStateService"]])
    ], AuthCallbackComponent);
    return AuthCallbackComponent;
}());



/***/ }),

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng-bootstrap-form-validation */ "./node_modules/ng-bootstrap-form-validation/fesm5/ng-bootstrap-form-validation.js");
/* harmony import */ var _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./sign-up/sign-up.component */ "./src/app/auth/sign-up/sign-up.component.ts");
/* harmony import */ var _sign_up_roles_sign_up_roles_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./sign-up-roles/sign-up-roles.component */ "./src/app/auth/sign-up-roles/sign-up-roles.component.ts");
/* harmony import */ var _emailsentpage_emailsentpage_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./emailsentpage/emailsentpage.component */ "./src/app/auth/emailsentpage/emailsentpage.component.ts");
/* harmony import */ var _sign_up_details_sign_up_details_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./sign-up-details/sign-up-details.component */ "./src/app/auth/sign-up-details/sign-up-details.component.ts");
/* harmony import */ var _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./unauthorized/unauthorized.component */ "./src/app/auth/unauthorized/unauthorized.component.ts");
/* harmony import */ var _auth_callback_auth_callback_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./auth-callback/auth-callback.component */ "./src/app/auth/auth-callback/auth-callback.component.ts");
/* harmony import */ var _services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../services/roles-auth-guard.service */ "./src/app/services/roles-auth-guard.service.ts");














var routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
        canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["RolesAuthGuardService"]],
        data: ['login']
    },
    {
        path: 'sign-up',
        component: _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_7__["SignUpComponent"],
        canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["RolesAuthGuardService"]],
        data: ['sign-up']
    },
    {
        path: "sign-up-roles",
        component: _sign_up_roles_sign_up_roles_component__WEBPACK_IMPORTED_MODULE_8__["SignUpRolesComponent"],
        canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["RolesAuthGuardService"]],
        data: ['sign-up-roles']
    },
    {
        path: "email-sent",
        component: _emailsentpage_emailsentpage_component__WEBPACK_IMPORTED_MODULE_9__["EmailsentpageComponent"],
        canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["RolesAuthGuardService"]],
        data: ['email-sent']
    },
    {
        path: "sign-up-details",
        component: _sign_up_details_sign_up_details_component__WEBPACK_IMPORTED_MODULE_10__["SignUpDetailsComponent"],
        canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["RolesAuthGuardService"]],
        data: ['sign-up-details']
    },
    {
        path: "sign-up-details/:data",
        component: _sign_up_details_sign_up_details_component__WEBPACK_IMPORTED_MODULE_10__["SignUpDetailsComponent"],
        canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_13__["RolesAuthGuardService"]],
        data: ['sign-up-details/:data']
    },
    {
        path: "linkedInLogin",
        component: _auth_callback_auth_callback_component__WEBPACK_IMPORTED_MODULE_12__["AuthCallbackComponent"]
    },
    {
        path: "un-authorized",
        component: _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_11__["UnauthorizedComponent"]
    },
    { path: '**', redirectTo: 'login' }
];
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_7__["SignUpComponent"],
                _sign_up_roles_sign_up_roles_component__WEBPACK_IMPORTED_MODULE_8__["SignUpRolesComponent"],
                _emailsentpage_emailsentpage_component__WEBPACK_IMPORTED_MODULE_9__["EmailsentpageComponent"],
                _sign_up_details_sign_up_details_component__WEBPACK_IMPORTED_MODULE_10__["SignUpDetailsComponent"],
                _unauthorized_unauthorized_component__WEBPACK_IMPORTED_MODULE_11__["UnauthorizedComponent"],
                _auth_callback_auth_callback_component__WEBPACK_IMPORTED_MODULE_12__["AuthCallbackComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_6__["NgBootstrapFormValidationModule"].forRoot(),
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/auth/emailsentpage/emailsentpage.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/auth/emailsentpage/emailsentpage.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row h-100 no-gutters\">\r\n\t<div class=\"col-md-6 col-sm-6 col-lg-6 col-xs-6\">\r\n\t\t<div class=\"slider-light\">\r\n\t\t\t<div class=\"position-relative h-100 bg-night-sky\">\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<span class=\"inventlogo pointer-ref\" (click)=\"routerNavigation('landing')\"></span>\r\n\t\t\t\t</div>\r\n\t\t\t\t<br />\r\n\t\t\t\t<br />\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<div\r\n\t\t\t\t\t\tclass=\"text-light d-flex flex-column center-align pt-5\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\t<h3>We Have Sent You An</h3>\r\n\t\t\t\t\t\t<h3>Email Please Check</h3>\r\n\t\t\t\t\t\t<p>some content</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n\t<div class=\"center-align col-md-6\">\r\n\t\t<div\r\n\t\t\tclass=\"mx-auto app-login-box col-sm-12 col-md-10 col-lg-10 col-xs-10\"\r\n\t\t>\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\t\t\t<div class=\"center-align\">\r\n\t\t\t\t<span class=\"img-fluid emailsent\"></span>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/auth/emailsentpage/emailsentpage.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/auth/emailsentpage/emailsentpage.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span.emailsent {\n  background: url('emailsent.svg') no-repeat top left;\n  background-size: contain;\n  cursor: pointer;\n  display: inline-block;\n  height: 300px;\n  width: 400px;\n  margin-top: 40px; }\n\nspan.inventlogo {\n  background: url('inventlogo.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 100px;\n  width: 100px;\n  margin-top: 42px;\n  margin-left: 30px; }\n\n.center-align {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9lbWFpbHNlbnRwYWdlL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxcYXV0aFxcZW1haWxzZW50cGFnZVxcZW1haWxzZW50cGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLG1EQUF5RTtFQUN6RSx3QkFBd0I7RUFDeEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUdqQjtFQUNDLG9EQUEwRTtFQUMxRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdsQjtFQUNDLFlBQVk7RUFDWixXQUFXO0VBQ1gsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2F1dGgvZW1haWxzZW50cGFnZS9lbWFpbHNlbnRwYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3Bhbi5lbWFpbHNlbnQge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2VtYWlsc2VudC5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGN1cnNvcjogcG9pbnRlcjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzMDBweDtcclxuXHR3aWR0aDogNDAwcHg7XHJcblx0bWFyZ2luLXRvcDogNDBweDtcclxufVxyXG5cclxuc3Bhbi5pbnZlbnRsb2dvIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9pbnZlbnRsb2dvLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMTAwcHg7XHJcblx0d2lkdGg6IDEwMHB4O1xyXG5cdG1hcmdpbi10b3A6IDQycHg7XHJcblx0bWFyZ2luLWxlZnQ6IDMwcHg7XHJcbn1cclxuXHJcbi5jZW50ZXItYWxpZ24ge1xyXG5cdGhlaWdodDogMTAwJTtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/auth/emailsentpage/emailsentpage.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/auth/emailsentpage/emailsentpage.component.ts ***!
  \***************************************************************/
/*! exports provided: EmailsentpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailsentpageComponent", function() { return EmailsentpageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var EmailsentpageComponent = /** @class */ (function () {
    function EmailsentpageComponent(router) {
        this.router = router;
    }
    EmailsentpageComponent.prototype.ngOnInit = function () { };
    EmailsentpageComponent.prototype.routerNavigation = function (data) {
        this.router.navigate(['landing/landing-page']);
    };
    EmailsentpageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'emailsentpage',
            template: __webpack_require__(/*! ./emailsentpage.component.html */ "./src/app/auth/emailsentpage/emailsentpage.component.html"),
            styles: [__webpack_require__(/*! ./emailsentpage.component.scss */ "./src/app/auth/emailsentpage/emailsentpage.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EmailsentpageComponent);
    return EmailsentpageComponent;
}());



/***/ }),

/***/ "./src/app/auth/login/login.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"conrtainer\">\r\n\t<div class=\"row no-gutters h-100\">\r\n\t\t<div class=\"col-md-6 col-sm-6 col-xs-6 col-lg-6 bg-night-sky\">\r\n\t\t\t<div>\r\n\t\t\t\t<span class=\"inventlogo pointer-ref\" (click)=\"routerNavigation('landing')\"></span>\r\n\t\t\t</div>\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\r\n\t\t\t<div class=\"container\">\r\n\t\t\t\t<div class=\"col-md-12 col-sm-12 col-xs-12 col-lg-12\">\r\n\t\t\t\t\t<div class=\"slide-img-bg\"></div>\r\n\t\t\t\t\t<div class=\"slider-content text-light\">\r\n\t\t\t\t\t\t<div\r\n\t\t\t\t\t\t\tclass=\"col-sm-12 col-md-10 col-lg-10 col-xs-6 justify-content align\"\r\n\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t<!-- <h3 class=\"fw\">Start Here</h3> -->\r\n\t\t\t\t\t\t\t<h2 style=\"font-weight: 500 !important;\">\r\n\t\t\t\t\t\t\t\tStart Here\r\n\t\t\t\t\t\t\t</h2>\r\n\t\t\t\t\t\t\t<p style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\tDocumentation and examples for Bootstrap’s\r\n\t\t\t\t\t\t\t\tpowerful, responsive navigation header, the\r\n\t\t\t\t\t\t\t\tnavbar. Includes support for branding\r\n\t\t\t\t\t\t\t\tDocumentation\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t<!-- <small>Documentation and examples for Bootstrap’s powerful, responsive navigation header, the navbar. Includes support for branding Documentation</small> -->\r\n\r\n\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\tExplore More <i class=\"fa fa-arrow-right\"></i>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t\t<span class=\"Group866 img-fluid\"></span>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<!-- <div class=\"d-sm-none d-xs-none d-md-block d-lg-block \">\r\n                            <span class=\"dot img-fluid\"></span>\r\n                        </div> -->\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t\t<div\r\n\t\t\tclass=\"col-md-6 col-sm-6 col-xs-6 col-lg-6 d-flex bg-white justify-content-center align-items-center\"\r\n\t\t>\r\n\t\t\t<div\r\n\t\t\t\tclass=\"login-box-padding mx-auto app-login-box col-sm-12 col-md-10 col-lg-10 col-xs-12 abc\"\r\n\t\t\t>\r\n\t\t\t\t<h2 class=\"centre-text-bold\">Login</h2>\r\n\r\n\t\t\t\t<div class=\"mt-4\">\r\n\t\t\t\t\t<form\r\n\t\t\t\t\t\t[formGroup]=\"loginForm\"\r\n\t\t\t\t\t\t(validSubmit)=\"submit($event, loginForm.value)\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t<div class=\"col-xl-8 margin-h-center\">\r\n\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t\t<input formControlName=\"email\"\r\n\t\t\t\t\t\t\t\t\t\t\tid=\"exampleEmail\"\r\n\t\t\t\t\t\t\t\t\t\t\tname=\"email\"\r\n\t\t\t\t\t\t\t\t\t\t\ttype=\"email\"\r\n\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Email id\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"form-control\"\r\n\t\t\t\t\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t<div class=\"col-xl-8 margin-h-center\">\r\n\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\t\t\t\t\tid=\"examplePassword\"\r\n\t\t\t\t\t\t\t\t\t\t\tformControlName=\"password\"\r\n\t\t\t\t\t\t\t\t\t\t\tname=\"password\"\r\n\t\t\t\t\t\t\t\t\t\t\ttype=\"password\"\r\n\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Password\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"form-control password-noImage\"\r\n\t\t\t\t\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t\t<button\r\n\t\t\t\t\t\t\t\ttype=\"submit\"\r\n\t\t\t\t\t\t\t\tclass=\"font-large btn btn-wide mb-2 mr-2 btn-success\"\r\n\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\tLogin\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-arrow-right\"></i>\r\n\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</form>\r\n\t\t\t\t\t<br />\r\n\t\t\t\t\t<br />\r\n\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t<div class=\"hrdivider\">\r\n\t\t\t\t\t\t\t<hr />\r\n\t\t\t\t\t\t\t<span>Or With</span>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div style=\"margin-top: -30px;\">\r\n\t\t\t\t\t\t\t<button\r\n\t\t\t\t\t\t\t\t(click)=\"signInWithGoogle()\"\r\n\t\t\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\t\t\tclass=\"btn btn-wide mb-2 mr-2 btn-white btnw\"\r\n\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t<span class=\"googleicon\"></span> Google\r\n\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t<button\r\n\t\t\t\t\t\t\t\t(click)=\"signInWithLinkedIn()\"\r\n\t\t\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\t\t\tclass=\"btn btn-wide mb-2 mr-2 btn-primary btnw\"\r\n\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t<i class=\"fab fa-linkedin-in\"></i> LinkedIn\r\n\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<br />\r\n\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t<p\r\n\t\t\t\t\t\t\tclass=\"centre-text\"\r\n\t\t\t\t\t\t\tstyle=\"margin-top: 0; margin-bottom: -2px;\"\r\n\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t<b>New Here?</b>\r\n\t\t\t\t\t\t\t<a href=\"#/auth/sign-up\"> Register Now</a>\r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t<a class=\"centre-text text-dark\" href=\"\"\r\n\t\t\t\t\t\t\t><b>Forgot Password?</b></a\r\n\t\t\t\t\t\t>\r\n\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t<br />\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/auth/login/login.component.scss":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span.inventlogo {\n  background: url('inventlogo.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 64px;\n  width: 85px;\n  margin-top: 42px;\n  margin-left: 30px; }\n\nspan.dot {\n  background: url('dotcommon.svg') no-repeat bottom center;\n  background-size: contain;\n  display: inline-block;\n  height: 100px;\n  width: 160px;\n  margin-left: 582px;\n  margin-top: -20px;\n  overflow: hidden;\n  position: relative;\n  z-index: 1; }\n\n.space {\n  padding-top: 0px; }\n\n.parap {\n  padding-right: 180px; }\n\n::-webkit-input-placeholder {\n  color: blue;\n  opacity: 0.5;\n  /* Firefox */ }\n\n::-moz-placeholder {\n  color: blue;\n  opacity: 0.5;\n  /* Firefox */ }\n\n::-ms-input-placeholder {\n  color: blue;\n  opacity: 0.5;\n  /* Firefox */ }\n\n::placeholder {\n  color: blue;\n  opacity: 0.5;\n  /* Firefox */ }\n\n:-ms-input-placeholder {\n  /* Internet Explorer 10-11 */\n  color: blue; }\n\n::-ms-input-placeholder {\n  /* Microsoft Edge */\n  color: blue; }\n\n.btn-white {\n  background-color: white;\n  border: 1px solid lightgrey; }\n\n.hrdivider {\n  position: relative;\n  margin-bottom: 20px;\n  width: 100%;\n  text-align: center; }\n\n.hrdivider span {\n  position: relative;\n  top: -30px;\n  background: #fff;\n  padding: 0 20px;\n  font-size: 14px;\n  color: blue; }\n\n.display_text_center {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\nspan.googleicon {\n  background: url('32px-Google__G__Logo.svg.png') no-repeat top left;\n  background-size: contain;\n  cursor: pointer;\n  display: inline-block;\n  height: 10px;\n  width: 10px; }\n\nspan.Group866 {\n  background: url('group866.svg') no-repeat top left;\n  background-size: contain;\n  cursor: pointer;\n  display: inline-block;\n  height: 230px;\n  width: 230px;\n  margin-top: 40px; }\n\n.btnw {\n  height: 34px;\n  width: 180px;\n  font-size: 14px; }\n\n.login-box-padding {\n  padding-top: 85px; }\n\n::ng-deep .password-noImage.is-valid {\n  border-color: #ced4da !important;\n  padding-right: 2.25rem !important;\n  background-repeat: no-repeat !important;\n  background-position: center right calc(2.25rem / 4) !important;\n  background-size: calc(2.25rem / 2) calc(2.25rem / 2) !important;\n  background-image: url(); }\n\n::ng-deep .password-noImage.is-invalid {\n  border-color: #d92550 !important;\n  padding-right: 2.25rem !important;\n  background-repeat: no-repeat !important;\n  background-position: center right calc(2.25rem / 4) !important;\n  background-size: calc(2.25rem / 2) calc(2.25rem / 2) !important;\n  background-image: url() !important; }\n\n.font-large {\n  font-size: 14px; }\n\n@media only screen and (max-width: 1024px) {\n  span.dot {\n    background: url('dotcommon.svg') no-repeat bottom center;\n    background-size: contain;\n    display: inline-block;\n    height: 94px;\n    width: 180px;\n    margin-top: 55px;\n    margin-left: 399px;\n    overflow: hidden;\n    position: relative;\n    z-index: 1; } }\n\n@media only screen and (max-width: 768px) {\n  span.dot {\n    background: url('dotcommon.svg') no-repeat bottom center;\n    background-size: contain;\n    display: inline-block;\n    height: 54px;\n    width: 129px;\n    margin-left: 295px;\n    overflow: hidden;\n    position: relative;\n    z-index: 1;\n    margin-top: -5px; }\n  span.Group866 {\n    background: url('group866.svg') no-repeat top left;\n    background-size: contain;\n    cursor: pointer;\n    display: inline-block;\n    height: 141px;\n    width: 188px;\n    margin-top: 10px; }\n  .align {\n    padding-left: 30px !important; }\n  .abc {\n    margin-top: 50px; }\n  .login-box-padding {\n    padding-top: 40px; } }\n\n@media only screen and (max-width: 425px) {\n  span.dot {\n    background: url('dotcommon.svg') no-repeat bottom center;\n    display: none; }\n  .abc {\n    margin-top: 50px; }\n  .align {\n    padding-left: 20px !important; }\n  span.Group866 {\n    background: url('group866.svg') no-repeat top left;\n    background-size: contain;\n    cursor: pointer;\n    display: inline-block;\n    height: 221px;\n    width: 188px;\n    margin-top: 10px; } }\n\n.fw {\n  font-weight: 500px !important; }\n\n::ng-deep .swal-title {\n  margin: 0px !important;\n  font-size: 18px !important;\n  margin-bottom: 8px !important;\n  position: relative !important;\n  max-width: 100% !important;\n  padding: 0 !important;\n  color: #595959 !important;\n  font-weight: 400 !important;\n  text-align: center !important;\n  text-transform: none !important;\n  word-wrap: break-word !important; }\n\n::ng-deep .swal2-actions {\n  margin: 0 !important; }\n\n::ng-deep .swal2-icon {\n  margin: 10px !important; }\n\n::ng-deep .swal2-content {\n  margin-bottom: 8px !important;\n  font-weight: 400 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9sb2dpbi9EOlxcV29ya1xcUHJvamVjdHNcXElOVkVOVEZVTkRcXEJSQU5DSEVTXFxpbnZlbnRmdW5kLXRlc3Qvc3JjXFxhcHBcXGF1dGhcXGxvZ2luXFxsb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLG9EQUEwRTtFQUMxRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdsQjtFQUNDLHdEQUNPO0VBQ1Asd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixhQUFhO0VBQ2IsWUFBWTtFQUVaLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixVQUFVLEVBQUE7O0FBR1g7RUFDQyxnQkFBZ0IsRUFBQTs7QUFHakI7RUFDQyxvQkFBb0IsRUFBQTs7QUFHckI7RUFDQyxXQUFXO0VBQ1gsWUFBWTtFQUNaLFlBQUEsRUFBYTs7QUFIZDtFQUNDLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBQSxFQUFhOztBQUhkO0VBQ0MsV0FBVztFQUNYLFlBQVk7RUFDWixZQUFBLEVBQWE7O0FBSGQ7RUFDQyxXQUFXO0VBQ1gsWUFBWTtFQUNaLFlBQUEsRUFBYTs7QUFHZDtFQUNDLDRCQUFBO0VBQ0EsV0FBVyxFQUFBOztBQUdaO0VBQ0MsbUJBQUE7RUFDQSxXQUFXLEVBQUE7O0FBR1o7RUFDQyx1QkFBdUI7RUFDdkIsMkJBQTJCLEVBQUE7O0FBRzVCO0VBQ0Msa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBR25CO0VBQ0Msa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGVBQWU7RUFDZixXQUFXLEVBQUE7O0FBR1o7RUFDQyxZQUFZO0VBQ1osV0FBVztFQUNYLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CLEVBQUE7O0FBR3BCO0VBQ0Msa0VBQ21CO0VBQ25CLHdCQUF3QjtFQUN4QixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBR1o7RUFDQyxrREFBd0U7RUFDeEUsd0JBQXdCO0VBQ3hCLGVBQWU7RUFDZixxQkFBcUI7RUFHckIsYUFBYTtFQUNiLFlBQVk7RUFDWixnQkFBZ0IsRUFBQTs7QUFHakI7RUFFQyxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxpQkFBaUIsRUFBQTs7QUFHbEI7RUFDQyxnQ0FBZ0M7RUFDaEMsaUNBQWlDO0VBQ2pDLHVDQUF1QztFQUN2Qyw4REFBOEQ7RUFDOUQsK0RBQStEO0VBQy9ELHVCQUF1QixFQUFBOztBQUd4QjtFQUNDLGdDQUFnQztFQUNoQyxpQ0FBaUM7RUFDakMsdUNBQXVDO0VBQ3ZDLDhEQUE4RDtFQUM5RCwrREFBK0Q7RUFDL0Qsa0NBQWtDLEVBQUE7O0FBR25DO0VBQ0MsZUFBZSxFQUFBOztBQUVoQjtFQUNDO0lBQ0Msd0RBQ087SUFDUCx3QkFBd0I7SUFDeEIscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWixZQUFZO0lBQ1osZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLFVBQVUsRUFBQSxFQUNWOztBQUdGO0VBQ0M7SUFDQyx3REFDTztJQUNQLHdCQUF3QjtJQUN4QixxQkFBcUI7SUFDckIsWUFBWTtJQUNaLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsZ0JBQWdCLEVBQUE7RUFFakI7SUFDQyxrREFBd0U7SUFDeEUsd0JBQXdCO0lBQ3hCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsYUFBYTtJQUNiLFlBQVk7SUFDWixnQkFBZ0IsRUFBQTtFQUVqQjtJQUNDLDZCQUE2QixFQUFBO0VBRTlCO0lBQ0MsZ0JBQWdCLEVBQUE7RUFFakI7SUFDQyxpQkFBaUIsRUFBQSxFQUNqQjs7QUFHRjtFQUNDO0lBQ0Msd0RBQ087SUFDUCxhQUFhLEVBQUE7RUFFZDtJQUNDLGdCQUFnQixFQUFBO0VBRWpCO0lBQ0MsNkJBQTZCLEVBQUE7RUFFOUI7SUFDQyxrREFBd0U7SUFDeEUsd0JBQXdCO0lBQ3hCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsYUFBYTtJQUNiLFlBQVk7SUFDWixnQkFBZ0IsRUFBQSxFQUNoQjs7QUFHRjtFQUNDLDZCQUE2QixFQUFBOztBQUc5QjtFQUNDLHNCQUFzQjtFQUN0QiwwQkFBMEI7RUFFMUIsNkJBQTZCO0VBQzdCLDZCQUE2QjtFQUM3QiwwQkFBMEI7RUFDMUIscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUV6QiwyQkFBMkI7RUFDM0IsNkJBQTZCO0VBQzdCLCtCQUErQjtFQUMvQixnQ0FBZ0MsRUFBQTs7QUFHakM7RUFDQyxvQkFBb0IsRUFBQTs7QUFHckI7RUFDQyx1QkFBdUIsRUFBQTs7QUFHeEI7RUFDQyw2QkFBNkI7RUFDN0IsMkJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hdXRoL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3Bhbi5pbnZlbnRsb2dvIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9pbnZlbnRsb2dvLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogNjRweDtcclxuXHR3aWR0aDogODVweDtcclxuXHRtYXJnaW4tdG9wOiA0MnB4O1xyXG5cdG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG59XHJcblxyXG5zcGFuLmRvdCB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZG90Y29tbW9uLnN2ZycpIG5vLXJlcGVhdCBib3R0b21cclxuXHRcdGNlbnRlcjtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMTAwcHg7XHJcblx0d2lkdGg6IDE2MHB4O1xyXG5cdC8vIG1hcmdpbi10b3A6IDQycHg7XHJcblx0bWFyZ2luLWxlZnQ6IDU4MnB4O1xyXG5cdG1hcmdpbi10b3A6IC0yMHB4O1xyXG5cdG92ZXJmbG93OiBoaWRkZW47XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbi5zcGFjZSB7XHJcblx0cGFkZGluZy10b3A6IDBweDtcclxufVxyXG5cclxuLnBhcmFwIHtcclxuXHRwYWRkaW5nLXJpZ2h0OiAxODBweDtcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlciB7XHJcblx0Y29sb3I6IGJsdWU7XHJcblx0b3BhY2l0eTogMC41O1xyXG5cdC8qIEZpcmVmb3ggKi9cclxufVxyXG5cclxuOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XHJcblx0LyogSW50ZXJuZXQgRXhwbG9yZXIgMTAtMTEgKi9cclxuXHRjb2xvcjogYmx1ZTtcclxufVxyXG5cclxuOjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xyXG5cdC8qIE1pY3Jvc29mdCBFZGdlICovXHJcblx0Y29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5idG4td2hpdGUge1xyXG5cdGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5cdGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcclxufVxyXG5cclxuLmhyZGl2aWRlciB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdG1hcmdpbi1ib3R0b206IDIwcHg7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uaHJkaXZpZGVyIHNwYW4ge1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHR0b3A6IC0zMHB4O1xyXG5cdGJhY2tncm91bmQ6ICNmZmY7XHJcblx0cGFkZGluZzogMCAyMHB4O1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxuXHRjb2xvcjogYmx1ZTtcclxufVxyXG5cclxuLmRpc3BsYXlfdGV4dF9jZW50ZXIge1xyXG5cdGhlaWdodDogMTAwJTtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbnNwYW4uZ29vZ2xlaWNvbiB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvMzJweC1Hb29nbGVfX0dfX0xvZ28uc3ZnLnBuZycpXHJcblx0XHRuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGN1cnNvcjogcG9pbnRlcjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAxMHB4O1xyXG5cdHdpZHRoOiAxMHB4O1xyXG59XHJcblxyXG5zcGFuLkdyb3VwODY2IHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9ncm91cDg2Ni5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGN1cnNvcjogcG9pbnRlcjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0Ly8gaGVpZ2h0OiAyMDBweDtcclxuXHQvLyB3aWR0aDogMjIwcHg7XHJcblx0aGVpZ2h0OiAyMzBweDtcclxuXHR3aWR0aDogMjMwcHg7XHJcblx0bWFyZ2luLXRvcDogNDBweDtcclxufVxyXG5cclxuLmJ0bncge1xyXG5cdC8vIGhlaWdodDogMzBweDtcclxuXHRoZWlnaHQ6IDM0cHg7XHJcblx0d2lkdGg6IDE4MHB4O1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuLmxvZ2luLWJveC1wYWRkaW5nIHtcclxuXHRwYWRkaW5nLXRvcDogODVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5wYXNzd29yZC1ub0ltYWdlLmlzLXZhbGlkIHtcclxuXHRib3JkZXItY29sb3I6ICNjZWQ0ZGEgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nLXJpZ2h0OiAyLjI1cmVtICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdCAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciByaWdodCBjYWxjKDIuMjVyZW0gLyA0KSAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY2FsYygyLjI1cmVtIC8gMikgY2FsYygyLjI1cmVtIC8gMikgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoKTtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5wYXNzd29yZC1ub0ltYWdlLmlzLWludmFsaWQge1xyXG5cdGJvcmRlci1jb2xvcjogI2Q5MjU1MCAhaW1wb3J0YW50O1xyXG5cdHBhZGRpbmctcmlnaHQ6IDIuMjVyZW0gIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0ICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIHJpZ2h0IGNhbGMoMi4yNXJlbSAvIDQpICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjYWxjKDIuMjVyZW0gLyAyKSBjYWxjKDIuMjVyZW0gLyAyKSAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtaW1hZ2U6IHVybCgpICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5mb250LWxhcmdlIHtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcclxuXHRzcGFuLmRvdCB7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb25zL2RvdGNvbW1vbi5zdmcpIG5vLXJlcGVhdCBib3R0b21cclxuXHRcdFx0Y2VudGVyO1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0aGVpZ2h0OiA5NHB4O1xyXG5cdFx0d2lkdGg6IDE4MHB4O1xyXG5cdFx0bWFyZ2luLXRvcDogNTVweDtcclxuXHRcdG1hcmdpbi1sZWZ0OiAzOTlweDtcclxuXHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHR6LWluZGV4OiAxO1xyXG5cdH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG5cdHNwYW4uZG90IHtcclxuXHRcdGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaWNvbnMvZG90Y29tbW9uLnN2Zykgbm8tcmVwZWF0IGJvdHRvbVxyXG5cdFx0XHRjZW50ZXI7XHJcblx0XHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHRoZWlnaHQ6IDU0cHg7XHJcblx0XHR3aWR0aDogMTI5cHg7XHJcblx0XHRtYXJnaW4tbGVmdDogMjk1cHg7XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0ei1pbmRleDogMTtcclxuXHRcdG1hcmdpbi10b3A6IC01cHg7XHJcblx0fVxyXG5cdHNwYW4uR3JvdXA4NjYge1xyXG5cdFx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZ3JvdXA4NjYuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0Y3Vyc29yOiBwb2ludGVyO1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0aGVpZ2h0OiAxNDFweDtcclxuXHRcdHdpZHRoOiAxODhweDtcclxuXHRcdG1hcmdpbi10b3A6IDEwcHg7XHJcblx0fVxyXG5cdC5hbGlnbiB7XHJcblx0XHRwYWRkaW5nLWxlZnQ6IDMwcHggIWltcG9ydGFudDtcclxuXHR9XHJcblx0LmFiYyB7XHJcblx0XHRtYXJnaW4tdG9wOiA1MHB4O1xyXG5cdH1cclxuXHQubG9naW4tYm94LXBhZGRpbmcge1xyXG5cdFx0cGFkZGluZy10b3A6IDQwcHg7XHJcblx0fVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyNXB4KSB7XHJcblx0c3Bhbi5kb3Qge1xyXG5cdFx0YmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pY29ucy9kb3Rjb21tb24uc3ZnKSBuby1yZXBlYXQgYm90dG9tXHJcblx0XHRcdGNlbnRlcjtcclxuXHRcdGRpc3BsYXk6IG5vbmU7XHJcblx0fVxyXG5cdC5hYmMge1xyXG5cdFx0bWFyZ2luLXRvcDogNTBweDtcclxuXHR9XHJcblx0LmFsaWduIHtcclxuXHRcdHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xyXG5cdH1cclxuXHRzcGFuLkdyb3VwODY2IHtcclxuXHRcdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2dyb3VwODY2LnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRcdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRcdGN1cnNvcjogcG9pbnRlcjtcclxuXHRcdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRcdGhlaWdodDogMjIxcHg7XHJcblx0XHR3aWR0aDogMTg4cHg7XHJcblx0XHRtYXJnaW4tdG9wOiAxMHB4O1xyXG5cdH1cclxufVxyXG5cclxuLmZ3IHtcclxuXHRmb250LXdlaWdodDogNTAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zd2FsLXRpdGxlIHtcclxuXHRtYXJnaW46IDBweCAhaW1wb3J0YW50O1xyXG5cdGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xyXG5cdC8vIGJveC1zaGFkb3c6IDBweCAxcHggMXB4IHJnYmEoMCwgMCwgMCwgMC4yMSkgIWltcG9ydGFudDtcclxuXHRtYXJnaW4tYm90dG9tOiA4cHggIWltcG9ydGFudDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcclxuXHRtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcblx0Y29sb3I6ICM1OTU5NTkgIWltcG9ydGFudDtcclxuXHQvLyBmb250LXNpemU6IDEuODc1ZW0gIWltcG9ydGFudDtcclxuXHRmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcblx0dGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcclxuXHR3b3JkLXdyYXA6IGJyZWFrLXdvcmQgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zd2FsMi1hY3Rpb25zIHtcclxuXHRtYXJnaW46IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zd2FsMi1pY29uIHtcclxuXHRtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zd2FsMi1jb250ZW50IHtcclxuXHRtYXJnaW4tYm90dG9tOiA4cHggIWltcG9ydGFudDtcclxuXHRmb250LXdlaWdodDo0MDAgIWltcG9ydGFudDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.es5.js");
/* harmony import */ var _providers_config__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../providers/config */ "./src/providers/config.ts");











var LoginComponent = /** @class */ (function () {
    /**
     *
     * @param fb reactive form builder
     * @param router navigate to different routes
     * @param inventFundsApiService services to get data from database using endpoint apis
     * @param appState get variables from appsate or methods
     * @param authService to authenticate the data
     * @param activatedRoute get current route and data from that url
     */
    function LoginComponent(fb, router, inventFundsApiService, appState, authService, activatedRoute) {
        // get data from activated route params
        var _this = this;
        this.router = router;
        this.inventFundsApiService = inventFundsApiService;
        this.appState = appState;
        this.authService = authService;
        this.activatedRoute = activatedRoute;
        this.activatedRoute.queryParams.subscribe(function (res) {
            // console.log('queryParams login linked in :' + JSON.stringify(res.email));
            if (res.email) {
                _this.linkedIn(res.email);
            }
        });
        // login from for inputes
        this.loginForm = fb.group({
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
            password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
        });
    }
    // ng onInit
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.authState.subscribe(function (user) {
            _this.user = user;
            _this.loggedIn = user != null;
        });
    };
    // mehod to save and validate data onclick ob submit button
    /**
     *
     * @param $ev event from the form
     * @param value input values from the form
     */
    LoginComponent.prototype.submit = function ($ev, value) {
        // console.log('form ' + JSON.stringify(value));
        for (var c in this.loginForm.controls) {
            this.loginForm.controls[c].markAsTouched();
        }
        if (this.loginForm.valid) {
            // console.log("Valid!");
            this.login(value);
        }
        else {
            // console.log('Form invalid!');
        }
    };
    LoginComponent.prototype.routerNavigation = function (data) {
        this.router.navigate(['landing/landing-page']);
    };
    // if form validate take form data and login
    LoginComponent.prototype.login = function (formValue) {
        var _this = this;
        var params = {
            collectionName: 'user',
            userName: formValue.email,
            password: formValue.password,
        };
        this.inventFundsApiService.signIn(params).then(function (res) {
            // console.log("login response-->" + JSON.stringify(res));
            if (res.code == 200 && res.signUpType == 'Custom') {
                _this.setApplicationData(res);
                // this.router.navigate(['/profile/dashboard'])
                _this.router.navigate(['profile/editProfile']);
            }
            else if (res.code == 400 && res.message == 'Invalid password!') {
                //   return this.invalidPassword = true;
                _this.loginForm.controls['password'].setErrors({
                    invalidPassword: true,
                });
            }
            else if (res.code == 400 && res.message == 'Invalid Email!') {
                //    return this.invalidEmail = true;
                _this.loginForm.controls['email'].setErrors({ invalidEmail: true });
            }
            else if (res.code == 200 && res.data.signUpType == 'Social') {
                _this.swalMessage('You have signed up using ' +
                    res.data.signUpProvider +
                    '! Please login via ' +
                    res.data.signUpProvider).then(function (result) {
                    _this.loginForm.reset();
                });
            }
        });
    };
    // set appsate data and session storage data in terms of to access user data
    LoginComponent.prototype.setApplicationData = function (data) {
        console.log('token set local : ', JSON.stringify(data));
        this.appState.globalData.token = data.token;
        this.appState.globalData.roleId = data.roleId;
        this.appState.globalData.roleCode = data.roleCode;
        this.appState.globalData.partyId = data.partyId;
        this.appState.globalData.personId = data.personId;
        this.appState.globalData.personEmail = data.personEmail;
        this.appState.globalData.personType = data.personType;
        this.appState.globalData.orgId = data.orgId;
        this.appState.globalData.partyCode = data.partyCode;
        this.appState.globalData.signUpProvider = data.signUpProvider;
        this.appState.globalData.name = data.name;
        sessionStorage.setItem('passwordType', data.passWordType);
        var profile = crypto_js__WEBPACK_IMPORTED_MODULE_7__["AES"].encrypt(JSON.stringify(this.appState.globalData), this.appState.encryptSecretKey, this.appState.encryptOptions).toString();
        sessionStorage.setItem('profile', profile);
        console.log('data for profile : ' + JSON.stringify(data));
        if (data.url != null) {
            this.appState.setProfileImage(data.url);
            // sessionStorage.setItem('profileImage', data.url);
            sessionStorage.setItem('profileImage', data.url);
            this.appState.getProfileImage();
            this.appState.setAvatar(data.url);
        }
        // else if ('path' in data && 'key' in data) {
        //   if (data.path !== null && data.key !== null) {
        //     this.appState.getSignedUrl(data.path, data.key);
        //   } else {
        //     this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
        //   }
        // }
        else {
            this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
        }
        this.appState.encryptProfileJson();
    };
    // signin with goole authentication
    LoginComponent.prototype.signInWithGoogle = function () {
        var _this = this;
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_8__["GoogleLoginProvider"].PROVIDER_ID).then(function (res) {
            // console.log('res:' + JSON.stringify(res));
            var params = {
                collectionName: 'user',
                userName: res.email,
                password: '',
            };
            _this.inventFundsApiService.signIn(params).then(function (res) {
                // console.log('Google login :' + JSON.stringify(res))
                if (res.code == 200 && res.signUpProvider == 'Google') {
                    _this.setApplicationData(res);
                    // this.router.navigate(['/profile/dashboard'])
                    _this.router.navigate(['profile/editProfile']);
                }
                else if (res.code == 400 && res.provider == '') {
                    _this.swalMessage('You have registered using Custom Sign Up.Please use the login form to login').then(function (result) {
                        _this.loginForm.reset();
                    });
                }
            });
        });
    };
    // login with linkedin authentication from data base
    LoginComponent.prototype.linkedIn = function (data) {
        var _this = this;
        var params = {
            collectionName: 'user',
            userName: data,
            password: '',
        };
        this.inventFundsApiService.signIn(params).then(function (res) {
            // console.log('LinkedIn login :' + JSON.stringify(res))
            if (res.code == 200 && res.signUpProvider == 'LinkedIn') {
                _this.setApplicationData(res);
                // this.router.navigate(['/profile/dashboard'])
                _this.router.navigate(['profile/editProfile']);
            }
            else if (res.code == 400) {
                _this.swalMessage('You have registered using Custom Sign Up.Please use the login form to login').then(function (result) {
                    _this.loginForm.reset();
                });
            }
        });
    };
    // logout from the application
    LoginComponent.prototype.signOut = function () {
        this.authService.signOut();
    };
    // to authenticate from linkedin
    LoginComponent.prototype.signInWithLinkedIn = function () {
        if (_providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERTYPE"] == 'DEVELOPMENT') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERREDRECTURLDEV"];
        }
        else if (_providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERTYPE"] == 'TESTING') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERREDRECTURLTESTIN"];
        }
        else if (_providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERTYPE"] == 'UAT') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERREDRECTURLUAT"];
        }
        else if (_providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERTYPE"] == 'PRODUCTION') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_9__["SERVERREDRECTURLPROD"];
        }
        sessionStorage.setItem('page', 'login');
        window.location.href =
            'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ctgyj14vau28&redirect_uri=' +
                this.redirectUrl +
                '&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social';
    };
    /**
     *
     * @param content text to show in swal message and returns the tru or false
     */
    LoginComponent.prototype.swalMessage = function (content) {
        return sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
            title: content,
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        })
            .then(function (result) {
            return result;
        })
            .catch(function (err) {
            return err;
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/auth/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__["InventFundsApiService"],
            _services_app_state_service__WEBPACK_IMPORTED_MODULE_6__["AppStateService"],
            angularx_social_login__WEBPACK_IMPORTED_MODULE_8__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/sign-up-details/sign-up-details.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/auth/sign-up-details/sign-up-details.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row no-gutters\">\r\n\t<div class=\"col-lg-6 col-sm-12 col-md-6 col-xl-6 col-xs-12\">\r\n\t\t<div class=\"position-relative h-100 bg-night-sky\">\r\n\t\t\t<div class=\"slide-img-bg\"></div>\r\n\t\t\t<div class=\"slider-content text-light\">\r\n\t\t\t\t<span class=\"inventlogo pointer-ref\"  (click)=\"routerNavigation('landing')\"></span>\r\n\t\t\t\t<div class=\"d-flex justify-content align-items flex-column spc\">\r\n\t\t\t\t\t<h5 style=\"font-weight: 500 !important;\">\r\n\t\t\t\t\t\tMore than 60K Founders are\r\n\t\t\t\t\t</h5>\r\n\t\t\t\t\t<h5 style=\"font-weight: 500 !important;\">\r\n\t\t\t\t\t\tLooking on Invent Fund\r\n\t\t\t\t\t</h5>\r\n\t\t\t\t\t<h5 style=\"font-weight: 500 !important;\">every week</h5>\r\n\t\t\t\t\t<span\r\n\t\t\t\t\t\t>Explore more\r\n\t\t\t\t\t\t<i class=\"fa fa-arrow-right a\" aria-hidden=\"true\"></i>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\t\t\t<div class=\"row no-gutters\">\r\n\t\t\t\t<div\r\n\t\t\t\t\tclass=\"col-lg-12 col-sm-12 col-md-12 col-xl-12 col-xs-10 d-flex justify-content-center align-items-center flex-column\"\r\n\t\t\t\t>\r\n\t\t\t\t\t<div class=\"slide-img-bg\"></div>\r\n\t\t\t\t\t<span class=\"register img-fluid\"></span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<!-- <span class=\"dots \"></span> -->\r\n\t\t</div>\r\n\t</div>\r\n\t<div\r\n\t\tclass=\"d-flex flex-column bg-white justify-content-center align-items-center center-align col-lg-6 col-sm-12 col-md-6 col-xl-6 col-xs-12\"\r\n\t>\r\n\t\t<h2 class=\"centre-text-bold\">Details</h2>\r\n\t\t<br /><br /><br />\r\n\t\t<div>\r\n\t\t\t<form\r\n\t\t\t\t[formGroup]=\"signUpDetailsForm\"\r\n\t\t\t\t(validSubmit)=\"submit($event, signUpDetailsForm.value)\"\r\n\t\t\t>\r\n\t\t\t\t<div class=\"row no-gutters\">\r\n\t\t\t\t\t<div class=\"col-md-10 col-lg-10 col-sm-6 col-xl-10\">\r\n\t\t\t\t\t\t<fieldset class=\"form-group custom-error-invalid\">\r\n\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\t\t\tid=\"examplePassword\"\r\n\t\t\t\t\t\t\t\t\tname=\"password\"\r\n\t\t\t\t\t\t\t\t\ttype=\"password\"\r\n\t\t\t\t\t\t\t\t\tplaceholder=\"Password\"\r\n\t\t\t\t\t\t\t\t\tclass=\"form-control password-noImage\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"password\"\r\n\t\t\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"row no-gutters\">\r\n\t\t\t\t\t<div class=\"col-md-10 col-lg-10 col-sm-6 col-xl-10\">\r\n\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\t\t\tid=\"retypePassword\"\r\n\t\t\t\t\t\t\t\t\tname=\"retypePassword\"\r\n\t\t\t\t\t\t\t\t\ttype=\"password\"\r\n\t\t\t\t\t\t\t\t\tplaceholder=\"Re-Type Password\"\r\n\t\t\t\t\t\t\t\t\tclass=\"form-control password-noImage\"\r\n\t\t\t\t\t\t\t\t\t(change)=\"\r\n\t\t\t\t\t\t\t\t\t\tcheckPasswords($event.target.value)\r\n\t\t\t\t\t\t\t\t\t\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"retypePassword\"\r\n\t\t\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"row no-gutters\">\r\n\t\t\t\t\t<div class=\"col-md-10 col-lg-10 col-sm-6 col-xl-10\">\r\n\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t<input\r\n\t\t\t\t\t\t\t\t\ttype=\"number\"\r\n\t\t\t\t\t\t\t\t\tid=\"phoneNumber\"\r\n\t\t\t\t\t\t\t\t\tname=\"phoneNumber\"\r\n\t\t\t\t\t\t\t\t\tplaceholder=\"Phone Number\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"phoneNumber\"\r\n\t\t\t\t\t\t\t\t\tclass=\"form-control password-noImage\" required\r\n\t\t\t\t\t\t\t\t/>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<br />\r\n\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t<button\r\n\t\t\t\t\t\tstyle=\"font-size: 14px; font-family: Montserrat;\"\r\n\t\t\t\t\t\ttype=\"submit\"\r\n\t\t\t\t\t\tclass=\"btn btn-wide mb-2 mr-2 btn-success btn-size\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\tNext\r\n\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t><i\r\n\t\t\t\t\t\t\t\tclass=\"fa fa-arrow-right\"\r\n\t\t\t\t\t\t\t\taria-hidden=\"true\"\r\n\t\t\t\t\t\t\t></i>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\t\t\t</form>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/auth/sign-up-details/sign-up-details.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/auth/sign-up-details/sign-up-details.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span.inventlogo {\n  background: url('inventlogo.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 64px;\n  width: 85px;\n  margin-top: 42px;\n  margin-left: 30px; }\n\nspan.register {\n  background: url('register.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 230px;\n  width: 230px; }\n\nspan.dots {\n  background: url('dots.svg') no-repeat bottom center;\n  background-size: contain;\n  display: inline-block;\n  height: 180px;\n  width: 180px;\n  margin-top: -130px;\n  margin-left: 560px;\n  overflow: hidden;\n  position: relative;\n  z-index: 1; }\n\ni.fa.fa-arrow-right.a {\n  cursor: default; }\n\n.spc {\n  padding-left: 80px;\n  margin-top: 65px;\n  cursor: default; }\n\n::-webkit-input-placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n::-moz-placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n::-ms-input-placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n::placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n:-ms-input-placeholder {\n  color: blue; }\n\n::-ms-input-placeholder {\n  color: blue; }\n\ninput.form-control {\n  border-color: #7d7dec;\n  width: 300px; }\n\n.btn-size {\n  width: 140px; }\n\n.fa.fa-arrow-right {\n  width: 40px;\n  font-size: 15px; }\n\n@media only screen and (max-width: 1024px) {\n  span.dots {\n    background: url('dots.svg') no-repeat bottom center;\n    background-size: contain;\n    display: inline-block;\n    height: 180px;\n    width: 180px;\n    margin-top: -47px;\n    margin-left: 430px;\n    overflow: hidden;\n    position: relative;\n    z-index: 1; }\n  .spc {\n    margin-top: 92px; } }\n\n@media only screen and (max-width: 768px) {\n  span.dots {\n    background: url('dots.svg') no-repeat bottom center;\n    background-size: contain;\n    display: inline-block;\n    height: 180px;\n    width: 180px;\n    margin-top: -147px;\n    margin-top: -147px;\n    margin-left: 300px;\n    overflow: hidden;\n    position: relative;\n    z-index: 1; }\n  .spc {\n    padding-left: 65px;\n    margin-top: 55px; } }\n\n@media only screen and (max-width: 425px) {\n  span.dots {\n    background: url('dots.svg') no-repeat bottom center;\n    display: none;\n    height: 0px; }\n  span.register {\n    background: url('register.svg') no-repeat top left;\n    background-size: contain;\n    display: inline-block;\n    height: 180px;\n    width: 180px; }\n  .spc {\n    padding-left: 65px;\n    margin-top: 55px; } }\n\n@media only screen and (max-width: 320px) {\n  span.register {\n    background: url('register.svg') no-repeat top left;\n    background-size: contain;\n    display: inline-block;\n    height: 180px;\n    width: 130px; } }\n\n::ng-deep .password-noImage.is-valid {\n  border-color: #ced4da !important;\n  padding-right: 2.25rem !important;\n  background-repeat: no-repeat !important;\n  background-position: center right calc(2.25rem / 4) !important;\n  background-size: calc(2.25rem / 2) calc(2.25rem / 2) !important;\n  background-image: url(); }\n\n::ng-deep .password-noImage.is-invalid {\n  border-color: #d92550 !important;\n  padding-right: 2.25rem !important;\n  background-repeat: no-repeat !important;\n  background-position: center right calc(2.25rem / 4) !important;\n  background-size: calc(2.25rem / 2) calc(2.25rem / 2) !important;\n  background-image: url() !important; }\n\n.error-class {\n  width: 100%;\n  margin-top: 0.25rem;\n  font-size: 80%;\n  color: #d92550; }\n\n::ng-deep .custom-error-invalid .invalid-feedback {\n  white-space: pre !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9zaWduLXVwLWRldGFpbHMvRDpcXFdvcmtcXFByb2plY3RzXFxJTlZFTlRGVU5EXFxCUkFOQ0hFU1xcaW52ZW50ZnVuZC10ZXN0L3NyY1xcYXBwXFxhdXRoXFxzaWduLXVwLWRldGFpbHNcXHNpZ24tdXAtZGV0YWlscy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLG9EQUEwRTtFQUMxRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdsQjtFQUNDLGtEQUF3RTtFQUN4RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLGFBQWE7RUFDYixZQUFZLEVBQUE7O0FBR2I7RUFDQyxtREFBeUU7RUFDekUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixhQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixVQUFVLEVBQUE7O0FBR1g7RUFDQyxlQUFlLEVBQUE7O0FBR2hCO0VBQ0Msa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBR2hCO0VBQ0MsV0FBVztFQUNYLFlBQVksRUFBQTs7QUFGYjtFQUNDLFdBQVc7RUFDWCxZQUFZLEVBQUE7O0FBRmI7RUFDQyxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUZiO0VBQ0MsV0FBVztFQUNYLFlBQVksRUFBQTs7QUFHYjtFQUNDLFdBQVcsRUFBQTs7QUFHWjtFQUNDLFdBQVcsRUFBQTs7QUFHWjtFQUNDLHFCQUFnQztFQUNoQyxZQUFZLEVBQUE7O0FBR2I7RUFDQyxZQUFZLEVBQUE7O0FBR2I7RUFDQyxXQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUdoQjtFQUNDO0lBQ0MsbURBQ087SUFDUCx3QkFBd0I7SUFDeEIscUJBQXFCO0lBQ3JCLGFBQWE7SUFDYixZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLFVBQVUsRUFBQTtFQUVYO0lBRUMsZ0JBQWdCLEVBQUEsRUFDaEI7O0FBR0Y7RUFDQztJQUNDLG1EQUNPO0lBQ1Asd0JBQXdCO0lBQ3hCLHFCQUFxQjtJQUNyQixhQUFhO0lBQ2IsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsVUFBVSxFQUFBO0VBRVg7SUFDQyxrQkFBa0I7SUFDbEIsZ0JBQWdCLEVBQUEsRUFDaEI7O0FBR0Y7RUFDQztJQUNDLG1EQUNPO0lBQ1AsYUFBYTtJQUNiLFdBQVcsRUFBQTtFQUVaO0lBQ0Msa0RBQXdFO0lBQ3hFLHdCQUF3QjtJQUN4QixxQkFBcUI7SUFDckIsYUFBYTtJQUNiLFlBQVksRUFBQTtFQUViO0lBQ0Msa0JBQWtCO0lBQ2xCLGdCQUFnQixFQUFBLEVBQ2hCOztBQUdGO0VBQ0M7SUFDQyxrREFBd0U7SUFDeEUsd0JBQXdCO0lBQ3hCLHFCQUFxQjtJQUNyQixhQUFhO0lBQ2IsWUFBWSxFQUFBLEVBQ1o7O0FBR0Y7RUFDQyxnQ0FBZ0M7RUFDaEMsaUNBQWlDO0VBQ2pDLHVDQUF1QztFQUN2Qyw4REFBOEQ7RUFDOUQsK0RBQStEO0VBQy9ELHVCQUF1QixFQUFBOztBQUd4QjtFQUNDLGdDQUFnQztFQUNoQyxpQ0FBaUM7RUFDakMsdUNBQXVDO0VBQ3ZDLDhEQUE4RDtFQUM5RCwrREFBK0Q7RUFDL0Qsa0NBQWtDLEVBQUE7O0FBR25DO0VBQ0MsV0FBVztFQUNSLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsY0FBYyxFQUFBOztBQUdsQjtFQUVFLDJCQUEyQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXV0aC9zaWduLXVwLWRldGFpbHMvc2lnbi11cC1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsic3Bhbi5pbnZlbnRsb2dvIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9pbnZlbnRsb2dvLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogNjRweDtcclxuXHR3aWR0aDogODVweDtcclxuXHRtYXJnaW4tdG9wOiA0MnB4O1xyXG5cdG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG59XHJcblxyXG5zcGFuLnJlZ2lzdGVyIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9yZWdpc3Rlci5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDIzMHB4O1xyXG5cdHdpZHRoOiAyMzBweDtcclxufVxyXG5cclxuc3Bhbi5kb3RzIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9kb3RzLnN2ZycpIG5vLXJlcGVhdCBib3R0b20gY2VudGVyO1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAxODBweDtcclxuXHR3aWR0aDogMTgwcHg7XHJcblx0bWFyZ2luLXRvcDogLTEzMHB4O1xyXG5cdG1hcmdpbi1sZWZ0OiA1NjBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHR6LWluZGV4OiAxO1xyXG59XHJcblxyXG5pLmZhLmZhLWFycm93LXJpZ2h0LmEge1xyXG5cdGN1cnNvcjogZGVmYXVsdDtcclxufVxyXG5cclxuLnNwYyB7XHJcblx0cGFkZGluZy1sZWZ0OiA4MHB4O1xyXG5cdG1hcmdpbi10b3A6IDY1cHg7XHJcblx0Y3Vyc29yOiBkZWZhdWx0O1xyXG59XHJcblxyXG46OnBsYWNlaG9sZGVyIHtcclxuXHRjb2xvcjogYmx1ZTtcclxuXHRvcGFjaXR5OiAwLjU7XHJcbn1cclxuXHJcbjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xyXG5cdGNvbG9yOiBibHVlO1xyXG59XHJcblxyXG46Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XHJcblx0Y29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbmlucHV0LmZvcm0tY29udHJvbCB7XHJcblx0Ym9yZGVyLWNvbG9yOiByZ2IoMTI1LCAxMjUsIDIzNik7XHJcblx0d2lkdGg6IDMwMHB4O1xyXG59XHJcblxyXG4uYnRuLXNpemUge1xyXG5cdHdpZHRoOiAxNDBweDtcclxufVxyXG5cclxuLmZhLmZhLWFycm93LXJpZ2h0IHtcclxuXHR3aWR0aDogNDBweDtcclxuXHRmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XHJcblx0c3Bhbi5kb3RzIHtcclxuXHRcdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2RvdHMuc3ZnJykgbm8tcmVwZWF0IGJvdHRvbVxyXG5cdFx0XHRjZW50ZXI7XHJcblx0XHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHRoZWlnaHQ6IDE4MHB4O1xyXG5cdFx0d2lkdGg6IDE4MHB4O1xyXG5cdFx0bWFyZ2luLXRvcDogLTQ3cHg7XHJcblx0XHRtYXJnaW4tbGVmdDogNDMwcHg7XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0ei1pbmRleDogMTtcclxuXHR9XHJcblx0LnNwYyB7XHJcblx0XHQvLyBwYWRkaW5nLWxlZnQ6IDQ1cHg7XHJcblx0XHRtYXJnaW4tdG9wOiA5MnB4O1xyXG5cdH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG5cdHNwYW4uZG90cyB7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9kb3RzLnN2ZycpIG5vLXJlcGVhdCBib3R0b21cclxuXHRcdFx0Y2VudGVyO1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0aGVpZ2h0OiAxODBweDtcclxuXHRcdHdpZHRoOiAxODBweDtcclxuXHRcdG1hcmdpbi10b3A6IC0xNDdweDtcclxuXHRcdG1hcmdpbi10b3A6IC0xNDdweDtcclxuXHRcdG1hcmdpbi1sZWZ0OiAzMDBweDtcclxuXHRcdG92ZXJmbG93OiBoaWRkZW47XHJcblx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHR6LWluZGV4OiAxO1xyXG5cdH1cclxuXHQuc3BjIHtcclxuXHRcdHBhZGRpbmctbGVmdDogNjVweDtcclxuXHRcdG1hcmdpbi10b3A6IDU1cHg7XHJcblx0fVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyNXB4KSB7XHJcblx0c3Bhbi5kb3RzIHtcclxuXHRcdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2RvdHMuc3ZnJykgbm8tcmVwZWF0IGJvdHRvbVxyXG5cdFx0XHRjZW50ZXI7XHJcblx0XHRkaXNwbGF5OiBub25lO1xyXG5cdFx0aGVpZ2h0OiAwcHg7XHJcblx0fVxyXG5cdHNwYW4ucmVnaXN0ZXIge1xyXG5cdFx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvcmVnaXN0ZXIuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0aGVpZ2h0OiAxODBweDtcclxuXHRcdHdpZHRoOiAxODBweDtcclxuXHR9XHJcblx0LnNwYyB7XHJcblx0XHRwYWRkaW5nLWxlZnQ6IDY1cHg7XHJcblx0XHRtYXJnaW4tdG9wOiA1NXB4O1xyXG5cdH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xyXG5cdHNwYW4ucmVnaXN0ZXIge1xyXG5cdFx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvcmVnaXN0ZXIuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0aGVpZ2h0OiAxODBweDtcclxuXHRcdHdpZHRoOiAxMzBweDtcclxuXHR9XHJcbn1cclxuXHJcbjo6bmctZGVlcCAucGFzc3dvcmQtbm9JbWFnZS5pcy12YWxpZCB7XHJcblx0Ym9yZGVyLWNvbG9yOiAjY2VkNGRhICFpbXBvcnRhbnQ7XHJcblx0cGFkZGluZy1yaWdodDogMi4yNXJlbSAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgcmlnaHQgY2FsYygyLjI1cmVtIC8gNCkgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNhbGMoMi4yNXJlbSAvIDIpIGNhbGMoMi4yNXJlbSAvIDIpICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1pbWFnZTogdXJsKCk7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAucGFzc3dvcmQtbm9JbWFnZS5pcy1pbnZhbGlkIHtcclxuXHRib3JkZXItY29sb3I6ICNkOTI1NTAgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nLXJpZ2h0OiAyLjI1cmVtICFpbXBvcnRhbnQ7XHJcblx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdCAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciByaWdodCBjYWxjKDIuMjVyZW0gLyA0KSAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY2FsYygyLjI1cmVtIC8gMikgY2FsYygyLjI1cmVtIC8gMikgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwoKSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uZXJyb3ItY2xhc3Mge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMC4yNXJlbTtcclxuICAgIGZvbnQtc2l6ZTogODAlO1xyXG4gICAgY29sb3I6ICNkOTI1NTA7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAuY3VzdG9tLWVycm9yLWludmFsaWQge1xyXG5cdC5pbnZhbGlkLWZlZWRiYWNrIHtcclxuXHRcdHdoaXRlLXNwYWNlOiBwcmUgIWltcG9ydGFudDtcclxuXHR9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/auth/sign-up-details/sign-up-details.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/auth/sign-up-details/sign-up-details.component.ts ***!
  \*******************************************************************/
/*! exports provided: SignUpDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpDetailsComponent", function() { return SignUpDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _utilities_customValidators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../utilities/customValidators */ "./src/app/utilities/customValidators.ts");







var SignUpDetailsComponent = /** @class */ (function () {
    /**
     *
     * @param fb reactive form builder
     * @param router navigate to different routes
     * @param activatedRoute get activated route for query params
     * @param inventFundsApiService api service to call endpoints
     */
    function SignUpDetailsComponent(fb, router, activatedRoute, inventFundsApiService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.inventFundsApiService = inventFundsApiService;
        //Query params of the route:
        this.routerData = activatedRoute.snapshot.params.data;
        if (this.routerData) {
            this.retrieveData(this.routerData);
        }
        // to compare the password and retype password
        var newPasswordFormControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern("^(?=.*?[A-Z])(?=.*?[A-Za-z])(?=.*?[0-9]).{7,}$")]));
        var confirmPasswordFromControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
            ng2_validation__WEBPACK_IMPORTED_MODULE_5__["CustomValidators"].equalTo(newPasswordFormControl)]);
        this.signUpDetailsForm = fb.group({
            'password': newPasswordFormControl,
            'retypePassword': confirmPasswordFromControl,
            'phoneNumber': ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _utilities_customValidators__WEBPACK_IMPORTED_MODULE_6__["phoneNumberValidator"]])],
        });
    }
    SignUpDetailsComponent.prototype.ngOnInit = function () { };
    /**
     *
     * @param personId person id to retrive data from database
     */
    // retrive person data from use collection
    SignUpDetailsComponent.prototype.retrieveData = function (personId) {
        var _this = this;
        var params = {
            "collectionName": "user",
            "queryStr": { "personId": { "$eq": personId } },
        };
        this.inventFundsApiService.retrieveMongoDBAll(params)
            .then(function (res) {
            if (res.status == 200) {
                console.log("user response : " + JSON.stringify(res));
                _this.emailId = res.response[0].userName;
                // this.userId = res.response[0]._id;
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    // validate and submit form
    SignUpDetailsComponent.prototype.submit = function ($ev, value) {
        // console.log('form ' + JSON.stringify(value))
        for (var c in this.signUpDetailsForm.controls) {
            this.signUpDetailsForm.controls[c].markAsTouched();
        }
        if (this.signUpDetailsForm.valid) {
            // console.log('Valid!');
            this.updatePasswordDetails(value);
        }
        else {
            // console.log('Form invalid!');
        }
    };
    // reset or update password of user
    SignUpDetailsComponent.prototype.updatePasswordDetails = function (value) {
        var _this = this;
        var params = {
            "userName": this.emailId,
            "oldPassword": "Forgot Password",
            "newPassword": value.password,
            "confirmPassword": value.retypePassword,
            "phoneNumber": value.phoneNumber
        };
        this.inventFundsApiService.changePassword(params)
            .then(function (res) {
            // console.log('update response->' + JSON.stringify(res))
            if (res.code == 200) {
                sessionStorage.setItem('personId', res.personId);
                _this.router.navigate(['auth/sign-up-roles']);
            }
            else {
                //error handling for failure to update password
            }
        });
    };
    // to compare password 
    SignUpDetailsComponent.prototype.checkPasswords = function (event) {
        console.log("hello one\n    two\n    three");
        // console.log('pass:' + this.signUpDetailsForm.controls['password'].value + ' re type:' + event)
        if ((this.signUpDetailsForm.controls['password'].value) != (event)) {
            // alert('mismatch');
            this.signUpDetailsForm.controls['retypePassword'].setErrors({ passwordMismatch: true });
        }
    };
    SignUpDetailsComponent.prototype.routerNavigation = function (data) {
        this.router.navigate(['landing/landing-page']);
    };
    SignUpDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sign-up-details',
            template: __webpack_require__(/*! ./sign-up-details.component.html */ "./src/app/auth/sign-up-details/sign-up-details.component.html"),
            styles: [__webpack_require__(/*! ./sign-up-details.component.scss */ "./src/app/auth/sign-up-details/sign-up-details.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__["InventFundsApiService"]])
    ], SignUpDetailsComponent);
    return SignUpDetailsComponent;
}());



/***/ }),

/***/ "./src/app/auth/sign-up-roles/sign-up-roles.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/auth/sign-up-roles/sign-up-roles.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row h-60 g no-gutters\">\r\n\t<div class=\"col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n\t\t<div class=\"slider-light\"></div>\r\n\t\t<div class=\"slide-img-bg\"></div>\r\n\r\n\t\t<div class=\"row d-flex flex-column no-gutters\">\r\n\t\t\t<span class=\"logo\"></span>\r\n\t\t\t<h3 class=\"txt\">Who you are?</h3>\r\n\t\t\t<h6 class=\"txt2\">Find What You Need And Engage With Confidence</h6>\r\n\t\t\t<br />\r\n\t\t\t<h6 class=\"txt3\">I am a...</h6>\r\n\t\t</div>\r\n\r\n\t\t<div class=\"row pt-2 justify-content-center no-gutters\">\r\n\t\t\t<div class=\"card-deck\">\r\n\t\t\t\t<div class=\"card col-centered col-auto mb-5 cd1\">\r\n\t\t\t\t\t<a\r\n\t\t\t\t\t\tclass=\"card-block stretched-link text-decoration-none\"\r\n\t\t\t\t\t\tid=\"sample\"\r\n\t\t\t\t\t\t(click)=\"Active($event, 0)\"\r\n\t\t\t\t\t\thref=\"javascript:void(0)\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t\t\t<img class=\"founder responsive pt-5\" />\r\n\t\t\t\t\t\t\t<p class=\"card-text pt-3 one center-align\">\r\n\t\t\t\t\t\t\t\tFounder\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t\t<ng-template #popContent\r\n\t\t\t\t\t\t>some content<br /><button class=\"btn1\">\r\n\t\t\t\t\t\t\tI got it\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<ng-template #popTitle3\r\n\t\t\t\t\t\t><img class=\"founder2 pt-5\" /><br />\r\n\t\t\t\t\t\t<p style=\"margin-top: -13px;\"><b>Who is Founder?</b></p>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<button\r\n\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\tclass=\"btn btt oneb m\"\r\n\t\t\t\t\t\t[ngbPopover]=\"popContent\"\r\n\t\t\t\t\t\t[popoverTitle]=\"popTitle3\"\r\n\t\t\t\t\t\tplacement=\"right\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\tLearn More\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div class=\"card col-centered col-auto mb-5 cd2\">\r\n\t\t\t\t\t<a\r\n\t\t\t\t\t\tclass=\"card-block stretched-link text-decoration-none\"\r\n\t\t\t\t\t\t(click)=\"Active($event, 1)\"\r\n\t\t\t\t\t\thref=\"javascript:void(0)\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t\t\t<img class=\"fixer responsive pt-5\" />\r\n\t\t\t\t\t\t\t<p class=\"card-text pt-2 two\">Fixer</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t\t<ng-template #popContent\r\n\t\t\t\t\t\t>Some content<button class=\"btn1\">I got it</button>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<ng-template #popTitle1\r\n\t\t\t\t\t\t><img class=\"fixer2 pt-5\" /><br />\r\n\t\t\t\t\t\t<p style=\"margin-top: -17px;\"><b>Who is Fixer?</b></p>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<button\r\n\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\tclass=\"btn btt twob m\"\r\n\t\t\t\t\t\t[ngbPopover]=\"popContent\"\r\n\t\t\t\t\t\t[popoverTitle]=\"popTitle1\"\r\n\t\t\t\t\t\tplacement=\"right\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\tLearn More\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div class=\"card col-centered col-auto mb-5 cd3\">\r\n\t\t\t\t\t<a\r\n\t\t\t\t\t\tclass=\"card-block stretched-link text-decoration-none\"\r\n\t\t\t\t\t\t(click)=\"Active($event, 2)\"\r\n\t\t\t\t\t\thref=\"javascript:void(0)\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\t<div class=\"card-body\">\r\n\t\t\t\t\t\t\t<img class=\"funder responsive pt-5\" />\r\n\t\t\t\t\t\t\t<p class=\"card-text pt-3 three\">Funder</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</a>\r\n\t\t\t\t\t<ng-template #popContent\r\n\t\t\t\t\t\t>some content<br /><button class=\"btn1\">\r\n\t\t\t\t\t\t\tI got it\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<ng-template #popTitle2\r\n\t\t\t\t\t\t><img class=\"funder2 pt-5\" /><br />\r\n\t\t\t\t\t\t<p style=\"margin-top: -17px;\"><b>Who is Funder?</b></p>\r\n\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t<button\r\n\t\t\t\t\t\ttype=\"button\"\r\n\t\t\t\t\t\tclass=\"btn btt threeb m\"\r\n\t\t\t\t\t\t[ngbPopover]=\"popContent\"\r\n\t\t\t\t\t\t[popoverTitle]=\"popTitle2\"\r\n\t\t\t\t\t\tplacement=\"right\"\r\n\t\t\t\t\t>\r\n\t\t\t\t\t\tLearn More\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\r\n\t\t\t<br />\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n<div class=\"row h-25 d-flex no-gutters g float-right pd\">\r\n\t<a href=\"javascript:void(0)\" (click)=\"genericRole()\">\r\n\t\t<p class=\"btn3 pr-3\" id=\"sample\" (click)=\"Deactive($event)\">\r\n\t\t\t<u><i>Skip for Now</i></u>\r\n\t\t</p>\r\n\t</a>\r\n\r\n\t<button\r\n\t\t[disabled]=\"!enable\"\r\n\t\ttype=\"button\"\r\n\t\t(click)=\"specificRole()\"\r\n\t\tclass=\"btn btn-wide mb-2 mr-5 btn-light text-justify-right float-right bt\"\r\n\t\tstyle=\"font-size: 14px;\"\r\n\t\t[ngClass]=\"changeBtn\"\r\n\t>\r\n\t\tNext<i class=\"fas fa-arrow-right pl-2\"></i>\r\n\t</button>\r\n</div>\r\n<div class=\"row no-gutters mg\" style=\"bottom: 0px !important;\">\r\n\t<!-- <span class=\"dot cs \" ></span> -->\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/auth/sign-up-roles/sign-up-roles.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/auth/sign-up-roles/sign-up-roles.component.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span.logo {\n  background: url('logoblue.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 90px;\n  width: 90px;\n  margin-top: 40px;\n  margin-left: 40px; }\n\nspan.dot {\n  background: url('group116.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block; }\n\n.txt {\n  font-family: Montserrat;\n  font-size: 30px;\n  font-weight: 800;\n  text-align: center;\n  color: black; }\n\n.txt2 {\n  font-family: Montserrat;\n  font-size: 20px;\n  text-align: center;\n  color: gray; }\n\n.txt3 {\n  font-family: Montserrat;\n  font-size: 20px;\n  text-align: center;\n  color: gray; }\n\n.two {\n  font-size: 17px;\n  color: rgba(24, 21, 21, 0.822);\n  font-weight: 500; }\n\n.one {\n  font-size: 17px;\n  color: rgba(24, 21, 21, 0.822);\n  font-weight: 500; }\n\n.three {\n  font-size: 17px;\n  color: rgba(24, 21, 21, 0.822);\n  font-weight: 500; }\n\nimg.founder2 {\n  background: url('founder.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 23px;\n  width: 23px;\n  margin-top: -7px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.founder {\n  background: url('founder.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 45px;\n  margin-top: 10px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.founder1 {\n  background: url('founder_1.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 45px;\n  margin-top: 10px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.funder2 {\n  background: url('funder.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 25px;\n  width: 25px;\n  margin-top: -1px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.funder {\n  background: url('funder.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 45px;\n  margin-top: 10px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.funder1 {\n  background: url('funder_1.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 45px;\n  margin-top: 10px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.fixer2 {\n  background: url('fixer.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 23px;\n  width: 23px;\n  margin-top: -1px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.fixer {\n  background: url('fixer.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 45px;\n  margin-top: 17px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\nimg.fixer1 {\n  background: url('fixer_1.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 45px;\n  margin-top: 17px;\n  margin-bottom: auto;\n  outline: #f3f3e8; }\n\n.row-centered {\n  text-align: center; }\n\n.col-centered {\n  height: 150px;\n  width: 150px;\n  display: inline-block;\n  float: none;\n  text-align: center;\n  margin-left: 10px;\n  margin-right: 10px;\n  margin-bottom: 10px;\n  outline-width: 0.5px;\n  border-radius: 5px;\n  background: #eceaea;\n  border: 0.5px solid #aaa9a9; }\n\n.col-centered1 {\n  height: 150px;\n  width: 150px;\n  display: inline-block;\n  float: none;\n  text-align: center;\n  margin-left: 10px;\n  margin-right: 10px;\n  margin-bottom: 10px;\n  outline-width: 0.5px;\n  border-radius: 5px;\n  background: #007bff;\n  border: 0.5px solid #aaa9a9;\n  color: black; }\n\n.btntrans {\n  font-size: 8px;\n  color: white; }\n\n.btt {\n  font-size: 8px;\n  color: grey; }\n\n.btn1 {\n  font-size: x-small;\n  border-style: solid;\n  border-width: 0.2px;\n  border-color: rgba(73, 71, 71, 0.63); }\n\n.btn3 {\n  font-family: italic;\n  font-size: 12px;\n  color: grey;\n  margin-top: 10px; }\n\n.m {\n  margin-top: -50px; }\n\n.bt {\n  color: #3f3333;\n  background-color: grey;\n  font-size: 10px; }\n\n.add {\n  color: white;\n  background-color: #2abc6e;\n  font-size: 10px; }\n\n.transition {\n  color: white; }\n\n.cs {\n  margin-left: -100px;\n  margin-top: 80px;\n  height: 90px;\n  width: 300px; }\n\n.g {\n  overflow-y: scroll; }\n\n.g::-webkit-scrollbar {\n  display: none; }\n\n.mg {\n  margin-top: 0px; }\n\n.pd {\n  margin-right: 400px; }\n\n@media only screen and (max-width: 1024px) {\n  .cs {\n    margin-left: -80px;\n    margin-top: -8px;\n    height: 70px;\n    width: 200px; }\n  .mg {\n    margin-top: -30px; }\n  .pd {\n    margin-right: 230px;\n    margin-top: -10px; } }\n\n@media only screen and (max-width: 768px) {\n  .cs {\n    margin-left: -50px;\n    margin-top: -20px;\n    height: 50px;\n    width: 200px; }\n  .mg {\n    margin-top: -6px; }\n  .pd {\n    margin-right: 130px;\n    margin-top: -10px; } }\n\n@media only screen and (max-width: 425px) {\n  div.pd {\n    margin-right: 75px; }\n  .cs {\n    margin-left: 0px;\n    margin-top: 0px;\n    height: 0px;\n    width: 0px; } }\n\n@media only screen and (max-width: 320px) {\n  div.pd {\n    margin-right: 40px; }\n  .cs {\n    margin-left: 0px;\n    margin-top: 0px;\n    height: 0px;\n    width: 0px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9zaWduLXVwLXJvbGVzL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxcYXV0aFxcc2lnbi11cC1yb2xlc1xcc2lnbi11cC1yb2xlcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGtEQUF3RTtFQUN4RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUdsQjtFQUNDLGtEQUF3RTtFQUN4RSx3QkFBd0I7RUFDeEIscUJBQXFCLEVBQUE7O0FBR3RCO0VBQ0MsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFlBQVksRUFBQTs7QUFHYjtFQUNDLHVCQUF1QjtFQUN2QixlQUFlO0VBRWYsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHWjtFQUNDLHVCQUF1QjtFQUN2QixlQUFlO0VBRWYsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHWjtFQUNDLGVBQWU7RUFDZiw4QkFBOEI7RUFDOUIsZ0JBQWdCLEVBQUE7O0FBR2pCO0VBQ0MsZUFBZTtFQUNmLDhCQUE4QjtFQUM5QixnQkFBZ0IsRUFBQTs7QUFHakI7RUFDQyxlQUFlO0VBQ2YsOEJBQThCO0VBQzlCLGdCQUFnQixFQUFBOztBQUdqQjtFQUNDLGlEQUF1RTtFQUN2RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUVuQixnQkFBMkIsRUFBQTs7QUFHNUI7RUFDQyxpREFBdUU7RUFDdkUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFFbkIsZ0JBQTJCLEVBQUE7O0FBRzVCO0VBQ0MsbURBQXlFO0VBQ3pFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBRW5CLGdCQUEyQixFQUFBOztBQUc1QjtFQUNDLGdEQUFzRTtFQUN0RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUVuQixnQkFBMkIsRUFBQTs7QUFHNUI7RUFDQyxnREFBc0U7RUFDdEUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFFbkIsZ0JBQTJCLEVBQUE7O0FBRzVCO0VBQ0Msa0RBQXdFO0VBQ3hFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBRW5CLGdCQUEyQixFQUFBOztBQUc1QjtFQUNDLCtDQUFxRTtFQUNyRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixnQkFBMkIsRUFBQTs7QUFHNUI7RUFDQywrQ0FBcUU7RUFDckUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZ0JBQTJCLEVBQUE7O0FBRzVCO0VBQ0MsaURBQXVFO0VBQ3ZFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGdCQUEyQixFQUFBOztBQUc1QjtFQUNDLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLGFBQWE7RUFDYixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixtQkFBOEI7RUFDOUIsMkJBQXNDLEVBQUE7O0FBR3ZDO0VBQ0MsYUFBYTtFQUNiLFlBQVk7RUFDWixxQkFBcUI7RUFDckIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQiwyQkFBc0M7RUFDdEMsWUFBWSxFQUFBOztBQUdiO0VBQ0MsY0FBYztFQUNkLFlBQVksRUFBQTs7QUFHYjtFQUNDLGNBQWM7RUFDZCxXQUFXLEVBQUE7O0FBR1o7RUFDQyxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixvQ0FBb0MsRUFBQTs7QUFHckM7RUFDQyxtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLFdBQVc7RUFDWCxnQkFBZ0IsRUFBQTs7QUFHakI7RUFDQyxpQkFBaUIsRUFBQTs7QUFHbEI7RUFDQyxjQUFzQjtFQUN0QixzQkFBc0I7RUFDdEIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLFlBQVk7RUFFWix5QkFBeUI7RUFDekIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLFlBQVksRUFBQTs7QUFHYjtFQUNDLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLFlBQVksRUFBQTs7QUFHYjtFQUNDLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLGFBQWEsRUFBQTs7QUFHZDtFQUNDLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxtQkFBbUIsRUFBQTs7QUFHcEI7RUFDQztJQUNDLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsWUFBWTtJQUNaLFlBQVksRUFBQTtFQUdiO0lBQ0MsaUJBQWlCLEVBQUE7RUFHbEI7SUFDQyxtQkFBbUI7SUFDbkIsaUJBQWlCLEVBQUEsRUFDakI7O0FBR0Y7RUFDQztJQUNDLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsWUFBWTtJQUNaLFlBQVksRUFBQTtFQUdiO0lBQ0MsZ0JBQWdCLEVBQUE7RUFHakI7SUFDQyxtQkFBbUI7SUFDbkIsaUJBQWlCLEVBQUEsRUFDakI7O0FBR0Y7RUFDQztJQUNDLGtCQUFrQixFQUFBO0VBR25CO0lBQ0MsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsVUFBVSxFQUFBLEVBQ1Y7O0FBR0Y7RUFDQztJQUNDLGtCQUFrQixFQUFBO0VBR25CO0lBQ0MsZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixXQUFXO0lBQ1gsVUFBVSxFQUFBLEVBQ1YiLCJmaWxlIjoic3JjL2FwcC9hdXRoL3NpZ24tdXAtcm9sZXMvc2lnbi11cC1yb2xlcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNwYW4ubG9nbyB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvbG9nb2JsdWUuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA5MHB4O1xyXG5cdHdpZHRoOiA5MHB4O1xyXG5cdG1hcmdpbi10b3A6IDQwcHg7XHJcblx0bWFyZ2luLWxlZnQ6IDQwcHg7XHJcbn1cclxuXHJcbnNwYW4uZG90IHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9ncm91cDExNi5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG5cclxuLnR4dCB7XHJcblx0Zm9udC1mYW1pbHk6IE1vbnRzZXJyYXQ7XHJcblx0Zm9udC1zaXplOiAzMHB4O1xyXG5cdGZvbnQtd2VpZ2h0OiA4MDA7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLnR4dDIge1xyXG5cdGZvbnQtZmFtaWx5OiBNb250c2VycmF0O1xyXG5cdGZvbnQtc2l6ZTogMjBweDtcclxuXHQvL2ZvbnQtd2VpZ2h0OjUwIDtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0Y29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbi50eHQzIHtcclxuXHRmb250LWZhbWlseTogTW9udHNlcnJhdDtcclxuXHRmb250LXNpemU6IDIwcHg7XHJcblx0Ly9mb250LXdlaWdodDoxMDA7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG4udHdvIHtcclxuXHRmb250LXNpemU6IDE3cHg7XHJcblx0Y29sb3I6IHJnYmEoMjQsIDIxLCAyMSwgMC44MjIpO1xyXG5cdGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbi5vbmUge1xyXG5cdGZvbnQtc2l6ZTogMTdweDtcclxuXHRjb2xvcjogcmdiYSgyNCwgMjEsIDIxLCAwLjgyMik7XHJcblx0Zm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5cclxuLnRocmVlIHtcclxuXHRmb250LXNpemU6IDE3cHg7XHJcblx0Y29sb3I6IHJnYmEoMjQsIDIxLCAyMSwgMC44MjIpO1xyXG5cdGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbmltZy5mb3VuZGVyMiB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZm91bmRlci5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDIzcHg7XHJcblx0d2lkdGg6IDIzcHg7XHJcblx0bWFyZ2luLXRvcDogLTdweDtcclxuXHRtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG5cdC8vbWFyZ2luLWxlZnQ6IDEwcHg7XHJcblx0b3V0bGluZTogcmdiKDI0MywgMjQzLCAyMzIpO1xyXG59XHJcblxyXG5pbWcuZm91bmRlciB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZm91bmRlci5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDQ1cHg7XHJcblx0d2lkdGg6IDQ1cHg7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxuXHRtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG5cdC8vbWFyZ2luLWxlZnQ6IDEwcHg7XHJcblx0b3V0bGluZTogcmdiKDI0MywgMjQzLCAyMzIpO1xyXG59XHJcblxyXG5pbWcuZm91bmRlcjEge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2ZvdW5kZXJfMS5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDQ1cHg7XHJcblx0d2lkdGg6IDQ1cHg7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxuXHRtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG5cdC8vbWFyZ2luLWxlZnQ6IDEwcHg7XHJcblx0b3V0bGluZTogcmdiKDI0MywgMjQzLCAyMzIpO1xyXG59XHJcblxyXG5pbWcuZnVuZGVyMiB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZnVuZGVyLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMjVweDtcclxuXHR3aWR0aDogMjVweDtcclxuXHRtYXJnaW4tdG9wOiAtMXB4O1xyXG5cdG1hcmdpbi1ib3R0b206IGF1dG87XHJcblx0Ly9tYXJnaW4tbGVmdDogMTBweDtcclxuXHRvdXRsaW5lOiByZ2IoMjQzLCAyNDMsIDIzMik7XHJcbn1cclxuXHJcbmltZy5mdW5kZXIge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2Z1bmRlci5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDQ1cHg7XHJcblx0d2lkdGg6IDQ1cHg7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxuXHRtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG5cdC8vbWFyZ2luLWxlZnQ6IDEwcHg7XHJcblx0b3V0bGluZTogcmdiKDI0MywgMjQzLCAyMzIpO1xyXG59XHJcblxyXG5pbWcuZnVuZGVyMSB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZnVuZGVyXzEuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA0NXB4O1xyXG5cdHdpZHRoOiA0NXB4O1xyXG5cdG1hcmdpbi10b3A6IDEwcHg7XHJcblx0bWFyZ2luLWJvdHRvbTogYXV0bztcclxuXHQvL21hcmdpbi1sZWZ0OiAxMHB4O1xyXG5cdG91dGxpbmU6IHJnYigyNDMsIDI0MywgMjMyKTtcclxufVxyXG5cclxuaW1nLmZpeGVyMiB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZml4ZXIuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAyM3B4O1xyXG5cdHdpZHRoOiAyM3B4O1xyXG5cdG1hcmdpbi10b3A6IC0xcHg7XHJcblx0bWFyZ2luLWJvdHRvbTogYXV0bztcclxuXHRvdXRsaW5lOiByZ2IoMjQzLCAyNDMsIDIzMik7XHJcbn1cclxuXHJcbmltZy5maXhlciB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZml4ZXIuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA0NXB4O1xyXG5cdHdpZHRoOiA0NXB4O1xyXG5cdG1hcmdpbi10b3A6IDE3cHg7XHJcblx0bWFyZ2luLWJvdHRvbTogYXV0bztcclxuXHRvdXRsaW5lOiByZ2IoMjQzLCAyNDMsIDIzMik7XHJcbn1cclxuXHJcbmltZy5maXhlcjEge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2ZpeGVyXzEuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA0NXB4O1xyXG5cdHdpZHRoOiA0NXB4O1xyXG5cdG1hcmdpbi10b3A6IDE3cHg7XHJcblx0bWFyZ2luLWJvdHRvbTogYXV0bztcclxuXHRvdXRsaW5lOiByZ2IoMjQzLCAyNDMsIDIzMik7XHJcbn1cclxuXHJcbi5yb3ctY2VudGVyZWQge1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmNvbC1jZW50ZXJlZCB7XHJcblx0aGVpZ2h0OiAxNTBweDtcclxuXHR3aWR0aDogMTUwcHg7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGZsb2F0OiBub25lO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRtYXJnaW4tbGVmdDogMTBweDtcclxuXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcblx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHRvdXRsaW5lLXdpZHRoOiAwLjVweDtcclxuXHRib3JkZXItcmFkaXVzOiA1cHg7XHJcblx0YmFja2dyb3VuZDogcmdiKDIzNiwgMjM0LCAyMzQpO1xyXG5cdGJvcmRlcjogMC41cHggc29saWQgcmdiKDE3MCwgMTY5LCAxNjkpO1xyXG59XHJcblxyXG4uY29sLWNlbnRlcmVkMSB7XHJcblx0aGVpZ2h0OiAxNTBweDtcclxuXHR3aWR0aDogMTUwcHg7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGZsb2F0OiBub25lO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRtYXJnaW4tbGVmdDogMTBweDtcclxuXHRtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcblx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHRvdXRsaW5lLXdpZHRoOiAwLjVweDtcclxuXHRib3JkZXItcmFkaXVzOiA1cHg7XHJcblx0YmFja2dyb3VuZDogIzAwN2JmZjtcclxuXHRib3JkZXI6IDAuNXB4IHNvbGlkIHJnYigxNzAsIDE2OSwgMTY5KTtcclxuXHRjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5idG50cmFucyB7XHJcblx0Zm9udC1zaXplOiA4cHg7XHJcblx0Y29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uYnR0IHtcclxuXHRmb250LXNpemU6IDhweDtcclxuXHRjb2xvcjogZ3JleTtcclxufVxyXG5cclxuLmJ0bjEge1xyXG5cdGZvbnQtc2l6ZTogeC1zbWFsbDtcclxuXHRib3JkZXItc3R5bGU6IHNvbGlkO1xyXG5cdGJvcmRlci13aWR0aDogMC4ycHg7XHJcblx0Ym9yZGVyLWNvbG9yOiByZ2JhKDczLCA3MSwgNzEsIDAuNjMpO1xyXG59XHJcblxyXG4uYnRuMyB7XHJcblx0Zm9udC1mYW1pbHk6IGl0YWxpYztcclxuXHRmb250LXNpemU6IDEycHg7XHJcblx0Y29sb3I6IGdyZXk7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuLm0ge1xyXG5cdG1hcmdpbi10b3A6IC01MHB4O1xyXG59XHJcblxyXG4uYnQge1xyXG5cdGNvbG9yOiByZ2IoNjMsIDUxLCA1MSk7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogZ3JleTtcclxuXHRmb250LXNpemU6IDEwcHg7XHJcbn1cclxuXHJcbi5hZGQge1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHQvLyBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjMmFiYzZlO1xyXG5cdGZvbnQtc2l6ZTogMTBweDtcclxufVxyXG5cclxuLnRyYW5zaXRpb24ge1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmNzIHtcclxuXHRtYXJnaW4tbGVmdDogLTEwMHB4O1xyXG5cdG1hcmdpbi10b3A6IDgwcHg7XHJcblx0aGVpZ2h0OiA5MHB4O1xyXG5cdHdpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLmcge1xyXG5cdG92ZXJmbG93LXk6IHNjcm9sbDtcclxufVxyXG5cclxuLmc6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuXHRkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4ubWcge1xyXG5cdG1hcmdpbi10b3A6IDBweDtcclxufVxyXG5cclxuLnBkIHtcclxuXHRtYXJnaW4tcmlnaHQ6IDQwMHB4O1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xyXG5cdC5jcyB7XHJcblx0XHRtYXJnaW4tbGVmdDogLTgwcHg7XHJcblx0XHRtYXJnaW4tdG9wOiAtOHB4O1xyXG5cdFx0aGVpZ2h0OiA3MHB4O1xyXG5cdFx0d2lkdGg6IDIwMHB4O1xyXG5cdH1cclxuXHJcblx0Lm1nIHtcclxuXHRcdG1hcmdpbi10b3A6IC0zMHB4O1xyXG5cdH1cclxuXHJcblx0LnBkIHtcclxuXHRcdG1hcmdpbi1yaWdodDogMjMwcHg7XHJcblx0XHRtYXJnaW4tdG9wOiAtMTBweDtcclxuXHR9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcclxuXHQuY3Mge1xyXG5cdFx0bWFyZ2luLWxlZnQ6IC01MHB4O1xyXG5cdFx0bWFyZ2luLXRvcDogLTIwcHg7XHJcblx0XHRoZWlnaHQ6IDUwcHg7XHJcblx0XHR3aWR0aDogMjAwcHg7XHJcblx0fVxyXG5cclxuXHQubWcge1xyXG5cdFx0bWFyZ2luLXRvcDogLTZweDtcclxuXHR9XHJcblxyXG5cdC5wZCB7XHJcblx0XHRtYXJnaW4tcmlnaHQ6IDEzMHB4O1xyXG5cdFx0bWFyZ2luLXRvcDogLTEwcHg7XHJcblx0fVxyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyNXB4KSB7XHJcblx0ZGl2LnBkIHtcclxuXHRcdG1hcmdpbi1yaWdodDogNzVweDtcclxuXHR9XHJcblxyXG5cdC5jcyB7XHJcblx0XHRtYXJnaW4tbGVmdDogMHB4O1xyXG5cdFx0bWFyZ2luLXRvcDogMHB4O1xyXG5cdFx0aGVpZ2h0OiAwcHg7XHJcblx0XHR3aWR0aDogMHB4O1xyXG5cdH1cclxufVxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAzMjBweCkge1xyXG5cdGRpdi5wZCB7XHJcblx0XHRtYXJnaW4tcmlnaHQ6IDQwcHg7XHJcblx0fVxyXG5cclxuXHQuY3Mge1xyXG5cdFx0bWFyZ2luLWxlZnQ6IDBweDtcclxuXHRcdG1hcmdpbi10b3A6IDBweDtcclxuXHRcdGhlaWdodDogMHB4O1xyXG5cdFx0d2lkdGg6IDBweDtcclxuXHR9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/auth/sign-up-roles/sign-up-roles.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/auth/sign-up-roles/sign-up-roles.component.ts ***!
  \***************************************************************/
/*! exports provided: SignUpRolesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpRolesComponent", function() { return SignUpRolesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_5__);






var SignUpRolesComponent = /** @class */ (function () {
    /**
     *
     * @param router navigate to different routes
     * @param appState get variables or methods from appstate
     * @param activatedRoute get activated routes and query params
     * @param inventFundsApiService api services to call end points
     */
    function SignUpRolesComponent(router, appState, activatedRoute, inventFundsApiService) {
        this.router = router;
        this.appState = appState;
        this.activatedRoute = activatedRoute;
        this.inventFundsApiService = inventFundsApiService;
        // global variables
        this.enable = false;
        this.roleChosen = false;
        this.personId = sessionStorage.getItem('personId');
    }
    SignUpRolesComponent.prototype.ngOnInit = function () { };
    // to change css of active block
    SignUpRolesComponent.prototype.Active = function (event, id) {
        this.enable = true;
        this.id = id;
        this.changeBtn = 'add';
        // console.log(id + "=>id")
        if (this.enable == true && this.id == 0) {
            $('.one').addClass('transition');
            $('.two').removeClass('transition');
            $('.three').removeClass('transition');
            $('.bt').addClass('add');
            $('.bt').removeClass('add');
            $('.bt').removeClass('add');
            $('.founder').addClass('founder1');
            $('.fixer').removeClass('fixer1');
            $('.funder').removeClass('funder1');
            $('.oneb').addClass('btntrans');
            $('.twob').removeClass('btntrans');
            $('.threeb').removeClass('btntrans');
            $('.cd1').addClass('col-centered1');
            $('.cd2').removeClass('col-centered1');
            $('.cd3').removeClass('col-centered1');
            $('.twob').addClass('btt');
            $('.oneb').removeClass('btt');
            $('.threeb').addClass('btt');
            $('.bt').addClass('add');
            this.roleName = 'Invent Fund Founder';
            this.roleChosen = true;
            this.founder = true;
            this.funder = false;
            this.fixer = false;
        }
        if (this.enable == true && this.id == 1) {
            $('.two').addClass('transition');
            $('.one').removeClass('transition');
            $('.three').removeClass('transition');
            $('.fixer').addClass('fixer1');
            $('.founder').removeClass('founder1');
            $('.funder').removeClass('funder1');
            $('.twob').addClass('btntrans');
            $('.oneb').removeClass('btntrans');
            $('.threeb').removeClass('btntrans');
            $('.twob').removeClass('btt');
            $('.oneb').addClass('btt');
            $('.threeb').addClass('btt');
            $('.cd2').addClass('col-centered1');
            $('.cd1').removeClass('col-centered1');
            $('.cd3').removeClass('col-centered1');
            $('.bt').addClass('add');
            this.roleName = 'Invent Fund Fixer';
            this.roleChosen = true;
            this.founder = false;
            this.funder = false;
            this.fixer = true;
        }
        if (this.enable == true && this.id == 2) {
            $('.three').addClass('transition');
            $('.two').removeClass('transition');
            $('.one').removeClass('transition');
            $('.funder').addClass('funder1');
            $('.founder').removeClass('founder1');
            $('.fixer').removeClass('fixer1');
            $('.threeb').addClass('btntrans');
            $('.twob').removeClass('btntrans');
            $('.oneb').removeClass('btntrans');
            $('.cd3').addClass('col-centered1');
            $('.cd2').removeClass('col-centered1');
            $('.cd1').removeClass('col-centered1');
            $('.twob').addClass('btt');
            $('.oneb').addClass('btt');
            $('.threeb').removeClass('btt');
            $('.bt').addClass('add');
            this.roleName = 'Invent Fund Funder';
            this.roleChosen = true;
            this.founder = false;
            this.funder = true;
            this.fixer = false;
        }
    };
    // deactivate card css
    SignUpRolesComponent.prototype.Deactive = function (event) {
        this.enable = false;
        if (this.id == 0) {
            $('.cd1').removeClass('col-centered1');
            $('.cd1').addClass('col-centered');
            $('.founder').removeClass('founder1');
            $('.founder').addClass('founder');
            $('.one').removeClass('transition');
            $('.one').addClass('one');
            $('.oneb').removeClass('btntrans');
            $('.oneb').addClass('btt');
            $('.bt').removeClass('add');
        }
        if (this.id == 1) {
            $('.cd2').removeClass('col-centered1');
            $('.cd2').addClass('col-centered');
            $('.fixer').removeClass('fixer1');
            $('.fixer').addClass('fixer');
            $('.two').removeClass('transition');
            $('.two').addClass('two');
            $('.twob').removeClass('btntrans');
            $('.twob').addClass('btt');
            $('.bt').removeClass('add');
        }
        if (this.id == 2) {
            $('.cd3').removeClass('col-centered1');
            $('.cd3').addClass('col-centered');
            $('.funder').removeClass('funder1');
            $('.funder').addClass('funder');
            $('.three').removeClass('transition');
            $('.three').addClass('three');
            $('.threeb').removeClass('btntrans');
            $('.threeb').addClass('btt');
            $('.bt').removeClass('add');
        }
    };
    // select generic role
    SignUpRolesComponent.prototype.genericRole = function () {
        var _this = this;
        //alert('generic selected')
        this.roleName = 'Generic';
        this.founder = false;
        this.funder = false;
        this.fixer = false;
        this.roleChosen = false;
        var params = {
            personId: this.personId,
            roleName: this.roleName,
        };
        this.inventFundsApiService.updateRoleAndOrgDetails(params).then(function (res) {
            // console.log('res of generic role update-->' + JSON.stringify(res))
            if (res.code == 200) {
                _this.router.navigate(['/dashboard']);
            }
        });
    };
    // specific roles
    SignUpRolesComponent.prototype.specificRole = function () {
        var _this = this;
        //alert('specific selected:'+this.roleName)
        var params = {
            personId: this.personId,
            roleName: this.roleName,
        };
        this.inventFundsApiService.updateRoleAndOrgDetails(params).then(function (res) {
            // console.log('res of specific role update-->' + JSON.stringify(res));
            // console.log("get personId from local storage : " + JSON.stringify(sessionStorage.getItem('personId')))
            var params = {
                personId: sessionStorage.getItem('personId'),
            };
            _this.inventFundsApiService.getToken(params).then(function (res) {
                // console.log("get token : " + JSON.stringify(res))
                if (res.status == 200) {
                    _this.setApplicationData(res.response);
                }
            });
        });
    };
    // after role setup application data in session storage
    SignUpRolesComponent.prototype.setApplicationData = function (data) {
        // console.log('data for set password : ' + JSON.stringify(data));
        sessionStorage.setItem('token', data.token);
        this.appState.globalData.token = data.token;
        this.appState.globalData.roleId = data.roleId;
        this.appState.globalData.roleCode = data.roleCode;
        this.appState.globalData.partyId = data.partyId;
        this.appState.globalData.personId = data.personId;
        this.appState.globalData.personEmail = data.personEmail;
        this.appState.globalData.personType = data.personType;
        this.appState.globalData.orgId = data.orgId;
        this.appState.globalData.partyCode = data.partyCode;
        this.appState.globalData.signUpProvider = data.signUpProvider;
        this.appState.globalData.name = data.name;
        sessionStorage.setItem('passwordType', data.passWordType);
        var profile = crypto_js__WEBPACK_IMPORTED_MODULE_5__["AES"].encrypt(JSON.stringify(this.appState.globalData), this.appState.encryptSecretKey, this.appState.encryptOptions).toString();
        sessionStorage.setItem('profile', profile);
        if (data.url != null) {
            this.appState.setProfileImage(data.url);
            sessionStorage.setItem('profileImage', data.url);
            // this.appState.getProfileImage('signup-role');
            this.appState.getProfileImage();
            this.appState.setAvatar(data.url);
        }
        // else if ('path' in data && 'key' in data) {
        //   if (data.path !== null && data.key !== null) {
        //     this.appState.getSignedUrl(data.path, data.key);
        //   } else {
        //     this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
        //   }
        // }
        else {
            this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
        }
        // this.router.navigate(['/profile/dashboard'])
        this.router.navigate(['profile/editProfile']);
        this.appState.encryptProfileJson();
    };
    SignUpRolesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'sign-up-roles',
            template: __webpack_require__(/*! ./sign-up-roles.component.html */ "./src/app/auth/sign-up-roles/sign-up-roles.component.html"),
            styles: [__webpack_require__(/*! ./sign-up-roles.component.scss */ "./src/app/auth/sign-up-roles/sign-up-roles.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_app_state_service__WEBPACK_IMPORTED_MODULE_4__["AppStateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_3__["InventFundsApiService"]])
    ], SignUpRolesComponent);
    return SignUpRolesComponent;
}());



/***/ }),

/***/ "./src/app/auth/sign-up/sign-up.component.html":
/*!*****************************************************!*\
  !*** ./src/app/auth/sign-up/sign-up.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"conrtainer\">\r\n\t<div class=\"row no-gutters h-100\">\r\n\t\t<div class=\"col-md-6 col-sm-6 col-xs-6 col-lg-6 bg-night-sky\">\r\n\t\t\t<div>\r\n\t\t\t\t<span class=\"inventlogo pointer-ref\" (click)=\"routerNavigation('landing')\"></span>\r\n\t\t\t</div>\r\n\t\t\t<br />\r\n\t\t\t<br />\r\n\r\n\t\t\t<div class=\"container\">\r\n\t\t\t\t<div class=\"col-md-12 col-sm-12 col-xs-12 col-lg-12\">\r\n\t\t\t\t\t<div class=\"slide-img-bg\"></div>\r\n\t\t\t\t\t<div class=\"slider-content text-light sd\">\r\n\t\t\t\t\t\t<div class=\"col-sm-6 col-md-10 col-lg-10 col-xs-6 justify-content align\">\r\n\t\t\t\t\t\t\t<h5 style=\"font-weight: 500 !important;\">\r\n\t\t\t\t\t\t\t\tMore than 60k Founders are\r\n\t\t\t\t\t\t\t</h5>\r\n\t\t\t\t\t\t\t<h5 style=\"font-weight: 500 !important;\">\r\n\t\t\t\t\t\t\t\tLooking on Invent fund\r\n\t\t\t\t\t\t\t</h5>\r\n\t\t\t\t\t\t\t<h5 style=\"font-weight: 500 !important;\">\r\n\t\t\t\t\t\t\t\tevery week\r\n\t\t\t\t\t\t\t</h5>\r\n\r\n\t\t\t\t\t\t\t<p class=\"pt-2 ps h5\">\r\n\t\t\t\t\t\t\t\tExplore More <i class=\"fa fa-arrow-right\"></i>\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t\t<span class=\"Group866 img-fluid\"></span>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t\t<div class=\"col-md-6 col-sm-6 col-xs-6 col-lg-6 d-flex bg-white justify-content-center align-items-center\">\r\n\t\t\t<div class=\"mx-auto app-login-box col-sm-12 col-md-10 col-lg-10 col-xs-12 abc\">\r\n\t\t\t\t<h2 class=\"centre-text-bold\">Register now</h2>\r\n\r\n\t\t\t\t<div class=\"mt-4\">\r\n\t\t\t\t\t<form [formGroup]=\"signUpForm\" (validSubmit)=\"submit($event, signUpForm.value)\">\r\n\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t<div class=\"col-xl-8 margin-h-center fc\">\r\n\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t\t<input formControlName=\"email\" id=\"exampleEmail\" name=\"email\" type=\"email\"\r\n\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Email id\" class=\"form-control\" />\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t\t\t<br />\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-wide mb-2 mr-2 btn-success btnP font-large\">\r\n\t\t\t\t\t\t\t\tSignup\r\n\t\t\t\t\t\t\t\t<i class=\"fa fa-arrow-right\"></i>\r\n\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</form>\r\n\t\t\t\t\t<br />\r\n\t\t\t\t\t<br />\r\n\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t<div class=\"hrdivider hr\">\r\n\t\t\t\t\t\t\t<hr />\r\n\t\t\t\t\t\t\t<span>Or With</span>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div style=\"margin-top: -30px;\">\r\n\t\t\t\t\t\t\t<button (click)=\"signUpWithGoogle()\" type=\"button\"\r\n\t\t\t\t\t\t\t\tclass=\"btn btn-wide mb-2 mr-2 btn-white btnw\">\r\n\t\t\t\t\t\t\t\t<span class=\"googleicon\"></span> Google\r\n\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t<button (click)=\"signUpWithLinkedIn()\" type=\"button\"\r\n\t\t\t\t\t\t\t\tclass=\"btn btn-wide mb-2 mr-2 btn-primary btnw\">\r\n\t\t\t\t\t\t\t\t<i class=\"fab fa-linkedin-in\"></i> LinkedIn\r\n\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<br />\r\n\t\t\t\t\t<div class=\"center-align\">\r\n\t\t\t\t\t\t<p class=\"centre-text pw\" style=\"margin-top: 0; margin-bottom: -2px;\">\r\n\t\t\t\t\t\t\tAlready have an account?\r\n\t\t\t\t\t\t\t<a href=\"#/auth/login\"> Login</a>\r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n\r\n<button (click)='signInWithFB()'>Facebook</button>"

/***/ }),

/***/ "./src/app/auth/sign-up/sign-up.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/auth/sign-up/sign-up.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "span.inventlogo {\n  background: url('inventfundlogo.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 64px;\n  width: 85px;\n  margin-top: 42px;\n  margin-left: 30px; }\n\nspan.dot {\n  background: url('group116.svg') no-repeat bottom center;\n  background-size: contain;\n  display: inline-block;\n  height: 100px;\n  width: 200px;\n  margin-top: 6px;\n  margin-left: 560px;\n  overflow: hidden;\n  position: relative;\n  z-index: 1; }\n\n.hr {\n  width: 8px; }\n\n.abc {\n  margin-top: -90px; }\n\n.fc {\n  margin-left: 6px;\n  height: 30px;\n  margin-top: 10px; }\n\n.btnP {\n  margin-top: 30px;\n  width: 140px; }\n\n.font-large {\n  font-size: 14px; }\n\n.ps {\n  font-size: 13px; }\n\n.space {\n  padding-top: 0px; }\n\n.parap {\n  padding-right: 180px; }\n\n::-webkit-input-placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n::-moz-placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n::-ms-input-placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n::placeholder {\n  color: blue;\n  opacity: 0.5; }\n\n:-ms-input-placeholder {\n  color: blue; }\n\n::-ms-input-placeholder {\n  color: blue; }\n\n.btn-white {\n  background-color: white;\n  border: 1px solid lightgrey; }\n\n.hrdivider {\n  position: relative;\n  margin-bottom: 20px;\n  width: 100%;\n  text-align: center; }\n\n.hrdivider span {\n  position: relative;\n  top: -30px;\n  background: #fff;\n  padding: 0 20px;\n  font-size: 14px;\n  color: blue; }\n\n.display_text_center {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\nspan.googleicon {\n  background: url('32px-Google__G__Logo.svg.png') no-repeat top left;\n  background-size: contain;\n  cursor: pointer;\n  display: inline-block;\n  height: 10px;\n  width: 10px; }\n\nspan.Group866 {\n  background: url('register.svg') no-repeat top left;\n  background-size: contain;\n  cursor: pointer;\n  display: inline-block;\n  height: 230px;\n  width: 230px;\n  margin-top: 10px;\n  margin-left: -60px; }\n\n.btnw {\n  height: 34px;\n  width: 180px;\n  font-size: 14px; }\n\n@media only screen and (max-width: 1024px) {\n  span.dot {\n    background: url('group116.svg') no-repeat bottom center;\n    background-size: contain;\n    display: inline-block;\n    height: 80px;\n    width: 160px;\n    margin-left: 410px;\n    overflow: hidden;\n    position: relative;\n    z-index: 1;\n    margin-top: -60px; }\n  span.inventlogo {\n    background: url('inventfundlogo.svg') no-repeat top left;\n    background-size: contain;\n    display: inline-block;\n    height: 80px;\n    width: 80px;\n    margin-top: 20px;\n    margin-left: 30px; }\n  div.sd {\n    margin-top: -40px; } }\n\n@media only screen and (max-width: 768px) {\n  span.dot {\n    background: url('group116.svg') no-repeat bottom center;\n    background-size: contain;\n    display: inline-block;\n    height: 80px;\n    width: 160px;\n    margin-left: 272px;\n    overflow: hidden;\n    position: relative;\n    z-index: 1; }\n  .h5 {\n    font-size: 10px; } }\n\n@media only screen and (max-width: 425px) {\n  span.dot {\n    background: url('group116.svg') no-repeat bottom center;\n    display: none; }\n  .abc {\n    margin-top: 50px; }\n  .align {\n    padding-left: 20px; } }\n\n.align {\n  padding-left: 70px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9zaWduLXVwL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxcYXV0aFxcc2lnbi11cFxcc2lnbi11cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHdEQUNLO0VBQ0wsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTs7QUFHbEI7RUFDQyx1REFDTztFQUNQLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsYUFBYTtFQUNiLFlBQVk7RUFDWixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsVUFBVSxFQUFBOztBQUdYO0VBQ0MsVUFBVSxFQUFBOztBQUdYO0VBQ0MsaUJBQWlCLEVBQUE7O0FBR2xCO0VBQ0MsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixnQkFBZ0IsRUFBQTs7QUFHakI7RUFDQyxnQkFBZ0I7RUFDaEIsWUFBWSxFQUFBOztBQUdiO0VBQ0MsZUFBZSxFQUFBOztBQUdoQjtFQUNDLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxnQkFBZ0IsRUFBQTs7QUFHakI7RUFDQyxvQkFBb0IsRUFBQTs7QUFHckI7RUFDQyxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUZiO0VBQ0MsV0FBVztFQUNYLFlBQVksRUFBQTs7QUFGYjtFQUNDLFdBQVc7RUFDWCxZQUFZLEVBQUE7O0FBRmI7RUFDQyxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUdiO0VBQ0MsV0FBVyxFQUFBOztBQUdaO0VBQ0MsV0FBVyxFQUFBOztBQUdaO0VBQ0MsdUJBQXVCO0VBQ3ZCLDJCQUEyQixFQUFBOztBQUc1QjtFQUNDLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixlQUFlO0VBQ2YsV0FBVyxFQUFBOztBQUdaO0VBQ0MsWUFBWTtFQUNaLFdBQVc7RUFDWCxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQUdwQjtFQUNDLGtFQUNtQjtFQUNuQix3QkFBd0I7RUFDeEIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUdaO0VBQ0Msa0RBQXdFO0VBQ3hFLHdCQUF3QjtFQUN4QixlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGFBQWE7RUFDYixZQUFZO0VBQ1osZ0JBQWdCO0VBQ2hCLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLFlBQVk7RUFDWixZQUFZO0VBQ1osZUFBZSxFQUFBOztBQUdoQjtFQUNDO0lBQ0MsdURBQ087SUFDUCx3QkFBd0I7SUFDeEIscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWixZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLGlCQUFpQixFQUFBO0VBR2xCO0lBQ0Msd0RBQ1M7SUFDVCx3QkFBd0I7SUFDeEIscUJBQXFCO0lBQ3JCLFlBQVk7SUFDWixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLGlCQUFpQixFQUFBO0VBR2xCO0lBQ0MsaUJBQWlCLEVBQUEsRUFDakI7O0FBR0Y7RUFDQztJQUNDLHVEQUNPO0lBQ1Asd0JBQXdCO0lBQ3hCLHFCQUFxQjtJQUNyQixZQUFZO0lBQ1osWUFBWTtJQUNaLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLFVBQVUsRUFBQTtFQUVYO0lBQ0MsZUFBZSxFQUFBLEVBQ2Y7O0FBR0Y7RUFDQztJQUNDLHVEQUNPO0lBQ1AsYUFBYSxFQUFBO0VBRWQ7SUFDQyxnQkFBZ0IsRUFBQTtFQUVqQjtJQUNDLGtCQUFrQixFQUFBLEVBQ2xCOztBQUdGO0VBQ0Msa0JBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hdXRoL3NpZ24tdXAvc2lnbi11cC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNwYW4uaW52ZW50bG9nbyB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvaW52ZW50ZnVuZGxvZ28uc3ZnJykgbm8tcmVwZWF0IHRvcFxyXG5cdFx0bGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogNjRweDtcclxuXHR3aWR0aDogODVweDtcclxuXHRtYXJnaW4tdG9wOiA0MnB4O1xyXG5cdG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG59XHJcblxyXG5zcGFuLmRvdCB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZ3JvdXAxMTYuc3ZnJykgbm8tcmVwZWF0IGJvdHRvbVxyXG5cdFx0Y2VudGVyO1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAxMDBweDtcclxuXHR3aWR0aDogMjAwcHg7XHJcblx0bWFyZ2luLXRvcDogNnB4O1xyXG5cdG1hcmdpbi1sZWZ0OiA1NjBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHR6LWluZGV4OiAxO1xyXG59XHJcblxyXG4uaHIge1xyXG5cdHdpZHRoOiA4cHg7XHJcbn1cclxuXHJcbi5hYmMge1xyXG5cdG1hcmdpbi10b3A6IC05MHB4O1xyXG59XHJcblxyXG4uZmMge1xyXG5cdG1hcmdpbi1sZWZ0OiA2cHg7XHJcblx0aGVpZ2h0OiAzMHB4O1xyXG5cdG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbi5idG5QIHtcclxuXHRtYXJnaW4tdG9wOiAzMHB4O1xyXG5cdHdpZHRoOiAxNDBweDtcclxufVxyXG5cclxuLmZvbnQtbGFyZ2Uge1xyXG5cdGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuLnBzIHtcclxuXHRmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi5zcGFjZSB7XHJcblx0cGFkZGluZy10b3A6IDBweDtcclxufVxyXG5cclxuLnBhcmFwIHtcclxuXHRwYWRkaW5nLXJpZ2h0OiAxODBweDtcclxufVxyXG5cclxuOjpwbGFjZWhvbGRlciB7XHJcblx0Y29sb3I6IGJsdWU7XHJcblx0b3BhY2l0eTogMC41O1xyXG59XHJcblxyXG46LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcclxuXHRjb2xvcjogYmx1ZTtcclxufVxyXG5cclxuOjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xyXG5cdGNvbG9yOiBibHVlO1xyXG59XHJcblxyXG4uYnRuLXdoaXRlIHtcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuXHRib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyZXk7XHJcbn1cclxuXHJcbi5ocmRpdmlkZXIge1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmhyZGl2aWRlciBzcGFuIHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0dG9wOiAtMzBweDtcclxuXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cdHBhZGRpbmc6IDAgMjBweDtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcblx0Y29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbi5kaXNwbGF5X3RleHRfY2VudGVyIHtcclxuXHRoZWlnaHQ6IDEwMCU7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG5zcGFuLmdvb2dsZWljb24ge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zLzMycHgtR29vZ2xlX19HX19Mb2dvLnN2Zy5wbmcnKVxyXG5cdFx0bm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRjdXJzb3I6IHBvaW50ZXI7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMTBweDtcclxuXHR3aWR0aDogMTBweDtcclxufVxyXG5cclxuc3Bhbi5Hcm91cDg2NiB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvcmVnaXN0ZXIuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRjdXJzb3I6IHBvaW50ZXI7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMjMwcHg7XHJcblx0d2lkdGg6IDIzMHB4O1xyXG5cdG1hcmdpbi10b3A6IDEwcHg7XHJcblx0bWFyZ2luLWxlZnQ6IC02MHB4O1xyXG59XHJcblxyXG4uYnRudyB7XHJcblx0aGVpZ2h0OiAzNHB4O1xyXG5cdHdpZHRoOiAxODBweDtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTAyNHB4KSB7XHJcblx0c3Bhbi5kb3Qge1xyXG5cdFx0YmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pY29ucy9ncm91cDExNi5zdmcpIG5vLXJlcGVhdCBib3R0b21cclxuXHRcdFx0Y2VudGVyO1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdFx0aGVpZ2h0OiA4MHB4O1xyXG5cdFx0d2lkdGg6IDE2MHB4O1xyXG5cdFx0bWFyZ2luLWxlZnQ6IDQxMHB4O1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdHotaW5kZXg6IDE7XHJcblx0XHRtYXJnaW4tdG9wOiAtNjBweDtcclxuXHR9XHJcblxyXG5cdHNwYW4uaW52ZW50bG9nbyB7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9pbnZlbnRmdW5kbG9nby5zdmcnKSBuby1yZXBlYXRcclxuXHRcdFx0dG9wIGxlZnQ7XHJcblx0XHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHRoZWlnaHQ6IDgwcHg7XHJcblx0XHR3aWR0aDogODBweDtcclxuXHRcdG1hcmdpbi10b3A6IDIwcHg7XHJcblx0XHRtYXJnaW4tbGVmdDogMzBweDtcclxuXHR9XHJcblxyXG5cdGRpdi5zZCB7XHJcblx0XHRtYXJnaW4tdG9wOiAtNDBweDtcclxuXHR9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcclxuXHRzcGFuLmRvdCB7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb25zL2dyb3VwMTE2LnN2Zykgbm8tcmVwZWF0IGJvdHRvbVxyXG5cdFx0XHRjZW50ZXI7XHJcblx0XHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0XHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0XHRoZWlnaHQ6IDgwcHg7XHJcblx0XHR3aWR0aDogMTYwcHg7XHJcblx0XHRtYXJnaW4tbGVmdDogMjcycHg7XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0ei1pbmRleDogMTtcclxuXHR9XHJcblx0Lmg1IHtcclxuXHRcdGZvbnQtc2l6ZTogMTBweDtcclxuXHR9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDI1cHgpIHtcclxuXHRzcGFuLmRvdCB7XHJcblx0XHRiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ljb25zL2dyb3VwMTE2LnN2Zykgbm8tcmVwZWF0IGJvdHRvbVxyXG5cdFx0XHRjZW50ZXI7XHJcblx0XHRkaXNwbGF5OiBub25lO1xyXG5cdH1cclxuXHQuYWJjIHtcclxuXHRcdG1hcmdpbi10b3A6IDUwcHg7XHJcblx0fVxyXG5cdC5hbGlnbiB7XHJcblx0XHRwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcblx0fVxyXG59XHJcblxyXG4uYWxpZ24ge1xyXG5cdHBhZGRpbmctbGVmdDogNzBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/auth/sign-up/sign-up.component.ts":
/*!***************************************************!*\
  !*** ./src/app/auth/sign-up/sign-up.component.ts ***!
  \***************************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.es5.js");
/* harmony import */ var _providers_config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../providers/config */ "./src/providers/config.ts");









var SignUpComponent = /** @class */ (function () {
    /**
     *
     * @param fb formbuilder for reactive forms
     * @param router naviagte to different routes
     * @param inventFundsApiService api services for endpoints to get data
     * @param authService to login or signup using google or linkedin
     * @param activatedRoute ativated routes to get query params
     */
    function SignUpComponent(fb, router, inventFundsApiService, authService, activatedRoute) {
        // query params data from router
        var _this = this;
        this.router = router;
        this.inventFundsApiService = inventFundsApiService;
        this.authService = authService;
        this.activatedRoute = activatedRoute;
        this.activatedRoute.queryParams.subscribe(function (res) {
            // console.log("queryParams :" + JSON.stringify(res));
            if (res.email) {
                // this.requestToken();
                _this.linkedInSignUp(res.email);
            }
        });
        this.signUpForm = fb.group({
            email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
        });
    }
    // ng onInit
    SignUpComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.authState.subscribe(function (user) {
            _this.user = user;
            _this.loggedIn = user != null;
        });
    };
    SignUpComponent.prototype.routerNavigation = function (data) {
        this.router.navigate(['landing/landing-page']);
    };
    SignUpComponent.prototype.signInWithFB = function () {
        this.authService
            .signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_6__["FacebookLoginProvider"].PROVIDER_ID)
            .then(function (result) {
            console.log('facebook login : ' + JSON.stringify(result));
        })
            .catch(function (err) {
            console.log('err : ' + JSON.stringify(err));
        });
    };
    // validate and signup user
    SignUpComponent.prototype.submit = function ($ev, value) {
        // console.log("form " + JSON.stringify(value));
        for (var c in this.signUpForm.controls) {
            this.signUpForm.controls[c].markAsTouched();
        }
        if (this.signUpForm.valid) {
            // console.log("Valid!");
            this.sendActivationEmail(value);
        }
        else {
            console.log('Form invalid!');
        }
    };
    // to send activation mail if custom signup
    SignUpComponent.prototype.sendActivationEmail = function (formValue) {
        var _this = this;
        var params = {
            roleName: '',
            type: 'Custom',
            provider: '',
            partyCode: '',
            partyType: '',
            partyName: '',
            partyDescription: '',
            orgId: '',
            emailId: formValue.email,
            passwordRequired: 'N',
            personType: '',
            name: '',
            designation: '',
            logIn: 'true',
        };
        this.inventFundsApiService.signUp(params).then(function (res) {
            // console.log("sign up response-->" + JSON.stringify(res));
            if (res.code == 200) {
                _this.router.navigate(['auth/email-sent']);
            }
            else {
                _this.signUpForm.controls['email'].setErrors({ duplicateEmail: true });
            }
        });
    };
    // signup with google
    SignUpComponent.prototype.signUpWithGoogle = function () {
        var _this = this;
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_6__["GoogleLoginProvider"].PROVIDER_ID).then(function (res) {
            // console.log("res:" + JSON.stringify(res));
            var dataToCreate = res;
            _this.socialUserEmail = res.email;
            var params = {
                collectionName: 'user',
                queryStr: { userName: { $eq: _this.socialUserEmail } },
            };
            _this.inventFundsApiService.retrieveMongoDBAll(params).then(function (res) {
                // console.log("resp-->" + JSON.stringify(res));
                if (res.response.length > 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                        text: 'You are already registered with us! Please Login',
                        icon: 'warning',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                    }).then(function (result) { });
                }
                else {
                    //create new user
                    var params = {
                        roleName: '',
                        type: 'Social',
                        provider: 'Google',
                        partyCode: '',
                        partyType: '',
                        partyName: '',
                        partyDescription: '',
                        orgId: '',
                        emailId: _this.socialUserEmail,
                        passwordRequired: 'N',
                        personType: '',
                        name: dataToCreate.name,
                        photoUrl: dataToCreate.photoUrl,
                        firstName: dataToCreate.firstName,
                        lastName: dataToCreate.lastName,
                        designation: '',
                        logIn: 'true',
                    };
                    // console.log("res params :" + JSON.stringify(params));
                    _this.inventFundsApiService.signUp(params).then(function (res) {
                        // console.log("res of google sign up--->" + JSON.stringify(res));
                        // console.log("res code--->" + JSON.stringify(res.code));
                        // console.log("res personId--->" + JSON.stringify(res.personId));
                        if (res.code == 200) {
                            sessionStorage.setItem('personId', res.personId);
                            sessionStorage.setItem('passwordType', res.passWordType);
                            _this.router.navigate(['auth/sign-up-roles']);
                        }
                    });
                }
            });
        });
    };
    // signup with linkedin
    SignUpComponent.prototype.signUpWithLinkedIn = function () {
        if (_providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERTYPE"] == 'DEVELOPMENT') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERREDRECTURLDEV"];
        }
        else if (_providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERTYPE"] == 'TESTING') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERREDRECTURLTESTIN"];
        }
        else if (_providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERTYPE"] == 'UAT') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERREDRECTURLUAT"];
        }
        else if (_providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERTYPE"] == 'PRODUCTION') {
            this.redirectUrl = _providers_config__WEBPACK_IMPORTED_MODULE_7__["SERVERREDRECTURLPROD"];
        }
        sessionStorage.setItem('page', 'signup');
        window.location.href =
            'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ctgyj14vau28&redirect_uri=' +
                this.redirectUrl +
                '&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social';
    };
    // create data for user if linkedin signup true
    SignUpComponent.prototype.linkedInSignUp = function (email) {
        var _this = this;
        var params = {
            collectionName: 'user',
            queryStr: { userName: { $eq: email } },
        };
        this.inventFundsApiService.retrieveMongoDBAll(params).then(function (res) {
            // console.log("resp of user table-->" + JSON.stringify(res));
            if (res.response.length > 0) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                    text: 'You are already registered with us! Please Login',
                    icon: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                }).then(function (result) { });
            }
            else {
                //create new user
                var params = {
                    roleName: '',
                    type: 'Social',
                    provider: 'LinkedIn',
                    partyCode: '',
                    partyType: '',
                    partyName: '',
                    partyDescription: '',
                    orgId: '',
                    emailId: email,
                    passwordRequired: 'N',
                    personType: '',
                    photoUrl: null,
                    firstName: '',
                    lastName: '',
                    name: '',
                    designation: '',
                    logIn: 'true',
                };
                // console.log('params:' + JSON.stringify(params))
                _this.inventFundsApiService.signUp(params).then(function (res) {
                    // console.log("res of Linkedin sign up--->" + JSON.stringify(res));
                    // console.log("res code--->" + JSON.stringify(res.code));
                    // console.log("res personId--->" + JSON.stringify(res.personId));
                    if (res.code == 200) {
                        sessionStorage.setItem('personId', res.personId);
                        sessionStorage.setItem('passwordType', res.passWordType);
                        _this.router.navigate(['auth/sign-up-roles']);
                    }
                });
            }
        });
    };
    SignUpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/auth/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.scss */ "./src/app/auth/sign-up/sign-up.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__["InventFundsApiService"],
            angularx_social_login__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], SignUpComponent);
    return SignUpComponent;
}());



/***/ }),

/***/ "./src/app/auth/unauthorized/unauthorized.component.html":
/*!***************************************************************!*\
  !*** ./src/app/auth/unauthorized/unauthorized.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"un-authorized\">\r\n\t<div class=\"row align-center\">\r\n\t\t<span>Sorry but you are not authorized to view this page</span\r\n\t\t>&nbsp;&nbsp;\r\n\t\t<a [routerLink]=\"'/profile/dashboard'\">Dashboard</a>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/auth/unauthorized/unauthorized.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/auth/unauthorized/unauthorized.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".align-center {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC91bmF1dGhvcml6ZWQvRDpcXFdvcmtcXFByb2plY3RzXFxJTlZFTlRGVU5EXFxCUkFOQ0hFU1xcaW52ZW50ZnVuZC10ZXN0L3NyY1xcYXBwXFxhdXRoXFx1bmF1dGhvcml6ZWRcXHVuYXV0aG9yaXplZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsUUFBUTtFQUNSLGdDQUFnQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXV0aC91bmF1dGhvcml6ZWQvdW5hdXRob3JpemVkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFsaWduLWNlbnRlciB7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IDUwJTtcclxuXHR0b3A6IDUwJTtcclxuXHR0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/auth/unauthorized/unauthorized.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/auth/unauthorized/unauthorized.component.ts ***!
  \*************************************************************/
/*! exports provided: UnauthorizedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnauthorizedComponent", function() { return UnauthorizedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UnauthorizedComponent = /** @class */ (function () {
    function UnauthorizedComponent() {
    }
    UnauthorizedComponent.prototype.ngOnInit = function () { };
    UnauthorizedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-unauthorized',
            template: __webpack_require__(/*! ./unauthorized.component.html */ "./src/app/auth/unauthorized/unauthorized.component.html"),
            styles: [__webpack_require__(/*! ./unauthorized.component.scss */ "./src/app/auth/unauthorized/unauthorized.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UnauthorizedComponent);
    return UnauthorizedComponent;
}());



/***/ })

}]);
//# sourceMappingURL=app-auth-auth-module.js.map