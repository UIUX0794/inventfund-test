(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-profile-profile-module"],{

/***/ "./node_modules/ng4-geoautocomplete/auto-complete.component.js":
/*!*********************************************************************!*\
  !*** ./node_modules/ng4-geoautocomplete/auto-complete.component.js ***!
  \*********************************************************************/
/*! exports provided: AutoCompleteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteComponent", function() { return AutoCompleteComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _windowRef_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./windowRef.service */ "./node_modules/ng4-geoautocomplete/windowRef.service.js");
/* harmony import */ var _auto_complete_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auto-complete.service */ "./node_modules/ng4-geoautocomplete/auto-complete.service.js");




var AutoCompleteComponent = (function () {
    function AutoCompleteComponent(platformId, _elmRef, _global, _autoCompleteSearchService) {
        this.platformId = platformId;
        this._elmRef = _elmRef;
        this._global = _global;
        this._autoCompleteSearchService = _autoCompleteSearchService;
        this.componentCallback = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.locationInput = '';
        this.gettingCurrentLocationFlag = false;
        this.dropdownOpen = false;
        this.recentDropdownOpen = false;
        this.queryItems = [];
        this.isSettingsError = false;
        this.settingsErrorMsg = '';
        this.settings = {};
        this.moduleinit = false;
        this.selectedDataIndex = -1;
        this.recentSearchData = [];
        this.userSelectedOption = '';
        this.defaultSettings = {
            geoPredictionServerUrl: '',
            geoLatLangServiceUrl: '',
            geoLocDetailServerUrl: '',
            geoCountryRestriction: [],
            geoTypes: [],
            geoLocation: [],
            geoRadius: 0,
            serverResponseListHierarchy: [],
            serverResponseatLangHierarchy: [],
            serverResponseDetailHierarchy: [],
            resOnSearchButtonClickOnly: false,
            useGoogleGeoApi: true,
            inputPlaceholderText: 'Enter Area Name',
            inputString: '',
            showSearchButton: true,
            showRecentSearch: true,
            showCurrentLocation: true,
            recentStorageName: 'recentSearches',
            noOfRecentSearchSave: 5,
            currentLocIconUrl: '',
            searchIconUrl: '',
            locationIconUrl: ''
        };
    }
    AutoCompleteComponent.prototype.ngOnInit = function () {
        if (!this.moduleinit) {
            this.moduleInit();
        }
    };
    AutoCompleteComponent.prototype.ngOnChanges = function () {
        this.moduleinit = true;
        this.moduleInit();
    };
    //function called when click event happens in input box. (Binded with view)
    AutoCompleteComponent.prototype.searchinputClickCallback = function (event) {
        event.target.select();
        this.searchinputCallback(event);
    };
    //function called when there is a change in input. (Binded with view)
    AutoCompleteComponent.prototype.searchinputCallback = function (event) {
        var inputVal = this.locationInput;
        if ((event.keyCode === 40) || (event.keyCode === 38) || (event.keyCode === 13)) {
            this.navigateInList(event.keyCode);
        }
        else if (inputVal) {
            this.getListQuery(inputVal);
        }
        else {
            this.queryItems = [];
            if (this.userSelectedOption) {
                this.userQuerySubmit('false');
            }
            this.userSelectedOption = '';
            if (this.settings.showRecentSearch) {
                this.showRecentSearch();
            }
            else {
                this.dropdownOpen = false;
            }
        }
    };
    //function to execute when user hover over autocomplete list.(binded with view)
    AutoCompleteComponent.prototype.activeListNode = function (index) {
        for (var i = 0; i < this.queryItems.length; i++) {
            if (index === i) {
                this.queryItems[i].active = true;
                this.selectedDataIndex = index;
            }
            else {
                this.queryItems[i].active = false;
            }
        }
    };
    //function to execute when user select the autocomplete list.(binded with view)
    AutoCompleteComponent.prototype.selectedListNode = function (index) {
        this.dropdownOpen = false;
        if (this.recentDropdownOpen) {
            this.setRecentLocation(this.queryItems[index]);
        }
        else {
            this.getPlaceLocationInfo(this.queryItems[index]);
        }
    };
    //function to close the autocomplete list when clicked outside. (binded with view)
    AutoCompleteComponent.prototype.closeAutocomplete = function (event) {
        if (!this._elmRef.nativeElement.contains(event.target)) {
            this.selectedDataIndex = -1;
            this.dropdownOpen = false;
        }
    };
    //function to manually trigger the callback to parent component when clicked search button.
    AutoCompleteComponent.prototype.userQuerySubmit = function (selectedOption) {
        var _userOption = selectedOption === 'false' ? '' : this.userSelectedOption;
        if (_userOption) {
            this.componentCallback.emit({ 'response': true, 'data': this.userSelectedOption });
        }
        else {
            this.componentCallback.emit({ 'response': false, 'reason': 'No user input' });
        }
    };
    //function to get user current location from the device.
    AutoCompleteComponent.prototype.currentLocationSelected = function () {
        var _this = this;
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId)) {
            this.gettingCurrentLocationFlag = true;
            this.dropdownOpen = false;
            this._autoCompleteSearchService.getGeoCurrentLocation().then(function (result) {
                if (!result) {
                    _this.gettingCurrentLocationFlag = false;
                    _this.componentCallback.emit({ 'response': false, 'reason': 'Failed to get geo location' });
                }
                else {
                    _this.getCurrentLocationInfo(result);
                }
            });
        }
    };
    //module initialization happens. function called by ngOninit and ngOnChange
    AutoCompleteComponent.prototype.moduleInit = function () {
        this.settings = this.setUserSettings();
        //condition to check if Radius is set without location detail.
        if (this.settings.geoRadius) {
            if (this.settings.geoLocation.length !== 2) {
                this.isSettingsError = true;
                this.settingsErrorMsg = this.settingsErrorMsg +
                    'Radius should be used with GeoLocation. Please use "geoLocation" key to set lat and lng. ';
            }
        }
        //condition to check if lat and lng is set and radious is not set then it will set to 20,000KM by default
        if ((this.settings.geoLocation.length === 2) && !this.settings.geoRadius) {
            this.settings.geoRadius = 20000000;
        }
        if (this.settings.showRecentSearch) {
            this.getRecentLocations();
        }
        if (!this.settings.useGoogleGeoApi) {
            if (!this.settings.geoPredictionServerUrl) {
                this.isSettingsError = true;
                this.settingsErrorMsg = this.settingsErrorMsg +
                    'Prediction custom server url is not defined. Please use "geoPredictionServerUrl" key to set. ';
            }
            if (!this.settings.geoLatLangServiceUrl) {
                this.isSettingsError = true;
                this.settingsErrorMsg = this.settingsErrorMsg +
                    'Latitude and longitude custom server url is not defined. Please use "geoLatLangServiceUrl" key to set. ';
            }
            if (!this.settings.geoLocDetailServerUrl) {
                this.isSettingsError = true;
                this.settingsErrorMsg = this.settingsErrorMsg +
                    'Location detail custom server url is not defined. Please use "geoLocDetailServerUrl" key to set. ';
            }
        }
        this.locationInput = this.settings.inputString;
    };
    //function to process the search query when pressed enter.
    AutoCompleteComponent.prototype.processSearchQuery = function () {
        if (this.queryItems.length) {
            if (this.selectedDataIndex > -1) {
                this.selectedListNode(this.selectedDataIndex);
            }
            else {
                this.selectedListNode(0);
            }
        }
    };
    //function to set user settings if it is available.
    AutoCompleteComponent.prototype.setUserSettings = function () {
        var _tempObj = {};
        if (this.userSettings && typeof (this.userSettings) === 'object') {
            var keys = Object.keys(this.defaultSettings);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var value = keys_1[_i];
                _tempObj[value] = (this.userSettings[value] !== undefined) ? this.userSettings[value] : this.defaultSettings[value];
            }
            return _tempObj;
        }
        else {
            return this.defaultSettings;
        }
    };
    //function to get the autocomplete list based on user input.
    AutoCompleteComponent.prototype.getListQuery = function (value) {
        var _this = this;
        this.recentDropdownOpen = false;
        if (this.settings.useGoogleGeoApi) {
            var _tempParams = {
                'query': value,
                'countryRestriction': this.settings.geoCountryRestriction,
                'geoTypes': this.settings.geoTypes
            };
            if (this.settings.geoLocation.length === 2) {
                _tempParams.geoLocation = this.settings.geoLocation;
                _tempParams.radius = this.settings.geoRadius;
            }
            this._autoCompleteSearchService.getGeoPrediction(_tempParams).then(function (result) {
                _this.updateListItem(result);
            });
        }
        else {
            this._autoCompleteSearchService.getPredictions(this.settings.geoPredictionServerUrl, value).then(function (result) {
                result = _this.extractServerList(_this.settings.serverResponseListHierarchy, result);
                _this.updateListItem(result);
            });
        }
    };
    //function to extratc custom data which is send by the server.
    AutoCompleteComponent.prototype.extractServerList = function (arrayList, data) {
        if (arrayList.length) {
            var _tempData = data;
            for (var _i = 0, arrayList_1 = arrayList; _i < arrayList_1.length; _i++) {
                var key = arrayList_1[_i];
                _tempData = _tempData[key];
            }
            return _tempData;
        }
        else {
            return data;
        }
    };
    //function to update the predicted list.
    AutoCompleteComponent.prototype.updateListItem = function (listData) {
        this.queryItems = listData ? listData : [];
        this.dropdownOpen = true;
    };
    //function to show the recent search result.
    AutoCompleteComponent.prototype.showRecentSearch = function () {
        var _this = this;
        this.recentDropdownOpen = true;
        this.dropdownOpen = true;
        this._autoCompleteSearchService.getRecentList(this.settings.recentStorageName).then(function (result) {
            if (result) {
                _this.queryItems = result;
            }
            else {
                _this.queryItems = [];
            }
        });
    };
    //function to navigate through list when up and down keyboard key is pressed;
    AutoCompleteComponent.prototype.navigateInList = function (keyCode) {
        var arrayIndex = 0;
        //arrow down
        if (keyCode === 40) {
            if (this.selectedDataIndex >= 0) {
                arrayIndex = ((this.selectedDataIndex + 1) <= (this.queryItems.length - 1)) ? (this.selectedDataIndex + 1) : 0;
            }
            this.activeListNode(arrayIndex);
        }
        else if (keyCode === 38) {
            if (this.selectedDataIndex >= 0) {
                arrayIndex = ((this.selectedDataIndex - 1) >= 0) ? (this.selectedDataIndex - 1) : (this.queryItems.length - 1);
            }
            else {
                arrayIndex = this.queryItems.length - 1;
            }
            this.activeListNode(arrayIndex);
        }
        else {
            this.processSearchQuery();
        }
    };
    //function to execute to get location detail based on latitude and longitude.
    AutoCompleteComponent.prototype.getCurrentLocationInfo = function (latlng) {
        var _this = this;
        if (this.settings.useGoogleGeoApi) {
            this._autoCompleteSearchService.getGeoLatLngDetail(latlng).then(function (result) {
                if (result) {
                    _this.setRecentLocation(result);
                }
                _this.gettingCurrentLocationFlag = false;
            });
        }
        else {
            this._autoCompleteSearchService.getLatLngDetail(this.settings.geoLatLangServiceUrl, latlng.lat, latlng.lng).then(function (result) {
                if (result) {
                    result = _this.extractServerList(_this.settings.serverResponseatLangHierarchy, result);
                    _this.setRecentLocation(result);
                }
                _this.gettingCurrentLocationFlag = false;
            });
        }
    };
    //function to retrive the location info based on goovle place id.
    AutoCompleteComponent.prototype.getPlaceLocationInfo = function (selectedData) {
        var _this = this;
        if (this.settings.useGoogleGeoApi) {
            this._autoCompleteSearchService.getGeoPlaceDetail(selectedData.place_id).then(function (data) {
                if (data) {
                    _this.setRecentLocation(data);
                }
            });
        }
        else {
            this._autoCompleteSearchService.getPlaceDetails(this.settings.geoLocDetailServerUrl, selectedData.place_id).then(function (result) {
                if (result) {
                    result = _this.extractServerList(_this.settings.serverResponseDetailHierarchy, result);
                    _this.setRecentLocation(result);
                }
            });
        }
    };
    //function to store the selected user search in the localstorage.
    AutoCompleteComponent.prototype.setRecentLocation = function (data) {
        data = JSON.parse(JSON.stringify(data));
        data.description = data.description ? data.description : data.formatted_address;
        data.active = false;
        this.selectedDataIndex = -1;
        this.locationInput = data.description;
        if (this.settings.showRecentSearch) {
            this._autoCompleteSearchService.addRecentList(this.settings.recentStorageName, data, this.settings.noOfRecentSearchSave);
            this.getRecentLocations();
        }
        this.userSelectedOption = data;
        //below code will execute only when user press enter or select any option selection and it emit a callback to the parent component.
        if (!this.settings.resOnSearchButtonClickOnly) {
            this.componentCallback.emit({ 'response': true, 'data': data });
        }
    };
    //function to retrive the stored recent user search from the localstorage.
    AutoCompleteComponent.prototype.getRecentLocations = function () {
        var _this = this;
        this._autoCompleteSearchService.getRecentList(this.settings.recentStorageName).then(function (data) {
            _this.recentSearchData = (data && data.length) ? data : [];
        });
    };
    return AutoCompleteComponent;
}());

AutoCompleteComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'ng4geo-autocomplete',
                template: "\n    <div class=\"custom-autocomplete\" *ngIf=\"!isSettingsError\">\n      <div class=\"custom-autocomplete__container\" >\n        <div class=\"custom-autocomplete__input\" [ngClass]=\"{'button-included':settings.showSearchButton}\">\n          <input  [(ngModel)]=\"locationInput\" (click)=\"searchinputClickCallback($event)\"  (keyup)=\"searchinputCallback($event)\"\n           type=\"search\" name=\"search\" id=\"search_places\" placeholder=\"{{settings.inputPlaceholderText}}\" autocomplete=\"off\">\n          <button class=\"search-icon\" *ngIf=\"settings.showSearchButton\" (click)=\"userQuerySubmit()\">\n            <i *ngIf=\"settings.searchIconUrl\" [ngStyle]=\"{'background-image': 'url(' + settings.searchIconUrl + ')'}\"></i>\n            <i *ngIf=\"!settings.searchIconUrl\" class=\"search-default-icon\"></i>\n          </button>\n        </div>\n        <pre class=\"custom-autocomplete__loader\" *ngIf=\"gettingCurrentLocationFlag\"><i class=\"gif\"></i></pre>\n      </div>\n      <ul class=\"custom-autocomplete__dropdown\" *ngIf=\"dropdownOpen && (settings.showCurrentLocation || queryItems.length)\">\n        <li *ngIf=\"settings.showCurrentLocation\" class=\"currentlocation\">\n          <a href=\"javascript:;\" (click)=\"currentLocationSelected()\">\n            <i class=\"location-icon\" *ngIf=\"settings.currentLocIconUrl\" [ngStyle]=\"{'background-image': 'url(' + settings.currentLocIconUrl + ')'}\"></i>Use Current Location\n            <i class=\"location-icon current-default-icon\" *ngIf=\"!settings.currentLocIconUrl\"></i>\n          </a>\n        </li>\n        <li class=\"heading heading-recent\" *ngIf=\"!recentDropdownOpen && queryItems.length\"><span>Locations</span><span class=\"line line-location\"></span></li>\n        <li class=\"heading heading-recent\" *ngIf=\"recentDropdownOpen && queryItems.length\">\n          <span>Recent Searches</span><span class=\"line line-recent\"></span>\n        </li>\n        <li *ngFor = \"let data of queryItems;let $index = index\" [ngClass]=\"{'active': data.active}\">\n          <a href=\"javascript:;\" (mouseover)=\"activeListNode($index)\" (click)=\"selectedListNode($index)\">\n            <i class=\"custom-icon\" *ngIf=\"settings.locationIconUrl\" [ngStyle]=\"{'background-image': 'url(' + settings.locationIconUrl + ')'}\"></i>\n            <i class=\"custom-icon location-default-icon\" *ngIf=\"!settings.locationIconUrl\"></i>\n              <span class=\"main-text\">\n                {{data.structured_formatting?.main_text ? data.structured_formatting.main_text : data.description}}\n              </span>\n              <span class=\"secondary_text\" *ngIf=\"data.structured_formatting?.secondary_text\">{{data.structured_formatting.secondary_text}}</span>\n          </a>\n        </li>\n      </ul>\n    </div>\n    <div class=\"custom-autocomplete--error\" *ngIf=\"isSettingsError\">{{settingsErrorMsg}}</div>\n  ",
                styles: ["\n    .custom-autocomplete {\n      display: block;\n      position: relative;\n      width: 100%;\n      float: left;\n    }\n\n    .custom-autocomplete a, .custom-autocomplete a:hover {\n      text-decoration: none;\n    }\n\n    .custom-autocomplete--error {\n      color: #fff;\n      background-color: #fd4f4f;\n      padding: 10px;\n    }\n\n    .custom-autocomplete__dropdown {\n      position: absolute;\n      background: #fff;\n      margin: 0;\n      padding: 0;\n      width: 100%;\n      list-style: none;\n      border: 1px solid #909090;\n      z-index: 99;\n      top: 50px;\n    }\n    .custom-autocomplete__dropdown li {\n      float: left;\n      width: 100%;\n      font-size: 15px;\n    }\n    .custom-autocomplete__dropdown a {\n      width: 100%;\n      color: #353535;\n      float: left;\n      padding: 8px 10px;\n    }\n    .custom-autocomplete__dropdown a:hover {\n      text-decoration: none;\n    }\n    .custom-autocomplete__dropdown .currentlocation {\n      text-transform: uppercase;\n      letter-spacing: 1px;\n    }\n    .custom-autocomplete__dropdown .currentlocation a {\n      padding: 10px 10px 10px 13px;\n      font-size: 14px;\n    }\n    .custom-autocomplete__dropdown .currentlocation a:hover {\n      background-color: #eeeded;\n    }\n    .custom-autocomplete__dropdown .currentlocation .location-icon {\n      width: 16px;\n      height: 16px;\n      background-size: cover;\n      background-image: url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDg3Ljg1OSA4Ny44NTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDg3Ljg1OSA4Ny44NTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8ZyBpZD0iTWFya2VyIj4KCQk8Zz4KCQkJPHBhdGggZD0iTTgwLjkzOCw0MC40ODNDNzkuMjk0LDIyLjcxMyw2NS4wOTMsOC41MjgsNDcuMzEyLDYuOTE3VjBoLTYuNzU3djYuOTE4QzIyLjc3Myw4LjUyOCw4LjU3MiwyMi43MTQsNi45Myw0MC40ODNIMHY2Ljc1NyAgICAgaDYuOTE5YzEuNTgyLDE3LjgzOCwxNS44MSwzMi4wODcsMzMuNjM2LDMzLjcwMXY2LjkxOGg2Ljc1N3YtNi45MThjMTcuODI2LTEuNjEzLDMyLjA1NC0xNS44NjIsMzMuNjM2LTMzLjcwMWg2LjkxMXYtNi43NTcgICAgIEg4MC45Mzh6IE00Ny4zMTIsNzQuMTQ2di02LjU1OGgtNi43NTd2Ni41NThDMjYuNDU3LDcyLjU4LDE1LjI0Miw2MS4zNDUsMTMuNzA4LDQ3LjI0aDYuNTY2di02Ljc1N2gtNi41NDkgICAgIGMxLjU5MS0xNC4wNDEsMTIuNzc3LTI1LjIxLDI2LjgyOS0yNi43NzF2Ni41NjRoNi43NTZ2LTYuNTY0YzE0LjA1MywxLjU2LDI1LjIzOSwxMi43MjksMjYuODMsMjYuNzcxaC02LjU1NnY2Ljc1N2g2LjU3MyAgICAgQzcyLjYyNSw2MS4zNDUsNjEuNDA5LDcyLjU4LDQ3LjMxMiw3NC4xNDZ6IE00My45MzQsMzMuNzI3Yy01LjU5NSwwLTEwLjEzNSw0LjUzMy0xMC4xMzUsMTAuMTMxICAgICBjMCw1LjU5OSw0LjU0LDEwLjEzOSwxMC4xMzUsMTAuMTM5czEwLjEzNC00LjU0LDEwLjEzNC0xMC4xMzlDNTQuMDY4LDM4LjI2LDQ5LjUyNywzMy43MjcsNDMuOTM0LDMzLjcyN3oiIGZpbGw9IiMwMDAwMDAiLz4KCQk8L2c+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==);\n      float: left;\n      margin-right: 10px;\n    }\n    .custom-autocomplete__dropdown .heading {\n      padding: 13px 10px 7px 13px;\n      text-transform: uppercase;\n      letter-spacing: 1px;\n      font-size: 13px;\n      position: relative;\n    }\n    .custom-autocomplete__dropdown .heading .line {\n      border-top: 1px solid #c2c2c2;\n      width: calc(100% - 115px);\n      display: inline-block;\n      position: absolute;\n      top: 21px;\n      left: 100px;\n    }\n\n    .custom-autocomplete__dropdown .heading .line-location {\n      left: 100px;\n      top: 16px;\n      width: calc(100% - 110px);\n    }\n\n    .custom-autocomplete__dropdown .heading .line-recent {\n      left: 158px;\n      top: 16px;\n      width: calc(100% - 168px);\n    }\n    .custom-autocomplete__dropdown .heading-recent {\n      padding-top: 8px;\n    }\n    .custom-autocomplete__dropdown .custom-icon {\n      width: 16px;\n      height: 16px;\n      background-size: cover;\n      vertical-align: bottom;\n      display: inline-block;\n      margin-right: 4px;\n    }\n    .custom-autocomplete__dropdown .main-text {\n      padding-right: 4px;\n      font-weight: 700;\n    }\n    .custom-autocomplete__dropdown .secondary_text {\n      font-size: 12px;\n      color: #909090;\n    }\n    .custom-autocomplete__dropdown .active a {\n      background-color: #ffe0cd;\n    }\n    .custom-autocomplete__loader {\n      position: absolute;\n      top: 0;\n      width: 100%;\n      height: 100%;\n      text-align: center;\n      background: white;\n    }\n    .custom-autocomplete__loader .gif {\n      background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzAiIGhlaWdodD0iMzAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDEwMCAxMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89InhNaWRZTWlkIiBjbGFzcz0idWlsLXJpcHBsZSI+PHBhdGggZmlsbD0ibm9uZSIgY2xhc3M9ImJrIiBkPSJNMCAwaDEwMHYxMDBIMHoiLz48Zz48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiBkdXI9IjJzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgYmVnaW49IjBzIiBrZXlUaW1lcz0iMDswLjMzOzEiIHZhbHVlcz0iMTsxOzAiLz48Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiByPSI0MCIgc3Ryb2tlPSIjYWZhZmI3IiBmaWxsPSJub25lIiBzdHJva2Utd2lkdGg9IjgiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCI+PGFuaW1hdGUgYXR0cmlidXRlTmFtZT0iciIgZHVyPSIycyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIGJlZ2luPSIwcyIga2V5VGltZXM9IjA7MC4zMzsxIiB2YWx1ZXM9IjA7MjI7NDQiLz48L2NpcmNsZT48L2c+PGc+PGFuaW1hdGUgYXR0cmlidXRlTmFtZT0ib3BhY2l0eSIgZHVyPSIycyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIGJlZ2luPSIxcyIga2V5VGltZXM9IjA7MC4zMzsxIiB2YWx1ZXM9IjE7MTswIi8+PGNpcmNsZSBjeD0iNTAiIGN5PSI1MCIgcj0iNDAiIHN0cm9rZT0iI2ZmYTYzMyIgZmlsbD0ibm9uZSIgc3Ryb2tlLXdpZHRoPSI4IiBzdHJva2UtbGluZWNhcD0icm91bmQiPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9InIiIGR1cj0iMnMiIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIiBiZWdpbj0iMXMiIGtleVRpbWVzPSIwOzAuMzM7MSIgdmFsdWVzPSIwOzIyOzQ0Ii8+PC9jaXJjbGU+PC9nPjwvc3ZnPg==);\n      background-size: cover;\n      width: 30px;\n      height: 30px;\n      top: 50%;\n      left: 50%;\n      transform: translate3d(-50%, -50%, 0);\n      position: absolute;\n    }\n    .custom-autocomplete__container,.custom-autocomplete__input {\n      width: inherit;\n      float: inherit;\n      position: relative;\n    }\n\n    .custom-autocomplete__input input{\n      margin: 0;\n      padding: 10px;\n      height: 50px;\n      border: 1px solid #ccc;\n      display: block;\n      width: 100%;\n      overflow: hidden;\n      text-overflow: ellipsis;\n      font-size: 16px;\n      &::-webkit-input-placeholder {\n         color: #868484;\n      }\n\n      &:-moz-placeholder { /* Firefox 18- */\n         color: #868484;\n      }\n\n      &::-moz-placeholder {  /* Firefox 19+ */\n         color: #868484;\n      }\n\n      &:-ms-input-placeholder {\n         color: #868484;\n      }\n    }\n\n    .button-included input{\n      padding-right: 60px;\n    }\n\n    .search-icon {\n      position: absolute;\n      right: 0;\n      width: 55px;\n      top: 0;\n      height: 100%;\n      background-color: transparent;\n      border-bottom: 0;\n      border-top: 0;\n      border-right: 0;\n      border-left: 1px solid #ccc;\n    }\n\n    .search-icon i {\n      background-size: cover;\n      height: 23px;\n      width: 23px;\n      display: inline-block;\n    }\n\n    .search-default-icon {\n      background-image: url('data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2Ljk2NiA1Ni45NjYiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU2Ljk2NiA1Ni45NjY7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMTZweCIgaGVpZ2h0PSIxNnB4Ij4KPHBhdGggZD0iTTU1LjE0Niw1MS44ODdMNDEuNTg4LDM3Ljc4NmMzLjQ4Ni00LjE0NCw1LjM5Ni05LjM1OCw1LjM5Ni0xNC43ODZjMC0xMi42ODItMTAuMzE4LTIzLTIzLTIzcy0yMywxMC4zMTgtMjMsMjMgIHMxMC4zMTgsMjMsMjMsMjNjNC43NjEsMCw5LjI5OC0xLjQzNiwxMy4xNzctNC4xNjJsMTMuNjYxLDE0LjIwOGMwLjU3MSwwLjU5MywxLjMzOSwwLjkyLDIuMTYyLDAuOTIgIGMwLjc3OSwwLDEuNTE4LTAuMjk3LDIuMDc5LTAuODM3QzU2LjI1NSw1NC45ODIsNTYuMjkzLDUzLjA4LDU1LjE0Niw1MS44ODd6IE0yMy45ODQsNmM5LjM3NCwwLDE3LDcuNjI2LDE3LDE3cy03LjYyNiwxNy0xNywxNyAgcy0xNy03LjYyNi0xNy0xN1MxNC42MSw2LDIzLjk4NCw2eiIgZmlsbD0iIzAwMDAwMCIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K');\n    }\n\n    .location-default-icon {\n      background-image: url('data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ4Ny43MjQgNDg3LjcyNCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDg3LjcyNCA0ODcuNzI0OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCI+CjxnPgoJPGc+CgkJPHBhdGggZD0iTTIzNi45MjUsMC4xMjRjLTk2LjksMy40LTE3Ny40LDc5LTE4Ni43LDE3NS41Yy0xLjksMTkuMy0wLjgsMzgsMi42LDU1LjlsMCwwYzAsMCwwLjMsMi4xLDEuMyw2LjEgICAgYzMsMTMuNCw3LjUsMjYuNCwxMy4xLDM4LjZjMTkuNSw0Ni4yLDY0LjYsMTIzLjUsMTY1LjgsMjA3LjZjNi4yLDUuMiwxNS4zLDUuMiwyMS42LDBjMTAxLjItODQsMTQ2LjMtMTYxLjMsMTY1LjktMjA3LjcgICAgYzUuNy0xMi4yLDEwLjEtMjUuMSwxMy4xLTM4LjZjMC45LTMuOSwxLjMtNi4xLDEuMy02LjFsMCwwYzIuMy0xMiwzLjUtMjQuMywzLjUtMzYuOUM0MzguNDI1LDg0LjcyNCwzNDcuNTI1LTMuNzc2LDIzNi45MjUsMC4xMjQgICAgeiBNMjQzLjgyNSwyOTEuMzI0Yy01Mi4yLDAtOTQuNS00Mi4zLTk0LjUtOTQuNXM0Mi4zLTk0LjUsOTQuNS05NC41czk0LjUsNDIuMyw5NC41LDk0LjVTMjk2LjAyNSwyOTEuMzI0LDI0My44MjUsMjkxLjMyNHoiIGZpbGw9IiMwMDAwMDAiLz4KCTwvZz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K');\n    }\n\n    .current-default-icon {\n      background-image: url('data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjE2cHgiIGhlaWdodD0iMTZweCIgdmlld0JveD0iMCAwIDg3Ljg1OSA4Ny44NTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDg3Ljg1OSA4Ny44NTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPGc+Cgk8ZyBpZD0iTWFya2VyIj4KCQk8Zz4KCQkJPHBhdGggZD0iTTgwLjkzOCw0MC40ODNDNzkuMjk0LDIyLjcxMyw2NS4wOTMsOC41MjgsNDcuMzEyLDYuOTE3VjBoLTYuNzU3djYuOTE4QzIyLjc3Myw4LjUyOCw4LjU3MiwyMi43MTQsNi45Myw0MC40ODNIMHY2Ljc1NyAgICAgaDYuOTE5YzEuNTgyLDE3LjgzOCwxNS44MSwzMi4wODcsMzMuNjM2LDMzLjcwMXY2LjkxOGg2Ljc1N3YtNi45MThjMTcuODI2LTEuNjEzLDMyLjA1NC0xNS44NjIsMzMuNjM2LTMzLjcwMWg2LjkxMXYtNi43NTcgICAgIEg4MC45Mzh6IE00Ny4zMTIsNzQuMTQ2di02LjU1OGgtNi43NTd2Ni41NThDMjYuNDU3LDcyLjU4LDE1LjI0Miw2MS4zNDUsMTMuNzA4LDQ3LjI0aDYuNTY2di02Ljc1N2gtNi41NDkgICAgIGMxLjU5MS0xNC4wNDEsMTIuNzc3LTI1LjIxLDI2LjgyOS0yNi43NzF2Ni41NjRoNi43NTZ2LTYuNTY0YzE0LjA1MywxLjU2LDI1LjIzOSwxMi43MjksMjYuODMsMjYuNzcxaC02LjU1NnY2Ljc1N2g2LjU3MyAgICAgQzcyLjYyNSw2MS4zNDUsNjEuNDA5LDcyLjU4LDQ3LjMxMiw3NC4xNDZ6IE00My45MzQsMzMuNzI3Yy01LjU5NSwwLTEwLjEzNSw0LjUzMy0xMC4xMzUsMTAuMTMxICAgICBjMCw1LjU5OSw0LjU0LDEwLjEzOSwxMC4xMzUsMTAuMTM5czEwLjEzNC00LjU0LDEwLjEzNC0xMC4xMzlDNTQuMDY4LDM4LjI2LDQ5LjUyNywzMy43MjcsNDMuOTM0LDMzLjcyN3oiIGZpbGw9IiMwMDAwMDAiLz4KCQk8L2c+Cgk8L2c+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==');\n    }\n\n    .custom-autocomplete__container .searchpage {\n      margin-top: 0;\n      padding: 0;\n      height: 55px;\n      border: none;\n    }\n\n  "],
                host: {
                    '(document:click)': 'closeAutocomplete($event)',
                }
            },] },
];
/** @nocollapse */
AutoCompleteComponent.ctorParameters = function () { return [
    { type: Object, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"],] },] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
    { type: _windowRef_service__WEBPACK_IMPORTED_MODULE_2__["GlobalRef"], },
    { type: _auto_complete_service__WEBPACK_IMPORTED_MODULE_3__["AutoCompleteSearchService"], },
]; };
AutoCompleteComponent.propDecorators = {
    'userSettings': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'componentCallback': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
};
//# sourceMappingURL=auto-complete.component.js.map

/***/ }),

/***/ "./node_modules/ng4-geoautocomplete/auto-complete.service.js":
/*!*******************************************************************!*\
  !*** ./node_modules/ng4-geoautocomplete/auto-complete.service.js ***!
  \*******************************************************************/
/*! exports provided: AutoCompleteSearchService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutoCompleteSearchService", function() { return AutoCompleteSearchService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _windowRef_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./windowRef.service */ "./node_modules/ng4-geoautocomplete/windowRef.service.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./storage.service */ "./node_modules/ng4-geoautocomplete/storage.service.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");






var AutoCompleteSearchService = (function () {
    function AutoCompleteSearchService(_http, platformId, _global, _localStorageService) {
        this._http = _http;
        this.platformId = platformId;
        this._global = _global;
        this._localStorageService = _localStorageService;
    }
    AutoCompleteSearchService.prototype.getPredictions = function (url, query) {
        var _this = this;
        return new Promise(function (resolve) {
            _this._http.get(url, { params: { query: query } })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data) {
                    resolve(data);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AutoCompleteSearchService.prototype.getLatLngDetail = function (url, lat, lng) {
        var _this = this;
        return new Promise(function (resolve) {
            _this._http.get(url, { params: { lat: lat, lng: lng } })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data) {
                    resolve(data);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AutoCompleteSearchService.prototype.getPlaceDetails = function (url, placeId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this._http.get(url, { params: { query: placeId } })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                if (data) {
                    resolve(data);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AutoCompleteSearchService.prototype.getGeoCurrentLocation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(_this.platformId)) {
                var _window = _this._global.nativeGlobal;
                if (_window.navigator.geolocation) {
                    _window.navigator.geolocation.getCurrentPosition(function (pos) {
                        var latlng = { lat: parseFloat(pos.coords.latitude + ''), lng: parseFloat(pos.coords.longitude + '') };
                        resolve(latlng);
                    }, function (error) {
                        resolve(false);
                    });
                }
                else {
                    resolve(false);
                }
            }
            else {
                resolve(false);
            }
        });
    };
    AutoCompleteSearchService.prototype.getGeoLatLngDetail = function (latlng) {
        var _this = this;
        return new Promise(function (resolve) {
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(_this.platformId)) {
                var _window = _this._global.nativeGlobal;
                var geocoder = new _window.google.maps.Geocoder;
                geocoder.geocode({ 'location': latlng }, function (results, status) {
                    if (status === 'OK') {
                        _this.getGeoPlaceDetail(results[0].place_id).then(function (result) {
                            if (result) {
                                resolve(result);
                            }
                            else {
                                resolve(false);
                            }
                        });
                    }
                    else {
                        resolve(false);
                    }
                });
            }
            else {
                resolve(false);
            }
        });
    };
    AutoCompleteSearchService.prototype.getGeoPrediction = function (params) {
        var _this = this;
        return new Promise(function (resolve) {
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(_this.platformId)) {
                var _window = _this._global.nativeGlobal;
                var placesService = new _window.google.maps.places.AutocompleteService();
                var queryInput = {};
                var promiseArr = [];
                if (params.countryRestriction.length) {
                    queryInput = {
                        input: params.query,
                        componentRestrictions: { country: params.countryRestriction },
                    };
                }
                else {
                    queryInput = {
                        input: params.query
                    };
                }
                if (params.geoLocation) {
                    queryInput.location = new _window.google.maps.LatLng(parseFloat(params.geoLocation[0]), parseFloat(params.geoLocation[1]));
                    queryInput.radius = params.radius;
                }
                if (params.geoTypes.length) {
                    for (var i = 0; i < params.geoTypes.length; i++) {
                        var _tempQuery = queryInput;
                        _tempQuery['types'] = new Array(params.geoTypes[i]);
                        promiseArr.push(_this.geoPredictionCall(placesService, _tempQuery));
                    }
                }
                else {
                    promiseArr.push(_this.geoPredictionCall(placesService, queryInput));
                }
                Promise.all(promiseArr).then(function (values) {
                    var val = values;
                    if (val.length > 1) {
                        var _tempArr = [];
                        for (var j = 0; j < val.length; j++) {
                            if (val[j] && val[j].length) {
                                _tempArr = _tempArr.concat(val[j]);
                            }
                        }
                        _tempArr = _this.getUniqueResults(_tempArr);
                        resolve(_tempArr);
                    }
                    else {
                        resolve(values[0]);
                    }
                });
            }
            else {
                resolve(false);
            }
        });
    };
    AutoCompleteSearchService.prototype.getGeoPlaceDetail = function (placeId) {
        var _this = this;
        return new Promise(function (resolve) {
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(_this.platformId)) {
                var _window = _this._global.nativeGlobal;
                var placesService = new _window.google.maps.places.PlacesService(document.createElement('div'));
                placesService.getDetails({ 'placeId': placeId }, function (result, status) {
                    if (result == null || result.length === 0) {
                        _this.getGeoPaceDetailByReferance(result.referance).then(function (referanceData) {
                            if (!referanceData) {
                                resolve(false);
                            }
                            else {
                                resolve(referanceData);
                            }
                        });
                    }
                    else {
                        resolve(result);
                    }
                });
            }
            else {
                resolve(false);
            }
        });
    };
    AutoCompleteSearchService.prototype.getGeoPaceDetailByReferance = function (referance) {
        var _this = this;
        return new Promise(function (resolve) {
            if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(_this.platformId)) {
                var _window_1 = _this._global.nativeGlobal;
                var placesService = new _window_1.google.maps.places.PlacesService();
                placesService.getDetails({ 'reference': referance }, function (result, status) {
                    if (status === _window_1.google.maps.places.PlacesServiceStatus.OK) {
                        resolve(result);
                    }
                    else {
                        resolve(false);
                    }
                });
            }
            else {
                resolve(false);
            }
        });
    };
    AutoCompleteSearchService.prototype.addRecentList = function (localStorageName, result, itemSavedLength) {
        var _this = this;
        this.getRecentList(localStorageName).then(function (data) {
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].description === result.description) {
                        data.splice(i, 1);
                        break;
                    }
                }
                data.unshift(result);
                if (data.length > itemSavedLength) {
                    data.pop();
                }
                _this._localStorageService.setItem(localStorageName, JSON.stringify(data));
            }
        });
    };
    ;
    AutoCompleteSearchService.prototype.getRecentList = function (localStorageName) {
        var _this = this;
        return new Promise(function (resolve) {
            var value = _this._localStorageService.getItem(localStorageName);
            if (value) {
                value = JSON.parse(value);
            }
            else {
                value = [];
            }
            resolve(value);
        });
    };
    AutoCompleteSearchService.prototype.getUniqueResults = function (arr) {
        return Array.from(arr.reduce(function (m, t) { return m.set(t.place_id, t); }, new Map()).values());
    };
    AutoCompleteSearchService.prototype.geoPredictionCall = function (placesService, queryInput) {
        var _window = this._global.nativeGlobal;
        return new Promise(function (resolve) {
            placesService.getPlacePredictions(queryInput, function (result, status) {
                if (status === _window.google.maps.places.PlacesServiceStatus.OK) {
                    resolve(result);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    return AutoCompleteSearchService;
}());

AutoCompleteSearchService.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/** @nocollapse */
AutoCompleteSearchService.ctorParameters = function () { return [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], },
    { type: Object, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"],] },] },
    { type: _windowRef_service__WEBPACK_IMPORTED_MODULE_2__["GlobalRef"], },
    { type: _storage_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"], },
]; };
//# sourceMappingURL=auto-complete.service.js.map

/***/ }),

/***/ "./node_modules/ng4-geoautocomplete/index.js":
/*!***************************************************!*\
  !*** ./node_modules/ng4-geoautocomplete/index.js ***!
  \***************************************************/
/*! exports provided: Ng4GeoautocompleteModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ng_4_geoautocomplete_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ng-4-geoautocomplete.module */ "./node_modules/ng4-geoautocomplete/ng-4-geoautocomplete.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ng4GeoautocompleteModule", function() { return _ng_4_geoautocomplete_module__WEBPACK_IMPORTED_MODULE_0__["Ng4GeoautocompleteModule"]; });


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/ng4-geoautocomplete/ng-4-geoautocomplete.module.js":
/*!*************************************************************************!*\
  !*** ./node_modules/ng4-geoautocomplete/ng-4-geoautocomplete.module.js ***!
  \*************************************************************************/
/*! exports provided: Ng4GeoautocompleteModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng4GeoautocompleteModule", function() { return Ng4GeoautocompleteModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _auto_complete_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auto-complete.component */ "./node_modules/ng4-geoautocomplete/auto-complete.component.js");
/* harmony import */ var _auto_complete_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auto-complete.service */ "./node_modules/ng4-geoautocomplete/auto-complete.service.js");
/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./storage.service */ "./node_modules/ng4-geoautocomplete/storage.service.js");
/* harmony import */ var _windowRef_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./windowRef.service */ "./node_modules/ng4-geoautocomplete/windowRef.service.js");








var Ng4GeoautocompleteModule = (function () {
    function Ng4GeoautocompleteModule() {
    }
    Ng4GeoautocompleteModule.forRoot = function () {
        return {
            ngModule: Ng4GeoautocompleteModule
        };
    };
    return Ng4GeoautocompleteModule;
}());

Ng4GeoautocompleteModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [
                    _auto_complete_component__WEBPACK_IMPORTED_MODULE_4__["AutoCompleteComponent"]
                ],
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _angular_http__WEBPACK_IMPORTED_MODULE_2__["HttpModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
                ],
                exports: [
                    _auto_complete_component__WEBPACK_IMPORTED_MODULE_4__["AutoCompleteComponent"]
                ],
                providers: [{ provide: _windowRef_service__WEBPACK_IMPORTED_MODULE_7__["GlobalRef"], useClass: _windowRef_service__WEBPACK_IMPORTED_MODULE_7__["BrowserGlobalRef"] }, _auto_complete_service__WEBPACK_IMPORTED_MODULE_5__["AutoCompleteSearchService"], _storage_service__WEBPACK_IMPORTED_MODULE_6__["LocalStorageService"]]
            },] },
];
/** @nocollapse */
Ng4GeoautocompleteModule.ctorParameters = function () { return []; };
//# sourceMappingURL=ng-4-geoautocomplete.module.js.map

/***/ }),

/***/ "./node_modules/ng4-geoautocomplete/storage.service.js":
/*!*************************************************************!*\
  !*** ./node_modules/ng4-geoautocomplete/storage.service.js ***!
  \*************************************************************/
/*! exports provided: LocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return LocalStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var LocalStorageService = (function () {
    function LocalStorageService() {
    }
    LocalStorageService.prototype.setItem = function (key, value) {
        localStorage.setItem(key, value);
    };
    LocalStorageService.prototype.getItem = function (key) {
        return localStorage.getItem(key);
    };
    LocalStorageService.prototype.removeItem = function (key) {
        localStorage.removeItem(key);
    };
    return LocalStorageService;
}());

LocalStorageService.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/** @nocollapse */
LocalStorageService.ctorParameters = function () { return []; };
//# sourceMappingURL=storage.service.js.map

/***/ }),

/***/ "./node_modules/ng4-geoautocomplete/windowRef.service.js":
/*!***************************************************************!*\
  !*** ./node_modules/ng4-geoautocomplete/windowRef.service.js ***!
  \***************************************************************/
/*! exports provided: GlobalRef, BrowserGlobalRef */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalRef", function() { return GlobalRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserGlobalRef", function() { return BrowserGlobalRef; });
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var GlobalRef = (function () {
    function GlobalRef() {
    }
    return GlobalRef;
}());

var BrowserGlobalRef = (function (_super) {
    __extends(BrowserGlobalRef, _super);
    function BrowserGlobalRef() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(BrowserGlobalRef.prototype, "nativeGlobal", {
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    return BrowserGlobalRef;
}(GlobalRef));

//# sourceMappingURL=windowRef.service.js.map

/***/ }),

/***/ "./src/app/layout/base-layout/base-layout.component.html":
/*!***************************************************************!*\
  !*** ./src/app/layout/base-layout/base-layout.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div\r\n  [ngClass]=\"{\r\n    'closed-sidebar': globals.toggleSidebar,\r\n    'closed-sidebar-md': globals.toggleSidebarMobile,\r\n    'settings-open': globals.toggleThemeOptions,\r\n    'closed-sidebar-open': globals.sidebarHover || globals.toggleSidebarMobile,\r\n    'header-menu-open': globals.toggleHeaderMobile,\r\n    'drawer-open': globals.toggleDrawer,\r\n    'fixed-footer': globals.toggleFixedFooter\r\n  }\"\r\n  [class]=\"'app-container app-theme-white'\"\r\n>\r\n  <!-- <app-header></app-header> -->\r\n  <app-landing-header></app-landing-header>\r\n  <!-- <div *ngIf=\" appState.showSideBar === false\"> -->\r\n  <app-sidebar></app-sidebar>\r\n  <!-- </div> -->\r\n  <!-- <div *ngIf=\" appState.showSideBar === true\"> -->\r\n  <!-- <app-sidebar ></app-sidebar> -->\r\n  <!-- </div> -->\r\n\r\n  <div class=\"app-main__outer\">\r\n    <div class=\"app-main__inner\">\r\n      <!-- <div [@architectUIAnimation]=\"o.isActivated ? o.activatedRoute : ''\"> -->\r\n      <router-outlet #o=\"outlet\"></router-outlet>\r\n      <!-- </div> -->\r\n    </div>\r\n    <!-- <app-footer></app-footer> -->\r\n  </div>\r\n  <app-options-drawer></app-options-drawer>\r\n  <app-drawer></app-drawer>\r\n  <div class=\"sidebar-menu-overlay\" (click)=\"toggleSidebarMobile()\"></div>\r\n</div>\r\n<!-- <ngx-loading-bar [color]=\"'var(--primary)'\"></ngx-loading-bar> -->\r\n"

/***/ }),

/***/ "./src/app/layout/base-layout/base-layout.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/layout/base-layout/base-layout.component.ts ***!
  \*************************************************************/
/*! exports provided: BaseLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseLayoutComponent", function() { return BaseLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_redux_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular-redux/store */ "./node_modules/@angular-redux/store/lib/src/index.js");
/* harmony import */ var _angular_redux_store__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_angular_redux_store__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ThemeOptions_store_config_actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ThemeOptions/store/config.actions */ "./src/app/ThemeOptions/store/config.actions.ts");
/* harmony import */ var _theme_options__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../theme-options */ "./src/app/theme-options.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_app_state_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/app-state.service */ "./src/app/services/app-state.service.ts");









var BaseLayoutComponent = /** @class */ (function () {
    function BaseLayoutComponent(globals, configActions, router, appState) {
        this.globals = globals;
        this.configActions = configActions;
        this.router = router;
        this.appState = appState;
        console.log("base layout");
    }
    BaseLayoutComponent.prototype.toggleSidebarMobile = function () {
        this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_redux_store__WEBPACK_IMPORTED_MODULE_2__["select"])('config'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"])
    ], BaseLayoutComponent.prototype, "config$", void 0);
    BaseLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-base-layout',
            template: __webpack_require__(/*! ./base-layout.component.html */ "./src/app/layout/base-layout/base-layout.component.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["trigger"])('architectUIAnimation', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':enter, :leave', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({
                                opacity: 0,
                                display: 'flex',
                                flex: '1',
                                transform: 'translateY(-20px)',
                                flexDirection: 'column'
                            }),
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])('600ms ease', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 1, transform: 'translateY(0)' })),
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':leave', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])('600ms ease', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({ opacity: 0, transform: 'translateY(-20px)' })),
                        ], { optional: true })
                    ]),
                ])
            ]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_theme_options__WEBPACK_IMPORTED_MODULE_5__["ThemeOptions"], _ThemeOptions_store_config_actions__WEBPACK_IMPORTED_MODULE_4__["ConfigActions"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"], src_app_services_app_state_service__WEBPACK_IMPORTED_MODULE_8__["AppStateService"]])
    ], BaseLayoutComponent);
    return BaseLayoutComponent;
}());



/***/ }),

/***/ "./src/app/profile/dashboard/dashboard.component.html":
/*!************************************************************!*\
  !*** ./src/app/profile/dashboard/dashboard.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>\r\n\tDashboard coming soon\r\n</h2>\r\n"

/***/ }),

/***/ "./src/app/profile/dashboard/dashboard.component.scss":
/*!************************************************************!*\
  !*** ./src/app/profile/dashboard/dashboard.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/profile/dashboard/dashboard.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/profile/dashboard/dashboard.component.ts ***!
  \**********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var _theme_options__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../theme-options */ "./src/app/theme-options.ts");




var DashboardComponent = /** @class */ (function () {
    /**
     *
     * @param appState get variables and methods from app state
     * @param globals global theme options
     */
    function DashboardComponent(appState, globals) {
        this.appState = appState;
        this.globals = globals;
        appState.showSideBar = true;
    }
    DashboardComponent.prototype.ngOnInit = function () { };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/profile/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/profile/dashboard/dashboard.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_app_state_service__WEBPACK_IMPORTED_MODULE_2__["AppStateService"], _theme_options__WEBPACK_IMPORTED_MODULE_3__["ThemeOptions"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile-modal/profile-modal.component.html":
/*!********************************************************************!*\
  !*** ./src/app/profile/profile-modal/profile-modal.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"model-window\">\r\n\t<div class=\"header-common\">\r\n\t\t<div class=\"modal-header\" style=\"padding: 0px 0px;\">\r\n\t\t\t<h5 class=\"modal-title\" id=\"modal-basic-title\" style=\"font-family: 'Montserrat', sans-serif;\">\r\n\t\t\t\t<span class=\"profileSummary\">\r\n\t\t\t\t\t<span [ngClass]=\"setImage\"></span>\r\n\t\t\t\t</span>\r\n\t\t\t\t<span class=\"m-h\">{{ heading }}</span>\r\n\t\t\t</h5>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"ct-form-builder\">\r\n\t\t<div *ngIf=\"profDetIndividual\" class=\"col-xl-12 margin-h-center p-0\">\r\n\t\t\t<app-ct-form-builder [formFieldsJson]=\"profDetailsIndJson\" [modelData]=\"profDetailsIndModelData\"\r\n\t\t\t\t(saveDataEmitter)=\"\r\n\t\t\t\t\tcreateData($event, 'professionalDetailsIndividual')\r\n\t\t\t\t\" (updateDataEmitter)=\"\r\n\t\t\t\t\tupdateData($event, 'professionalDetailsIndividual')\r\n\t\t\t\t\" (cancelDataEmitter)=\"cancelData($event)\">\r\n\t\t\t</app-ct-form-builder>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"reactive-forms\">\r\n\t\t<div class=\"summary\" *ngIf=\"profileSummaryFlag\">\r\n\t\t\t<form class=\"form p-10\" name=\"form\" [formGroup]=\"form\">\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<!-- <h6 style=\" font-family: 'Montserrat', sans-serif;padding-left: 40px;\">Summary</h6><br> -->\r\n\t\t\t\t\t<div class=\"row margin-h-center\">\r\n\t\t\t\t\t\t<div class=\"col-xl-12\">\r\n\t\t\t\t\t\t\t<label class=\"p-l-16\">Summary</label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group ml-3 mr-2\">\r\n\t\t\t\t\t\t\t\t<textarea rows=\"5\" formControlName=\"profileSummary\" name=\"profileSummary\"\r\n\t\t\t\t\t\t\t\t\ttype=\"textarea\"\r\n\t\t\t\t\t\t\t\t\tplaceholder=\"Write a paragraph about experience summary,specialities,key highlights,etc.(Not more than 1000 words)\"\r\n\t\t\t\t\t\t\t\t\tclass=\"form-control\">\r\n\t\t\t\t\t\t\t\t</textarea>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</form>\r\n\t\t</div>\r\n\t\t<div class=\"education\" *ngIf=\"educationFlag\">\r\n\t\t\t<div class=\"form p-10\">\r\n\t\t\t\t<form name=\"form\" [formGroup]=\"form\">\r\n\t\t\t\t\t<div class=\"row margin-h-center m-b-4\">\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Country<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<select (change)=\"countryChange($event.target.value)\" class=\"mb-2 form-control\"\r\n\t\t\t\t\t\t\t\tformControlName=\"country\">\r\n\t\t\t\t\t\t\t\t<option *ngFor=\"let option of country\" [value]=\"option.value\">{{ option.label }}\r\n\t\t\t\t\t\t\t\t</option>\r\n\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<h6>\r\n\t\t\t\t\t\t\t\tSearch for Colleges/Universities<span class=\"mandatory_star\">&nbsp;*</span>\r\n\t\t\t\t\t\t\t</h6>\r\n\t\t\t\t\t\t\t<ng4geo-autocomplete [userSettings]=\"locationConfig\" (componentCallback)=\"\r\n\t\t\t\t\t\t\t\t\tautoCompleteCallback1($event)\r\n\t\t\t\t\t\t\t\t\">\r\n\t\t\t\t\t\t\t</ng4geo-autocomplete>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row margin-h-center m-b-4\">\r\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>College</label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<input formControlName=\"college\" disabled name=\"college\" type=\"text\" class=\"form-control\" />\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row margin-h-center m-b-4\">\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Education<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<input formControlName=\"education\" type=\"text\" class=\"form-control\" />\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Specialization<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<input formControlName=\"specialization\" type=\"text\" class=\"form-control\" />\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row margin-h-center m-b-4\">\r\n\t\t\t\t\t\t<!-- <div class=\"col-lg-7 col-md-12 col-sm-12 col-xs-12\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n                  <label>From Date<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n                  <input formControlName=\"from\" type=\"date\" placeholder=\"Enter From Date\" class=\"form-control\"></div>\r\n                <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n                  <label>To Date</label>\r\n                  <input formControlName=\"to\" type=\"date\" placeholder=\"Enter To Date\" class=\"form-control\">\r\n                </div>\r\n              </div>\r\n            </div> -->\r\n\r\n\t\t\t\t\t\t<div class=\"col-lg-7 col-md-12 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>From Date<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t\t\t<input formControlName=\"from\" type=\"date\" placeholder=\"Enter From Date\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tclass=\"form-control\" (change)=\"toDateValidation('from')\" />\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>To Date</label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t<input formControlName=\"to\" type=\"date\" placeholder=\"Enter To Date\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"form-control\" (change)=\"toDateValidation('to')\"/>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-lg-5 col-md-12 col-sm-12 col-xs-12 m-b-4-m\">\r\n\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-6 col-md-4 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>CGPA(Max)<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t<input formControlName=\"cgpaMax\" type=\"number\" class=\"form-control\" />\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-6 col-md-4 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>CGPA(Scored)<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t<input formControlName=\"cgpaScored\" type=\"number\" class=\"form-control\" />\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<!-- <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\r\n                  <label>Grade<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n                  <input formControlName=\"grade\" type=\"text\" class=\"form-control\">\r\n                </div> -->\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row margin-h-center m-b-4\">\r\n\t\t\t\t\t\t<div class=\"col-lg-7 col-md-12 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<div class=\"row\">\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>Grade<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t<input formControlName=\"grade\" type=\"text\" class=\"form-control\" />\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>Percentile<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t<input formControlName=\"percentile\" type=\"number\" class=\"form-control\" />\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t\t\t<label>Percentage<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t\t\t<input formControlName=\"percentage\" type=\"number\" class=\"form-control\" />\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<!-- <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"></div> -->\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row margin-h-center\">\r\n\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Key Highlights<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<textarea rows=\"2\" formControlName=\"keyHighlights\" name=\"percentage\" type=\"textarea\"\r\n\t\t\t\t\t\t\t\tplaceholder=\"\" class=\"form-control\">\r\n\t\t\t\t\t\t\t</textarea>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"skills\" *ngIf=\"skillsFlag\">\r\n\t\t\t<div class=\"form p-10\">\r\n\t\t\t\t<form name=\"form\" [formGroup]=\"form\">\r\n\t\t\t\t\t<div class=\"row margin-h-center\">\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Skillset Category<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<ng-multiselect-dropdown name=\"city\" [placeholder]=\"'Skillset Category'\" [data]=\"skillsCat\"\r\n\t\t\t\t\t\t\t\tformControlName=\"skillCategory\" [disabled]=\"disabled\" [settings]=\"dropdownSettings\">\r\n\t\t\t\t\t\t\t</ng-multiselect-dropdown>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Skillset<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<ng-multiselect-dropdown name=\"city\" [placeholder]=\"'skillset'\" [data]=\"skillsSet\"\r\n\t\t\t\t\t\t\t\tformControlName=\"skillSet\" [disabled]=\"disabled\" [settings]=\"dropdownSettings\">\r\n\t\t\t\t\t\t\t</ng-multiselect-dropdown>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row margin-h-center m-t-10\">\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Self-Assessment(out of 10)<span class=\"mandatory_star\">&nbsp;*</span></label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<div tabindex=\"-1\" role=\"group\">\r\n\t\t\t\t\t\t\t<select class=\"mb-2 form-control\" formControlName=\"selfAssessmentScore\">\r\n\t\t\t\t\t\t\t\t<option *ngFor=\"let option of selfAssessmentArray\" [value]=\"option.value\">\r\n\t\t\t\t\t\t\t\t\t{{ option.label }}\r\n\t\t\t\t\t\t\t\t</option>\r\n\t\t\t\t\t\t\t</select>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\">\r\n\t\t\t\t\t\t\t<label>Ratings from Endorser(out of 10)</label>\r\n\t\t\t\t\t\t\t<fieldset class=\"form-group\">\r\n\t\t\t\t\t\t\t\t<input formControlName=\"rating\" type=\"text\" class=\"form-control\" disabled />\r\n\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div *ngIf=\"showcaseFlag\">\r\n\t\t\t<div class=\"upload-docs\">\r\n\t\t\t\t<div class=\"upload-img\">\r\n\t\t\t\t\t<div class=\"dropzone\" ng2FileDrop [ngClass]=\"{ 'nv-file-over': hasBaseDropZoneOver }\"\r\n\t\t\t\t\t\t(fileOver)=\"fileOverBase($event)\" [uploader]=\"uUploader\" (onFileDrop)=\"onFileSelected($event)\"\r\n\t\t\t\t\t\t[disabled]=\"disableUpload\">\r\n\t\t\t\t\t\t<span>\r\n\t\t\t\t\t\t\t<img class=\"image-p-m-b\" src=\"../../assets/icons/storage.svg\" />\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t<div class=\"\">\r\n\t\t\t\t\t\t\tDrag and drop or\r\n\t\t\t\t\t\t\t<label class=\"\">\r\n\t\t\t\t\t\t\t\t<input class=\"file-input\" type=\"file\" ng2FileSelect [uploader]=\"uUploader\"\r\n\t\t\t\t\t\t\t\t\t(onFileSelected)=\"onFileSelected($event)\" [disabled]=\"disableUpload\" />\r\n\t\t\t\t\t\t\t\t<span class=\"file-label\">\r\n\t\t\t\t\t\t\t\t\tbrowse\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</label>\r\n\t\t\t\t\t\t\tyour file\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"description\">\r\n\t\t\t\t\t<form class=\"form\" name=\"form\" [formGroup]=\"form\">\r\n\t\t\t\t\t\t<div class=\"row margin-h-center width-95\">\r\n\t\t\t\t\t\t\t<div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-h-center p-0\">\r\n\t\t\t\t\t\t\t\t<label class=\"p-l-16\">Description</label>\r\n\t\t\t\t\t\t\t\t<fieldset class=\"form-group ml-3 mr-2\">\r\n\t\t\t\t\t\t\t\t\t<textarea rows=\"2\" formControlName=\"description\" name=\"description\" type=\"textarea\"\r\n\t\t\t\t\t\t\t\t\t\tplaceholder=\"Please enter a short description\" class=\"form-control\">\r\n\t\t\t\t\t\t\t\t\t</textarea>\r\n\t\t\t\t\t\t\t\t</fieldset>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</form>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"uploded-image\">\r\n\t\t\t\t<div class=\"row margin-h-center width-95 p-0 m-b-10\"\r\n\t\t\t\t\t*ngFor=\"let item of uUploader.queue; let i = index\">\r\n\t\t\t\t\t<div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\">\r\n\t\t\t\t\t\t<span>\r\n\t\t\t\t\t\t\t<!-- <img src=\"../../assets/icons/storage.svg\" class=\"width-30px\"> -->\r\n\t\t\t\t\t\t\t<i class=\"{{ getFileExtension(item?.file?.type) }}\" style=\"color: #4199ff;\"></i>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n\t\t\t\t\t\t<span>{{ item?.file?.name }}</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-2\">\r\n\t\t\t\t\t\t<span>{{\r\n\t\t\t\t\t\t\t\t((item?.file?.size / 1024 / 1024\r\n\t\t\t\t\t\t\t\t\t| number: '.2') *\r\n\t\t\t\t\t\t\t\t\titem.progress) /\r\n\t\t\t\t\t\t\t\t\t100\r\n\t\t\t\t\t\t\t}}\r\n\t\t\t\t\t\t\t/\r\n\t\t\t\t\t\t\t{{\r\n\t\t\t\t\t\t\t\titem?.file?.size / 1024 / 1024 | number: '.2'\r\n\t\t\t\t\t\t\t}}</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\">\r\n\t\t\t\t\t\t<span *ngIf=\"item.isSuccess\">\r\n\t\t\t\t\t\t\t<a tooltip=\"Remove from queue\" (click)=\"removeFromQue(item, i)\" class=\"float-right\">\r\n\t\t\t\t\t\t\t\t<img src=\"../../assets/icons/red-fail.svg\" class=\"pointer width-50\" align=\"right\" />\r\n\t\t\t\t\t\t\t\t<!-- <mat-icon class=\"color-grey pointer\" color=\"red\" >clear</mat-icon> -->\r\n\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"progress-bar progress-bar-success\" [ngStyle]=\"{\r\n\t\t\t\t\t\t\twidth: item.progress + '%',\r\n\t\t\t\t\t\t\theight: '2px',\r\n\t\t\t\t\t\t\tmargin: 'auto 10px',\r\n\t\t\t\t\t\t\t'margin-top': '4px'\r\n\t\t\t\t\t\t}\" tooltip=\"{{ item.progress }}% uploaded\" placement=\"bottom\"></div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"reactive-forms-buttons\" *ngIf=\"!profDetIndividual\">\r\n\t\t\t<div class=\"modal-footer align-items-center justify-content-center\">\r\n\t\t\t\t<button (click)=\"cancel()\" type=\"submit\" class=\"btn btn-outline-secondary btn-group-lg\"\r\n\t\t\t\t\tstyle=\"height: 40px; width: 120px;\">\r\n\t\t\t\t\t<span class=\"font-size-md\"> CANCEL</span>\r\n\t\t\t\t</button>\r\n\t\t\t\t<!-- {{form.valid}} -->\r\n\t\t\t\t<button (click)=\"save(form.value)\" type=\"submit\" style=\"height: 40px; width: 120px;\"\r\n\t\t\t\t\tclass=\"btn btn-primary btn-group-lg\" [disabled]=\"!form.valid\">\r\n\t\t\t\t\t<span class=\"font-size-lg\"> SAVE</span>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "./src/app/profile/profile-modal/profile-modal.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/profile/profile-modal/profile-modal.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-right {\n  float: right;\n  margin: 10px; }\n\n#search_places {\n  display: block;\n  width: 100%;\n  height: calc(2.25rem + 2px);\n  padding: 0.375rem 0.75rem;\n  font-size: 1rem;\n  font-weight: 400;\n  line-height: 1.5;\n  color: #495057;\n  background-color: #fff;\n  background-clip: padding-box;\n  border: 1px solid #ced4da;\n  border-radius: 0.25rem;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out; }\n\n.profile {\n  color: white;\n  width: 65px;\n  height: 60px;\n  padding-left: 10px; }\n\nspan.profileSummary {\n  background-size: contain;\n  display: inline-block;\n  height: 54px;\n  width: 56px;\n  z-index: 0;\n  background-color: #4199ff;\n  border-top-left-radius: 4px; }\n\nspan.profileSum {\n  background: url('profileSum.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 40px;\n  width: 47px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\nspan.professional {\n  background: url('professional.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 50px;\n  width: 50px;\n  z-index: 0; }\n\nspan.experience {\n  background: url('experience.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\n::ng-deep .search-icon {\n  display: none !important; }\n\n::ng-deep input[type='search'] {\n  display: block !important;\n  width: 100% !important;\n  height: calc(2.35rem + 2px) !important;\n  padding: 0.375rem 0.75rem !important;\n  font-size: 1rem !important;\n  font-weight: 400 !important;\n  line-height: 1.5 !important;\n  color: #495057 !important;\n  background-color: #fff !important;\n  background-clip: padding-box !important;\n  border: 1px solid #ced4da !important;\n  border-radius: 0.25rem !important; }\n\n.p-10 {\n  padding: 10px; }\n\n.m-t-10 {\n  margin-top: 10px; }\n\nspan.skills {\n  background: url('experience.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\n.m-h {\n  padding-left: 10px;\n  padding-bottom: 12px;\n  position: relative;\n  bottom: 6px;\n  font-size: 16px;\n  font-family: 'Montserrat';\n  font-weight: 550;\n  color: #3b3b3b; }\n\n.p-l-16 {\n  padding-left: 16px; }\n\n.p-l-r-10 {\n  padding-left: 10px;\n  padding-right: 10px; }\n\n::ng-deep .modal-dialog {\n  box-shadow: none !important; }\n\n.m-b-4 {\n  margin-bottom: 4px !important; }\n\n@media only screen and (max-width: 769px) {\n  .m-b-4-m {\n    margin-top: 4px !important; } }\n\nspan.summary {\n  background: url('profileSum.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 12px;\n  margin-top: 8px; }\n\nspan.education {\n  background: url('page-1.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\nspan.professional {\n  background: url('experience.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\nspan.achievements {\n  background: url('achievement.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\nspan.skills {\n  background: url('skills.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\nspan.showcase {\n  background: url('showcase-1.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 10px;\n  margin-top: 6px; }\n\n.mandatory_star {\n  color: red; }\n\n.my-drop-zone {\n  border: dotted 3px lightgray;\n  height: 80px; }\n\n.nv-file-over {\n  border: dotted 3px red; }\n\n.dropzone {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n  font-weight: 100;\n  height: 200px;\n  border: 0.1em dashed #6cb0fd;\n  border-radius: 5px;\n  background: #fafafa; }\n\n.upload-img {\n  margin: 20px auto;\n  position: relative;\n  width: 90%; }\n\n.dropzone.hovering {\n  border: 2px solid #f16624;\n  color: #dadada !important; }\n\n.dropzone .file-label {\n  font-size: 1.2em;\n  color: #4199ff;\n  font-weight: 400; }\n\nprogress::-webkit-progress-value {\n  transition: width 0.1s ease; }\n\n.file-input {\n  display: none; }\n\n.my-drop-zone {\n  border: 0.1em dashed #6cb0fd; }\n\n.nv-file-over {\n  border: 0.1em dashed grey; }\n\n.image-p-m-b {\n  width: 50%;\n  margin-bottom: 10px; }\n\n.width-90 {\n  width: 90%; }\n\n.width-95 {\n  width: 95%; }\n\n.p-0 {\n  padding: 0; }\n\n.pointer {\n  cursor: pointer; }\n\n.float-right {\n  float: right !important; }\n\n.width-50 {\n  width: 50%; }\n\n.width-30 {\n  width: 30%; }\n\n.m-b-10 {\n  margin-bottom: 10px; }\n\n.width-30px {\n  width: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLW1vZGFsL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxccHJvZmlsZVxccHJvZmlsZS1tb2RhbFxccHJvZmlsZS1tb2RhbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQVk7RUFDWixZQUFZLEVBQUE7O0FBR2I7RUFDQyxjQUFjO0VBQ2QsV0FBVztFQUNYLDJCQUEyQjtFQUMzQix5QkFBeUI7RUFDekIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLHNCQUFzQjtFQUN0Qiw0QkFBNEI7RUFDNUIseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0Qix3RUFBd0UsRUFBQTs7QUFHekU7RUFDQyxZQUFZO0VBQ1osV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLHlCQUF5QjtFQUN6QiwyQkFBMkIsRUFBQTs7QUFHNUI7RUFDQyxvREFBMEU7RUFDMUUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLHNEQUE0RTtFQUM1RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVSxFQUFBOztBQUdYO0VBQ0Msb0RBQTBFO0VBQzFFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyx3QkFBd0IsRUFBQTs7QUFHekI7RUFDQyx5QkFBeUI7RUFDekIsc0JBQXNCO0VBRXRCLHNDQUFzQztFQUN0QyxvQ0FBb0M7RUFDcEMsMEJBQTBCO0VBQzFCLDJCQUEyQjtFQUMzQiwyQkFBMkI7RUFDM0IseUJBQXlCO0VBQ3pCLGlDQUFpQztFQUNqQyx1Q0FBdUM7RUFDdkMsb0NBQW9DO0VBQ3BDLGlDQUFpQyxFQUFBOztBQUdsQztFQUNDLGFBQWEsRUFBQTs7QUFHZDtFQUNDLGdCQUFnQixFQUFBOztBQUdqQjtFQUNDLG9EQUEwRTtFQUMxRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBR2hCO0VBQ0Msa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIsV0FBVztFQUNYLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHZjtFQUNDLGtCQUFrQixFQUFBOztBQUduQjtFQUNDLGtCQUFrQjtFQUNsQixtQkFBbUIsRUFBQTs7QUFFcEI7RUFDQywyQkFBMkIsRUFBQTs7QUFHNUI7RUFDQyw2QkFBNkIsRUFBQTs7QUFHOUI7RUFDQztJQUNDLDBCQUEwQixFQUFBLEVBQzFCOztBQUdGO0VBQ0Msb0RBQTBFO0VBQzFFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsaUJBQWlCO0VBQ2QsZUFBZSxFQUFBOztBQUduQjtFQUNDLGdEQUFzRTtFQUN0RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBR2hCO0VBQ0Msb0RBQTBFO0VBQzFFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxxREFBMkU7RUFDM0Usd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLGdEQUFzRTtFQUN0RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBR2hCO0VBQ0Msb0RBQTBFO0VBQzFFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxVQUFVLEVBQUE7O0FBR1g7RUFDQyw0QkFBNEI7RUFDNUIsWUFBWSxFQUFBOztBQUViO0VBQ0Msc0JBQXNCLEVBQUE7O0FBR3ZCO0VBQ0MsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsNEJBQTRCO0VBQzVCLGtCQUFrQjtFQUNsQixtQkFBbUIsRUFBQTs7QUFHcEI7RUFDQyxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLFVBQVUsRUFBQTs7QUFHWDtFQUNDLHlCQUF5QjtFQUN6Qix5QkFBeUIsRUFBQTs7QUFHMUI7RUFDQyxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGdCQUFnQixFQUFBOztBQUdqQjtFQUNDLDJCQUEyQixFQUFBOztBQUc1QjtFQUNDLGFBQWEsRUFBQTs7QUFHZDtFQUNDLDRCQUE0QixFQUFBOztBQUc3QjtFQUNDLHlCQUF5QixFQUFBOztBQUcxQjtFQUNDLFVBQVU7RUFDVixtQkFBbUIsRUFBQTs7QUFHcEI7RUFDQyxVQUFVLEVBQUE7O0FBR1g7RUFDQyxVQUFVLEVBQUE7O0FBR1g7RUFDQyxVQUFVLEVBQUE7O0FBR1g7RUFDQyxlQUFlLEVBQUE7O0FBR2hCO0VBQ0MsdUJBQXVCLEVBQUE7O0FBR3hCO0VBQ0MsVUFBVSxFQUFBOztBQUdYO0VBQ0MsVUFBVSxFQUFBOztBQUdYO0VBQ0MsbUJBQW1CLEVBQUE7O0FBR3BCO0VBQ0MsV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLW1vZGFsL3Byb2ZpbGUtbW9kYWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLXJpZ2h0IHtcclxuXHRmbG9hdDogcmlnaHQ7XHJcblx0bWFyZ2luOiAxMHB4O1xyXG59XHJcblxyXG4jc2VhcmNoX3BsYWNlcyB7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0aGVpZ2h0OiBjYWxjKDIuMjVyZW0gKyAycHgpO1xyXG5cdHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XHJcblx0Zm9udC1zaXplOiAxcmVtO1xyXG5cdGZvbnQtd2VpZ2h0OiA0MDA7XHJcblx0bGluZS1oZWlnaHQ6IDEuNTtcclxuXHRjb2xvcjogIzQ5NTA1NztcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG5cdGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgI2NlZDRkYTtcclxuXHRib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xyXG5cdHRyYW5zaXRpb246IGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLnByb2ZpbGUge1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHR3aWR0aDogNjVweDtcclxuXHRoZWlnaHQ6IDYwcHg7XHJcblx0cGFkZGluZy1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG5zcGFuLnByb2ZpbGVTdW1tYXJ5IHtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogNTRweDtcclxuXHR3aWR0aDogNTZweDtcclxuXHR6LWluZGV4OiAwO1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICM0MTk5ZmY7XHJcblx0Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNHB4O1xyXG59XHJcblxyXG5zcGFuLnByb2ZpbGVTdW0ge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Byb2ZpbGVTdW0uc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA0MHB4O1xyXG5cdHdpZHRoOiA0N3B4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDhweDtcclxuXHRtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbnNwYW4ucHJvZmVzc2lvbmFsIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9wcm9mZXNzaW9uYWwuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA1MHB4O1xyXG5cdHdpZHRoOiA1MHB4O1xyXG5cdHotaW5kZXg6IDA7XHJcbn1cclxuXHJcbnNwYW4uZXhwZXJpZW5jZSB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZXhwZXJpZW5jZS5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDM1cHg7XHJcblx0d2lkdGg6IDM3cHg7XHJcblx0ei1pbmRleDogMTtcclxuXHRtYXJnaW4tbGVmdDogOHB4O1xyXG5cdG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zZWFyY2gtaWNvbiB7XHJcblx0ZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgaW5wdXRbdHlwZT0nc2VhcmNoJ10ge1xyXG5cdGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcblx0d2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuXHQvLyBoZWlnaHQ6IGNhbGMoMi4yNXJlbSArIDJweCkgIWltcG9ydGFudDtcclxuXHRoZWlnaHQ6IGNhbGMoMi4zNXJlbSArIDJweCkgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtICFpbXBvcnRhbnQ7XHJcblx0Zm9udC1zaXplOiAxcmVtICFpbXBvcnRhbnQ7XHJcblx0Zm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xyXG5cdGxpbmUtaGVpZ2h0OiAxLjUgIWltcG9ydGFudDtcclxuXHRjb2xvcjogIzQ5NTA1NyAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtY29sb3I6ICNmZmYgIWltcG9ydGFudDtcclxuXHRiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94ICFpbXBvcnRhbnQ7XHJcblx0Ym9yZGVyOiAxcHggc29saWQgI2NlZDRkYSAhaW1wb3J0YW50O1xyXG5cdGJvcmRlci1yYWRpdXM6IDAuMjVyZW0gIWltcG9ydGFudDtcclxufVxyXG5cclxuLnAtMTAge1xyXG5cdHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5tLXQtMTAge1xyXG5cdG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcbnNwYW4uc2tpbGxzIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9leHBlcmllbmNlLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMzVweDtcclxuXHR3aWR0aDogMzdweDtcclxuXHR6LWluZGV4OiAxO1xyXG5cdG1hcmdpbi1sZWZ0OiA4cHg7XHJcblx0bWFyZ2luLXRvcDogNXB4O1xyXG59XHJcblxyXG4ubS1oIHtcclxuXHRwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcblx0cGFkZGluZy1ib3R0b206IDEycHg7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdGJvdHRvbTogNnB4O1xyXG5cdGZvbnQtc2l6ZTogMTZweDtcclxuXHRmb250LWZhbWlseTogJ01vbnRzZXJyYXQnO1xyXG5cdGZvbnQtd2VpZ2h0OiA1NTA7XHJcblx0Y29sb3I6ICMzYjNiM2I7XHJcbn1cclxuXHJcbi5wLWwtMTYge1xyXG5cdHBhZGRpbmctbGVmdDogMTZweDtcclxufVxyXG5cclxuLnAtbC1yLTEwIHtcclxuXHRwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcblx0cGFkZGluZy1yaWdodDogMTBweDtcclxufVxyXG46Om5nLWRlZXAgLm1vZGFsLWRpYWxvZyB7XHJcblx0Ym94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubS1iLTQge1xyXG5cdG1hcmdpbi1ib3R0b206IDRweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OXB4KSB7XHJcblx0Lm0tYi00LW0ge1xyXG5cdFx0bWFyZ2luLXRvcDogNHB4ICFpbXBvcnRhbnQ7XHJcblx0fVxyXG59XHJcblxyXG5zcGFuLnN1bW1hcnkge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Byb2ZpbGVTdW0uc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzNXB4O1xyXG5cdHdpZHRoOiAzN3B4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDEycHg7XHJcbiAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbn1cclxuXHJcbnNwYW4uZWR1Y2F0aW9uIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9wYWdlLTEuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzNXB4O1xyXG5cdHdpZHRoOiAzN3B4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDhweDtcclxuXHRtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbnNwYW4ucHJvZmVzc2lvbmFsIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9leHBlcmllbmNlLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMzVweDtcclxuXHR3aWR0aDogMzdweDtcclxuXHR6LWluZGV4OiAxO1xyXG5cdG1hcmdpbi1sZWZ0OiA4cHg7XHJcblx0bWFyZ2luLXRvcDogNXB4O1xyXG59XHJcblxyXG5zcGFuLmFjaGlldmVtZW50cyB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvYWNoaWV2ZW1lbnQuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzNXB4O1xyXG5cdHdpZHRoOiAzN3B4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDhweDtcclxuXHRtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbnNwYW4uc2tpbGxzIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9za2lsbHMuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzNXB4O1xyXG5cdHdpZHRoOiAzN3B4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDhweDtcclxuXHRtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbnNwYW4uc2hvd2Nhc2Uge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Nob3djYXNlLTEuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzNXB4O1xyXG5cdHdpZHRoOiAzN3B4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDEwcHg7XHJcblx0bWFyZ2luLXRvcDogNnB4O1xyXG59XHJcblxyXG4ubWFuZGF0b3J5X3N0YXIge1xyXG5cdGNvbG9yOiByZWQ7XHJcbn1cclxuXHJcbi5teS1kcm9wLXpvbmUge1xyXG5cdGJvcmRlcjogZG90dGVkIDNweCBsaWdodGdyYXk7XHJcblx0aGVpZ2h0OiA4MHB4O1xyXG59XHJcbi5udi1maWxlLW92ZXIge1xyXG5cdGJvcmRlcjogZG90dGVkIDNweCByZWQ7XHJcbn1cclxuXHJcbi5kcm9wem9uZSB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0Zm9udC13ZWlnaHQ6IDEwMDtcclxuXHRoZWlnaHQ6IDIwMHB4O1xyXG5cdGJvcmRlcjogMC4xZW0gZGFzaGVkICM2Y2IwZmQ7XHJcblx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdGJhY2tncm91bmQ6ICNmYWZhZmE7XHJcbn1cclxuXHJcbi51cGxvYWQtaW1nIHtcclxuXHRtYXJnaW46IDIwcHggYXV0bztcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0d2lkdGg6IDkwJTtcclxufVxyXG5cclxuLmRyb3B6b25lLmhvdmVyaW5nIHtcclxuXHRib3JkZXI6IDJweCBzb2xpZCAjZjE2NjI0O1xyXG5cdGNvbG9yOiAjZGFkYWRhICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5kcm9wem9uZSAuZmlsZS1sYWJlbCB7XHJcblx0Zm9udC1zaXplOiAxLjJlbTtcclxuXHRjb2xvcjogIzQxOTlmZjtcclxuXHRmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG5wcm9ncmVzczo6LXdlYmtpdC1wcm9ncmVzcy12YWx1ZSB7XHJcblx0dHJhbnNpdGlvbjogd2lkdGggMC4xcyBlYXNlO1xyXG59XHJcblxyXG4uZmlsZS1pbnB1dCB7XHJcblx0ZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLm15LWRyb3Atem9uZSB7XHJcblx0Ym9yZGVyOiAwLjFlbSBkYXNoZWQgIzZjYjBmZDtcclxufVxyXG5cclxuLm52LWZpbGUtb3ZlciB7XHJcblx0Ym9yZGVyOiAwLjFlbSBkYXNoZWQgZ3JleTtcclxufVxyXG5cclxuLmltYWdlLXAtbS1iIHtcclxuXHR3aWR0aDogNTAlO1xyXG5cdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi53aWR0aC05MCB7XHJcblx0d2lkdGg6IDkwJTtcclxufVxyXG5cclxuLndpZHRoLTk1IHtcclxuXHR3aWR0aDogOTUlO1xyXG59XHJcblxyXG4ucC0wIHtcclxuXHRwYWRkaW5nOiAwO1xyXG59XHJcblxyXG4ucG9pbnRlciB7XHJcblx0Y3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uZmxvYXQtcmlnaHQge1xyXG5cdGZsb2F0OiByaWdodCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ud2lkdGgtNTAge1xyXG5cdHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcbi53aWR0aC0zMCB7XHJcblx0d2lkdGg6IDMwJTtcclxufVxyXG5cclxuLm0tYi0xMCB7XHJcblx0bWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLndpZHRoLTMwcHgge1xyXG5cdHdpZHRoOiAzMHB4O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/profile/profile-modal/profile-modal.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-modal/profile-modal.component.ts ***!
  \******************************************************************/
/*! exports provided: ProfileModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModalComponent", function() { return ProfileModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_add_observable_of__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/observable/of */ "./node_modules/rxjs-compat/_esm5/add/observable/of.js");
/* harmony import */ var rxjs_add_operator_filter__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/filter */ "./node_modules/rxjs-compat/_esm5/add/operator/filter.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/fesm5/ng2-file-upload.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var _services_attachment_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/attachment.service */ "./src/app/services/attachment.service.ts");
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-validation */ "./node_modules/ng2-validation/dist/index.js");
/* harmony import */ var ng2_validation__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(ng2_validation__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _utilities_customValidators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../utilities/customValidators */ "./src/app/utilities/customValidators.ts");












var URL = 'api/attachment/uploadDirectToS3';
var ProfileModalComponent = /** @class */ (function () {
    /**
     *
     * @param activeModal to get active model window for pop up
     * @param formBuilder reactive form builder
     * @param inventFundApi service to get data from end points
     * @param attachmentService attachment service returns the s3 folder structure
     * @param appState get variables and methods from app state
     */
    function ProfileModalComponent(activeModal, formBuilder, inventFundApi, attachmentService, appState) {
        this.activeModal = activeModal;
        this.formBuilder = formBuilder;
        this.inventFundApi = inventFundApi;
        this.attachmentService = attachmentService;
        this.appState = appState;
        // global variables
        // attachment
        this.uUploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_6__["FileUploader"]({
            url: URL,
            additionalParameter: { type: 'inventFund' },
            headers: [
            // {
            //   name: 'Accept',
            //   value: 'application/pdf',
            // },
            ],
            allowedFileType: ['image', 'video', 'pdf', 'doc'],
            maxFileSize: 20 * 1024 * 1024,
            queueLimit: 1,
        });
        this.hasBaseDropZoneOver = false;
        this.uiImageInObj = {
            locationLocal: 'assets/img/icons/plain-white-background.jpg',
            locationS3Url: '',
            newLocation: '',
            certificateType: '',
            awsS3FileLocation: '',
            type: 'showcase',
            fileName: '',
            size: '',
            data: '',
        };
        this.arrayToEmit = [];
        this.disableUpload = false;
        this.iconList = [
            // array of icon class list based on type
            { type: 'xlsx', icon: 'fa fa-file-excel fa-2x' },
            { type: 'pdf', icon: 'fa fa-file-pdf fa-2x' },
            { type: 'jpg', icon: 'fa fa-file-image fa-2x' },
            { type: 'mp4', icon: 'fa fa-file-image fa-2x' },
            { type: 'jpeg', icon: 'fa fa-file-image fa-2x' },
        ];
        this.passEntry = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.formlyData = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.locationConfig = {
            geoCountryRestriction: ['in'],
            geoTypes: ['establishment'],
        };
        this.country = [];
        this.skillsFlag = false;
        this.skills = [];
        this.dropdownSettings = {};
        this.disabled = false;
        this.ShowFilter = true;
        this.showAll = true;
        this.limitSelection = false;
        this.selectedItems = [];
        this.skillsCat = [];
        this.skillsSet = [];
        this.educationFlag = false;
        this.selfAssessmentArray = [];
    }
    // ngOnInit
    ProfileModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        // console.log("data received:" + JSON.stringify(this.data));
        this.heading = this.data.heading;
        if (this.data.hasOwnProperty('profileSummary')) {
            this.setImage = 'summary';
            this.form = this.formBuilder.group({
                profileSummary: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(1000),
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('(?!^ +$)^.+$'),
                    ]),
                ],
            });
            this.profileSummaryFlag = true;
            var pfSummary = this.data.profileSummary;
            // this.form.controls['profileSummary'].setValue[this.data.profileSummary]
            this.form.controls['profileSummary'].setValue(pfSummary);
        }
        else if (this.data.hasOwnProperty('educationalDetails')) {
            this.setImage = 'education';
            // console.log('Educational details Data:' + JSON.stringify(this.data))
            this.heading = this.data.headline;
            this.country = this.data.educationalDetails;
            this.educationFlag = true;
            this.typeOfOperation = this.data.operation;
            var date = new Date();
            // console.log('locationConfig===>'+JSON.stringify(this.locationConfig))
            this.form = this.formBuilder.group({
                country: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
                address: [null],
                college: [null],
                education: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _utilities_customValidators__WEBPACK_IMPORTED_MODULE_11__["alphaValidator"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('(?!^ +$)^.+$'),
                    ]),
                ],
                specialization: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _utilities_customValidators__WEBPACK_IMPORTED_MODULE_11__["alphaValidator"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('(?!^ +$)^.+$'),
                    ]),
                ],
                from: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].minDate('1980-12-12'),
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].maxDate(date),
                    ]),
                ],
                to: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])],
                grade: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _utilities_customValidators__WEBPACK_IMPORTED_MODULE_11__["alphaNumericValidator"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('(?!^ +$)^.+$'),
                    ]),
                ],
                cgpaMax: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].min(1),
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].max(100),
                    ]),
                ],
                cgpaScored: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].min(1),
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].max(100),
                    ]),
                ],
                percentile: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].min(1),
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].max(100),
                    ]),
                ],
                percentage: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].min(1),
                        ng2_validation__WEBPACK_IMPORTED_MODULE_10__["CustomValidators"].max(100),
                    ]),
                ],
                keyHighlights: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required,
                        _utilities_customValidators__WEBPACK_IMPORTED_MODULE_11__["alphaNumericValidator"],
                        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('(?!^ +$)^.+$'),
                    ]),
                ],
            });
            if (this.typeOfOperation == 'update') {
                // console.log('Update data->' + JSON.stringify(this.data.updateData))
                for (var c in this.form.controls) {
                    // console.log('C--->' + JSON.stringify(c))
                    this.form.controls[c].setValue(this.data.updateData[c]);
                    this.updateId = this.data.updateData._id;
                }
            }
        }
        else if (this.data.hasOwnProperty('profDetailsJson')) {
            this.setImage = 'professional';
            // console.log('data received in modal:' + JSON.stringify(this.data))
            this.profDetIndividual = true;
            this.formly = true;
            this.profDetailsIndJson = this.data.profDetailsJson;
            this.heading = this.data.headline;
            this.profDetailsIndModelData = this.data.model[0];
        }
        else if (this.data.hasOwnProperty('achievementsDetails')) {
            this.setImage = 'achievements';
            // console.log('data received in modal:' + JSON.stringify(this.data))
            this.profDetIndividual = true;
            this.formly = true;
            this.profDetailsIndJson = this.data.achievementsDetails;
            this.heading = this.data.headline;
            this.typeOfOperation = this.data.operation;
            if (this.typeOfOperation == 'update') {
                // console.log('Update data->' + JSON.stringify(this.data.updateData))
                this.profDetailsIndModelData = this.data.updateData;
                this.updateId = this.data.updateData._id;
            }
        }
        else if (this.data.hasOwnProperty('skillsJson')) {
            this.setImage = 'skills';
            // console.log('Educational details Data:' + JSON.stringify(this.data))
            this.heading = this.data.headline;
            var modelData = this.data.updateData;
            this.skillsFlag = true;
            this.skillsSet = this.data.skillSet;
            this.skillsCat = this.data.skillCategory;
            this.selfAssessmentArray = this.data.selfAssessment;
            // console.log('model details Data:' + JSON.stringify(modelData.rating))
            // console.log('Educational details Data:' + JSON.stringify(this.skillsCat))
            this.typeOfOperation = this.data.operation;
            this.dropdownSettings = {
                singleSelection: false,
                defaultOpen: false,
                idField: 'value',
                textField: 'label',
                selectAllText: 'Select All',
                unSelectAllText: 'UnSelect All',
                enableCheckAll: false,
                itemsShowLimit: 3,
                allowSearchFilter: true,
            };
            this.form = this.formBuilder.group({
                skillCategory: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
                skillSet: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
                selfAssessmentScore: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
                rating: [10],
            });
            if (this.typeOfOperation == 'update') {
                this.form.controls['skillCategory'].setValue(modelData.skillCategory);
                this.form.controls['skillSet'].setValue(modelData.skillSet);
                this.form.controls['selfAssessmentScore'].setValue(modelData.selfAssessmentScore);
                if (modelData.rating) {
                    this.form.controls['rating'].setValue(modelData.rating);
                }
                this.updateId = this.data.updateData._id;
            }
        }
        else if (this.data.hasOwnProperty('showcase')) {
            this.heading = this.data.headline;
            this.setImage = 'showcase';
            this.showcaseFlag = true;
            // console.log('In showcase:' + JSON.stringify(this.data))
            this.form = this.formBuilder.group({
                description: [
                    null,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern('(?!^ +$)^.+$')]),
                ],
            });
            this.form.controls['description'].disable();
            this.uUploader.onBuildItemForm = function (item, form) {
                // console.log("append type : " + JSON.stringify(this.attachmentService.getImagePath('SHOWCASE', this.appState.encryptedDataValue('personId'))))
                form.append('TYPE', _this.attachmentService.getImagePath('SHOWCASE', _this.appState.encryptedDataValue('personId')));
                // form.append("TYPE", 'ShowCase');
            };
            this.uUploader.onCompleteItem = function (item, response, status, headers) {
                var data = response;
                // var image = JSON.parse(data);var data: any = response;
                // var image = JSON.parse(data);
                // console.log("response data : " + JSON.parse(data));
                console.log('response data : ' + response);
                _this.uiImageInObj.awsS3FileLocation = JSON.parse(response).signedData;
                _this.uiImageInObj.locationLocal = JSON.parse(response).signedData;
                _this.uiImageInObj.locationS3Url = JSON.parse(response).signedData;
                _this.uiImageInObj.newLocation = JSON.parse(response).signedData;
                _this.uiImageInObj.data = JSON.parse(response).data;
                // this.uiImageInObj.certificateType = this.form.controls['description'].value
                var data = _this.uiImageInObj;
                _this.disableUpload = true;
                var json = JSON.stringify(_this.uiImageInObj);
                // console.log("final data : " + json)
                _this.arrayToEmit.push(_this.uiImageInObj);
                _this.form.controls['description'].enable();
            };
        }
    };
    // validate and save data
    ProfileModalComponent.prototype.save = function (value) {
        // this.activeModal.close({ value, 'action': this.typeOfOperation, 'id': this.updateId });
        if (this.data.hasOwnProperty('showcase')) {
            this.showCaseDataSend(value);
        }
        else {
            for (var c in this.form.controls) {
                this.form.controls[c].markAsTouched();
            }
            if (this.form.valid) {
                // console.log("Valid! ");
                // console.log("data for attachment : " + JSON.stringify(this.arrayToEmit))
                this.activeModal.close({
                    value: value,
                    action: this.typeOfOperation,
                    id: this.updateId,
                });
            }
            else {
                // console.log("Form invalid!");
            }
        }
    };
    // emit show case data
    ProfileModalComponent.prototype.showCaseDataSend = function (data) {
        console.log('data for des : ' + JSON.stringify(data));
        console.log('data for url save : ' + JSON.stringify(this.arrayToEmit));
        if (data && this.arrayToEmit.length) {
            var obj = {
                imageUrl: this.arrayToEmit[0].awsS3FileLocation,
                imageType: this.arrayToEmit[0].type,
                description: data.description,
                data: this.arrayToEmit[0].data,
            };
            console.log('data for obj : ' + JSON.stringify(obj));
            this.activeModal.close({ obj: obj });
        }
    };
    // ng4geo auto complete callback
    ProfileModalComponent.prototype.autoCompleteCallback1 = function (selectedData) {
        // console.log('data in auto complete:' + JSON.stringify(selectedData))
        this.form.controls['address'].setValue(selectedData.data);
        this.form.controls['college'].setValue(selectedData.data.name);
    };
    // cancel button to navigate to component
    ProfileModalComponent.prototype.cancel = function () {
        this.activeModal.close({ action: 'cancelled' });
    };
    // create data in the component
    ProfileModalComponent.prototype.createData = function (event, type) {
        // console.log('Create data event and type:' + JSON.stringify(event) + ' ' + type)
        var profileIndParams = {
            data: event,
            type: type,
            action: 'create',
        };
        this.activeModal.close(profileIndParams);
    };
    // update data in the component
    ProfileModalComponent.prototype.updateData = function (event, type) {
        // console.log('Update data event and type:' + JSON.stringify(event) + ' ' + type)
        var profileIndParams = {
            data: event,
            type: type,
            action: 'update',
        };
        this.activeModal.close(profileIndParams);
    };
    // close active model
    ProfileModalComponent.prototype.cancelData = function (event) {
        this.activeModal.close({ action: 'cancelled' });
    };
    // ng4geo country change
    ProfileModalComponent.prototype.countryChange = function (event) {
        // alert('country change event')
        this.locationConfig.geoCountryRestriction = [];
        // console.log('locationConfig:' + JSON.stringify(this.locationConfig))
        this.locationConfig.geoCountryRestriction.push(event);
    };
    // toggle hover
    ProfileModalComponent.prototype.toggleHover = function (event) {
        this.isHovering = event;
    };
    // file upload hover
    ProfileModalComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    // on file select emit url
    ProfileModalComponent.prototype.onFileSelected = function (event) {
        console.log(event);
        var file = event[0];
        this.uUploader.uploadAll();
        for (var i = 0; i < this.uUploader.queue.length; i++) {
            // const json = JSON.stringify(this.uUploader.queue[i].file.name);
            // console.log("original : " + json)
            this.uiImageInObj.type = this.uUploader.queue[i].file.type;
        }
    };
    // remove image from upload array queue
    ProfileModalComponent.prototype.removeFromQue = function (item, index) {
        // console.log("remove from array : " + JSON.stringify(this.arrayToEmit))
        this.deleteAttIns3(this.arrayToEmit[index], item);
    };
    // delete attachment from s3 bucket
    ProfileModalComponent.prototype.deleteAttIns3 = function (array, item) {
        var _this = this;
        // console.log("image array for delete : " + JSON.stringify(array))
        var arrayDeleteImage = [array];
        var fileData = {
            imageArray: arrayDeleteImage,
        };
        // console.log(">>>>" + JSON.stringify(fileData))
        this.inventFundApi
            .deleatAttachmentFile(fileData)
            .then(function (res) {
            // console.log("delete res s3 : " + JSON.stringify(res))
            if (res.code == 200) {
                item.remove();
                _this.form.reset();
                _this.form.controls['description'].disable();
                _this.disableUpload = false;
            }
        })
            .catch(function (err) {
            console.log('Error:');
        });
    };
    // get type of attachment
    ProfileModalComponent.prototype.getFileExtension = function (filename) {
        // this will give you icon class name
        var ext = filename.split('/').pop();
        // console.log("file name : "+JSON.stringify(ext))
        var obj = this.iconList.filter(function (row) {
            if (row.type === ext) {
                return true;
            }
        });
        // console.log({obj})
        if (obj.length > 0) {
            var icon = obj[0].icon;
            return icon;
        }
        else {
            return '';
        }
    };
    ProfileModalComponent.prototype.toDateValidation = function (dataType) {
        var to = new Date(this.form.controls['to'].value); //Year, Month, Date
        var from = new Date(this.form.controls['from'].value); //Year, Month, Date
        console.log('to date validation : ' + JSON.stringify(to));
        console.log('to date validation : ' + JSON.stringify(from));
        if (to < from) {
            this.form.controls['to'].setErrors({ fromDateTo: true });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ProfileModalComponent.prototype, "data", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ProfileModalComponent.prototype, "passEntry", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ProfileModalComponent.prototype, "formlyData", void 0);
    ProfileModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-modal',
            template: __webpack_require__(/*! ./profile-modal.component.html */ "./src/app/profile/profile-modal/profile-modal.component.html"),
            styles: [__webpack_require__(/*! ./profile-modal.component.scss */ "./src/app/profile/profile-modal/profile-modal.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_7__["InventFundsApiService"],
            _services_attachment_service__WEBPACK_IMPORTED_MODULE_8__["AttachmentService"],
            _services_app_state_service__WEBPACK_IMPORTED_MODULE_9__["AppStateService"]])
    ], ProfileModalComponent);
    return ProfileModalComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile-page/profile-page.component.html":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-page/profile-page.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"profileData == 'Invent Fund Fixer'\">\r\n  <h5 style=\"font-family: Montserrat; font-weight: 600;\">Profile Overview</h5>\r\n  <br />\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-9\">\r\n      <div class=\"main-card mb-3 card bgcolor mx-auto\">\r\n        <div class=\"card-body p-0\">\r\n          <div class=\"row\">\r\n            <div class=\"col-md-3 col-lg-3 col-xl-3 pr-0 verticalLine\">\r\n              <img\r\n                src=\"{{ profileImg | async }}\"\r\n                class=\"rounded-circle imgsize image-fluid mt-5 mx-auto d-block\"\r\n                alt=\"abc\"\r\n              />\r\n              &nbsp;&nbsp;<br />\r\n\r\n              <p class=\"cent\"><b>Individual</b></p>\r\n              <br />\r\n              <div class=\"row no-gutters\">\r\n                <div class=\"col-3\">\r\n                  <span\r\n                    style=\"margin-top: 4px;\"\r\n                    class=\"Calendar float-right\"\r\n                  ></span>\r\n                </div>\r\n\r\n                <div class=\"col-9\">\r\n                  <p class=\"mb-5 ml-2 sm-mb\">\r\n                    <b> Member Since:</b>\r\n                    {{\r\n                      fixerProfileSchema.fixerPersonDetails?.createdDate\r\n                        | date: 'MM/dd/yyyy'\r\n                    }}\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"col-md-9 col-xl-9 col-lg-9\">\r\n              <div class=\"row\">\r\n                <div\r\n                  style=\"padding-left: 6px; padding-top: 6px;\"\r\n                  class=\"col-md-9 col-lg-9 col-xl-9 col-sm-4 mt-3 pl-3 sm-align\"\r\n                >\r\n                  &nbsp;\r\n                  <div\r\n                    *ngIf=\"editmainsection == false\"\r\n                    class=\"d-sm-block d-md-none p-0\"\r\n                  >\r\n                    <button\r\n                      class=\"btn btn-primary btn-right ml-3 mt-4\"\r\n                      style=\"height: 35px; width: 203px; margin-bottom: 20px;\"\r\n                    >\r\n                      <span class=\"linkedin\"></span>\r\n                      <span>Sync your LinkedIn Profile</span>\r\n                    </button>\r\n\r\n                    <span\r\n                      class=\"edit float-right mr-3 mt-4\"\r\n                      (click)=\"onclick()\"\r\n                    ></span>\r\n                  </div>\r\n\r\n                  <h6\r\n                    class=\"mb-0\"\r\n                    style=\"font-family: montserrat; font-weight: 600;\"\r\n                  >\r\n                    {{ fixerProfileSchema.fixerPersonDetails?.name }}\r\n                  </h6>\r\n                  <!-- <small>banglore,India</small> -->\r\n                  <h6\r\n                    class=\"mt-3\"\r\n                    *ngIf=\"editmainsection == false\"\r\n                    style=\"font-family: montserrat; font-weight: 600;\"\r\n                  >\r\n                    Professional Headline\r\n                  </h6>\r\n\r\n                  <p\r\n                    class=\"mt-0\"\r\n                    *ngIf=\"\r\n                      editmainsection == false &&\r\n                      fixerProfileSchema.fixerProfileSummary.headline\r\n                    \"\r\n                  >\r\n                    {{ fixerProfileSchema.fixerProfileSummary.headline }}\r\n                  </p>\r\n                  <p\r\n                    class=\"mt-0\"\r\n                    *ngIf=\"\r\n                      editmainsection == false &&\r\n                      !fixerProfileSchema.fixerProfileSummary.headline\r\n                    \"\r\n                  >\r\n                    Enter Headline(Not more than 75 words)\r\n                  </p>\r\n                  <h6\r\n                    class=\"mt-3\"\r\n                    *ngIf=\"editmainsection == false\"\r\n                    style=\"font-family: 'Montserrat'; font-weight: 600;\"\r\n                  >\r\n                    Professional Summary\r\n                  </h6>\r\n\r\n                  <p\r\n                    class=\"mt-0\"\r\n                    *ngIf=\"\r\n                      editmainsection == false &&\r\n                      fixerProfileSchema.fixerProfileSummary.professionalSummary\r\n                    \"\r\n                  >\r\n                    {{\r\n                      fixerProfileSchema.fixerProfileSummary.professionalSummary\r\n                    }}\r\n                  </p>\r\n                  <p\r\n                    class=\"mt-0\"\r\n                    *ngIf=\"\r\n                      editmainsection == false &&\r\n                      !fixerProfileSchema.fixerProfileSummary\r\n                        .professionalSummary\r\n                    \"\r\n                  >\r\n                    Enter Summary(Not more than 300 words)\r\n                  </p>\r\n                </div>\r\n\r\n                <div class=\"col-md-3 col-xl-3 col-lg-3 sm-none\">\r\n                  <button\r\n                    class=\"btn btn-primary btn-right mr-3 mt-4\"\r\n                    style=\"float: right; height: 35px; width: 203px;\"\r\n                  >\r\n                    <span class=\"linkedin\"></span>\r\n                    <span>Sync your LinkedIn Profile</span>\r\n                  </button>\r\n                  <br /><br />\r\n                  <span class=\"edit mt-4 ml-5\" (click)=\"onclick()\"></span>\r\n                </div>\r\n\r\n                <div\r\n                  class=\"col-md-11 col-lg-11 col-xl-11 p-0\"\r\n                  *ngIf=\"editmainsection == true\"\r\n                >\r\n                  <app-ct-form-builder\r\n                    [formFieldsJson]=\"profileSummaryJson\"\r\n                    [modelData]=\"modelData\"\r\n                    (saveDataEmitter)=\"createProfileSummaryData($event)\"\r\n                    (updateDataEmitter)=\"updateProfileSummaryData($event)\"\r\n                    (cancelDataEmitter)=\"cancelProfileSummary($event)\"\r\n                  >\r\n                  </app-ct-form-builder>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <br />\r\n      <br />\r\n      <!-- Showcase  -->\r\n      <mat-expansion-panel>\r\n        <mat-expansion-panel-header\r\n          expandedHeight=\"45px\"\r\n          collapsedHeight=\"45px\"\r\n          class=\"pl-0 bgcolor\"\r\n        >\r\n          <mat-panel-title class=\"accordion-heading\">\r\n            <span class=\"showcase\"></span>\r\n            <span class=\"headerFont\">\r\n              <span class=\"m-h\">Showcase</span>\r\n            </span>\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n            <div class=\"btn-actions-pane-right\">\r\n              <button\r\n                (click)=\"openModal({ showcase: '', heading: 'Add Showcase' })\"\r\n                class=\"btn-icon btn btn-link\"\r\n                style=\"height: 8px;\"\r\n              >\r\n                <span class=\"more\"></span>\r\n              </button>\r\n            </div>\r\n          </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n        <br />\r\n\r\n        <app-carousel\r\n          *ngIf=\"show && showcaseArray\"\r\n          #carousel\r\n          [showControls]=\"false\"\r\n        >\r\n          <ng-container *ngFor=\"let slide of showcaseArray\">\r\n            <!-- {{showcaseArray | json}} -->\r\n            <div\r\n              *carouselSlide\r\n              class=\"mx-2 card shadow-none testimonial-carousel\"\r\n            >\r\n              <!-- {{slide?.fileType}} -->\r\n              <img\r\n                class=\"close\"\r\n                src=\"../../../assets/images-ui/delete.svg\"\r\n                style=\"cursor: pointer;\"\r\n                (click)=\"deleteCarosulDoc(slide)\"\r\n                *ngIf=\"slide?.url && slide?.fileType == 'application/pdf'\"\r\n              />\r\n              <img\r\n                class=\"image-center border-light\"\r\n                style=\"height: 140px; width: 120px;\"\r\n                *ngIf=\"slide?.url && slide?.fileType == 'application/pdf'\"\r\n                src=\"../../../assets/images-ui/pdf-image-viewer.png\"\r\n                alt=\"...\"\r\n                (click)=\"emitImageObject(slide)\"\r\n              />\r\n              <img\r\n                class=\"close\"\r\n                src=\"../../../assets/images-ui/delete.svg\"\r\n                style=\"cursor: pointer;\"\r\n                (click)=\"deleteCarosulDoc(slide)\"\r\n                *ngIf=\"slide?.url && slide?.fileType == 'image/jpeg'\"\r\n              />\r\n\r\n              <img\r\n                class=\"border-light\"\r\n                style=\"height: 140px; width: 120px;\"\r\n                *ngIf=\"slide?.url && slide?.fileType == 'image/jpeg'\"\r\n                [src]=\"slide?.url\"\r\n                alt=\"...\"\r\n              />\r\n\r\n              <img\r\n                class=\"close z-index\"\r\n                src=\"../../../assets/images-ui/delete.svg\"\r\n                style=\"cursor: pointer;\"\r\n                (click)=\"deleteCarosulDoc(slide)\"\r\n                *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n              />\r\n              <video\r\n                class=\"border-light\"\r\n                style=\"height: 134px; width: 120px;\"\r\n                controls\r\n                *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n              >\r\n                <source\r\n                  [src]=\"slide?.url\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                  type=\"video/mp4\"\r\n                />\r\n              </video>\r\n\r\n              <div class=\"\">\r\n                <p\r\n                  style=\"\r\n                    text-align: center;\r\n                    font-weight: 500;\r\n                    word-wrap: break-word;\r\n                  \"\r\n                  class=\"card-text des-p-t-w\"\r\n                >\r\n                  {{ slide.description }}\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n\r\n          <div class=\"prev-button\" (click)=\"carousel.prev()\">\r\n            <a class=\"carousel-control-prev-icon\"></a>\r\n          </div>\r\n          <div class=\"next-button\" (click)=\"carousel.next()\">\r\n            <a class=\"carousel-control-next-icon\"></a>\r\n          </div>\r\n        </app-carousel>\r\n      </mat-expansion-panel>\r\n      <br /><br />\r\n\r\n      <!-- Profile Summary Section  -->\r\n\r\n      <mat-expansion-panel [expanded]=\"true\">\r\n        <mat-expansion-panel-header\r\n          expandedHeight=\"45px\"\r\n          collapsedHeight=\"45px\"\r\n          class=\"pl-0 bgcolor\"\r\n        >\r\n          <mat-panel-title class=\"accordion-heading pb-0\">\r\n            <span class=\"profileSummary\">\r\n              <span class=\"profileSum\"></span>\r\n            </span>\r\n            <span class=\"headerFont\">\r\n              <span class=\"m-h\">Profile Summary</span>\r\n            </span>\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n            <div class=\"btn-actions-pane-right\">\r\n              <button\r\n                (click)=\"\r\n                  openModal({\r\n                    profileSummary: modelData?.profileSummary,\r\n                    heading: 'Edit Profile Summary'\r\n                  })\r\n                \"\r\n                class=\"btn-icon btn btn-link\"\r\n                style=\"height: 15px;\"\r\n              >\r\n                <span class=\"edit\"></span>\r\n              </button>\r\n            </div>\r\n          </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n        <br />\r\n        <p *ngIf=\"!fixerProfileSchema.fixerProfileSummary.profileSummary\">\r\n          Free text write a paragragh About, experience summary, Specialities,\r\n          Key highlights etc.\r\n          <br />\r\n          (Not more than 1000 words)\r\n        </p>\r\n        <p *ngIf=\"fixerProfileSchema.fixerProfileSummary.profileSummary\">\r\n          {{ fixerProfileSchema.fixerProfileSummary.profileSummary }}\r\n        </p>\r\n      </mat-expansion-panel>\r\n      <br /><br />\r\n\r\n      <!--------------------------------------->\r\n      <!-- Education Details Summary Section  -->\r\n\r\n      <mat-expansion-panel>\r\n        <mat-expansion-panel-header\r\n          expandedHeight=\"45px\"\r\n          collapsedHeight=\"45px\"\r\n          class=\"pl-0 bgcolor\"\r\n        >\r\n          <mat-panel-title class=\"accordion-heading pb-0\">\r\n            <span class=\"education\">\r\n              <span class=\"education1\"></span>\r\n            </span>\r\n            <span class=\"headerFont\">\r\n              <span class=\"m-h\">Education Details</span>\r\n            </span>\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n            <div class=\"btn-actions-pane-right\" style=\"margin-top: 8px;\">\r\n              <button\r\n                (click)=\"\r\n                  openModal({\r\n                    educationalDetails: '',\r\n                    heading: 'Add Educational Details'\r\n                  })\r\n                \"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"more mt-1\"></span>\r\n              </button>\r\n            </div>\r\n          </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n\r\n        <div\r\n          style=\"margin-top: 12px;\"\r\n          *ngFor=\"let item of fixerProfileSchema.fixerEducationalDetails\"\r\n          class=\"mb-3\"\r\n        >\r\n          <div *ngIf=\"item.personId\">\r\n            <button\r\n              style=\"float: right;\"\r\n              (click)=\"\r\n                openModal({\r\n                  updateData: item,\r\n                  educationalDetails: '',\r\n                  heading: 'Edit Education Details'\r\n                })\r\n              \"\r\n              class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n            >\r\n              <span class=\"edit\"></span>\r\n            </button>\r\n            <button\r\n              style=\"float: right;\"\r\n              (click)=\"deleteEducationData(item._id)\"\r\n              class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n            >\r\n              <span class=\"trash\"></span>\r\n            </button>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-10\">\r\n                <p *ngIf=\"item?.college\" class=\"font-weight-bold mb-1 margin\">\r\n                  {{ item?.college }}\r\n                </p>\r\n                <p\r\n                  *ngIf=\"item?.education && item.specialization\"\r\n                  class=\"mb-1 margin\"\r\n                >\r\n                  {{ item.education }},&nbsp;{{ item.specialization }}\r\n                </p>\r\n                <span *ngIf=\"item.grade\" class=\"mb-1 margin\"\r\n                  >Grade:\r\n                  <b>{{ item.grade }}</b>\r\n                </span>\r\n                &nbsp;&nbsp;&nbsp;\r\n                <span *ngIf=\"item.cgpaScored\" class=\"mb-1 margin\"\r\n                  >&nbsp;&nbsp;&nbsp;CGPA:\r\n                  <b>{{ item.cgpaScored }}/{{ item.cgpaMax }}</b>\r\n                </span>\r\n                &nbsp;&nbsp;&nbsp;\r\n                <span *ngIf=\"item.percentile\" class=\"mb-1 margin\"\r\n                  >&nbsp;&nbsp;&nbsp;Percentile:\r\n                  <b>{{ item.percentile }}%</b>\r\n                </span>\r\n                &nbsp;&nbsp;&nbsp;\r\n                <span *ngIf=\"item.percentage\" class=\"mb-1 margin\"\r\n                  >&nbsp;&nbsp;&nbsp;Percentage:\r\n                  <b>{{ item.percentile }}%</b>\r\n                </span>\r\n                <p *ngIf=\"item.from\" class=\"mb-1 margin\">\r\n                  {{ item.from | date: 'MMM yyyy' }}-{{\r\n                    item.to | date: 'MMM yyyy'\r\n                  }}\r\n                </p>\r\n                <p *ngIf=\"item.keyHighlights\" class=\"margin\">\r\n                  Key Highlights : <b>{{ item.keyHighlights }}</b>\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </mat-expansion-panel>\r\n      <br /><br />\r\n      <!--------------------------------------->\r\n\r\n      <!-- Professional Summary Details Summary Section  -->\r\n\r\n      <mat-expansion-panel>\r\n        <mat-expansion-panel-header\r\n          expandedHeight=\"45px\"\r\n          collapsedHeight=\"45px\"\r\n          class=\"pl-0 bgcolor\"\r\n        >\r\n          <mat-panel-title class=\"accordion-heading\">\r\n            <span class=\"professional\"><span class=\"experience\"></span></span>\r\n\r\n            <span class=\"headerFont\">\r\n              <span class=\"m-h\">Professional Experience Details</span>\r\n            </span>\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n            <div class=\"btn-actions-pane-right\">\r\n              <button\r\n                (click)=\"\r\n                  openModal({\r\n                    professionalDetailsIndividual: '',\r\n                    heading: 'Add Professional Details'\r\n                  })\r\n                \"\r\n                class=\"btn-icon btn btn-link\"\r\n                style=\"height: 8px;\"\r\n              >\r\n                <span class=\"more\"></span>\r\n              </button>\r\n            </div>\r\n          </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n\r\n        <div\r\n          style=\"margin-top: 4px;\"\r\n          *ngFor=\"let item of fixerProfileSchema.fixerProfessionalDetails\"\r\n          class=\"mb-3\"\r\n        >\r\n          <div *ngIf=\"item.personId\" class=\"card-body\">\r\n            <button\r\n              style=\"float: right;\"\r\n              (click)=\"\r\n                openModal({\r\n                  model: item._id,\r\n                  professionalDetailsIndividual: '',\r\n                  heading: 'Add Professional Details'\r\n                })\r\n              \"\r\n              class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n            >\r\n              <span class=\"edit\"></span>\r\n            </button>\r\n            <button\r\n              style=\"float: right;\"\r\n              (click)=\"deleteProfessionData(item._id)\"\r\n              class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n            >\r\n              <span class=\"trash\"></span>\r\n            </button>\r\n            <div class=\"row\">\r\n              <div class=\"col-6\">\r\n                <p class=\"font-weight-bold mb-0\">{{ item.designation }}</p>\r\n                <p class=\"mb-0\">\r\n                  {{ item.company }} - {{ item.employmentType }}\r\n                </p>\r\n                <p class=\"mb-0\">\r\n                  {{ item.location }},{{ item.from | date: 'MMM-yyyy' }} to\r\n                  {{ item.to | date: 'MMM-yyyy' }}\r\n                </p>\r\n              </div>\r\n              <div class=\"col-4 mb-0\">\r\n                <p *ngIf=\"item.experienceSummary\">\r\n                  Summary - {{ item.experienceSummary }}\r\n                </p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </mat-expansion-panel>\r\n\r\n      <br /><br />\r\n      <!--------------------------------------->\r\n\r\n      <!-- achievementsDetails -->\r\n\r\n      <mat-expansion-panel>\r\n        <mat-expansion-panel-header\r\n          expandedHeight=\"45px\"\r\n          collapsedHeight=\"45px\"\r\n          class=\"pl-0 bgcolor\"\r\n        >\r\n          <mat-panel-title class=\"accordion-heading\">\r\n            <span class=\"achiev\"><span class=\"achievement\"></span></span>\r\n\r\n            <span class=\"headerFont\">\r\n              <span class=\"m-h\">Achievements Details</span>\r\n            </span>\r\n          </mat-panel-title>\r\n          <mat-panel-description>\r\n            <div class=\"btn-actions-pane-right\">\r\n              <button\r\n                (click)=\"\r\n                  openModal({\r\n                    achievementsDetails: '',\r\n                    heading: 'Add Achievements Details'\r\n                  })\r\n                \"\r\n                class=\"btn-icon btn btn-link\"\r\n                style=\"height: 8px;\"\r\n              >\r\n                <span class=\"more\"></span>\r\n              </button>\r\n            </div>\r\n          </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n        <br />\r\n\r\n        <div\r\n          class=\"container\"\r\n          style=\"margin-top: 4px;\"\r\n          *ngFor=\"let item of fixerProfileSchema.fixerAchievementDetails\"\r\n        >\r\n          <div *ngIf=\"item.personId\" class=\"row\">\r\n            <div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10\">\r\n              <div class=\"row\">\r\n                <div class=\"col-12\">\r\n                  <span\r\n                    >Headlines/Title :<span class=\"font-weight-bold\">\r\n                      {{ item.headlineTitles }}</span\r\n                    ></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span\r\n                    >Category :<span class=\"font-weight-bold\">\r\n                      {{ item.achievementsCategory }}</span\r\n                    ></span\r\n                  >&nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.achievementsSummary\"\r\n                    >Summary:&nbsp; {{ item.achievementsSummary }}</span\r\n                  >\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-lg-2 col-md-2 col-sm-2 col-xs-2\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\r\n                  <button\r\n                    (click)=\"deleteAchi(item._id)\"\r\n                    class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n                  >\r\n                    <span class=\"trash\"></span>\r\n                  </button>\r\n                </div>\r\n                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-6\">\r\n                  <button\r\n                    (click)=\"\r\n                      openModal({\r\n                        updateData: item._id,\r\n                        achievementsDetails: '',\r\n                        heading: 'Edit Achievements Details'\r\n                      })\r\n                    \"\r\n                    class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n                  >\r\n                    <span class=\"edit\"></span>\r\n                  </button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </mat-expansion-panel>\r\n      <br /><br />\r\n\r\n      <!-- --------------------------------------------- -->\r\n\r\n      <!-- Skillsets section  -->\r\n      <mat-accordion>\r\n        <mat-expansion-panel>\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading\">\r\n              <span class=\"professional\"><span class=\"skills\"></span></span>\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Skillsets, Self-Assessments, Ratings</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\">\r\n                <button\r\n                  (click)=\"\r\n                    openModal({\r\n                      skillsDetails: '',\r\n                      heading: 'Add Skillsets,Self-Assessments,Ratings Details'\r\n                    })\r\n                  \"\r\n                  class=\"btn-icon btn btn-link\"\r\n                  style=\"height: 8px;\"\r\n                >\r\n                  <span class=\"more\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <div\r\n            style=\"margin-top: 4px;\"\r\n            *ngFor=\"let item of fixerProfileSchema.fixerSkillDetails\"\r\n            class=\"mb-3\"\r\n          >\r\n            <div *ngIf=\"item.personId\" class=\"card-body\">\r\n              <button\r\n                style=\"float: right;\"\r\n                (click)=\"\r\n                  openModal({\r\n                    updateData: item,\r\n                    skillsDetails: '',\r\n                    heading: 'Edit Skillsets,Self-Assessments,Ratings Details'\r\n                  })\r\n                \"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"edit\"></span>\r\n              </button>\r\n              <button\r\n                style=\"float: right;\"\r\n                (click)=\"deleteSkills(item._id)\"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"trash\"></span>\r\n              </button>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-12\">\r\n                  <span\r\n                    >Category :\r\n                    <span class=\"font-weight-bold\"\r\n                      >{{ item.skillCategory }}\r\n                    </span></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span\r\n                    >Skillset :\r\n                    <span class=\"font-weight-bold\">{{\r\n                      item.skillSet\r\n                    }}</span></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span\r\n                    >Assessment :\r\n                    <span class=\"font-weight-bold\">{{\r\n                      item.selfAssessmentScore\r\n                    }}</span>\r\n                  </span>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n\r\n      <br /><br /><br />\r\n\r\n      <!-- ----------------------------  -->\r\n    </div>\r\n\r\n    <!-- {{profileStatus?.profileSummary | async}} -->\r\n    <div class=\"col-lg-3\">\r\n      <mat-accordion>\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header\r\n            class=\"bgcolor\"\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n          >\r\n            <mat-panel-title>\r\n              Profile Status\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <mat-list class=\"\">\r\n            <mat-list-item style=\"font-size: 10px;\">\r\n              <div class=\"row p-t-10\">\r\n                <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                  <span\r\n                    [ngClass]=\"\r\n                      (profileStatus?.profileSummary | async) ? '' : 's-c-g'\r\n                    \"\r\n                    >Profile Summary</span\r\n                  >\r\n                </div>\r\n                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                  <img\r\n                    *ngIf=\"profileStatus?.profileSummary | async\"\r\n                    class=\"img-list\"\r\n                    src=\"../../assets/icons/green-sucess.svg\"\r\n                  />\r\n                  <img\r\n                    *ngIf=\"(profileStatus?.profileSummary | async) == null\"\r\n                    class=\"img-list-cancel\"\r\n                    src=\"../../assets/icons/red-fail.svg\"\r\n                  />\r\n                </div>\r\n              </div>\r\n            </mat-list-item>\r\n            <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                  <span\r\n                    [ngClass]=\"\r\n                      (profileStatus?.educationDetails | async) ? '' : 's-c-g'\r\n                    \"\r\n                    >Education details</span\r\n                  >\r\n                </div>\r\n                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                  <img\r\n                    *ngIf=\"profileStatus?.educationDetails | async\"\r\n                    class=\"img-list\"\r\n                    src=\"../../assets/icons/green-sucess.svg\"\r\n                  />\r\n                  <img\r\n                    *ngIf=\"(profileStatus?.educationDetails | async) == null\"\r\n                    class=\"img-list-cancel\"\r\n                    src=\"../../assets/icons/red-fail.svg\"\r\n                  />\r\n                </div>\r\n              </div>\r\n            </mat-list-item>\r\n            <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                  <span\r\n                    [ngClass]=\"\r\n                      (profileStatus?.professionalDetails | async)\r\n                        ? ''\r\n                        : 's-c-g'\r\n                    \"\r\n                    >Experience details</span\r\n                  >\r\n                </div>\r\n                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                  <img\r\n                    *ngIf=\"profileStatus?.professionalDetails | async\"\r\n                    class=\"img-list\"\r\n                    src=\"../../assets/icons/green-sucess.svg\"\r\n                  />\r\n                  <img\r\n                    *ngIf=\"(profileStatus?.professionalDetails | async) == null\"\r\n                    class=\"img-list-cancel\"\r\n                    src=\"../../assets/icons/red-fail.svg\"\r\n                  />\r\n                </div>\r\n              </div>\r\n            </mat-list-item>\r\n            <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n            <!-- {{profileStatus?.achievementsDetails | async}} -->\r\n            <mat-list-item style=\"font-size: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                  <span\r\n                    [ngClass]=\"\r\n                      (profileStatus?.achievementsDetails | async)\r\n                        ? ''\r\n                        : 's-c-g'\r\n                    \"\r\n                    >Achievements</span\r\n                  >\r\n                </div>\r\n                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                  <img\r\n                    *ngIf=\"profileStatus?.achievementsDetails | async\"\r\n                    class=\"img-list\"\r\n                    src=\"../../assets/icons/green-sucess.svg\"\r\n                  />\r\n                  <img\r\n                    *ngIf=\"(profileStatus?.achievementsDetails | async) == null\"\r\n                    class=\"img-list-cancel\"\r\n                    src=\"../../assets/icons/red-fail.svg\"\r\n                  />\r\n                </div>\r\n              </div>\r\n            </mat-list-item>\r\n            <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                  <span\r\n                    [ngClass]=\"\r\n                      (profileStatus?.skillsDetails | async) ? '' : 's-c-g'\r\n                    \"\r\n                    >Skills</span\r\n                  >\r\n                </div>\r\n                <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                  <img\r\n                    *ngIf=\"profileStatus?.skillsDetails | async\"\r\n                    class=\"img-list\"\r\n                    src=\"../../assets/icons/green-sucess.svg\"\r\n                  />\r\n                  <img\r\n                    *ngIf=\"(profileStatus?.skillsDetails | async) == null\"\r\n                    class=\"img-list-cancel\"\r\n                    src=\"../../assets/icons/red-fail.svg\"\r\n                  />\r\n                </div>\r\n              </div>\r\n            </mat-list-item>\r\n          </mat-list>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n      <br />\r\n\r\n      <mat-accordion>\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header\r\n            class=\"bgcolor\"\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n          >\r\n            <mat-panel-title>\r\n              Verifications\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <mat-list>\r\n            <mat-list-item style=\"font-size: 10px;\">Linkedin</mat-list-item\r\n            ><br />\r\n            <mat-divider style=\"background-color: blue;\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 10px;\">Education</mat-list-item\r\n            ><br />\r\n            <mat-divider style=\"background-color: blue;\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 10px;\">Experience</mat-list-item\r\n            ><br />\r\n            <mat-divider style=\"background-color: blue;\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 10px;\"\r\n              >Identity\r\n              <a style=\"color: blue;\" class=\"ml-5 aTag\"\r\n                >Verify</a\r\n              ></mat-list-item\r\n            ><br />\r\n            <mat-divider style=\"background-color: blue;\"></mat-divider><br />\r\n            <mat-list-item style=\"font-size: 9px;\"\r\n              >Payment\r\n              <a style=\"color: blue;\" class=\"ml-5 aTag\"\r\n                >Verify</a\r\n              ></mat-list-item\r\n            >\r\n          </mat-list>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n      <br />\r\n      <mat-accordion>\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header\r\n            class=\"bgcolor\"\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n          >\r\n            <mat-panel-title>\r\n              News/Feeds\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <mat-card>\r\n            <mat-list>\r\n              <mat-list-item style=\"font-size: 10px;\"\r\n                >News Topics 1</mat-list-item\r\n              >\r\n              <mat-divider\r\n                [inset]=\"true\"\r\n                style=\"background-color: blue;\"\r\n              ></mat-divider>\r\n              <mat-list-item style=\"font-size: 10px;\"\r\n                >News Topics 2</mat-list-item\r\n              >\r\n              <mat-divider\r\n                [inset]=\"true\"\r\n                style=\"background-color: blue;\"\r\n              ></mat-divider>\r\n              <mat-list-item style=\"font-size: 10px;\"\r\n                >News Topics 3</mat-list-item\r\n              ><br />\r\n              <span class=\"ml-3\" style=\"color: blue; font-size: 8px;\"\r\n                >Show more\r\n                <i\r\n                  class=\"fa fa-chevron-down fa-sm\"\r\n                  style=\"color: blue;\"\r\n                  aria-hidden=\"true\"\r\n                ></i>\r\n              </span>\r\n            </mat-list>\r\n          </mat-card>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n      <br />\r\n      <mat-accordion>\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header\r\n            class=\"bgcolor\"\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n          >\r\n            <mat-panel-title>\r\n              Top Skills\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <mat-card>\r\n            <mat-list>\r\n              <mat-list-item style=\"font-size: 10px;\">Skill 1</mat-list-item>\r\n              <mat-divider\r\n                [inset]=\"true\"\r\n                style=\"background-color: blue;\"\r\n              ></mat-divider>\r\n              <mat-list-item style=\"font-size: 10px;\">Skill 2</mat-list-item\r\n              ><br />\r\n              <span class=\"ml-3\" style=\"color: blue; font-size: 8px;\"\r\n                >Show more\r\n                <i\r\n                  class=\"fa fa-chevron-down fa-sm\"\r\n                  style=\"color: blue;\"\r\n                  aria-hidden=\"true\"\r\n                ></i>\r\n              </span>\r\n            </mat-list>\r\n          </mat-card>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n      <br />\r\n      <mat-accordion>\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header\r\n            class=\"bgcolor\"\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n          >\r\n            <mat-panel-title>\r\n              Top Founders\r\n            </mat-panel-title>\r\n          </mat-expansion-panel-header>\r\n          <mat-card>\r\n            <mat-card-content>\r\n              <mat-list>\r\n                <mat-list-item style=\"font-size: 10px;\"\r\n                  >Founder 1</mat-list-item\r\n                >\r\n                <mat-divider\r\n                  [inset]=\"true\"\r\n                  style=\"background-color: blue;\"\r\n                ></mat-divider>\r\n                <mat-list-item style=\"font-size: 10px;\">Founder 2</mat-list-item\r\n                ><br />\r\n                <span class=\"ml-3\" style=\"color: blue; font-size: 8px;\"\r\n                  >Show more\r\n                  <i\r\n                    class=\"fa fa-chevron-down fa-sm\"\r\n                    style=\"color: blue;\"\r\n                    aria-hidden=\"true\"\r\n                  ></i>\r\n                </span>\r\n              </mat-list>\r\n            </mat-card-content>\r\n          </mat-card>\r\n        </mat-expansion-panel>\r\n      </mat-accordion>\r\n      <br />\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div *ngIf=\"profileData == 'Invent Fund Founder'\">\r\n  <div class=\"container-md\">\r\n    <h5 style=\"font-family: Montserrat; font-weight: 600;\">Profile Overview</h5>\r\n    <br />\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-9\">\r\n        <div class=\"main-card mb-3 card bgcolor mx-auto\">\r\n          <div class=\"card-body p-0\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-3 col-lg-3 col-xl-3 pr-0 verticalLine\">\r\n                <img\r\n                  src=\"{{ profileImg | async }}\"\r\n                  class=\"rounded-circle imgsize image-fluid mt-5 mx-auto d-block\"\r\n                  alt=\"abc\"\r\n                />\r\n                &nbsp;&nbsp;<br />\r\n\r\n                <p class=\"cent\"><b>Individual</b></p>\r\n                <br />\r\n                <div class=\"row no-gutters\">\r\n                  <div class=\"col-3\">\r\n                    <!-- <span class=\"Calendar float-right mt-2\"></span> -->\r\n                    <span\r\n                      style=\"margin-top: 4px;\"\r\n                      class=\"Calendar float-right\"\r\n                    ></span>\r\n                  </div>\r\n\r\n                  <div class=\"col-9\">\r\n                    <p class=\"mb-5 ml-2 sm-mb\">\r\n                      <b> Member Since:</b>\r\n                      {{\r\n                        founderProfileSchema.founderPersonDetails?.createdDate\r\n                          | date: 'MM/dd/yyyy'\r\n                      }}\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"col-md-9 col-xl-9 col-lg-9\">\r\n                <div class=\"row\">\r\n                  <div\r\n                    style=\"padding-left: 6px; padding-top: 6px;\"\r\n                    class=\"col-md-9 col-lg-9 col-xl-9 col-sm-4 mt-3 pl-3 sm-align\"\r\n                  >\r\n                    &nbsp;\r\n                    <div\r\n                      *ngIf=\"editmainsection == false\"\r\n                      class=\"d-sm-block d-md-none p-0\"\r\n                    >\r\n                      <button\r\n                        class=\"btn btn-primary btn-right ml-3 mt-4\"\r\n                        style=\"height: 35px; width: 203px; margin-bottom: 20px;\"\r\n                      >\r\n                        <span class=\"linkedin\"></span>\r\n                        <span>Sync your LinkedIn Profile</span>\r\n                      </button>\r\n\r\n                      <span\r\n                        class=\"edit float-right mr-3 mt-4\"\r\n                        (click)=\"onclick()\"\r\n                      ></span>\r\n                    </div>\r\n\r\n                    <h6\r\n                      class=\"mb-0\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      {{ founderProfileSchema.founderPersonDetails.name }}\r\n                    </h6>\r\n                    <!-- <small>banglore,India</small> -->\r\n                    <h6\r\n                      class=\"mt-3\"\r\n                      *ngIf=\"editmainsection == false\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      Professional Headline\r\n                    </h6>\r\n\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        founderProfileSchema.founderProfileSummary.headline\r\n                      \"\r\n                    >\r\n                      {{ founderProfileSchema.founderProfileSummary.headline }}\r\n                    </p>\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        !founderProfileSchema.founderProfileSummary.headline\r\n                      \"\r\n                    >\r\n                      Enter Headline(Not more than 75 words)\r\n                    </p>\r\n                    <h6\r\n                      class=\"mt-3\"\r\n                      *ngIf=\"editmainsection == false\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      Professional Summary\r\n                    </h6>\r\n\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        founderProfileSchema.founderProfileSummary\r\n                          .professionalSummary\r\n                      \"\r\n                    >\r\n                      {{\r\n                        founderProfileSchema.founderProfileSummary\r\n                          .professionalSummary\r\n                      }}\r\n                    </p>\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        !founderProfileSchema.founderProfileSummary\r\n                          .professionalSummary\r\n                      \"\r\n                    >\r\n                      Enter Summary(Not more than 300 words)\r\n                    </p>\r\n                    <!-- <app-ct-form-builder *ngIf=\"editmainsection==true\" [formFieldsJson]=\"profileSummaryJson\" [modelData]=\"modelData\" (saveDataEmitter)='createProfileSummaryData($event)' (updateDataEmitter)='updateProfileSummaryData($event)' (cancelDataEmitter)=\"cancelProfileSummary($event)\">\r\n                                    </app-ct-form-builder> -->\r\n                  </div>\r\n\r\n                  <div class=\"col-md-3 col-xl-3 col-lg-3 sm-none\">\r\n                    <button\r\n                      class=\"btn btn-primary btn-right mr-3 mt-4\"\r\n                      style=\"float: right; height: 35px; width: 203px;\"\r\n                    >\r\n                      <span class=\"linkedin\"></span>\r\n                      <span>Sync your LinkedIn Profile</span>\r\n                    </button>\r\n                    <br /><br />\r\n                    <span class=\"edit mt-4 ml-5\" (click)=\"onclick()\"></span>\r\n                  </div>\r\n\r\n                  <div\r\n                    class=\"col-md-11 col-lg-11 col-xl-11 p-0\"\r\n                    *ngIf=\"editmainsection == true\"\r\n                  >\r\n                    <!-- <h6 class=\" mb-0\" style=\"font-family:montserrat;font-weight: 600; padding-top: 8px;\">\r\n                                        {{founderProfileSchema.founderPersonDetails?.name}}</h6> -->\r\n                    <app-ct-form-builder\r\n                      [formFieldsJson]=\"profileSummaryJson\"\r\n                      [modelData]=\"modelData\"\r\n                      (saveDataEmitter)=\"createProfileSummaryData($event)\"\r\n                      (updateDataEmitter)=\"updateProfileSummaryData($event)\"\r\n                      (cancelDataEmitter)=\"cancelProfileSummary($event)\"\r\n                    >\r\n                    </app-ct-form-builder>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <br />\r\n        <!-- Showcase  -->\r\n        <mat-expansion-panel>\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading\">\r\n              <span class=\"showcase\"></span>\r\n\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Showcase</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\">\r\n                <button\r\n                  (click)=\"openModal({ showcase: '', heading: 'Add Showcase' })\"\r\n                  class=\"btn-icon btn btn-link\"\r\n                  style=\"height: 8px;\"\r\n                >\r\n                  <span class=\"more\"></span>\r\n                </button>\r\n                <!-- <button class=\"btn-icon btn btn-link \" style=\"height:8px\">\r\n                                <span class=\"more\"></span>\r\n                               </button> -->\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <br />\r\n\r\n          <app-carousel\r\n            *ngIf=\"show && showcaseArray\"\r\n            #carousel\r\n            [showControls]=\"false\"\r\n          >\r\n            <!-- {{showcaseArray | json}} -->\r\n            <ng-container *ngFor=\"let slide of showcaseArray; let i = index\">\r\n              <div\r\n                *carouselSlide\r\n                class=\"mx-2 card shadow-none testimonial-carousel\"\r\n              >\r\n                <!-- {{slide?.fileType}} -->\r\n\r\n                <img\r\n                  class=\"close\"\r\n                  src=\"../../../assets/images-ui/delete.svg\"\r\n                  style=\"cursor: pointer;\"\r\n                  (click)=\"deleteCarosulDoc(slide)\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'application/pdf'\"\r\n                />\r\n                <img\r\n                  class=\"image-center border-light\"\r\n                  style=\"height: 140px; width: 120px;\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'application/pdf'\"\r\n                  src=\"../../../assets/images-ui/pdf-image-viewer.png\"\r\n                  alt=\"...\"\r\n                  (click)=\"emitImageObject(slide)\"\r\n                />\r\n                <img\r\n                  class=\"close\"\r\n                  src=\"../../../assets/images-ui/delete.svg\"\r\n                  style=\"cursor: pointer;\"\r\n                  (click)=\"deleteCarosulDoc(slide)\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'image/jpeg'\"\r\n                />\r\n\r\n                <img\r\n                  class=\"border-light\"\r\n                  style=\"height: 140px; width: 120px;\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'image/jpeg'\"\r\n                  [src]=\"slide?.url\"\r\n                  alt=\"...\"\r\n                />\r\n\r\n                <img\r\n                  class=\"close z-index\"\r\n                  src=\"../../../assets/images-ui/delete.svg\"\r\n                  style=\"cursor: pointer;\"\r\n                  (click)=\"deleteCarosulDoc(slide)\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                />\r\n                <video\r\n                  class=\"border-light\"\r\n                  style=\"height: 134px; width: 120px;\"\r\n                  controls\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                >\r\n                  <source\r\n                    [src]=\"slide?.url\"\r\n                    *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                    type=\"video/mp4\"\r\n                  />\r\n                </video>\r\n\r\n                <div class=\"\">\r\n                  <p\r\n                    style=\"\r\n                      text-align: center;\r\n                      font-weight: 500;\r\n                      word-wrap: break-word;\r\n                    \"\r\n                    class=\"card-text des-p-t-w\"\r\n                  >\r\n                    {{ slide.description }}\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </ng-container>\r\n\r\n            <div class=\"prev-button\" (click)=\"carousel.prev()\">\r\n              <a class=\"carousel-control-prev-icon\"></a>\r\n            </div>\r\n            <div class=\"next-button\" (click)=\"carousel.next()\">\r\n              <a class=\"carousel-control-next-icon\"></a>\r\n            </div>\r\n          </app-carousel>\r\n        </mat-expansion-panel>\r\n        <br /><br />\r\n\r\n        <!-- Profile Summary Section  -->\r\n\r\n        <mat-expansion-panel [expanded]=\"true\">\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading pb-0\">\r\n              <span class=\"profileSummary\">\r\n                <span class=\"profileSum\"></span>\r\n              </span>\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Profile Summary</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\">\r\n                <button\r\n                  (click)=\"\r\n                    openModal({\r\n                      profileSummary: modelData?.profileSummary,\r\n                      heading: 'Edit Profile Summary'\r\n                    })\r\n                  \"\r\n                  class=\"btn-icon btn btn-link\"\r\n                  style=\"height: 15px;\"\r\n                >\r\n                  <span class=\"edit\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <br />\r\n          <p *ngIf=\"!founderProfileSchema.founderProfileSummary.profileSummary\">\r\n            Free text write a paragraph About, experience summary, Specialities,\r\n            Key highlights etc.<br />\r\n            (Not more than 1000 words)\r\n          </p>\r\n          <p *ngIf=\"founderProfileSchema.founderProfileSummary.profileSummary\">\r\n            {{ founderProfileSchema.founderProfileSummary.profileSummary }}\r\n          </p>\r\n        </mat-expansion-panel>\r\n        <br /><br />\r\n\r\n        <!--------------------------------------->\r\n\r\n        <!-- Education Details Summary Section  -->\r\n\r\n        <mat-expansion-panel>\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading pb-0\">\r\n              <span class=\"education\"><span class=\"education1\"></span></span>\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Education Details</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\" style=\"margin-top: 8px;\">\r\n                <button\r\n                  (click)=\"\r\n                    openModal({\r\n                      educationalDetails: '',\r\n                      heading: 'Add Educational Details'\r\n                    })\r\n                  \"\r\n                  class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n                >\r\n                  <span class=\"more mt-1\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n\r\n          <div\r\n            style=\"margin-top: 12px;\"\r\n            *ngFor=\"let item of founderProfileSchema.founderEducationalDetails\"\r\n            class=\"mb-3\"\r\n          >\r\n            <div *ngIf=\"item.personId\">\r\n              <button\r\n                style=\"float: right;\"\r\n                (click)=\"\r\n                  openModal({\r\n                    updateData: item,\r\n                    educationalDetails: '',\r\n                    heading: 'Edit Education Details'\r\n                  })\r\n                \"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"edit\"></span>\r\n              </button>\r\n              <button\r\n                style=\"float: right;\"\r\n                (click)=\"deleteEducationData(item._id)\"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"trash\"></span>\r\n              </button>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-10\">\r\n                  <p *ngIf=\"item?.college\" class=\"font-weight-bold mb-1 margin\">\r\n                    {{ item?.college }}\r\n                  </p>\r\n                  <p\r\n                    *ngIf=\"item?.education && item.specialization\"\r\n                    class=\"mb-1 margin\"\r\n                  >\r\n                    {{ item.education }},&nbsp;{{ item.specialization }}\r\n                  </p>\r\n                  <span *ngIf=\"item.grade\" class=\"mb-1 margin\"\r\n                    >Grade:<b>{{ item.grade }}</b></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.cgpaScored\" class=\"mb-1 margin\"\r\n                    >&nbsp;&nbsp;&nbsp;CGPA:<b\r\n                      >{{ item.cgpaScored }}/{{ item.cgpaMax }}</b\r\n                    ></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.percentile\" class=\"mb-1 margin\"\r\n                    >&nbsp;&nbsp;&nbsp;Percentile:<b\r\n                      >{{ item.percentile }}%</b\r\n                    ></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.percentage\" class=\"mb-1 margin\"\r\n                    >&nbsp;&nbsp;&nbsp;Percentage:<b\r\n                      >{{ item.percentile }}%</b\r\n                    ></span\r\n                  >\r\n                  <p *ngIf=\"item.from\" class=\"mb-1 margin\">\r\n                    {{ item.from | date: 'MMM yyyy' }}-{{\r\n                      item.to | date: 'MMM yyyy'\r\n                    }}\r\n                  </p>\r\n                  <p *ngIf=\"item.keyHighlights\" class=\"margin\">\r\n                    Key Highlights : <b>{{ item.keyHighlights }}</b>\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </mat-expansion-panel>\r\n        <br /><br />\r\n\r\n        <br /><br />\r\n\r\n        <!-- --------------------------------------------- -->\r\n\r\n        <!-- Skillsets section  -->\r\n        <br /><br /><br />\r\n\r\n        <!-- ----------------------------  -->\r\n      </div>\r\n\r\n      <div class=\"col-lg-3\">\r\n        <mat-accordion>\r\n          <mat-expansion-panel [expanded]=\"true\">\r\n            <mat-expansion-panel-header\r\n              class=\"bgcolor\"\r\n              expandedHeight=\"45px\"\r\n              collapsedHeight=\"45px\"\r\n            >\r\n              <mat-panel-title>\r\n                Profile Status\r\n              </mat-panel-title>\r\n            </mat-expansion-panel-header>\r\n\r\n            <mat-list class=\"\">\r\n              <mat-list-item style=\"font-size: 10px;\">\r\n                <div class=\"row p-t-10\">\r\n                  <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                    <span\r\n                      [ngClass]=\"\r\n                        (profileStatus?.profileSummary | async) ? '' : 's-c-g'\r\n                      \"\r\n                      >Profile Summary</span\r\n                    >\r\n                  </div>\r\n                  <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                    <img\r\n                      *ngIf=\"profileStatus?.profileSummary | async\"\r\n                      class=\"img-list\"\r\n                      src=\"../../assets/icons/green-sucess.svg\"\r\n                    />\r\n                    <img\r\n                      *ngIf=\"(profileStatus?.profileSummary | async) == null\"\r\n                      class=\"img-list-cancel\"\r\n                      src=\"../../assets/icons/red-fail.svg\"\r\n                    />\r\n                  </div>\r\n                </div>\r\n              </mat-list-item>\r\n              <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n              <mat-list-item style=\"font-size: 10px;\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                    <span\r\n                      [ngClass]=\"\r\n                        (profileStatus?.educationDetails | async) ? '' : 's-c-g'\r\n                      \"\r\n                      >Education details</span\r\n                    >\r\n                  </div>\r\n                  <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                    <img\r\n                      *ngIf=\"profileStatus?.educationDetails | async\"\r\n                      class=\"img-list\"\r\n                      src=\"../../assets/icons/green-sucess.svg\"\r\n                    />\r\n                    <img\r\n                      *ngIf=\"(profileStatus?.educationDetails | async) == null\"\r\n                      class=\"img-list-cancel\"\r\n                      src=\"../../assets/icons/red-fail.svg\"\r\n                    />\r\n                  </div>\r\n                </div>\r\n              </mat-list-item>\r\n            </mat-list>\r\n          </mat-expansion-panel>\r\n        </mat-accordion>\r\n        <br />\r\n\r\n        <br />\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div *ngIf=\"profileData == 'Invent Fund Funder'\">\r\n  <div class=\"container-md\">\r\n    <h5 style=\"font-family: Montserrat; font-weight: 600;\">Profile Overview</h5>\r\n    <br />\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-9\">\r\n        <div class=\"main-card mb-3 card bgcolor mx-auto\">\r\n          <div class=\"card-body p-0\">\r\n            <div class=\"row\">\r\n              <div class=\"col-md-3 col-lg-3 col-xl-3 pr-0 verticalLine\">\r\n                <img\r\n                  src=\"{{ profileImg | async }}\"\r\n                  class=\"rounded-circle imgsize image-fluid mt-5 mx-auto d-block\"\r\n                  alt=\"abc\"\r\n                />\r\n                &nbsp;&nbsp;<br />\r\n\r\n                <!-- <mat-form-field [floatLabel]=\"auto\" class=\"ml-4\">\r\n                        \r\n                                    <mat-select (selectionChange)=\"changeInType($event)\" [(ngModel)]=\"selectedValue\" #matSelect required>\r\n                        \r\n                                        <mat-option *ngFor=\"let type of types\" [value]=\"type.value\">{{type.viewValue}}\r\n                                        </mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field> -->\r\n\r\n                <form name=\"form\" [formGroup]=\"form\">\r\n                  <select\r\n                    formControlName=\"roleType\"\r\n                    (change)=\"changeInType($event.target.value, selectedValue)\"\r\n                    style=\"\r\n                      border: none;\r\n                      background-color: whitesmoke;\r\n                      width: 85%;\r\n                    \"\r\n                    class=\"mb-2 form-control\"\r\n                  >\r\n                    <option *ngFor=\"let type of types\" [value]=\"type.value\"\r\n                      >{{ type.viewValue }}\r\n                    </option>\r\n                  </select>\r\n                  <!-- <mat-select (selectionChange)=\"changeInType($event)\" formControlName=\"roleType\" #matSelect>\r\n                        \r\n                                    <mat-option *ngFor=\"let type of types\" [value]=\"type.value\">{{type.viewValue}}\r\n                                    </mat-option>\r\n                                </mat-select> -->\r\n                </form>\r\n                <br />\r\n                <br />\r\n\r\n                <div class=\"row no-gutters\">\r\n                  <div class=\"col-3\">\r\n                    <!-- <span class=\"Calendar float-right mt-2 mr-2\"></span> -->\r\n                    <span\r\n                      style=\"margin-top: 4px;\"\r\n                      class=\"Calendar float-right\"\r\n                    ></span>\r\n                  </div>\r\n\r\n                  <div class=\"col-9\">\r\n                    <p class=\"mb-5 ml-2 mr-2\">\r\n                      <b> Member Since:</b>\r\n                      {{\r\n                        funderProfileSchema.funderPersonDetails?.createdDate\r\n                          | date: 'MM/dd/yyyy'\r\n                      }}\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div\r\n                *ngIf=\"selectedValue == 'value-0' || selectedValue == 'value-1'\"\r\n                class=\"col-9 col-md-9 col-xl-9 col-lg-9\"\r\n              >\r\n                <div class=\"row\">\r\n                  <div\r\n                    style=\"padding-left: 6px; padding-top: 6px;\"\r\n                    class=\"col-md-9 col-lg-9 col-xl-9 col-sm-4 mt-3 pl-3 sm-align\"\r\n                  >\r\n                    &nbsp;\r\n                    <div\r\n                      *ngIf=\"editmainsection == false\"\r\n                      class=\"d-sm-block d-md-none p-0\"\r\n                    >\r\n                      <button\r\n                        class=\"btn btn-primary btn-right ml-3 mt-4\"\r\n                        style=\"height: 35px; width: 203px; margin-bottom: 20px;\"\r\n                      >\r\n                        <span class=\"linkedin\"></span>\r\n                        <span>Sync your LinkedIn Profile</span>\r\n                      </button>\r\n\r\n                      <span\r\n                        class=\"edit float-right mr-3 mt-4\"\r\n                        (click)=\"onclick()\"\r\n                      ></span>\r\n                    </div>\r\n\r\n                    <h6\r\n                      class=\"mb-0\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      {{ funderProfileSchema.funderPersonDetails?.name }}\r\n                    </h6>\r\n                    <!-- <small>banglore,India</small> -->\r\n                    <h6\r\n                      class=\"mt-3\"\r\n                      *ngIf=\"editmainsection == false\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      Professional Headline\r\n                    </h6>\r\n\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        funderProfileSchema.funderProfileSummary.headline\r\n                      \"\r\n                    >\r\n                      {{ funderProfileSchema.funderProfileSummary.headline }}\r\n                    </p>\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        !funderProfileSchema.funderProfileSummary.headline\r\n                      \"\r\n                    >\r\n                      Enter Headline(Not more than 75 words)\r\n                    </p>\r\n                    <h6\r\n                      class=\"mt-3\"\r\n                      *ngIf=\"editmainsection == false\"\r\n                      style=\"font-family: 'Montserrat'; font-weight: 600;\"\r\n                    >\r\n                      Professional Summary\r\n                    </h6>\r\n\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        funderProfileSchema.funderProfileSummary\r\n                          .professionalSummary\r\n                      \"\r\n                    >\r\n                      {{\r\n                        funderProfileSchema.funderProfileSummary\r\n                          .professionalSummary\r\n                      }}\r\n                    </p>\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        !funderProfileSchema.funderProfileSummary\r\n                          .professionalSummary\r\n                      \"\r\n                    >\r\n                      Enter Summary(Not more than 300 words)\r\n                    </p>\r\n                  </div>\r\n\r\n                  <div class=\"col-md-3 col-xl-3 col-lg-3 sm-none\">\r\n                    <button\r\n                      class=\"btn btn-primary btn-right mr-3 mt-4\"\r\n                      style=\"float: right; height: 35px; width: 203px;\"\r\n                    >\r\n                      <span class=\"linkedin\"></span>\r\n                      <span>Sync your LinkedIn Profile</span>\r\n                    </button>\r\n                    <br /><br />\r\n                    <span class=\"edit mt-4 ml-5\" (click)=\"onclick()\"></span>\r\n                  </div>\r\n\r\n                  <div\r\n                    class=\"col-md-11 col-lg-11 col-xl-11 p-0\"\r\n                    *ngIf=\"editmainsection == true\"\r\n                  >\r\n                    <app-ct-form-builder\r\n                      [formFieldsJson]=\"profileSummaryJson\"\r\n                      [modelData]=\"modelData\"\r\n                      (saveDataEmitter)=\"createProfileSummaryData($event)\"\r\n                      (updateDataEmitter)=\"updateProfileSummaryData($event)\"\r\n                      (cancelDataEmitter)=\"cancelProfileSummary($event)\"\r\n                    >\r\n                    </app-ct-form-builder>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div\r\n                *ngIf=\"selectedValue == 'value-2' || selectedValue == 'value-3'\"\r\n                class=\"col-md-9 col-xl-9 col-lg-9\"\r\n              >\r\n                <div class=\"row\">\r\n                  <div\r\n                    style=\"padding-left: 6px; padding-top: 6px;\"\r\n                    class=\"col-md-9 col-lg-9 col-xl-9 col-sm-4 mt-3 pl-3 sm-align\"\r\n                  >\r\n                    &nbsp;\r\n                    <div\r\n                      *ngIf=\"editmainsection == false\"\r\n                      class=\"d-sm-block d-md-none p-0\"\r\n                    >\r\n                      <button\r\n                        class=\"btn btn-primary btn-right ml-3 mt-4\"\r\n                        style=\"height: 35px; width: 203px; margin-bottom: 20px;\"\r\n                      >\r\n                        <span class=\"linkedin\"></span>\r\n                        <span>Sync your LinkedIn Profile</span>\r\n                      </button>\r\n\r\n                      <span\r\n                        class=\"edit float-right mr-3 mt-4\"\r\n                        (click)=\"onclick()\"\r\n                      ></span>\r\n                    </div>\r\n\r\n                    <h6\r\n                      class=\"mb-0\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      {{ funderProfileSchema.funderPersonDetails?.name }}\r\n                    </h6>\r\n                    <!-- <small>banglore,India</small> -->\r\n                    <h6\r\n                      class=\"mt-3\"\r\n                      *ngIf=\"editmainsection == false\"\r\n                      style=\"font-family: montserrat; font-weight: 600;\"\r\n                    >\r\n                      Organization Headline\r\n                    </h6>\r\n\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        funderProfileSchema.funderProfileSummary.headline\r\n                      \"\r\n                    >\r\n                      {{ funderProfileSchema.funderProfileSummary.headline }}\r\n                    </p>\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        !funderProfileSchema.funderProfileSummary.headline\r\n                      \"\r\n                    >\r\n                      Enter Headline(Not more than 75 words)\r\n                    </p>\r\n                    <h6\r\n                      class=\"mt-3\"\r\n                      *ngIf=\"editmainsection == false\"\r\n                      style=\"font-family: 'Montserrat'; font-weight: 600;\"\r\n                    >\r\n                      Organization Summary\r\n                    </h6>\r\n\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        funderProfileSchema.funderProfileSummary\r\n                          .professionalSummary\r\n                      \"\r\n                    >\r\n                      {{\r\n                        funderProfileSchema.funderProfileSummary\r\n                          .professionalSummary\r\n                      }}\r\n                    </p>\r\n                    <p\r\n                      class=\"mt-0\"\r\n                      *ngIf=\"\r\n                        editmainsection == false &&\r\n                        !funderProfileSchema.funderProfileSummary\r\n                          .professionalSummary\r\n                      \"\r\n                    >\r\n                      Enter Summary(Not more than 300 words)\r\n                    </p>\r\n                  </div>\r\n\r\n                  <div class=\"col-md-3 col-xl-3 col-lg-3 sm-none\">\r\n                    <button\r\n                      class=\"btn btn-primary btn-right mr-3 mt-4\"\r\n                      style=\"float: right; height: 35px; width: 203px;\"\r\n                    >\r\n                      <span class=\"linkedin\"></span>\r\n                      <span>Sync your LinkedIn Profile</span>\r\n                    </button>\r\n                    <br /><br />\r\n                    <span class=\"edit mt-4 ml-5\" (click)=\"onclick()\"></span>\r\n                  </div>\r\n\r\n                  <div\r\n                    class=\"col-md-11 col-lg-11 col-xl-11 p-0\"\r\n                    *ngIf=\"editmainsection == true\"\r\n                  >\r\n                    <app-ct-form-builder\r\n                      [formFieldsJson]=\"profileSummaryJson\"\r\n                      [modelData]=\"modelData\"\r\n                      (saveDataEmitter)=\"createProfileSummaryData($event)\"\r\n                      (updateDataEmitter)=\"updateProfileSummaryData($event)\"\r\n                      (cancelDataEmitter)=\"cancelProfileSummary($event)\"\r\n                    >\r\n                    </app-ct-form-builder>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <br />\r\n        <!--About Organization-->\r\n        <mat-expansion-panel\r\n          [expanded]=\"true\"\r\n          *ngIf=\"selectedValue == 'value-2' || selectedValue == 'value-3'\"\r\n        >\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading pb-0\">\r\n              <span class=\"profileSummary\">\r\n                <span class=\"organize\"></span>\r\n              </span>\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">About Organization</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\">\r\n                <button\r\n                  (click)=\"\r\n                    openModal({\r\n                      profileSummary: modelData?.profileSummary,\r\n                      heading: 'Edit About Organization'\r\n                    })\r\n                  \"\r\n                  class=\"btn-icon btn btn-link\"\r\n                  style=\"height: 15px;\"\r\n                >\r\n                  <span class=\"edit\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <br />\r\n          <p *ngIf=\"!funderProfileSchema.funderProfileSummary.profileSummary\">\r\n            Free text write a paragragh About, experience summary, Specialities,\r\n            Key highlights etc.<br />\r\n            (Not more than 1000 words)\r\n          </p>\r\n          <p *ngIf=\"funderProfileSchema.funderProfileSummary.profileSummary\">\r\n            {{ funderProfileSchema.funderProfileSummary.profileSummary }}\r\n          </p>\r\n        </mat-expansion-panel>\r\n        <br />\r\n        <!-- Showcase  -->\r\n        <mat-expansion-panel\r\n          *ngIf=\"selectedValue == 'value-0' || selectedValue == 'value-1'\"\r\n        >\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading\">\r\n              <span class=\"showcase\"></span>\r\n\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Showcase</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\">\r\n                <button\r\n                  (click)=\"openModal({ showcase: '', heading: 'Add Showcase' })\"\r\n                  class=\"btn-icon btn btn-link\"\r\n                  style=\"height: 8px;\"\r\n                >\r\n                  <span class=\"more\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <br />\r\n\r\n          <app-carousel\r\n            *ngIf=\"show && showcaseArray\"\r\n            #carousel\r\n            [showControls]=\"false\"\r\n          >\r\n            <!-- {{showcaseArray | json}} -->\r\n            <ng-container *ngFor=\"let slide of showcaseArray\">\r\n              <div\r\n                *carouselSlide\r\n                class=\"mx-2 card shadow-none testimonial-carousel\"\r\n              >\r\n                <!-- {{slide?.fileType}} -->\r\n                <img\r\n                  class=\"close\"\r\n                  src=\"../../../assets/images-ui/delete.svg\"\r\n                  style=\"cursor: pointer;\"\r\n                  (click)=\"deleteCarosulDoc(slide)\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'application/pdf'\"\r\n                />\r\n                <img\r\n                  class=\"image-center border-light\"\r\n                  style=\"height: 140px; width: 120px;\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'application/pdf'\"\r\n                  src=\"../../../assets/images-ui/pdf-image-viewer.png\"\r\n                  alt=\"...\"\r\n                  (click)=\"emitImageObject(slide)\"\r\n                />\r\n                <img\r\n                  class=\"close\"\r\n                  src=\"../../../assets/images-ui/delete.svg\"\r\n                  style=\"cursor: pointer;\"\r\n                  (click)=\"deleteCarosulDoc(slide)\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'image/jpeg'\"\r\n                />\r\n\r\n                <img\r\n                  class=\"border-light\"\r\n                  style=\"height: 140px; width: 120px;\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'image/jpeg'\"\r\n                  [src]=\"slide?.url\"\r\n                  alt=\"...\"\r\n                />\r\n\r\n                <img\r\n                  class=\"close z-index\"\r\n                  src=\"../../../assets/images-ui/delete.svg\"\r\n                  style=\"cursor: pointer;\"\r\n                  (click)=\"deleteCarosulDoc(slide)\"\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                />\r\n                <video\r\n                  class=\"border-light\"\r\n                  style=\"height: 134px; width: 120px;\"\r\n                  controls\r\n                  *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                >\r\n                  <source\r\n                    [src]=\"slide?.url\"\r\n                    *ngIf=\"slide?.url && slide?.fileType == 'video/mp4'\"\r\n                    type=\"video/mp4\"\r\n                  />\r\n                </video>\r\n\r\n                <div class=\"\">\r\n                  <p\r\n                    style=\"\r\n                      text-align: center;\r\n                      font-weight: 500;\r\n                      word-wrap: break-word;\r\n                    \"\r\n                    class=\"card-text des-p-t-w\"\r\n                  >\r\n                    {{ slide.description }}\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </ng-container>\r\n\r\n            <div class=\"prev-button\" (click)=\"carousel.prev()\">\r\n              <a class=\"carousel-control-prev-icon\"></a>\r\n            </div>\r\n            <div class=\"next-button\" (click)=\"carousel.next()\">\r\n              <a class=\"carousel-control-next-icon\"></a>\r\n            </div>\r\n          </app-carousel>\r\n        </mat-expansion-panel>\r\n\r\n        <br />\r\n\r\n        <!-- Profile Summary Section  -->\r\n\r\n        <mat-expansion-panel\r\n          [expanded]=\"true\"\r\n          *ngIf=\"selectedValue == 'value-0' || selectedValue == 'value-1'\"\r\n        >\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading pb-0\">\r\n              <span class=\"profileSummary\">\r\n                <span class=\"profileSum\"></span>\r\n              </span>\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Profile Summary</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\">\r\n                <button\r\n                  (click)=\"\r\n                    openModal({\r\n                      profileSummary: modelData?.profileSummary,\r\n                      heading: 'Edit Profile Summary'\r\n                    })\r\n                  \"\r\n                  class=\"btn-icon btn btn-link\"\r\n                  style=\"height: 15px;\"\r\n                >\r\n                  <span class=\"edit\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n          <br />\r\n          <p *ngIf=\"!funderProfileSchema.funderProfileSummary.profileSummary\">\r\n            Free text write a paragragh About, experience summary, Specialities,\r\n            Key highlights etc.<br />\r\n            (Not more than 1000 words)\r\n          </p>\r\n          <p *ngIf=\"funderProfileSchema.funderProfileSummary.profileSummary\">\r\n            {{ funderProfileSchema.funderProfileSummary.profileSummary }}\r\n          </p>\r\n        </mat-expansion-panel>\r\n\r\n        <br />\r\n        <!-- Education Details Summary Section  -->\r\n\r\n        <mat-expansion-panel\r\n          *ngIf=\"selectedValue == 'value-0' || selectedValue == 'value-1'\"\r\n        >\r\n          <mat-expansion-panel-header\r\n            expandedHeight=\"45px\"\r\n            collapsedHeight=\"45px\"\r\n            class=\"pl-0 bgcolor\"\r\n          >\r\n            <mat-panel-title class=\"accordion-heading pb-0\">\r\n              <span class=\"education\"><span class=\"education1\"></span></span>\r\n              <span class=\"headerFont\">\r\n                <span class=\"m-h\">Education Details</span>\r\n              </span>\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n              <div class=\"btn-actions-pane-right\" style=\"margin-top: 8px;\">\r\n                <button\r\n                  (click)=\"\r\n                    openModal({\r\n                      educationalDetails: '',\r\n                      heading: 'Add Educational Details'\r\n                    })\r\n                  \"\r\n                  class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n                >\r\n                  <span class=\"more mt-1\"></span>\r\n                </button>\r\n              </div>\r\n            </mat-panel-description>\r\n          </mat-expansion-panel-header>\r\n\r\n          <div\r\n            style=\"margin-top: 12px;\"\r\n            *ngFor=\"let item of funderProfileSchema.funderEducationalDetails\"\r\n            class=\"mb-3\"\r\n          >\r\n            <div *ngIf=\"item.personId\">\r\n              <button\r\n                style=\"float: right;\"\r\n                (click)=\"\r\n                  openModal({\r\n                    updateData: item,\r\n                    educationalDetails: '',\r\n                    heading: 'Edit Education Details'\r\n                  })\r\n                \"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"edit\"></span>\r\n              </button>\r\n              <button\r\n                style=\"float: right;\"\r\n                (click)=\"deleteEducationData(item._id)\"\r\n                class=\"mb-2 mr-2 btn-icon btn btn-link\"\r\n              >\r\n                <span class=\"trash\"></span>\r\n              </button>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-10\">\r\n                  <p *ngIf=\"item?.college\" class=\"font-weight-bold mb-1 margin\">\r\n                    {{ item?.college }}\r\n                  </p>\r\n                  <p\r\n                    *ngIf=\"item?.education && item.specialization\"\r\n                    class=\"mb-1 margin\"\r\n                  >\r\n                    {{ item.education }},&nbsp;{{ item.specialization }}\r\n                  </p>\r\n                  <span *ngIf=\"item.grade\" class=\"mb-1 margin\"\r\n                    >Grade:<b>{{ item.grade }}</b></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.cgpaScored\" class=\"mb-1 margin\"\r\n                    >&nbsp;&nbsp;&nbsp;CGPA:<b\r\n                      >{{ item.cgpaScored }}/{{ item.cgpaMax }}</b\r\n                    ></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.percentile\" class=\"mb-1 margin\"\r\n                    >&nbsp;&nbsp;&nbsp;Percentile:<b\r\n                      >{{ item.percentile }}%</b\r\n                    ></span\r\n                  >\r\n                  &nbsp;&nbsp;&nbsp;\r\n                  <span *ngIf=\"item.percentage\" class=\"mb-1 margin\"\r\n                    >&nbsp;&nbsp;&nbsp;Percentage:<b\r\n                      >{{ item.percentile }}%</b\r\n                    ></span\r\n                  >\r\n                  <p *ngIf=\"item.from\" class=\"mb-1 margin\">\r\n                    {{ item.from | date: 'MMM yyyy' }}-{{\r\n                      item.to | date: 'MMM yyyy'\r\n                    }}\r\n                  </p>\r\n                  <p *ngIf=\"item.keyHighlights\" class=\"margin\">\r\n                    Key Highlights : <b>{{ item.keyHighlights }}</b>\r\n                  </p>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </mat-expansion-panel>\r\n        <br />\r\n      </div>\r\n\r\n      <!-- {{profileStatus?.profileSummary | async}} -->\r\n\r\n      <div\r\n        class=\"col-lg-3\"\r\n        *ngIf=\"selectedValue == 'value-0' || selectedValue == 'value-1'\"\r\n      >\r\n        <mat-accordion>\r\n          <mat-expansion-panel [expanded]=\"true\">\r\n            <mat-expansion-panel-header\r\n              class=\"bgcolor\"\r\n              expandedHeight=\"45px\"\r\n              collapsedHeight=\"45px\"\r\n            >\r\n              <mat-panel-title>\r\n                Profile Status\r\n              </mat-panel-title>\r\n            </mat-expansion-panel-header>\r\n            <mat-list class=\"\">\r\n              <mat-list-item style=\"font-size: 10px;\">\r\n                <div class=\"row p-t-10\">\r\n                  <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                    <span\r\n                      [ngClass]=\"\r\n                        (profileStatus?.profileSummary | async) ? '' : 's-c-g'\r\n                      \"\r\n                      >Profile Summary</span\r\n                    >\r\n                  </div>\r\n                  <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                    <img\r\n                      *ngIf=\"profileStatus?.profileSummary | async\"\r\n                      class=\"img-list\"\r\n                      src=\"../../assets/icons/green-sucess.svg\"\r\n                    />\r\n                    <img\r\n                      *ngIf=\"(profileStatus?.profileSummary | async) == null\"\r\n                      class=\"img-list-cancel\"\r\n                      src=\"../../assets/icons/red-fail.svg\"\r\n                    />\r\n                  </div>\r\n                </div>\r\n              </mat-list-item>\r\n              <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n              <mat-list-item style=\"font-size: 10px;\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                    <span\r\n                      [ngClass]=\"\r\n                        (profileStatus?.educationDetails | async) ? '' : 's-c-g'\r\n                      \"\r\n                      >Education details</span\r\n                    >\r\n                  </div>\r\n                  <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                    <img\r\n                      *ngIf=\"profileStatus?.educationDetails | async\"\r\n                      class=\"img-list\"\r\n                      src=\"../../assets/icons/green-sucess.svg\"\r\n                    />\r\n                    <img\r\n                      *ngIf=\"(profileStatus?.educationDetails | async) == null\"\r\n                      class=\"img-list-cancel\"\r\n                      src=\"../../assets/icons/red-fail.svg\"\r\n                    />\r\n                  </div>\r\n                </div>\r\n              </mat-list-item>\r\n              <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n\r\n              <!-- {{profileStatus?.achievementsDetails | async}} -->\r\n            </mat-list>\r\n          </mat-expansion-panel>\r\n        </mat-accordion>\r\n        <br />\r\n      </div>\r\n\r\n      <div\r\n        class=\"col-lg-3\"\r\n        *ngIf=\"selectedValue == 'value-2' || selectedValue == 'value-3'\"\r\n      >\r\n        <mat-accordion>\r\n          <mat-expansion-panel [expanded]=\"true\">\r\n            <mat-expansion-panel-header\r\n              class=\"bgcolor\"\r\n              expandedHeight=\"45px\"\r\n              collapsedHeight=\"45px\"\r\n            >\r\n              <mat-panel-title>\r\n                Profile Status\r\n              </mat-panel-title>\r\n            </mat-expansion-panel-header>\r\n\r\n            <mat-list class=\"\">\r\n              <mat-list-item style=\"font-size: 10px;\">\r\n                <div class=\"row p-t-10\">\r\n                  <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-8\">\r\n                    <span\r\n                      [ngClass]=\"\r\n                        (profileStatus?.profileSummary | async) ? '' : 's-c-g'\r\n                      \"\r\n                      >About Organization</span\r\n                    >\r\n                  </div>\r\n                  <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">\r\n                    <img\r\n                      *ngIf=\"profileStatus?.profileSummary | async\"\r\n                      class=\"img-list\"\r\n                      src=\"../../assets/icons/green-sucess.svg\"\r\n                    />\r\n                    <img\r\n                      *ngIf=\"(profileStatus?.profileSummary | async) == null\"\r\n                      class=\"img-list-cancel\"\r\n                      src=\"../../assets/icons/red-fail.svg\"\r\n                    />\r\n                  </div>\r\n                </div>\r\n              </mat-list-item>\r\n              <mat-divider class=\"b-c-b\"></mat-divider><br />\r\n            </mat-list>\r\n          </mat-expansion-panel>\r\n        </mat-accordion>\r\n        <br />\r\n        <br />\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/profile/profile-page/profile-page.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/profile/profile-page/profile-page.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".form-control.is-valid {\n  background-image: none; }\n\n.margin-auto {\n  margin-top: auto;\n  margin-bottom: auto; }\n\nimg.a {\n  display: block;\n  height: auto;\n  max-width: 100%;\n  line-height: 1;\n  margin: auto;\n  width: 100%;\n  height: 100%; }\n\n.iconsize {\n  opacity: 0.5; }\n\nspan.linkedin {\n  background: url('linkedin.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 15px;\n  width: 15px;\n  margin-right: 3px; }\n\n.aTag {\n  color: blue;\n  text-decoration: underline; }\n\n.accordion-heading {\n  color: rgba(13, 27, 62, 0.7) !important;\n  font-weight: bold !important;\n  font-size: 0.88rem !important; }\n\n.box {\n  max-height: 250px;\n  max-width: 250px; }\n\n.iconhover:hover {\n  color: blue; }\n\n.profile {\n  color: white;\n  width: 50px;\n  height: 50px;\n  padding-left: 10px; }\n\n.headerFont {\n  font-family: Montserrat;\n  display: inline-block;\n  margin-top: 15px;\n  margin-left: 5px;\n  font-weight: 700; }\n\nspan.profileSummary {\n  background-color: #268aff;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 52px;\n  z-index: 0; }\n\nspan.profileSum {\n  background: url('profileSum.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 32px;\n  width: 38px;\n  z-index: 1;\n  margin-left: 12px;\n  margin-top: 7px; }\n\nspan.edit {\n  background: url('edit.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 15px;\n  width: 15px; }\n\nspan.trash {\n  background: url('trash.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 15px;\n  width: 15px; }\n\nspan.Calendar {\n  background: url('calendar.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 20px;\n  width: 20px; }\n\nspan.professional {\n  background: url('professional.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 50px;\n  z-index: 0; }\n\nspan.showcase {\n  background: url('showcase.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 50px;\n  width: 50px;\n  z-index: 0; }\n\nspan.experience {\n  background: url('experience.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 7px;\n  margin-top: 5px; }\n\nspan.education {\n  background: url('professional.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 50px;\n  width: 50px;\n  z-index: 0; }\n\nspan.education1 {\n  background: url('page-1.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 32px;\n  width: 35px;\n  z-index: 1;\n  margin-left: 7px;\n  margin-top: 7px; }\n\nspan.organize {\n  background: url('organization.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 40px;\n  width: 32px;\n  z-index: 1;\n  margin-left: 9px;\n  margin-top: 5px; }\n\nspan.skill {\n  background: url('worker.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 32px;\n  width: 35px;\n  z-index: 1;\n  margin-left: 7px;\n  margin-top: 5px; }\n\nspan.skill {\n  background: url('worker.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 32px;\n  width: 35px;\n  z-index: 1;\n  margin-left: 8px;\n  margin-top: 5px; }\n\nspan.skills {\n  background: url('skills.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 7px;\n  margin-top: 5px; }\n\nspan.more {\n  background: url('more.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 13px;\n  width: 13px;\n  position: relative;\n  top: 2px; }\n\nspan.delete {\n  background: url('delete.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 15px;\n  width: 15px; }\n\nspan.achiev {\n  background: url('achiev.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 45px;\n  width: 50px;\n  z-index: 0; }\n\n.imgsize {\n  display: inline-block;\n  height: 120px;\n  width: 120px; }\n\nspan.achievement {\n  background: url('achievement.svg') no-repeat top left;\n  background-size: contain;\n  display: inline-block;\n  height: 35px;\n  width: 37px;\n  z-index: 1;\n  margin-left: 7px;\n  margin-top: 5px; }\n\n.cent {\n  display: flex;\n  justify-content: center;\n  align-items: center; }\n\n.verticalLine {\n  border-right: 1px solid lightgrey;\n  height: 100%; }\n\n.bgcolor {\n  background-color: whitesmoke; }\n\n.margin {\n  margin-left: -10px; }\n\n.margin1 {\n  margin-left: -20px;\n  margin-right: 3px; }\n\n::ng-deep .modal-footer {\n  background: #f5f5f5 !important; }\n\n.m-h {\n  padding-left: 4px;\n  position: relative;\n  bottom: 2px;\n  font-size: 16px;\n  font-family: Montserrat;\n  font-weight: 550;\n  color: #3b3b3b; }\n\n.img-list {\n  width: 16px !important;\n  float: right !important; }\n\n.img-list-cancel {\n  width: 12px !important;\n  float: right !important; }\n\n.p-t-10 {\n  padding-top: 10px !important; }\n\n::ng-deep .b-c-g {\n  border-top-color: #a7a7a7 !important; }\n\n::ng-deep .b-c-b {\n  border-top-color: #268aff !important; }\n\n::ng-deep .s-c-g {\n  color: #a7a7a7; }\n\n@media only screen and (max-width: 425px) and (min-width: 320px) {\n  .sm-none {\n    display: none; }\n  .sm-align {\n    margin-top: 0px !important;\n    padding-top: 0; }\n  .mb-5 {\n    margin-bottom: 0px !important; } }\n\n.mat-option-text {\n  flex-grow: 1;\n  display: flex;\n  align-items: center; }\n\n.testimonial-carousel {\n  display: inline-block; }\n\n.prev-button {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  z-index: 3;\n  color: black;\n  line-height: 200px; }\n\n.next-button {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  right: 0;\n  margin: auto;\n  z-index: 2;\n  color: black;\n  line-height: 200px; }\n\n.image-center {\n  margin-left: auto !important;\n  margin-right: auto !important;\n  display: block; }\n\n.close {\n  position: absolute;\n  right: 0; }\n\n.des-p-t-w {\n  padding-top: 8px;\n  width: 120px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis; }\n\n::ng-deep .border-light {\n  border: 2px solid #f4f9ff !important;\n  border-radius: 6px !important; }\n\n.z-index {\n  z-index: 1; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLXBhZ2UvRDpcXFdvcmtcXFByb2plY3RzXFxJTlZFTlRGVU5EXFxCUkFOQ0hFU1xcaW52ZW50ZnVuZC10ZXN0L3NyY1xcYXBwXFxwcm9maWxlXFxwcm9maWxlLXBhZ2VcXHByb2ZpbGUtcGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHNCQUFzQixFQUFBOztBQUV2QjtFQUNDLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFHcEI7RUFDQyxjQUFjO0VBQ2QsWUFBWTtFQUNaLGVBQWU7RUFDZixjQUFjO0VBQ2QsWUFBWTtFQUNaLFdBQVc7RUFDWCxZQUFZLEVBQUE7O0FBR2I7RUFJQyxZQUFZLEVBQUE7O0FBS2I7RUFDQyxrREFBd0U7RUFDeEUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLGlCQUFpQixFQUFBOztBQUdsQjtFQUNDLFdBQVc7RUFDWCwwQkFBMEIsRUFBQTs7QUFHM0I7RUFDQyx1Q0FBdUM7RUFDdkMsNEJBQTRCO0VBQzVCLDZCQUE2QixFQUFBOztBQUk5QjtFQUNDLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFHakI7RUFDQyxXQUFXLEVBQUE7O0FBR1o7RUFFQyxZQUFZO0VBRVosV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyx1QkFBdUI7RUFDdkIscUJBQXFCO0VBRXJCLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBR2pCO0VBRUMseUJBQXlCO0VBQ3pCLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVLEVBQUE7O0FBR1g7RUFDQyxvREFBMEU7RUFDMUUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixpQkFBaUI7RUFDakIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLDhDQUFvRTtFQUNwRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBR1o7RUFDQywrQ0FBcUU7RUFDckUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVyxFQUFBOztBQUdaO0VBQ0Msa0RBQXdFO0VBQ3hFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFHWjtFQUNDLHNEQUE0RTtFQUM1RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVSxFQUFBOztBQUdYO0VBQ0Msa0RBQXdFO0VBQ3hFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVLEVBQUE7O0FBR1g7RUFDQyxvREFBMEU7RUFDMUUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLHNEQUE0RTtFQUM1RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVSxFQUFBOztBQUdYO0VBQ0MsZ0RBQXNFO0VBQ3RFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxzREFBNEU7RUFDNUUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLGdEQUFzRTtFQUN0RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBS2hCO0VBQ0MsZ0RBQXNFO0VBQ3RFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHaEI7RUFFQyxnREFBc0U7RUFDdEUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUdoQjtFQUNDLDhDQUFvRTtFQUNwRSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLFFBQVEsRUFBQTs7QUFHVDtFQUNDLGdEQUFzRTtFQUN0RSx3QkFBd0I7RUFDeEIscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBR1o7RUFDQyxnREFBc0U7RUFDdEUsd0JBQXdCO0VBQ3hCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osV0FBVztFQUNYLFVBQVUsRUFBQTs7QUFHWDtFQUNDLHFCQUFxQjtFQUNyQixhQUFhO0VBQ2IsWUFBWSxFQUFBOztBQUdiO0VBQ0MscURBQTJFO0VBQzNFLHdCQUF3QjtFQUN4QixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLFdBQVc7RUFDWCxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQixFQUFBOztBQUdwQjtFQUNDLGlDQUFpQztFQUVqQyxZQUFZLEVBQUE7O0FBR2I7RUFDQyw0QkFBNEIsRUFBQTs7QUFHN0I7RUFDQyxrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyxrQkFBa0I7RUFDbEIsaUJBQWlCLEVBQUE7O0FBR2xCO0VBQ0MsOEJBQThCLEVBQUE7O0FBRy9CO0VBQ0MsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUtsQixXQUFXO0VBQ1gsZUFBZTtFQUNmLHVCQUF1QjtFQUN2QixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUdmO0VBQ0Msc0JBQXNCO0VBQ3RCLHVCQUF1QixFQUFBOztBQUd4QjtFQUNDLHNCQUFzQjtFQUN0Qix1QkFBdUIsRUFBQTs7QUFHeEI7RUFDQyw0QkFBNEIsRUFBQTs7QUFHN0I7RUFDQyxvQ0FBb0MsRUFBQTs7QUFHckM7RUFDQyxvQ0FBb0MsRUFBQTs7QUFHckM7RUFDQyxjQUFjLEVBQUE7O0FBR2Y7RUFDQztJQUNDLGFBQWEsRUFBQTtFQUVkO0lBQ0MsMEJBQTBCO0lBQzFCLGNBQWMsRUFBQTtFQUVmO0lBQ0MsNkJBQTZCLEVBQUEsRUFDN0I7O0FBR0Y7RUFDQyxZQUFZO0VBQ1osYUFBYTtFQUNiLG1CQUFtQixFQUFBOztBQUdwQjtFQUVDLHFCQUFxQixFQUFBOztBQUd0QjtFQUNDLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sU0FBUztFQUNULE9BQU87RUFDUCxZQUFZO0VBQ1osVUFBVTtFQUNWLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyxrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLFNBQVM7RUFDVCxRQUFRO0VBQ1IsWUFBWTtFQUNaLFVBQVU7RUFDVixZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBU25CO0VBQ0MsNEJBQTRCO0VBQzVCLDZCQUE2QjtFQUM3QixjQUFjLEVBQUE7O0FBR2I7RUFDRCxrQkFBa0I7RUFDbEIsUUFBUSxFQUFBOztBQUdQO0VBQ0QsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixtQkFBbUI7RUFDaEIsZ0JBQWdCO0VBQ2hCLHVCQUF1QixFQUFBOztBQUd6QjtFQUNELG9DQUFvQztFQUNqQyw2QkFBNkIsRUFBQTs7QUFHakM7RUFDQyxVQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUtcGFnZS9wcm9maWxlLXBhZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9ybS1jb250cm9sLmlzLXZhbGlkIHtcclxuXHRiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xyXG59XHJcbi5tYXJnaW4tYXV0byB7XHJcblx0bWFyZ2luLXRvcDogYXV0bztcclxuXHRtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG59XHJcblxyXG5pbWcuYSB7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0aGVpZ2h0OiBhdXRvO1xyXG5cdG1heC13aWR0aDogMTAwJTtcclxuXHRsaW5lLWhlaWdodDogMTtcclxuXHRtYXJnaW46IGF1dG87XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0aGVpZ2h0OiAxMDAlOyAvLyBBZGQgdGhpc1xyXG59XHJcblxyXG4uaWNvbnNpemUge1xyXG5cdC8vIGNvbG9yOiB0cmFuc3BhcmVudDtcclxuXHQvLyAgcG9zaXRpb246IGZpeGVkO1xyXG5cdC8vICB6LWluZGV4OiAxO1xyXG5cdG9wYWNpdHk6IDAuNTtcclxuXHQvLyBvdmVyZmxvdy14OiBoaWRkZW47XHJcblx0Ly8gY29sb3I6IGJsdWU7XHJcbn1cclxuXHJcbnNwYW4ubGlua2VkaW4ge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2xpbmtlZGluLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMTVweDtcclxuXHR3aWR0aDogMTVweDtcclxuXHRtYXJnaW4tcmlnaHQ6IDNweDtcclxufVxyXG5cclxuLmFUYWcge1xyXG5cdGNvbG9yOiBibHVlO1xyXG5cdHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG59XHJcblxyXG4uYWNjb3JkaW9uLWhlYWRpbmcge1xyXG5cdGNvbG9yOiByZ2JhKDEzLCAyNywgNjIsIDAuNykgIWltcG9ydGFudDtcclxuXHRmb250LXdlaWdodDogYm9sZCAhaW1wb3J0YW50O1xyXG5cdGZvbnQtc2l6ZTogMC44OHJlbSAhaW1wb3J0YW50O1xyXG5cdC8vICBwYWRkaW5nLXRvcDogNnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5ib3gge1xyXG5cdG1heC1oZWlnaHQ6IDI1MHB4O1xyXG5cdG1heC13aWR0aDogMjUwcHg7XHJcbn1cclxuXHJcbi5pY29uaG92ZXI6aG92ZXIge1xyXG5cdGNvbG9yOiBibHVlO1xyXG59XHJcblxyXG4ucHJvZmlsZSB7XHJcblx0Ly9iYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTcsIDE3LCAyMDQpO1xyXG5cdGNvbG9yOiB3aGl0ZTtcclxuXHQvLyBvcGFjaXR5OiAwLjU7XHJcblx0d2lkdGg6IDUwcHg7XHJcblx0aGVpZ2h0OiA1MHB4O1xyXG5cdHBhZGRpbmctbGVmdDogMTBweDtcclxufVxyXG5cclxuLmhlYWRlckZvbnQge1xyXG5cdGZvbnQtZmFtaWx5OiBNb250c2VycmF0O1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHQvL2ZvbnQtc2l6ZTogMThweDtcclxuXHRtYXJnaW4tdG9wOiAxNXB4O1xyXG5cdG1hcmdpbi1sZWZ0OiA1cHg7XHJcblx0Zm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuc3Bhbi5wcm9maWxlU3VtbWFyeSB7XHJcblx0Ly8gYmFja2dyb3VuZDogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Byb2ZpbGVTdW1tYXJ5LnN2Z1wiKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogIzI2OGFmZjtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogNDVweDtcclxuXHR3aWR0aDogNTJweDtcclxuXHR6LWluZGV4OiAwO1xyXG59XHJcblxyXG5zcGFuLnByb2ZpbGVTdW0ge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Byb2ZpbGVTdW0uc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzMnB4O1xyXG5cdHdpZHRoOiAzOHB4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDEycHg7XHJcblx0bWFyZ2luLXRvcDogN3B4O1xyXG59XHJcblxyXG5zcGFuLmVkaXQge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2VkaXQuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAxNXB4O1xyXG5cdHdpZHRoOiAxNXB4O1xyXG59XHJcblxyXG5zcGFuLnRyYXNoIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy90cmFzaC5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDE1cHg7XHJcblx0d2lkdGg6IDE1cHg7XHJcbn1cclxuXHJcbnNwYW4uQ2FsZW5kYXIge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL2NhbGVuZGFyLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMjBweDtcclxuXHR3aWR0aDogMjBweDtcclxufVxyXG5cclxuc3Bhbi5wcm9mZXNzaW9uYWwge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Byb2Zlc3Npb25hbC5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDQ1cHg7XHJcblx0d2lkdGg6IDUwcHg7XHJcblx0ei1pbmRleDogMDtcclxufVxyXG5cclxuc3Bhbi5zaG93Y2FzZSB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvc2hvd2Nhc2Uuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiA1MHB4O1xyXG5cdHdpZHRoOiA1MHB4O1xyXG5cdHotaW5kZXg6IDA7XHJcbn1cclxuXHJcbnNwYW4uZXhwZXJpZW5jZSB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvZXhwZXJpZW5jZS5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDM1cHg7XHJcblx0d2lkdGg6IDM3cHg7XHJcblx0ei1pbmRleDogMTtcclxuXHRtYXJnaW4tbGVmdDogN3B4O1xyXG5cdG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuc3Bhbi5lZHVjYXRpb24ge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3Byb2Zlc3Npb25hbC5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDUwcHg7XHJcblx0d2lkdGg6IDUwcHg7XHJcblx0ei1pbmRleDogMDtcclxufVxyXG5cclxuc3Bhbi5lZHVjYXRpb24xIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9wYWdlLTEuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzMnB4O1xyXG5cdHdpZHRoOiAzNXB4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDdweDtcclxuXHRtYXJnaW4tdG9wOiA3cHg7XHJcbn1cclxuXHJcbnNwYW4ub3JnYW5pemUge1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL29yZ2FuaXphdGlvbi5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDQwcHg7XHJcblx0d2lkdGg6IDMycHg7XHJcblx0ei1pbmRleDogMTtcclxuXHRtYXJnaW4tbGVmdDogOXB4O1xyXG5cdG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuc3Bhbi5za2lsbCB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvd29ya2VyLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMzJweDtcclxuXHR3aWR0aDogMzVweDtcclxuXHR6LWluZGV4OiAxO1xyXG5cdG1hcmdpbi1sZWZ0OiA3cHg7XHJcblx0bWFyZ2luLXRvcDogNXB4O1xyXG5cdC8vIG1hcmdpbi1sZWZ0OiA4cHg7XHJcblx0Ly8gbWFyZ2luLXRvcDogN3B4O1xyXG59XHJcblxyXG5zcGFuLnNraWxsIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy93b3JrZXIuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAzMnB4O1xyXG5cdHdpZHRoOiAzNXB4O1xyXG5cdHotaW5kZXg6IDE7XHJcblx0bWFyZ2luLWxlZnQ6IDhweDtcclxuXHRtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbnNwYW4uc2tpbGxzIHtcclxuXHQvLyBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaWNvbnMvZXhwZXJpZW5jZS5zdmdcIikgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ljb25zL3NraWxscy5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDM1cHg7XHJcblx0d2lkdGg6IDM3cHg7XHJcblx0ei1pbmRleDogMTtcclxuXHRtYXJnaW4tbGVmdDogN3B4O1xyXG5cdG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuc3Bhbi5tb3JlIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9tb3JlLnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMTNweDtcclxuXHR3aWR0aDogMTNweDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0dG9wOiAycHg7XHJcbn1cclxuXHJcbnNwYW4uZGVsZXRlIHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9kZWxldGUuc3ZnJykgbm8tcmVwZWF0IHRvcCBsZWZ0O1xyXG5cdGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0aGVpZ2h0OiAxNXB4O1xyXG5cdHdpZHRoOiAxNXB4O1xyXG59XHJcblxyXG5zcGFuLmFjaGlldiB7XHJcblx0YmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaWNvbnMvYWNoaWV2LnN2ZycpIG5vLXJlcGVhdCB0b3AgbGVmdDtcclxuXHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogNDVweDtcclxuXHR3aWR0aDogNTBweDtcclxuXHR6LWluZGV4OiAwO1xyXG59XHJcblxyXG4uaW1nc2l6ZSB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGhlaWdodDogMTIwcHg7XHJcblx0d2lkdGg6IDEyMHB4O1xyXG59XHJcblxyXG5zcGFuLmFjaGlldmVtZW50IHtcclxuXHRiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pY29ucy9hY2hpZXZlbWVudC5zdmcnKSBuby1yZXBlYXQgdG9wIGxlZnQ7XHJcblx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRoZWlnaHQ6IDM1cHg7XHJcblx0d2lkdGg6IDM3cHg7XHJcblx0ei1pbmRleDogMTtcclxuXHRtYXJnaW4tbGVmdDogN3B4O1xyXG5cdG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuLmNlbnQge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLnZlcnRpY2FsTGluZSB7XHJcblx0Ym9yZGVyLXJpZ2h0OiAxcHggc29saWQgbGlnaHRncmV5O1xyXG5cdC8vIGhlaWdodDogMzI2cHg7XHJcblx0aGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uYmdjb2xvciB7XHJcblx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcclxufVxyXG5cclxuLm1hcmdpbiB7XHJcblx0bWFyZ2luLWxlZnQ6IC0xMHB4O1xyXG59XHJcblxyXG4ubWFyZ2luMSB7XHJcblx0bWFyZ2luLWxlZnQ6IC0yMHB4O1xyXG5cdG1hcmdpbi1yaWdodDogM3B4O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1vZGFsLWZvb3RlciB7XHJcblx0YmFja2dyb3VuZDogI2Y1ZjVmNSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubS1oIHtcclxuXHRwYWRkaW5nLWxlZnQ6IDRweDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0Ly8gYm90dG9tOiA0cHg7XHJcblx0Ly8gZm9udC1zaXplOiAyMHB4O1xyXG5cdC8vIGZvbnQtd2VpZ2h0OiA1NDA7XHJcblx0Ly8gZm9udC1mYW1pbHk6IHNhbnMtc2VyaWY7XHJcblx0Ym90dG9tOiAycHg7XHJcblx0Zm9udC1zaXplOiAxNnB4O1xyXG5cdGZvbnQtZmFtaWx5OiBNb250c2VycmF0O1xyXG5cdGZvbnQtd2VpZ2h0OiA1NTA7XHJcblx0Y29sb3I6ICMzYjNiM2I7XHJcbn1cclxuXHJcbi5pbWctbGlzdCB7XHJcblx0d2lkdGg6IDE2cHggIWltcG9ydGFudDtcclxuXHRmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmltZy1saXN0LWNhbmNlbCB7XHJcblx0d2lkdGg6IDEycHggIWltcG9ydGFudDtcclxuXHRmbG9hdDogcmlnaHQgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnAtdC0xMCB7XHJcblx0cGFkZGluZy10b3A6IDEwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5iLWMtZyB7XHJcblx0Ym9yZGVyLXRvcC1jb2xvcjogI2E3YTdhNyAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmItYy1iIHtcclxuXHRib3JkZXItdG9wLWNvbG9yOiAjMjY4YWZmICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAucy1jLWcge1xyXG5cdGNvbG9yOiAjYTdhN2E3O1xyXG59XHJcblxyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDQyNXB4KSBhbmQgKG1pbi13aWR0aDogMzIwcHgpIHtcclxuXHQuc20tbm9uZSB7XHJcblx0XHRkaXNwbGF5OiBub25lO1xyXG5cdH1cclxuXHQuc20tYWxpZ24ge1xyXG5cdFx0bWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcblx0XHRwYWRkaW5nLXRvcDogMDtcclxuXHR9XHJcblx0Lm1iLTUge1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XHJcblx0fVxyXG59XHJcblxyXG4ubWF0LW9wdGlvbi10ZXh0IHtcclxuXHRmbGV4LWdyb3c6IDE7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4udGVzdGltb25pYWwtY2Fyb3VzZWwge1xyXG5cdC8vIHdpZHRoOiAxMjBweDtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5wcmV2LWJ1dHRvbiB7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHRvcDogMDtcclxuXHRib3R0b206IDA7XHJcblx0bGVmdDogMDtcclxuXHRtYXJnaW46IGF1dG87XHJcblx0ei1pbmRleDogMztcclxuXHRjb2xvcjogYmxhY2s7XHJcblx0bGluZS1oZWlnaHQ6IDIwMHB4O1xyXG59XHJcblxyXG4ubmV4dC1idXR0b24ge1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHR0b3A6IDA7XHJcblx0Ym90dG9tOiAwO1xyXG5cdHJpZ2h0OiAwO1xyXG5cdG1hcmdpbjogYXV0bztcclxuXHR6LWluZGV4OiAyO1xyXG5cdGNvbG9yOiBibGFjaztcclxuXHRsaW5lLWhlaWdodDogMjAwcHg7XHJcbn1cclxuXHJcbi8vIC5jYXJvdXNlbC1jb250cm9sLXByZXYtaWNvbixcclxuLy8gLmNhcm91c2VsLWNvbnRyb2wtbmV4dC1pY29uIHtcclxuLy8gXHRiYWNrZ3JvdW5kLWNvbG9yOiAjMDA1OGIwO1xyXG4vLyBcdGJvcmRlci1yYWRpdXM6IDIwJTtcclxuLy8gfVxyXG5cclxuLmltYWdlLWNlbnRlciB7XHJcblx0bWFyZ2luLWxlZnQ6IGF1dG8gIWltcG9ydGFudDtcclxuXHRtYXJnaW4tcmlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxuXHRkaXNwbGF5OiBibG9jaztcclxufVxyXG4gIFxyXG4gIC5jbG9zZSB7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHJpZ2h0OiAwO1xyXG4gIH1cclxuXHJcbiAgLmRlcy1wLXQtdyB7XHJcblx0cGFkZGluZy10b3A6IDhweDtcclxuXHR3aWR0aDogMTIwcHg7XHJcblx0d2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICB9XHJcblxyXG4gIDo6bmctZGVlcCAuYm9yZGVyLWxpZ2h0IHsgICAgXHJcblx0Ym9yZGVyOiAycHggc29saWQgI2Y0ZjlmZiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNnB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuLnotaW5kZXgge1xyXG5cdHotaW5kZXg6IDE7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/profile/profile-page/profile-page.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/profile/profile-page/profile-page.component.ts ***!
  \****************************************************************/
/*! exports provided: ProfilePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageComponent", function() { return ProfilePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _theme_options__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../theme-options */ "./src/app/theme-options.ts");
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../profile-modal/profile-modal.component */ "./src/app/profile/profile-modal/profile-modal.component.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _utilities_profileJson__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../utilities/profileJson */ "./src/app/utilities/profileJson.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");














var ProfilePageComponent = /** @class */ (function () {
    /**
     *
     * @param globals setting global options
     * @param appState get variables and methods from app state
     * @param inventFundsApiService services to call api end points
     * @param router navigate to different routes
     * @param formBuilder reactive form builder
     * @param sanitizer
     * @param modalService open model service for popup
     */
    function ProfilePageComponent(globals, appState, inventFundsApiService, router, formBuilder, sanitizer, modalService, cdr) {
        this.globals = globals;
        this.appState = appState;
        this.inventFundsApiService = inventFundsApiService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.sanitizer = sanitizer;
        this.modalService = modalService;
        this.cdr = cdr;
        this.type = 'Individual/Freelancer';
        this.lookupArray = [];
        this.individualProfessionalDetails = [];
        this.achievementsDetails = [];
        this.educationDetails = [];
        this.fixerProfileSchema = _utilities_profileJson__WEBPACK_IMPORTED_MODULE_11__["fixerProfileSchema"];
        this.founderProfileSchema = _utilities_profileJson__WEBPACK_IMPORTED_MODULE_11__["founderProfileSchema"];
        this.funderProfileSchema = _utilities_profileJson__WEBPACK_IMPORTED_MODULE_11__["funderProfileSchema"];
        this.show = false;
        this.skillSet = [];
        this.skillCategory = [];
        this.skillsDetails = [];
        this.editmainsection = false;
        this.showcaseArray = [];
        this.slideConfig4 = {
            slidesToShow: 3,
            dots: true,
        };
        this.types = [
            { value: 'value-0', viewValue: 'Individual' },
            { value: 'value-1', viewValue: 'Seed Funder' },
            { value: 'value-2', viewValue: 'Organization' },
            { value: 'value-3', viewValue: 'Venture Capital' },
        ];
        this.loading = true;
        this.icon = false;
        this.globals.toggleSidebar = true;
        this.currentRouter = this.router.url;
        this.appState.showSideBar = true;
    }
    // ng onInit
    ProfilePageComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, showCaseData;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.typeOfRole = this.appState.encryptedDataValue('personType');
                        // console.log('this.typeOfRole: ' + this.typeOfRole)
                        this.form = this.formBuilder.group({
                            roleType: [null],
                        });
                        this.currentRouter = this.appState.getRouter();
                        this.retrieveProfileInfo();
                        this.getProfileSummaryJson();
                        this.getProfileSummary();
                        this.getSkillsDetails(null);
                        this.getEducationData();
                        // console.log('profile img:' + localStorage.getItem('profileImage'));
                        this.profileImg = this.appState.getAvatar();
                        _a = this;
                        return [4 /*yield*/, this.retrieveAllProfileInfo().toPromise()];
                    case 1:
                        _a.fullProfileInfo = _b.sent();
                        //  this.fullProfileInfo = profileJson.fixerProfileSchema
                        // console.log('fullProfileInfo:' + JSON.stringify(this.fullProfileInfo))
                        if (this.typeOfRole == 'Invent Fund Fixer') {
                            // console.log('Fixer data : ' + JSON.stringify(this.fullProfileInfo));
                            Object.assign(this.fixerProfileSchema.fixerPersonDetails, this.fullProfileInfo[0].response[0]);
                            Object.assign(this.fixerProfileSchema.fixerProfileSummary, this.fullProfileInfo[1].response[0]);
                            Object.assign(this.fixerProfileSchema.fixerProfessionalDetails, this.fullProfileInfo[2].response);
                            Object.assign(this.fixerProfileSchema.fixerEducationalDetails, this.fullProfileInfo[3].response);
                            Object.assign(this.fixerProfileSchema.fixerAchievementDetails, this.fullProfileInfo[4].response);
                            Object.assign(this.fixerProfileSchema.fixerSkillDetails, this.fullProfileInfo[5].response);
                            showCaseData = this.fullProfileInfo[6].response.slice();
                            // Object.assign(this.fixerProfileSchema.showcaseDetails, showCaseData);
                            this.fixerProfileSchema.showcaseDetails = showCaseData;
                            Object.assign(this.showcaseArray, this.fullProfileInfo[6].response);
                            // console.log('show case array : ' + JSON.stringify(this.fullProfileInfo));
                            this.recomputeAttachmentArray();
                            // this.showcaseArray.map((item) => {
                            //   console.log('item-->' + JSON.stringify(item));
                            //   item.url = this.sanitizeUrl(item.url);
                            //   item.render = item.url;
                            // });
                            // this.reload();
                            // console.log('sanitized->' + JSON.stringify(this.showcaseArray))
                            this.setDataProfileStatus(this.fixerProfileSchema.fixerProfileSummary, this.fixerProfileSchema.fixerEducationalDetails, this.fixerProfileSchema.fixerProfessionalDetails, this.fixerProfileSchema.fixerAchievementDetails, this.fixerProfileSchema.fixerSkillDetails);
                            this.profileStatus = this.appState.getProfileStatus();
                            // console.log('fixer profile data:' + JSON.stringify(this.fixerProfileSchema))
                            // console.log('Showcase details:' + JSON.stringify(this.fixerProfileSchema.showcaseDetails))
                        }
                        else if (this.typeOfRole == 'Invent Fund Founder') {
                            // console.log('founder data : ' + JSON.stringify(this.fullProfileInfo));
                            Object.assign(this.founderProfileSchema.founderPersonDetails, this.fullProfileInfo[0].response[0]);
                            Object.assign(this.founderProfileSchema.founderProfileSummary, this.fullProfileInfo[1].response[0]);
                            Object.assign(this.founderProfileSchema.founderEducationalDetails, this.fullProfileInfo[2].response);
                            Object.assign(this.showcaseArray, this.fullProfileInfo[3].response);
                            this.recomputeAttachmentArray();
                            // this.showcaseArray.map((item) => {
                            //   console.log('item-->' + JSON.stringify(item));
                            //   item.url = this.sanitizeUrl(item.url);
                            // });
                            // this.reload();
                            this.setDataProfileStatus(this.founderProfileSchema.founderProfileSummary, this.founderProfileSchema.founderEducationalDetails, [], [], []);
                            this.profileStatus = this.appState.getProfileStatus();
                            // console.log('founder profile data:' + JSON.stringify(this.founderProfileSchema))
                        }
                        else if (this.typeOfRole == 'Invent Fund Funder') {
                            // console.log('Funder data : ' + JSON.stringify(this.fullProfileInfo));
                            Object.assign(this.funderProfileSchema.funderPersonDetails, this.fullProfileInfo[0].response[0]);
                            Object.assign(this.funderProfileSchema.funderProfileSummary, this.fullProfileInfo[1].response[0]);
                            Object.assign(this.funderProfileSchema.funderEducationalDetails, this.fullProfileInfo[2].response);
                            Object.assign(this.showcaseArray, this.fullProfileInfo[3].response);
                            this.recomputeAttachmentArray();
                            // this.showcaseArray.map((item) => {
                            //   console.log('item-->' + JSON.stringify(item));
                            //   item.url = this.sanitizeUrl(item.url);
                            // });
                            // this.reload();
                            this.setDataProfileStatus(this.funderProfileSchema.funderProfileSummary, this.funderProfileSchema.funderEducationalDetails, [], [], []);
                            this.profileStatus = this.appState.getProfileStatus();
                            // console.log('funder profile data:' + JSON.stringify(this.funderProfileSchema))
                        }
                        if (this.funderProfileSchema.funderProfileSummary.personType) {
                            this.types.map(function (item) {
                                // console.log("item:" + JSON.stringify(item) + ', ' + this.funderProfileSchema.funderProfileSummary
                                //   .personType);
                                if (_this.funderProfileSchema.funderProfileSummary.personType == item['value'] ||
                                    _this.funderProfileSchema.funderProfileSummary.personType == item['viewValue']) {
                                    // alert('item["value"]:'+item["value"])
                                    _this.selectedValue = item['value'];
                                    _this.form.controls['roleType'].setValue(item['value']);
                                    // this.form.controls['roleType'].markAsUntouched
                                }
                            });
                            // this.selectedValue = this.funderProfileSchema.funderProfileSummary.personType
                        }
                        else {
                            this.selectedValue = 'value-0';
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // set data for profile status
    ProfilePageComponent.prototype.setDataProfileStatus = function (data1, data2, data3, data4, data5) {
        // console.log("data 1 : " + Object.keys(data1).length)
        // console.log("data 2 : " + data2.length)
        // console.log("data 3 : " + data3.length)
        // console.log("data 4 : " + data4.length)
        // console.log("data 5 : " + data5.length)
        this.appState.setDataProfile(data1, data2, data3, data4, data5);
    };
    //Fork Join to retrieve all profile related details
    ProfilePageComponent.prototype.retrieveAllProfileInfo = function () {
        var profileCol, professionCol, educationCol, achievementCol, skillCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            profileCol = 'fixerProfileSummary';
            professionCol = 'fixerProfessionalDetails';
            educationCol = 'fixerEducationDetails';
            (achievementCol = 'fixerAchievementDetails'), (skillCol = 'fixerSkillDetails');
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            profileCol = 'founderProfileSummary';
            professionCol = '';
            educationCol = 'founderEducationDetails';
            (achievementCol = ''), (skillCol = '');
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            profileCol = 'funderProfileSummary';
            professionCol = '';
            educationCol = 'funderEducationDetails';
            (achievementCol = ''), (skillCol = '');
        }
        var person = {
            collectionName: 'person',
            _id: this.appState.encryptedDataValue('personId'),
            endPoint: 'RETRIVEPROFILE',
        };
        var attachment = {
            collectionName: 'attachment',
            queryStr: {
                $and: [{ personId: this.appState.encryptedDataValue('personId') }, { type: 'SHOWCASE' }],
            },
            endPoint: 'RETRIEVEATTACHMENTDATA',
        };
        var profileSummary = {
            collectionName: profileCol,
            endPoint: 'RETRIEVEPROFILESUMMARY',
            queryStr: { personId: this.appState.encryptedDataValue('personId') },
        };
        var professionalDetails = {
            collectionName: professionCol,
            endPoint: 'RETRIEVEPROFESSIONALDETAILS',
            queryStr: { personId: this.appState.encryptedDataValue('personId') },
        };
        var educationDetails = {
            collectionName: educationCol,
            endPoint: 'RETRIEVEEDUCATIONDETAILS',
            queryStr: { personId: this.appState.encryptedDataValue('personId') },
        };
        var achievementDetails = {
            collectionName: achievementCol,
            endPoint: 'RETRIEVEACHIDETAILS',
            queryStr: { personId: this.appState.encryptedDataValue('personId') },
        };
        var skillDetails = {
            collectionName: skillCol,
            endPoint: 'RETRIEVESKILLSDETAILS',
            queryStr: {
                personId: this.appState.encryptedDataValue('personId'),
            },
        };
        if (this.typeOfRole == 'Invent Fund Fixer') {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["forkJoin"])([
                this.inventFundsApiService.retrieveGeneric(person),
                this.inventFundsApiService.retrieveGeneric(profileSummary),
                this.inventFundsApiService.retrieveGeneric(professionalDetails),
                this.inventFundsApiService.retrieveGeneric(educationDetails),
                this.inventFundsApiService.retrieveGeneric(achievementDetails),
                this.inventFundsApiService.retrieveGeneric(skillDetails),
                this.inventFundsApiService.retrieveGeneric(attachment),
            ]);
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["forkJoin"])([
                this.inventFundsApiService.retrieveGeneric(person),
                this.inventFundsApiService.retrieveGeneric(profileSummary),
                this.inventFundsApiService.retrieveGeneric(educationDetails),
                this.inventFundsApiService.retrieveGeneric(attachment),
            ]);
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_10__["forkJoin"])([
                this.inventFundsApiService.retrieveGeneric(person),
                this.inventFundsApiService.retrieveGeneric(profileSummary),
                this.inventFundsApiService.retrieveGeneric(educationDetails),
                this.inventFundsApiService.retrieveGeneric(attachment),
            ]);
        }
    };
    // edit main section
    ProfilePageComponent.prototype.onclick = function () {
        var data = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.modelData);
        this.updateSummary = data;
        // console.log('update modelData : ' + JSON.stringify(this.modelData));
        this.editmainsection = true;
    };
    // change or toggle icon
    ProfilePageComponent.prototype.click = function () {
        this.icon = !this.icon;
    };
    // retrive profile info
    ProfilePageComponent.prototype.retrieveProfileInfo = function () {
        var _this = this;
        var params = {
            collectionName: 'person',
            _id: this.appState.encryptedDataValue('personId'),
            endPoint: 'RETRIVEPROFILE',
        };
        this.inventFundsApiService.retrieveGeneric(params).then(function (res) {
            // console.log('resp:' + JSON.stringify(res.response))
            _this.profileData = res.response[0].personType;
        });
    };
    // get skill details
    ProfilePageComponent.prototype.getSkillsDetails = function (data) {
        var _this = this;
        var params = {
            collectionName: 'fixerSkillDetails',
            endPoint: 'RETRIEVESKILLSDETAILS',
            queryStr: { personId: this.appState.encryptedDataValue('personId') },
        };
        this.inventFundsApiService
            .retrieveGeneric(params)
            .then(function (res) {
            _this.skillsDetails = [];
            _this.fixerProfileSchema.fixerSkillDetails = [];
            // console.log('achi response!' + JSON.stringify(res))
            if (res.status == 200 && res.response.length != 0) {
                res.response.map(function (item) {
                    _this.skillsDetails.push(item);
                    _this.fixerProfileSchema.fixerSkillDetails.push(item);
                });
                _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
            }
            else if (res.status == 200) {
                _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
            }
            if (res.status == 200) {
                if (data == 'create') {
                    _this.inventFundsApiService.showAlertCreateMessage();
                }
                else if (data == 'update') {
                    _this.inventFundsApiService.showAlertUpdateMessage();
                }
                else if (data == 'delete') {
                    _this.inventFundsApiService.showAlertDeleteMessage();
                }
            }
            else {
                if (data == 'create') {
                    _this.inventFundsApiService.showAlertCreateErrorMessage();
                }
                else if (data == 'update') {
                    _this.inventFundsApiService.showAlertUpdateErrorMessage();
                }
                else if (data == 'delete') {
                    _this.inventFundsApiService.showAlertDeleteErrorMessage();
                }
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // profileSummary screen builder data
    ProfilePageComponent.prototype.getProfileSummaryJson = function () {
        var _this = this;
        var params = {
            collectionName: 'profileSummary',
        };
        this.inventFundsApiService.retrieveScreenbuilderAll(params).then(function (res) {
            _this.profileSummaryJson = res.response;
            // console.log("profileSummary res " + JSON.stringify(res.response))
        });
    };
    // get profile summary
    ProfilePageComponent.prototype.getProfileSummary = function () {
        var _this = this;
        var profileCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            profileCol = 'fixerProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            profileCol = 'founderProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            profileCol = 'funderProfileSummary';
        }
        var params = {
            collectionName: profileCol,
            endPoint: 'RETRIEVEPROFILESUMMARY',
        };
        this.inventFundsApiService
            .retrieveGeneric(params)
            .then(function (res) {
            // console.log('Profile summary response!' + JSON.stringify(res));
            res.response.map(function (item) {
                if (item.personId == _this.appState.encryptedDataValue('personId')) {
                    // alert('id already exists')
                    _this.profileSummaryExists = true;
                    _this.existingProfileSummaryId = item._id;
                    _this.type = item.type;
                    // this.updateSummary = item;
                    _this.modelData = item;
                }
            });
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // get achivements details
    ProfilePageComponent.prototype.getAchiDetails = function (data) {
        var _this = this;
        var params = {
            collectionName: 'fixerAchievementDetails',
            endPoint: 'RETRIEVEACHIDETAILS',
            queryStr: { personId: this.appState.encryptedDataValue('personId') },
        };
        this.inventFundsApiService
            .retrieveGeneric(params)
            .then(function (res) {
            _this.achievementsDetails = [];
            _this.fixerProfileSchema.fixerAchievementDetails = [];
            var achievement;
            // console.log('achi response!' + JSON.stringify(res))
            if (res.status == 200 && res.response.length != 0) {
                res.response.map(function (item) {
                    // console.log('ITEM:' + JSON.stringify(item))
                    _this.achievementsDetails.push(item);
                    _this.fixerProfileSchema.fixerAchievementDetails.push(item);
                });
                _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                // var index = this.fixerProfileSchema.fixerAchievementDetails.findIndex(p => p._id == achievement._id)
                // this.fixerProfileSchema.fixerAchievementDetails.splice(index, 1);
            }
            else if (res.status == 200) {
                _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
            }
            if (res.status == 200) {
                if (data == 'create') {
                    _this.inventFundsApiService.showAlertCreateMessage();
                }
                else if (data == 'update') {
                    _this.inventFundsApiService.showAlertUpdateMessage();
                }
                else if (data == 'delete') {
                    _this.inventFundsApiService.showAlertDeleteMessage();
                }
            }
            else {
                if (data == 'create') {
                    _this.inventFundsApiService.showAlertCreateErrorMessage();
                }
                else if (data == 'update') {
                    _this.inventFundsApiService.showAlertUpdateErrorMessage();
                }
                else if (data == 'delete') {
                    _this.inventFundsApiService.showAlertDeleteErrorMessage();
                }
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // create profile summary
    ProfilePageComponent.prototype.createProfileSummaryData = function (event) {
        var _this = this;
        var profileCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            profileCol = 'fixerProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            profileCol = 'founderProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            profileCol = 'funderProfileSummary';
        }
        // console.log("create profile summary:" + JSON.stringify(event))
        var personSummaryData = event.response;
        Object.assign(personSummaryData, { personId: this.appState.encryptedDataValue('personId') });
        Object.assign(personSummaryData, { personType: 'Individual' });
        if (this.typeOfRole == 'Invent Fund Funder') {
            // alert(this.selectedValue);
            var personType;
            this.types.map(function (item) {
                // console.log('item:' + JSON.stringify(item));
                if (_this.selectedValue == item['value']) {
                    personType = item['viewValue'];
                }
            });
            Object.assign(personSummaryData, { personType: personType });
        }
        var profile = {
            collectionName: profileCol,
            updateData: personSummaryData,
            endPoint: 'CREATEPROFILESUMMARY',
        };
        if (!this.profileSummaryExists) {
            this.inventFundsApiService
                .createGeneric(profile)
                .then(function (res) {
                // this.getProfile();
                // console.log('create response-->' + JSON.stringify(res));
                if (res.data) {
                    // alert('create successful');
                    _this.modelData = res.data[0];
                    // this.updateSummary = res.data[0];
                    _this.type = res.data[0].type;
                    _this.profileSummaryExists = true;
                    _this.existingProfileSummaryId = res.data[0]._id;
                    if (_this.typeOfRole == 'Invent Fund Fixer') {
                        _this.fixerProfileSchema.fixerProfileSummary = res.data[0];
                        _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Founder') {
                        _this.founderProfileSchema.founderProfileSummary = res.data[0];
                        _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Funder') {
                        _this.funderProfileSchema.funderProfileSummary = res.data[0];
                        _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
                    }
                    _this.inventFundsApiService.showAlertCreateMessage();
                }
            })
                .catch(function (err) {
                console.log('Error:' + err);
            });
        }
        else {
            this.updateProfileSummarySection(personSummaryData);
        }
        this.editmainsection = false;
    };
    // update profile summary
    ProfilePageComponent.prototype.updateProfileSummaryData = function (event) {
        var _this = this;
        // console.log("update profile summary:" + JSON.stringify(event))
        var profileCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            profileCol = 'fixerProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            profileCol = 'founderProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            profileCol = 'funderProfileSummary';
        }
        var emittedData = event.response;
        var profileSummary = {
            collectionName: profileCol,
            pkId: event.id,
            updateData: emittedData,
            endPoint: 'UPDATEPROFILESUMMARY',
        };
        this.inventFundsApiService
            .updateGeneric(profileSummary)
            .then(function (res) {
            // console.log('update response-->' + JSON.stringify(res));
            // this.type = res.data.type;
            var params = {
                collectionName: profileCol,
                _id: res.data._id,
                endPoint: 'RETRIEVEPROFILESUMMARYONE',
            };
            _this.inventFundsApiService.retrieveGeneric(params).then(function (res2) {
                // console.log('retrieve one response-->' + JSON.stringify(res2));
                // this.type = res2.response[0].type;
                if (_this.typeOfRole == 'Invent Fund Fixer') {
                    _this.fixerProfileSchema.fixerProfileSummary = res2.response[0];
                    _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                }
                else if (_this.typeOfRole == 'Invent Fund Founder') {
                    _this.founderProfileSchema.founderProfileSummary = res2.response[0];
                    _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
                }
                else if (_this.typeOfRole == 'Invent Fund Funder') {
                    _this.funderProfileSchema.funderProfileSummary = res2.response[0];
                    _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
                }
            });
            _this.inventFundsApiService.showAlertUpdateMessageNoReload();
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
        this.editmainsection = false;
    };
    // update profile summary section
    ProfilePageComponent.prototype.updateProfileSummarySection = function (data) {
        var _this = this;
        var profileCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            profileCol = 'fixerProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            profileCol = 'founderProfileSummary';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            profileCol = 'funderProfileSummary';
        }
        // console.log('Profile summary section data:' + JSON.stringify(data))
        if (this.profileSummaryExists) {
            //update
            // existingProfileSummaryId
            var profileSummary = {
                collectionName: profileCol,
                pkId: this.existingProfileSummaryId,
                updateData: data,
                endPoint: 'UPDATEPROFILESUMMARY',
            };
            this.inventFundsApiService
                .updateGeneric(profileSummary)
                .then(function (res) {
                // console.log('updated profile sumamry section response->' + JSON.stringify(res))
                _this.modelData = res.data;
                // this.updateSummary = res.data;
                //f this.type = res.data[0].type;
                var params = {
                    collectionName: profileCol,
                    _id: res.data._id,
                    endPoint: 'RETRIEVEPROFILESUMMARYONE',
                };
                _this.inventFundsApiService.retrieveGeneric(params).then(function (res2) {
                    // console.log('retrieve one response-->' + JSON.stringify(res2));
                    // this.type = res2.response[0].type;
                    _this.modelData.profileSummary = res2.response[0].profileSummary;
                    if (_this.typeOfRole == 'Invent Fund Fixer') {
                        _this.fixerProfileSchema.fixerProfileSummary.profileSummary =
                            res2.response[0].profileSummary;
                        _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Founder') {
                        _this.founderProfileSchema.founderProfileSummary.profileSummary =
                            res2.response[0].profileSummary;
                        _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Funder') {
                        _this.funderProfileSchema.funderProfileSummary.profileSummary =
                            res2.response[0].profileSummary;
                        _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
                    }
                    _this.inventFundsApiService.showAlertUpdateMessageNoReload();
                });
            })
                .catch(function (err) {
                console.log('Error:' + err);
            });
        }
        else if (!this.profileSummaryExists) {
            //create
            // alert('create data in table')
            var personSummaryData = data;
            Object.assign(personSummaryData, { personId: this.appState.encryptedDataValue('personId') });
            var profile = {
                collectionName: profileCol,
                updateData: personSummaryData,
                endPoint: 'CREATEPROFILESUMMARY',
            };
            this.inventFundsApiService
                .createGeneric(profile)
                .then(function (res) {
                // this.getProfile();
                // console.log('create response-->' + JSON.stringify(res));
                if (res.data) {
                    // alert('create successful');
                    _this.modelData = res.data[0];
                    // this.updateSummary = res.data[0];
                    _this.modelData.profileSummary = res.data[0].profileSummary;
                    if (_this.typeOfRole == 'Invent Fund Fixer') {
                        _this.fixerProfileSchema.fixerProfileSummary.profileSummary =
                            res.data[0].profileSummary;
                        _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Founder') {
                        _this.founderProfileSchema.founderProfileSummary.profileSummary =
                            res.data[0].profileSummary;
                        _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Funder') {
                        _this.funderProfileSchema.funderProfileSummary.profileSummary =
                            res.data[0].profileSummary;
                        _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
                    }
                    // this.type = res.data[0].type;
                    _this.profileSummaryExists = true;
                    _this.existingProfileSummaryId = res.data[0]._id;
                    _this.inventFundsApiService.showAlertCreateMessage();
                }
            })
                .catch(function (err) {
                console.log('Error:' + err);
            });
            this.editmainsection = false;
        }
    };
    // cancel profile summary data
    ProfilePageComponent.prototype.cancelProfileSummary = function (event) {
        // alert(event)
        // if (event == "true") {
        //   // alert("true")
        //   let { headline, professionalSummary, type, ...rest } = this.modelData;
        //   this.modelData = rest;
        //   console.log("modelData : " + JSON.stringify(this.modelData))
        // }
        // console.log('update summary : ' + JSON.stringify(this.updateSummary));
        this.modelData = this.updateSummary;
        // console.log('update modelData : ' + JSON.stringify(this.modelData));
        // this.modelData = this.updateSummary;
        this.editmainsection = false;
    };
    // open(content) {
    //   this.modalService.open(content, {centered: true});
    // }
    // open model async
    ProfilePageComponent.prototype.openModal = function (content) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var lookupParams, params, lookupParams, lookupParams, achiLookup, params, modalRef, modalRef;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!content.hasOwnProperty('professionalDetailsIndividual')) return [3 /*break*/, 1];
                        lookupParams = {
                            collectionName: 'lookUp',
                            endPoint: 'RETRIEVELOOKUPDATA',
                        };
                        this.inventFundsApiService
                            .retrieveScreenbuilderAll(lookupParams)
                            .then(function (res) {
                            if (res.status == 200) {
                                // console.log('Lookup data:' + JSON.stringify(res));
                                if (res.status == 200) {
                                    res.response.filter(function (item) {
                                        if (item.lookupKey == 'employmentType') {
                                            _this.lookupArray.push({
                                                label: item.label,
                                                value: item.value,
                                            });
                                        }
                                    });
                                }
                            }
                        })
                            .catch(function (err) {
                            console.log('error:' + err);
                        });
                        params = {
                            collectionName: 'professionalDetailsIndividual',
                        };
                        this.inventFundsApiService.retrieveScreenbuilderAll(params).then(function (res) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            var profDetailsJson, params2, modelData, modalRef;
                            var _this = this;
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        profDetailsJson = res.response;
                                        profDetailsJson.map(function (item) {
                                            item.fieldGroup.map(function (item2) {
                                                if (item2.key == 'employmentType') {
                                                    item2.templateOptions.options = _this.lookupArray;
                                                    _this.lookupArray = [];
                                                }
                                            });
                                        });
                                        params2 = {
                                            _id: content.model,
                                            collectionName: 'fixerProfessionalDetails',
                                            endPoint: 'RETRIEVEPROFESSIONALDETAILSONE',
                                        };
                                        return [4 /*yield*/, this.inventFundsApiService.retrieveGeneric(params2)];
                                    case 1:
                                        modelData = _a.sent();
                                        modalRef = this.modalService.open(_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__["ProfileModalComponent"], {
                                            size: 'lg',
                                            centered: true,
                                            backdrop: 'static',
                                            keyboard: false,
                                        });
                                        modalRef.componentInstance.data = {
                                            profDetailsJson: profDetailsJson,
                                            headline: 'Edit Professional Details',
                                            model: modelData.response,
                                        };
                                        modalRef.result
                                            .then(function (result) {
                                            if (result) {
                                                // console.log("receivedEntry:" + JSON.stringify(result));
                                                if (result.action == 'create') {
                                                    _this.createDataFormly(result.data, result.type);
                                                }
                                                else if (result.action == 'cancelled') {
                                                    // console.log('cancelled:' + JSON.stringify(this.fixerProfileSchema.fixerProfessionalDetails) + ' item:' + JSON.stringify(content.model))
                                                    // content.model
                                                }
                                                else {
                                                    _this.updateDataFormly(result.data, result.type);
                                                }
                                            }
                                        })
                                            .catch(function (cancel) { return console.log(cancel); });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [3 /*break*/, 5];
                    case 1:
                        if (!content.hasOwnProperty('educationalDetails')) return [3 /*break*/, 2];
                        lookupParams = {
                            collectionName: 'lookUp',
                            endPoint: 'RETRIEVELOOKUPDATA',
                        };
                        this.inventFundsApiService
                            .retrieveScreenbuilderAll(lookupParams)
                            .then(function (res) {
                            if (res.status == 200) {
                                _this.lookupArray = [];
                                // console.log('Lookup data:' + JSON.stringify(res))
                                if (res.status == 200) {
                                    res.response.filter(function (item) {
                                        if (item.lookupKey == 'country') {
                                            _this.lookupArray.push({
                                                label: item.label,
                                                value: item.value,
                                            });
                                        }
                                    });
                                    var modalRef = _this.modalService.open(_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__["ProfileModalComponent"], {
                                        size: 'lg',
                                        centered: true,
                                    });
                                    if (content.hasOwnProperty('updateData')) {
                                        modalRef.componentInstance.data = {
                                            educationalDetails: _this.lookupArray,
                                            updateData: content.updateData,
                                            headline: content.heading,
                                            operation: 'update',
                                        };
                                    }
                                    else {
                                        modalRef.componentInstance.data = {
                                            educationalDetails: _this.lookupArray,
                                            headline: content.heading,
                                            operation: 'create',
                                        };
                                    }
                                    _this.lookupArray = [];
                                    modalRef.result
                                        .then(function (result) {
                                        if (result) {
                                            // console.log("receivedEntry in education details:" + JSON.stringify(result));
                                            if (result.action == 'create') {
                                                _this.createEducationData(result.value);
                                            }
                                            else if (result.action == 'cancelled') {
                                                //cancelled
                                            }
                                            else {
                                                _this.updateEducationData(result.value, result.id);
                                            }
                                        }
                                    })
                                        .catch(function (cancel) { return console.log(cancel); });
                                }
                            }
                        })
                            .catch(function (err) {
                            console.log('error:' + err);
                        });
                        return [3 /*break*/, 5];
                    case 2:
                        if (!content.hasOwnProperty('achievementsDetails')) return [3 /*break*/, 4];
                        lookupParams = {
                            collectionName: 'lookUp',
                            endPoint: 'RETRIEVELOOKUPDATA',
                        };
                        return [4 /*yield*/, this.inventFundsApiService.retrieveScreenbuilderAll(lookupParams)];
                    case 3:
                        achiLookup = _a.sent();
                        // console.log('achievements lookup-->' + JSON.stringify(achiLookup))
                        achiLookup.response.filter(function (item) {
                            if (item.lookupKey == 'achievementType') {
                                _this.lookupArray.push({
                                    label: item.label,
                                    value: item.value,
                                });
                            }
                        });
                        params = {
                            collectionName: 'achievements',
                        };
                        this.inventFundsApiService.retrieveScreenbuilderAll(params).then(function (res) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            var achievementsJson, params2, achiModelData, modalRef;
                            var _this = this;
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        achievementsJson = res.response;
                                        achievementsJson.map(function (item) {
                                            // console.log("achievements item1 " + JSON.stringify(item))
                                            item.fieldGroup.map(function (item2) {
                                                // console.log("achievements item2 " + JSON.stringify(item2))
                                                // console.log(item2.key === 'achievementsCategory')
                                                if (item2.key == 'achievementsCategory') {
                                                    item2.templateOptions.options = _this.lookupArray;
                                                    _this.lookupArray = [];
                                                }
                                            });
                                        });
                                        params2 = {
                                            _id: content.updateData,
                                            collectionName: 'fixerAchievementDetails',
                                            endPoint: 'RETRIEVEACHIDETAILSONE',
                                        };
                                        return [4 /*yield*/, this.inventFundsApiService.retrieveGeneric(params2)];
                                    case 1:
                                        achiModelData = _a.sent();
                                        modalRef = this.modalService.open(_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__["ProfileModalComponent"], {
                                            size: 'lg',
                                            centered: true,
                                        });
                                        if (content.hasOwnProperty('updateData')) {
                                            // modalRef.componentInstance.data = { "achievementsDetails": achievementsJson, "updateData": content.updateData, "headline": content.heading, "operation": "update" };
                                            modalRef.componentInstance.data = {
                                                achievementsDetails: achievementsJson,
                                                updateData: achiModelData.response[0],
                                                headline: content.heading,
                                                operation: 'update',
                                            };
                                        }
                                        else {
                                            modalRef.componentInstance.data = {
                                                achievementsDetails: achievementsJson,
                                                headline: content.heading,
                                                operation: 'create',
                                            };
                                        }
                                        modalRef.result
                                            .then(function (result) {
                                            if (result) {
                                                // console.log("achievements results:" + JSON.stringify(result));
                                                if (result.action != 'cancelled') {
                                                    if (result.action == 'create') {
                                                        _this.createDataAchi(result.data, result.type);
                                                    }
                                                    else {
                                                        _this.updateDataAchi(result.data, result.type);
                                                    }
                                                }
                                            }
                                        })
                                            .catch(function (cancel) { return console.log(cancel); });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [3 /*break*/, 5];
                    case 4:
                        if (content.hasOwnProperty('skillsDetails')) {
                            this.skillsData(content);
                        }
                        else if (content.hasOwnProperty('showcase')) {
                            modalRef = this.modalService.open(_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__["ProfileModalComponent"], {
                                size: 'lg',
                                centered: true,
                            });
                            modalRef.componentInstance.data = {
                                showcase: '',
                                headline: content.heading,
                                operation: 'create',
                            };
                            modalRef.result
                                .then(function (result) {
                                // console.log(result.action)
                                if (result.obj) {
                                    console.log('receivedEntry:' + JSON.stringify(result));
                                    // {"obj":
                                    // {"imageUrl":
                                    // "https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFund/HOST/100/IFFIXER/SHOWCASE/5ed8eb4ff2ae0d4f70cbf102/download.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA6CMG2RRKEAAYQ65F%2F20200616%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200616T102257Z&X-Amz-Expires=30&X-Amz-Signature=8538e119d3cf4ea4b3b97de42e7eacca9eadd2f1d19a98991951c4c943f2d9fd&X-Amz-SignedHeaders=host",
                                    // "imageType":"image/jpeg","description":"hello!",
                                    // "data":"https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFund/HOST/100/IFFIXER/SHOWCASE/5ed8eb4ff2ae0d4f70cbf102/download.jpg"}}
                                    // var n = result.obj.data.lastIndexOf('/');
                                    // var key = result.obj.data.substring(n + 1);
                                    // var path = 'inventFund' + result.obj.data.split('inventFund')[1].replace(key, '');
                                    var imageParams = {
                                        collectionName: 'attachment',
                                        createData: {
                                            id: _this.appState.encryptedDataValue('personId'),
                                            // "url": result.obj.imageUrl,
                                            fileType: result.obj.imageType,
                                            description: result.obj.description,
                                            type: 'SHOWCASE',
                                            // path: path,
                                            // key: key,
                                            url: result.obj.data,
                                        },
                                    };
                                    // console.log("imageParams : " + JSON.stringify(imageParams))
                                    _this.inventFundsApiService.attachmentSingleFile(imageParams).then(function (res) {
                                        // console.log('Image upload res-->' + JSON.stringify(res));
                                        if (res.code == 200) {
                                            _this.recomputeAttachmentArray();
                                            // this.fixerProfileSchema.showcaseDetails.push(res.data[0])
                                            // this.showcaseArray.push(res.data[0])
                                        }
                                    });
                                }
                            })
                                .catch(function (cancel) { return console.log(cancel); });
                        }
                        else {
                            modalRef = this.modalService.open(_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__["ProfileModalComponent"], { size: 'lg' });
                            modalRef.componentInstance.data = content;
                            modalRef.result
                                .then(function (result) {
                                if (result) {
                                    // console.log('receivedEntry:' + JSON.stringify(result));
                                    if (result.action != 'cancelled') {
                                        if (result.value.hasOwnProperty('profileSummary')) {
                                            _this.updateProfileSummarySection(result.value);
                                        }
                                    }
                                }
                            })
                                .catch(function (cancel) { return console.log(cancel); });
                        }
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    // get skills data for pop up
    ProfilePageComponent.prototype.skillsData = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var skillSet, skillCat, selfAssessment;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getSkillSet()];
                    case 1:
                        skillSet = _a.sent();
                        return [4 /*yield*/, this.getSkillCat()];
                    case 2:
                        skillCat = _a.sent();
                        return [4 /*yield*/, this.getSelfAsesment()];
                    case 3:
                        selfAssessment = _a.sent();
                        return [4 /*yield*/, this.openSkillsModel(data, skillCat, skillSet, selfAssessment)];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // get skills set from collection
    ProfilePageComponent.prototype.getSkillSet = function () {
        var lookupParamsKey = {
            collectionName: 'lookUp',
            endPoint: 'RETRIEVESCREENBUILDERLOOKUPSKILLS',
            queryStr: { lookupCode: 'skillSet' },
            sortQuery: { order: 1 },
        };
        return this.inventFundsApiService
            .retrieveScreenbuilderLookUp(lookupParamsKey)
            .then(function (res) {
            if (res.status == 200) {
                // console.log('Lookup data:' + JSON.stringify(res));
                if (res.status == 200) {
                    return res.response;
                }
                else {
                    return null;
                }
            }
        })
            .catch(function (err) {
            console.log('error:' + err);
        });
    };
    // get skill category
    ProfilePageComponent.prototype.getSkillCat = function () {
        var lookupParamsKey = {
            collectionName: 'lookUp',
            endPoint: 'RETRIEVESCREENBUILDERLOOKUPSKILLS',
            queryStr: { lookupCode: 'skillCategory' },
            sortQuery: { order: 1 },
        };
        return this.inventFundsApiService
            .retrieveScreenbuilderLookUp(lookupParamsKey)
            .then(function (res) {
            if (res.status == 200) {
                // console.log('Lookup data:' + JSON.stringify(res));
                if (res.status == 200) {
                    return res.response;
                }
                else {
                    return null;
                }
            }
        })
            .catch(function (err) {
            console.log('error:' + err);
        });
    };
    // get self assment
    ProfilePageComponent.prototype.getSelfAsesment = function () {
        var lookupParamsKey = {
            collectionName: 'lookUp',
            endPoint: 'RETRIEVESCREENBUILDERLOOKUPSKILLS',
            queryStr: { lookupCode: 'selfAssessment' },
            sortQuery: { order: 1 },
        };
        return this.inventFundsApiService
            .retrieveScreenbuilderLookUp(lookupParamsKey)
            .then(function (res) {
            if (res.status == 200) {
                // console.log('Lookup data:' + JSON.stringify(res));
                if (res.status == 200) {
                    return res.response;
                }
                else {
                    return null;
                }
            }
        })
            .catch(function (err) {
            console.log('error:' + err);
        });
    };
    // open skill model
    ProfilePageComponent.prototype.openSkillsModel = function (data, skillCat, skillSet, selfAssessment) {
        var _this = this;
        // console.log("data for skills : " + JSON.stringify(data))
        var modalRef = this.modalService.open(_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_8__["ProfileModalComponent"], { size: 'lg', centered: true });
        modalRef.componentInstance.data = {
            skillsJson: '',
            headline: data.heading,
            model: data.model,
        };
        if (data.hasOwnProperty('updateData')) {
            modalRef.componentInstance.data = {
                skillsJson: '',
                selfAssessment: selfAssessment,
                skillSet: skillSet,
                skillCategory: skillCat,
                headline: data.heading,
                updateData: data.updateData,
                model: data.model,
                operation: 'update',
            };
        }
        else {
            modalRef.componentInstance.data = {
                skillsJson: '',
                selfAssessment: selfAssessment,
                skillSet: skillSet,
                skillCategory: skillCat,
                headline: data.heading,
                model: data.model,
                operation: 'create',
            };
        }
        modalRef.result
            .then(function (result) {
            if (result) {
                // console.log('receivedEntry:' + JSON.stringify(result));
                if (result.action != 'cancelled') {
                    if (result.action == 'create') {
                        _this.createDataSkills(result, result.type);
                    }
                    else {
                        _this.updateDataSkills(result, result.type);
                    }
                }
            }
        })
            .catch(function (cancel) { return console.log(cancel); });
    };
    // create data for achivements
    ProfilePageComponent.prototype.createDataAchi = function (data, type) {
        var _this = this;
        // console.log('formly create data:' + JSON.stringify(data) + '' + type)
        Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
        var params = {
            updateData: data.response,
            collectionName: 'fixerAchievementDetails',
            endPoint: 'CREATEACHIDETAILS',
        };
        this.inventFundsApiService.createGeneric(params).then(function (res) {
            if (res.status == 200) {
                _this.getAchiDetails('create');
            }
        });
    };
    // update achivements
    ProfilePageComponent.prototype.updateDataAchi = function (data, type) {
        var _this = this;
        // console.log('formly update data:' + JSON.stringify(data) + '' + type)
        Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
        var params = {
            updateData: data.response,
            pkId: data.id,
            collectionName: 'fixerAchievementDetails',
            endPoint: 'UPDATEACHIDETAILS',
        };
        this.inventFundsApiService.updateGeneric(params).then(function (res) {
            if (res.status == 200) {
                _this.getAchiDetails('update');
            }
        });
    };
    // create data from formly
    ProfilePageComponent.prototype.createDataFormly = function (data, type) {
        // console.log('formly create data:' + JSON.stringify(data) + '' + type)
        var _this = this;
        if (type == 'professionalDetailsIndividual') {
            Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
            var params = {
                updateData: data.response,
                collectionName: 'fixerProfessionalDetails',
                endPoint: 'CREATEPROFESSIONALDETAILS',
            };
            this.inventFundsApiService.createGeneric(params).then(function (res) {
                // console.log('data created : ' + JSON.stringify(res));
                _this.individualProfessionalDetails.push(res.data[0]);
                // console.log('ind prof det:' + JSON.stringify(this.individualProfessionalDetails));
                _this.fixerProfileSchema.fixerProfessionalDetails.push(res.data[0]);
                _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                _this.inventFundsApiService.showAlertCreateMessage();
            });
        }
    };
    // update data from formly
    ProfilePageComponent.prototype.updateDataFormly = function (data, type) {
        // console.log('formly update data:' + JSON.stringify(data) + '' + type)
        var _this = this;
        if (type == 'professionalDetailsIndividual') {
            Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
            var params = {
                updateData: data.response,
                pkId: data.id,
                collectionName: 'fixerProfessionalDetails',
                endPoint: 'UPDATEPROFESSIONALDETAILS',
            };
            this.inventFundsApiService.updateGeneric(params).then(function (res) {
                // console.log("data updated : " + JSON.stringify(res));
                if (res.status == 200) {
                    var params = {
                        collectionName: 'fixerProfessionalDetails',
                        endPoint: 'RETRIEVEPROFESSIONALDETAILSONE',
                        _id: res.data._id,
                    };
                    _this.inventFundsApiService
                        .retrieveGeneric(params)
                        .then(function (res2) {
                        // console.log('retrieve fixerProfessionalDetails one response-->' + JSON.stringify(res2))
                        // this.educationDetails = res
                        var index = _this.fixerProfileSchema.fixerProfessionalDetails.findIndex(function (p) { return p._id == res2.response[0]._id; });
                        _this.fixerProfileSchema.fixerProfessionalDetails[index] = res2.response[0];
                        _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                        _this.inventFundsApiService.showAlertUpdateMessage();
                    })
                        .catch(function (err) {
                        console.log('Error:' + err);
                    });
                }
            });
        }
    };
    // get education data
    ProfilePageComponent.prototype.getEducationData = function () {
        var _this = this;
        var params = {
            collectionName: 'fixerEducationDetails',
            endPoint: 'RETRIEVEEDUCATIONDETAILS',
        };
        this.inventFundsApiService
            .retrieveGeneric(params)
            .then(function (res) {
            // console.log('educationDetails response!' + JSON.stringify(res))
            res.response.map(function (item) {
                if (item.personId == _this.appState.encryptedDataValue('personId')) {
                    // alert('professionalDetails already exists')
                    _this.educationDetails.push(item);
                }
            });
            // console.log('edu details array:' + JSON.stringify(this.educationDetails))
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // create education data
    ProfilePageComponent.prototype.createEducationData = function (value) {
        var _this = this;
        var eduCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            eduCol = 'fixerEducationDetails';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            eduCol = 'founderEducationDetails';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            eduCol = 'funderEducationDetails';
        }
        var educationData = value;
        Object.assign(educationData, { personId: this.appState.encryptedDataValue('personId') });
        var eduDetails = {
            collectionName: eduCol,
            updateData: educationData,
            endPoint: 'CREATEEDUCATIONDETAILS',
        };
        this.inventFundsApiService
            .createGeneric(eduDetails)
            .then(function (res) {
            // console.log('education create response==>' + JSON.stringify(res));
            _this.educationDetails.push(res.data[0]);
            if (_this.typeOfRole == 'Invent Fund Fixer') {
                _this.fixerProfileSchema.fixerEducationalDetails.push(res.data[0]);
                _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
            }
            else if (_this.typeOfRole == 'Invent Fund Founder') {
                _this.founderProfileSchema.founderEducationalDetails.push(res.data[0]);
                _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
            }
            else if (_this.typeOfRole == 'Invent Fund Funder') {
                _this.funderProfileSchema.funderEducationalDetails.push(res.data[0]);
                _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
            }
            _this.inventFundsApiService.showAlertCreateMessage();
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // update education data
    ProfilePageComponent.prototype.updateEducationData = function (value, id) {
        var _this = this;
        var eduCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            eduCol = 'fixerEducationDetails';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            eduCol = 'founderEducationDetails';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            eduCol = 'funderEducationDetails';
        }
        var eduDetails = {
            collectionName: eduCol,
            pkId: id,
            updateData: value,
            endPoint: 'UPDATEEDUCATIONDETAILS',
        };
        this.inventFundsApiService
            .updateGeneric(eduDetails)
            .then(function (res) {
            // console.log('Update education response->' + JSON.stringify(res))
            if (res.status == 200) {
                var params = {
                    collectionName: eduCol,
                    _id: res.data._id,
                    endPoint: 'RETRIEVEEDUCATIONDETAILSONE',
                };
                _this.inventFundsApiService
                    .retrieveGeneric(params)
                    .then(function (res2) {
                    // console.log('retrieve educationDetails one response-->' + JSON.stringify(res2));
                    // this.educationDetails = res2
                    var index = _this.educationDetails.findIndex(function (p) { return p._id == res2.response[0]._id; });
                    _this.educationDetails[index] = res2.response[0];
                    if (_this.typeOfRole == 'Invent Fund Fixer') {
                        var index = _this.fixerProfileSchema.fixerEducationalDetails.findIndex(function (p) { return p._id == res2.response[0]._id; });
                        _this.fixerProfileSchema.fixerEducationalDetails[index] = res2.response[0];
                        _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Founder') {
                        // alert('in founder edu update')
                        var index = _this.founderProfileSchema.founderEducationalDetails.findIndex(function (p) { return p._id == res2.response[0]._id; });
                        _this.founderProfileSchema.founderEducationalDetails[index] = res2.response[0];
                        // console.log(
                        //   'edu details updated:' +
                        //     JSON.stringify(this.founderProfileSchema.founderEducationalDetails)
                        // );
                        _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
                    }
                    else if (_this.typeOfRole == 'Invent Fund Funder') {
                        // alert('in funder edu update')
                        var index = _this.funderProfileSchema.funderEducationalDetails.findIndex(function (p) { return p._id == res2.response[0]._id; });
                        _this.funderProfileSchema.funderEducationalDetails[index] = res2.response[0];
                        // console.log(
                        //   'edu details updated:' +
                        //     JSON.stringify(this.funderProfileSchema.funderEducationalDetails)
                        // );
                        _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
                    }
                    _this.inventFundsApiService.showAlertUpdateMessage();
                })
                    .catch(function (err) {
                    console.log('Error:' + err);
                });
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // delete education
    ProfilePageComponent.prototype.deleteEducationData = function (id) {
        var _this = this;
        var eduCol;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            eduCol = 'fixerEducationDetails';
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            eduCol = 'founderEducationDetails';
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            eduCol = 'funderEducationDetails';
        }
        this.swalMessage('Are you sure you want to delete this entry?')
            .then(function (res) {
            var variable = Object.keys(res).includes('value');
            // alert(JSON.stringify(res) + JSON.stringify(id))
            if (variable) {
                var params = {
                    pkId: id,
                    collectionName: eduCol,
                    endPoint: 'DELETEEDUCATIONDETAILS',
                };
                _this.inventFundsApiService
                    .deleteGeneric(params)
                    .then(function (res2) {
                    // console.log('delete response->' + JSON.stringify(res2))
                    if (res2.status == 200) {
                        // var index = this.educationDetails.findIndex(p => p._id == id)
                        // this.educationDetails.splice(index, 1);
                        if (_this.typeOfRole == 'Invent Fund Fixer') {
                            var index = _this.fixerProfileSchema.fixerEducationalDetails.findIndex(function (p) { return p._id == id; });
                            _this.fixerProfileSchema.fixerEducationalDetails.splice(index, 1);
                            _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                        }
                        else if (_this.typeOfRole == 'Invent Fund Founder') {
                            var index = _this.founderProfileSchema.founderEducationalDetails.findIndex(function (p) { return p._id == id; });
                            _this.founderProfileSchema.founderEducationalDetails.splice(index, 1);
                            _this.setDataProfileStatus(_this.founderProfileSchema.founderProfileSummary, _this.founderProfileSchema.founderEducationalDetails, [], [], []);
                        }
                        else if (_this.typeOfRole == 'Invent Fund Funder') {
                            var index = _this.funderProfileSchema.funderEducationalDetails.findIndex(function (p) { return p._id == id; });
                            _this.funderProfileSchema.funderEducationalDetails.splice(index, 1);
                            _this.setDataProfileStatus(_this.funderProfileSchema.funderProfileSummary, _this.funderProfileSchema.funderEducationalDetails, [], [], []);
                        }
                        _this.inventFundsApiService.showAlertDeleteMessage();
                    }
                })
                    .catch(function (err) {
                    console.log('Error:' + err);
                });
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // delete profession
    ProfilePageComponent.prototype.deleteProfessionData = function (id) {
        var _this = this;
        this.swalMessage('Are you sure you want to delete this entry?')
            .then(function (res) {
            var variable = Object.keys(res).includes('value');
            // alert(JSON.stringify(res) + JSON.stringify(id))
            if (variable) {
                var params = {
                    pkId: id,
                    endPoint: 'DELETEPROFESSIONALDETAILS',
                };
                _this.inventFundsApiService
                    .deleteGeneric(params)
                    .then(function (res2) {
                    // console.log('delete response->' + JSON.stringify(res2))
                    if (res2.status == 200) {
                        var index = _this.fixerProfileSchema.fixerProfessionalDetails.findIndex(function (p) { return p._id == id; });
                        _this.fixerProfileSchema.fixerProfessionalDetails.splice(index, 1);
                        _this.setDataProfileStatus(_this.fixerProfileSchema.fixerProfileSummary, _this.fixerProfileSchema.fixerEducationalDetails, _this.fixerProfileSchema.fixerProfessionalDetails, _this.fixerProfileSchema.fixerAchievementDetails, _this.fixerProfileSchema.fixerSkillDetails);
                        _this.inventFundsApiService.showAlertDeleteMessage();
                    }
                })
                    .catch(function (err) {
                    console.log('Error:' + err);
                });
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // delete achivements
    ProfilePageComponent.prototype.deleteAchi = function (id) {
        var _this = this;
        this.swalMessage('Are you sure you want to delete this entry?')
            .then(function (res) {
            var variable = Object.keys(res).includes('value');
            // alert(JSON.stringify(res) + JSON.stringify(id))
            if (variable) {
                var params = {
                    pkId: id,
                    endPoint: 'DELETEACHIDETAILS',
                };
                _this.inventFundsApiService
                    .deleteGeneric(params)
                    .then(function (res2) {
                    // console.log('delete response->' + JSON.stringify(res2));
                    if (res2.status == 200) {
                        // var index = this.educationDetails.findIndex(p => p._id == id)
                        // this.educationDetails.splice(index, 1);
                        _this.getAchiDetails('delete');
                    }
                })
                    .catch(function (err) {
                    console.log('Error:' + err);
                });
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // create skills data
    ProfilePageComponent.prototype.createDataSkills = function (data, type) {
        var _this = this;
        // console.log('formly create data:' + JSON.stringify(data) + '' + type)
        var skillsCat = [];
        var skillSet = [];
        if (data.value.skillCategory) {
            data.value.skillCategory.filter(function (item) {
                skillsCat.push(item.value);
            });
            // console.log("skills cat : " + JSON.stringify(skillsCat))
        }
        if (data.value.skillSet) {
            data.value.skillSet.filter(function (item) {
                skillSet.push(item.value);
            });
            // console.log("skills set : " + JSON.stringify(skillSet))
        }
        var obj = {
            skillCategory: skillsCat,
            skillSet: skillSet,
            selfAssessmentScore: data.value.selfAssessmentScore,
            personId: this.appState.encryptedDataValue('personId'),
        };
        var params = {
            updateData: obj,
            collectionName: 'fixerSkillDetails',
            endPoint: 'CREATESKILLSDETAILS',
        };
        this.inventFundsApiService.createGeneric(params).then(function (res) {
            // console.log("data created : " + JSON.stringify(res));
            _this.getSkillsDetails('create');
            // this.individualProfessionalDetails.push(res.data[0])
            // console.log('ind prof det:' + JSON.stringify(this.individualProfessionalDetails))
        });
    };
    // update skills
    ProfilePageComponent.prototype.updateDataSkills = function (data, type) {
        var _this = this;
        // console.log('formly update data:' + JSON.stringify(data) + '' + type)
        var skillsCat = [];
        var skillSet = [];
        if (data.value.skillCategory) {
            data.value.skillCategory.filter(function (item) {
                if (item.value) {
                    skillsCat.push(item.value);
                }
                else {
                    skillsCat.push(item);
                }
            });
        }
        if (data.value.skillSet) {
            data.value.skillSet.filter(function (item) {
                // skillSet.push(item.value);
                if (item.value) {
                    skillSet.push(item.value);
                }
                else {
                    skillSet.push(item);
                }
            });
        }
        var obj = {
            skillCategory: skillsCat,
            skillSet: skillSet,
            selfAssessmentScore: data.value.selfAssessmentScore,
            personId: this.appState.encryptedDataValue('personId'),
        };
        var params = {
            updateData: obj,
            pkId: data.id,
            collectionName: 'fixerSkillDetails',
            endPoint: 'UPDATESKILLSDETAILS',
        };
        this.inventFundsApiService.updateGeneric(params).then(function (res) {
            // console.log("data updated : " + JSON.stringify(res));
            _this.getSkillsDetails('update');
            // this.individualProfessionalDetails.push(res.data[0])
            // console.log('ind prof det:'+JSON.stringify(this.individualProfessionalDetails))
        });
    };
    // delete skills
    ProfilePageComponent.prototype.deleteSkills = function (id) {
        var _this = this;
        this.swalMessage('Are you sure you want to delete this entry?')
            .then(function (res) {
            var variable = Object.keys(res).includes('value');
            // alert(JSON.stringify(res) + JSON.stringify(id))
            if (variable) {
                var params = {
                    pkId: id,
                    endPoint: 'DELETESKILLSDETAILS',
                };
                _this.inventFundsApiService
                    .deleteGeneric(params)
                    .then(function (res2) {
                    // console.log('delete response->' + JSON.stringify(res2))
                    if (res2.status == 200) {
                        // var index = this.educationDetails.findIndex(p => p._id == id)
                        // this.educationDetails.splice(index, 1);
                        _this.getSkillsDetails('delete');
                    }
                })
                    .catch(function (err) {
                    console.log('Error:' + err);
                });
            }
        })
            .catch(function (err) {
            console.log('Error:' + err);
        });
    };
    // recompute attachmant array
    ProfilePageComponent.prototype.recomputeAttachmentArray = function () {
        var _this = this;
        var attachment = {
            collectionName: 'attachment',
            queryStr: {
                $and: [{ personId: this.appState.encryptedDataValue('personId') }, { type: 'SHOWCASE' }],
            },
            endPoint: 'RETRIEVEATTACHMENTDATASIGNED',
        };
        this.inventFundsApiService.retrieveGeneric(attachment).then(function (res) {
            // console.log('recomputeAttachmentArray res :' + JSON.stringify(res));
            if (res.status == 200 && res.data.length != 0) {
                // this.fixerProfileSchema.showcaseDetails.push(res.data[0])
                // this.showcaseArray.push(res.data[0])
                _this.fixerProfileSchema.showcaseDetails = res.response;
                var sanitizedData = res.data.map(function (item) { return ({
                    _id: item._id,
                    personId: item.personId,
                    url: _this.sanitizeUrl(item.url),
                    type: item.type,
                    // path: item.path,
                    // key: item.key,
                    fileType: item.fileType,
                    description: item.description,
                    createdDate: item.description,
                    createdBy: item.createdBy,
                    updatedDate: item.updatedDate,
                    updatedBy: item.updatedBy,
                }); });
                var changedArrayAttachment = sanitizedData.slice();
                _this.showcaseArray = changedArrayAttachment;
                _this.reload();
                // this.cdr.detectChanges();
                // console.log('after recompute : ' + JSON.stringify(this.showcaseArray));
                _this.loading = false;
            }
            else {
                _this.showcaseArray = [];
            }
        });
    };
    // change type of profession
    ProfilePageComponent.prototype.changeInType = function (event, previous) {
        var _this = this;
        // alert('change in type:'+JSON.stringify(event))
        var profileCol, updateId;
        if (this.typeOfRole == 'Invent Fund Fixer') {
            profileCol = 'fixerProfileSummary';
            updateId = this.fixerProfileSchema.fixerProfileSummary._id;
        }
        else if (this.typeOfRole == 'Invent Fund Founder') {
            profileCol = 'founderProfileSummary';
            updateId = this.founderProfileSchema.founderProfileSummary._id;
        }
        else if (this.typeOfRole == 'Invent Fund Funder') {
            profileCol = 'funderProfileSummary';
            updateId = this.funderProfileSchema.funderProfileSummary._id;
        }
        this.selectedValue = event;
        var updateValue;
        this.types.map(function (item) {
            // console.log('item:' + JSON.stringify(item))
            if (event == item['value']) {
                updateValue = item['viewValue'];
            }
        });
        // alert('updateValue:'+updateValue+'pk id:'+updateId)
        if (updateId) {
            this.swalMessage('Are you sure you want to change your role?').then(function (res) {
                var variable = Object.keys(res).includes('value');
                if (variable) {
                    var profileSummary = {
                        collectionName: profileCol,
                        pkId: updateId,
                        updateData: { personType: updateValue },
                        endPoint: 'UPDATEPROFILESUMMARY',
                    };
                    _this.inventFundsApiService
                        .updateGeneric(profileSummary)
                        .then(function (res) {
                        // console.log('personType update resp:' + JSON.stringify(res))
                        _this.types.map(function (item) {
                            // console.log('item:' + JSON.stringify(item))
                            if (res.data.personType == item['value']) {
                                _this.selectedValue = item['viewValue'];
                            }
                        });
                        _this.inventFundsApiService.showAlertUpdateMessage();
                    })
                        .catch(function (err) {
                        console.log('error:' + err);
                    });
                }
                else {
                    // console.log("selected value : " + JSON.stringify(this.selectedValue), { previous });
                    _this.selectedValue = previous;
                    _this.form.controls['roleType'].setValue(previous);
                }
            });
        }
    };
    /**
     *
     * @param url it helps preventing Cross Site Scripting Security bugs (XSS) by
     *            sanitizing values to be safe to use in the different DOM contexts
     */
    ProfilePageComponent.prototype.sanitizeUrl = function (url) {
        // return this.sanitizer.bypassSecurityTrustResourceUrl(url);
        return url;
    };
    /**
     *
     * @param $event object which consits url and type of file to render in new window if type is pdf
     */
    ProfilePageComponent.prototype.emitImageObject = function ($event) {
        console.log('render obj : ' + JSON.stringify($event));
        if ($event.fileType == 'application/pdf') {
            // var data = {
            //   path: $event.path,
            //   key: $event.key,
            // };
            // this.inventFundsApiService
            //   .getImageData(data)
            //   .then((result) => {
            //     if (result.status === 200) {
            // console.log(result);
            var w = window.open($event['url']);
            w.focus();
            // }
            // })
            // .catch((err) => {
            //   console.log('err data : ' + JSON.stringify(err));
            // });
        }
    };
    /**
     *
     * @param obj contains url and id to delete image or doc
     */
    ProfilePageComponent.prototype.deleteCarosulDoc = function (obj) {
        var _this = this;
        // console.log('delete object : ' + JSON.stringify(obj));
        this.swalMessage('Are you sure you want to delete?').then(function (response) {
            var variable = Object.keys(response).includes('value');
            // console.log(JSON.stringify(response));
            if (variable) {
                var params = {
                    collectionName: 'attachment',
                    data: {
                        url: obj.url,
                        _id: obj._id,
                    },
                    endPoint: 'DELETEATTACHMENTDATARECORD',
                };
                _this.inventFundsApiService
                    .deleatAttachmentFileSingle(params)
                    .then(function (result) {
                    // console.log(result);
                    if (result.code == 200) {
                        _this.recomputeAttachmentArray();
                    }
                })
                    .catch(function (err) {
                    console.log(err);
                });
            }
        });
    };
    /**
     *
     * @param content text to show in swal message and returns the tru or false
     */
    ProfilePageComponent.prototype.swalMessage = function (content) {
        return sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire({
            title: content,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            customClass: {
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        })
            .then(function (result) {
            return result;
        })
            .catch(function (err) {
            return err;
        });
    };
    // reload showcase data dippends on change to sync data
    ProfilePageComponent.prototype.reload = function () {
        var _this = this;
        this.show = false;
        setTimeout(function () { return (_this.show = true); });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('matSelect'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_12__["MatSelect"])
    ], ProfilePageComponent.prototype, "matSelect", void 0);
    ProfilePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-profile-page',
            template: __webpack_require__(/*! ./profile-page.component.html */ "./src/app/profile/profile-page/profile-page.component.html"),
            styles: [__webpack_require__(/*! ./profile-page.component.scss */ "./src/app/profile/profile-page/profile-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_theme_options__WEBPACK_IMPORTED_MODULE_2__["ThemeOptions"],
            _services_app_state_service__WEBPACK_IMPORTED_MODULE_3__["AppStateService"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_4__["InventFundsApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__["DomSanitizer"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], ProfilePageComponent);
    return ProfilePageComponent;
}());



/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfileModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileModule", function() { return ProfileModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _layout_base_layout_base_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../layout/base-layout/base-layout.component */ "./src/app/layout/base-layout/base-layout.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng-bootstrap-form-validation */ "./node_modules/ng-bootstrap-form-validation/fesm5/ng-bootstrap-form-validation.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/profile/dashboard/dashboard.component.ts");
/* harmony import */ var _profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./profile-page/profile-page.component */ "./src/app/profile/profile-page/profile-page.component.ts");
/* harmony import */ var _profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./profile-modal/profile-modal.component */ "./src/app/profile/profile-modal/profile-modal.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/profile/settings/settings.component.ts");
/* harmony import */ var ng4_geoautocomplete__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng4-geoautocomplete */ "./node_modules/ng4-geoautocomplete/index.js");
/* harmony import */ var _services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/roles-auth-guard.service */ "./src/app/services/roles-auth-guard.service.ts");
/* harmony import */ var ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-slick-carousel */ "./node_modules/ngx-slick-carousel/fesm5/ngx-slick-carousel.js");
/* harmony import */ var _shared_layouts_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../shared/layouts.module */ "./src/app/shared/layouts.module.ts");















var routes = [
    {
        path: "",
        component: _layout_base_layout_base_layout_component__WEBPACK_IMPORTED_MODULE_4__["BaseLayoutComponent"],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            {
                path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"],
                canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["RolesAuthGuardService"]],
                data: ['dashboard']
            },
            {
                path: "editProfile",
                component: _profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_8__["ProfilePageComponent"],
                canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["RolesAuthGuardService"]],
                data: ['editProfile']
            },
            {
                path: 'settings',
                component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_10__["SettingsComponent"],
                canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["RolesAuthGuardService"]],
                data: ['settings']
            },
            {
                path: 'settings/:data',
                component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_10__["SettingsComponent"],
                canActivate: [_services_roles_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["RolesAuthGuardService"]],
                data: ['settings/:data']
            },
        ]
    },
    { path: '**', redirectTo: 'dashboard' }
];
var ProfileModule = /** @class */ (function () {
    function ProfileModule() {
    }
    ProfileModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _layout_base_layout_base_layout_component__WEBPACK_IMPORTED_MODULE_4__["BaseLayoutComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"],
                _profile_page_profile_page_component__WEBPACK_IMPORTED_MODULE_8__["ProfilePageComponent"],
                _profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_9__["ProfileModalComponent"],
                _settings_settings_component__WEBPACK_IMPORTED_MODULE_10__["SettingsComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
                ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_13__["SlickCarouselModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_6__["NgBootstrapFormValidationModule"].forRoot(),
                ng4_geoautocomplete__WEBPACK_IMPORTED_MODULE_11__["Ng4GeoautocompleteModule"].forRoot(),
                _shared_layouts_module__WEBPACK_IMPORTED_MODULE_14__["LayoutsModule"]
            ],
            entryComponents: [_profile_modal_profile_modal_component__WEBPACK_IMPORTED_MODULE_9__["ProfileModalComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["NO_ERRORS_SCHEMA"]]
        })
    ], ProfileModule);
    return ProfileModule;
}());



/***/ }),

/***/ "./src/app/profile/settings/settings.component.html":
/*!**********************************************************!*\
  !*** ./src/app/profile/settings/settings.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-account-settings\r\n\t[profile]=\"profileJson\"\r\n\t[profileData]=\"profileData\"\r\n\t[password]=\"passwordJson\"\r\n\t[passwordData]=\"passwordData\"\r\n\t[profileImage]=\"appState.profileImage\"\r\n\t[refMapPassword]=\"refMapPasswordChange\"\r\n\t[refMapProfile]=\"refMapProfileChange\"\r\n\t(imageBase64Data)=\"getImageBase64Dat($event)\"\r\n\t(formlyData)=\"getFormlyData($event)\"\r\n\t(updateData)=\"getFormlyUpdateData($event)\"\r\n\t(updatePasswordData)=\"updatePasswordEmit($event)\"\r\n>\r\n</app-account-settings>\r\n"

/***/ }),

/***/ "./src/app/profile/settings/settings.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/profile/settings/settings.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .swal-title {\n  margin: 0px !important;\n  font-size: 18px !important;\n  margin-bottom: 28px !important;\n  position: relative !important;\n  max-width: 100% !important;\n  padding: 0 !important;\n  color: #595959 !important;\n  font-weight: 600 !important;\n  text-align: center !important;\n  text-transform: none !important;\n  word-wrap: break-word !important; }\n\n::ng-deep .swal2-actions {\n  margin: 0 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9zZXR0aW5ncy9EOlxcV29ya1xcUHJvamVjdHNcXElOVkVOVEZVTkRcXEJSQU5DSEVTXFxpbnZlbnRmdW5kLXRlc3Qvc3JjXFxhcHBcXHByb2ZpbGVcXHNldHRpbmdzXFxzZXR0aW5ncy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHNCQUFzQjtFQUN0QiwwQkFBMEI7RUFFMUIsOEJBQThCO0VBQzlCLDZCQUE2QjtFQUM3QiwwQkFBMEI7RUFDMUIscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUV6QiwyQkFBMkI7RUFDM0IsNkJBQTZCO0VBQzdCLCtCQUErQjtFQUMvQixnQ0FBZ0MsRUFBQTs7QUFHakM7RUFDQyxvQkFBb0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvc2V0dGluZ3Mvc2V0dGluZ3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLnN3YWwtdGl0bGUge1xyXG5cdG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XHJcblx0Zm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XHJcblx0Ly8gYm94LXNoYWRvdzogMHB4IDFweCAxcHggcmdiYSgwLCAwLCAwLCAwLjIxKSAhaW1wb3J0YW50O1xyXG5cdG1hcmdpbi1ib3R0b206IDI4cHggIWltcG9ydGFudDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcclxuXHRtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcblx0Y29sb3I6ICM1OTU5NTkgIWltcG9ydGFudDtcclxuXHQvLyBmb250LXNpemU6IDEuODc1ZW0gIWltcG9ydGFudDtcclxuXHRmb250LXdlaWdodDogNjAwICFpbXBvcnRhbnQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcblx0dGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcclxuXHR3b3JkLXdyYXA6IGJyZWFrLXdvcmQgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zd2FsMi1hY3Rpb25zIHtcclxuXHRtYXJnaW46IDAgIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/profile/settings/settings.component.ts":
/*!********************************************************!*\
  !*** ./src/app/profile/settings/settings.component.ts ***!
  \********************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");
/* harmony import */ var _services_app_state_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var _theme_options__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../theme-options */ "./src/app/theme-options.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var URL = 'api/attachment/uploadDirectToS3';


var SettingsComponent = /** @class */ (function () {
    /**
     *
     * @param globals set theme options
     * @param appState get appstate variable and methods
     * @param inventFundsApiService call apis from services
     * @param router navigate to different components
     */
    function SettingsComponent(globals, appState, inventFundsApiService, router) {
        this.globals = globals;
        this.appState = appState;
        this.inventFundsApiService = inventFundsApiService;
        this.router = router;
        // global variables
        this.profileJson = [];
        this.passwordJson = [];
        this.refMapPasswordChange = { sourceId: '', sourceType: '' };
        this.refMapProfileChange = { sourceId: '', sourceType: '' };
        this.getPasswordChangeJson();
        this.getProfileJson();
        this.globals.toggleSidebar = true;
    }
    // ng onInit
    SettingsComponent.prototype.ngOnInit = function () {
        this.getProfileData();
    };
    // password change screen builder data
    SettingsComponent.prototype.getPasswordChangeJson = function () {
        var _this = this;
        var params = {
            collectionName: 'passwordChange',
        };
        this.inventFundsApiService.retrieveScreenbuilderAll(params).then(function (res) {
            // console.log("passwordChange res " + JSON.stringify(res.response))
            // this.passwordJson = res.response;
            _this.passwordJson = [
                {
                    key: 'password',
                    validators: {
                        fieldMatch: {
                            expression: function (control) {
                                var value = control.value;
                                return (value.confirmNewPassword === value.newPassword ||
                                    // avoid displaying the message error when values are empty
                                    !value.confirmNewPassword ||
                                    !value.newPassword);
                            },
                            message: 'Password Not Matching',
                            errorPath: 'confirmNewPassword',
                        },
                    },
                    fieldGroup: res.response[0].fieldGroup,
                },
            ];
        });
    };
    // profile screen builder data
    SettingsComponent.prototype.getProfileJson = function () {
        var _this = this;
        var params = {
            collectionName: 'profile',
        };
        this.inventFundsApiService.retrieveScreenbuilderAll(params).then(function (res) {
            _this.profileJson = res.response;
            // console.log("profile res " + JSON.stringify(res.response))
        });
    };
    // To get image base64 data
    SettingsComponent.prototype.getImageBase64Dat = function (event) {
        var _this = this;
        console.log('settings profile imge>>>> : ' + JSON.stringify(event));
        // https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFundMulter/HOST/100/IFFIXER/LOGO/5ecf7f4f01207222c87f5054/0266554465.jpeg
        // var n = event.lastIndexOf('/');
        // var key = event.substring(n + 1);
        // var path = 'inventFund' + event.split('inventFund')[1].replace(key, '');
        // // console.log('after data split : ' + JSON.stringify(path));
        // // console.log('key data : ' + JSON.stringify(key));
        // // console.log("person id : " + JSON.stringify(this.appState.encryptedDataValue('profile')))
        if (sessionStorage.getItem('profileImage')) {
            // console.log('object if');
            var imageParams = {
                collectionName: 'attachment',
                createData: {
                    id: this.appState.encryptedDataValue('personId'),
                    // path: path,
                    // key: key,
                    url: event,
                    type: 'PROFILE',
                },
            };
            // console.log('settings profile imge>>>> : ' + JSON.stringify(imageParams));
            this.inventFundsApiService
                .attachmentUpdateSingleFile(imageParams)
                .then(function (res) {
                if (res.code == 200) {
                    console.log('each image attache : ' + JSON.stringify(res));
                    // var param = {
                    //   path: res.data[0].path,
                    //   key: res.data[0].key,
                    // };
                    // // console.log('response data : ' + JSON.stringify(param));
                    // this.inventFundsApiService
                    //   .getImageData(param)
                    //   .then((result) => {
                    //     // console.log('result data : ' + JSON.stringify(result));
                    //     if (result.status == 200) {
                    // this.appState.profileImage = res.data[0].url;
                    // this.appState.setProfileImage(res.data[0].url);
                    // this.appState.setAvatar(res.data[0].url);
                    // this.appState.profileImageSigned = res.data[0].url;
                    // sessionStorage.removeItem('profileImage');
                    // sessionStorage.setItem('profileImage', res.data[0].url);
                    //     }
                    //   })
                    //   .catch((err) => {
                    //     console.log('err data : ' + JSON.stringify(err));
                    //   });
                }
            })
                .catch(function (err) {
                console.log(err);
            });
        }
        else {
            // console.log('object else');
            var imageParams = {
                collectionName: 'attachment',
                createData: {
                    id: this.appState.encryptedDataValue('personId'),
                    // path: path,
                    // key: key,
                    url: event,
                    type: 'PROFILE',
                },
            };
            // console.log('settings profile imge>>>> : ' + JSON.stringify(imageParams));
            this.inventFundsApiService
                .attachmentSingleFile(imageParams)
                .then(function (res) {
                if (res.code == 200 && res.data.length != 0) {
                    console.log('data response for image else : ' + JSON.stringify(res.data));
                    //   var param = {
                    //     path: res.data[0].path,
                    //     key: res.data[0].key,
                    //   };
                    //   // console.log('response data : ' + JSON.stringify(param));
                    //   this.inventFundsApiService
                    //     .getImageData(param)
                    //     .then((result) => {
                    //       // console.log('result data : ' + JSON.stringify(result));
                    //       if (result.status == 200) {
                    _this.appState.profileImage = res.data[0].url;
                    _this.appState.setProfileImage(res.data[0].url);
                    _this.appState.setAvatar(res.data[0].url);
                    _this.appState.profileImageSigned = res.data[0].url;
                    //         // console.log(
                    //         //   'app state profile image : ' + JSON.stringify(this.appState.profileImageSigned)
                    //         // );
                    sessionStorage.removeItem('profileImage');
                    sessionStorage.setItem('profileImage', res.data[0].url);
                    _this.appState.getProfileImage();
                    //       }
                    //     })
                    //     .catch((err) => {
                    //       console.log('err data : ' + JSON.stringify(err));
                    //     });
                }
            })
                .catch(function (err) {
                console.log(err);
            });
        }
    };
    // on sucess
    SettingsComponent.prototype.onSuccess = function (response) {
        this.appState.setAvatar(response.newAvatar);
    };
    // Tp get formly data
    SettingsComponent.prototype.getFormlyData = function (event) {
        var _this = this;
        // console.log("getFormlyData : " + JSON.stringify(event));
        if (event.type == 'profile') {
            Object.assign(event.data, {
                personId: this.appState.encryptedDataValue('personId'),
                sourceType: 'Profile',
            });
            var params = {
                updateData: event.data,
                module: 'user',
                endPoint: 'CREATEPROFILE',
            };
            this.inventFundsApiService
                .createGeneric(params)
                .then(function (res) {
                // console.log('data created : ' + JSON.stringify(res));
                _this.inventFundsApiService.showAlertCreateMessage();
            })
                .catch(function (err) {
                _this.inventFundsApiService.showAlertCreateErrorMessage();
            });
        }
    };
    // get formly update data
    SettingsComponent.prototype.getFormlyUpdateData = function ($event) {
        // console.log('data to update : ' + JSON.stringify($event));
        if ($event.type == 'profile') {
            this.updateProfileData($event);
        }
    };
    // update profile data
    SettingsComponent.prototype.updateProfileData = function ($event) {
        var _this = this;
        // console.log('update : ' + JSON.stringify($event));
        var emittedData = $event['data'].response;
        // Object.assign(emittedData,{partyId:sessionStorage.getItem('partyId')});
        var profile = {
            collectionName: 'person',
            pkId: $event['data'].id,
            updateData: emittedData,
            endPoint: 'UPDATEPROFILE',
        };
        this.inventFundsApiService
            .updateGeneric(profile)
            .then(function (res) {
            _this.inventFundsApiService.showAlertUpdateMessageNoReload();
            _this.getProfile();
        })
            .catch(function (err) {
            console.log('Error:' + JSON.stringify(err));
            _this.inventFundsApiService.showAlertUpdateErrorMessageNoReload();
        });
    };
    // get profile data
    SettingsComponent.prototype.getProfileData = function () {
        this.getProfile();
    };
    // get profile data
    SettingsComponent.prototype.getProfile = function () {
        var _this = this;
        // console.log('fetchAdd:' + JSON.stringify(this.appState.encryptedDataValue('personId')));
        // this.dataEmitted = eventArgs;
        var params = {
            collectionName: 'person',
            _id: this.appState.encryptedDataValue('personId'),
            endPoint: 'RETRIVEPROFILE',
        };
        this.inventFundsApiService
            .retrieveGeneric(params)
            .then(function (res) {
            // console.log("data : " + JSON.stringify(res))
            if (res.status == 200) {
                if (res.response.length != 0) {
                    // console.log("profileData " + JSON.stringify(res.response[0]))
                    //   // this.selectedIndex = 1;
                    var objectData = res.response[0];
                    Object.assign(objectData, {
                        userName: _this.appState.encryptedDataValue('personEmail'),
                    });
                    _this.profileData = res.response[0];
                    _this.refMapProfileChange = {
                        sourceId: res.response[0]._id,
                        sourceType: 'Profile',
                    };
                }
                else {
                    // console.log("else : " + JSON.stringify(this.refMapProfileChange))
                    _this.refMapProfileChange = {
                        sourceId: _this.appState.encryptedDataValue('personId'),
                        sourceType: 'Profile',
                    };
                }
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    // update password emited from settings compnent
    SettingsComponent.prototype.updatePasswordEmit = function ($event) {
        var _this = this;
        // console.log("data for update password new : " + JSON.stringify($event))
        // var emittedData = $event['data'].response;
        // Object.assign(emittedData,{partyId:sessionStorage.getItem('partyId')});
        // console.log("update password : " + JSON.stringify($event))
        if ($event) {
            var params = {
                module: 'user',
                userName: this.appState.encryptedDataValue('personEmail'),
                newPassword: $event.newPassword,
                endPoint: 'RESETPASSWORD',
            };
            this.inventFundsApiService.resetPassword(params).then(function (res) {
                // console.log("data created : " + JSON.stringify(res))
                if (res.code == 200) {
                    _this.passwordData = null;
                    // this.inventFundsApiService.showAlertUpdateMessageNoReload()
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default.a.fire({
                        title: 'Your password as been changed sucessffully, please Login with new password to access your application!',
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Okay',
                        customClass: {
                            // container: 'sweet_containerImportant',
                            title: 'swal-title',
                            actions: 'swal2-actions',
                            confirmButton: 'font-large btn btn-wide mb-2 mr-2 btn-primary',
                        },
                    }).then(function () {
                        sessionStorage.clear();
                        _this.router.navigate(['/login']);
                    });
                }
                else {
                    _this.inventFundsApiService.showAlertUpdateErrorMessageNoReload();
                }
            });
        }
    };
    SettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/profile/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.scss */ "./src/app/profile/settings/settings.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_theme_options__WEBPACK_IMPORTED_MODULE_4__["ThemeOptions"],
            _services_app_state_service__WEBPACK_IMPORTED_MODULE_3__["AppStateService"],
            _services_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_2__["InventFundsApiService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/utilities/profileJson.ts":
/*!******************************************!*\
  !*** ./src/app/utilities/profileJson.ts ***!
  \******************************************/
/*! exports provided: fixerProfileSchema, founderProfileSchema, funderProfileSchema */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fixerProfileSchema", function() { return fixerProfileSchema; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "founderProfileSchema", function() { return founderProfileSchema; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "funderProfileSchema", function() { return funderProfileSchema; });
var fixerProfileSchema = {
    fixerPersonDetails: {
        _id: '',
        partyId: '',
        personType: '',
        name: '',
        phoneNumber: '',
        createdDate: '',
        createdBy: '',
        updatedDate: '',
        updatedBy: '',
    },
    fixerProfileSummary: {
        _id: '',
        personId: '',
        headline: '',
        profileSummary: '',
        professionalSummary: '',
    },
    fixerEducationalDetails: [
        {
            _id: '',
            personId: '',
            country: '',
            address: {},
            college: '',
            education: '',
            specialization: '',
            from: '',
            grade: '',
            cgpaMax: 0,
            cgpaScored: 0,
            percentile: 0,
            percentage: 0,
            keyHighlights: '',
        },
    ],
    fixerProfessionalDetails: [
        {
            _id: '',
            personId: '',
            designation: '',
            company: '',
            from: '',
            to: '',
            currentJob: false,
            employmentType: '',
            location: '',
            experienceSummary: '',
        },
    ],
    fixerAchievementDetails: [
        {
            _id: '',
            personId: '',
            headlineTitles: '',
            achievementsCategory: '',
            achievementsSummary: '',
        },
    ],
    fixerSkillDetails: [
        {
            _id: '',
            personId: '',
            skillCategory: '',
            skillSet: '',
            selfAssessmentScore: 0,
            ratingFromEndorser: 0,
        },
    ],
    showcaseDetails: [
        {
            _id: '',
            personId: '',
            url: '',
            path: '',
            key: '',
            type: '',
            fileType: '',
        },
    ],
};
var founderProfileSchema = {
    founderPersonDetails: {
        _id: '',
        partyId: '',
        personType: '',
        name: '',
        phoneNumber: '',
        createdDate: '',
        createdBy: '',
        updatedDate: '',
        updatedBy: '',
    },
    founderProfileSummary: {
        _id: '',
        personId: '',
        headline: '',
        profileSummary: '',
        professionalSummary: '',
    },
    founderEducationalDetails: [
        {
            _id: '',
            personId: '',
            country: '',
            address: {},
            college: '',
            education: '',
            specialization: '',
            from: '',
            grade: '',
            cgpaMax: 0,
            cgpaScored: 0,
            percentile: 0,
            percentage: 0,
            keyHighlights: '',
        },
    ],
    showcaseDetails: [
        {
            _id: '',
            personId: '',
            url: '',
            type: '',
            fileType: '',
        },
    ],
};
var funderProfileSchema = {
    funderPersonDetails: {
        _id: '',
        partyId: '',
        personType: '',
        name: '',
        phoneNumber: '',
        createdDate: '',
        createdBy: '',
        updatedDate: '',
        updatedBy: '',
    },
    funderProfileSummary: {
        _id: '',
        personId: '',
        headline: '',
        profileSummary: '',
        professionalSummary: '',
        personType: '',
    },
    funderEducationalDetails: [
        {
            _id: '',
            personId: '',
            country: '',
            address: {},
            college: '',
            education: '',
            specialization: '',
            from: '',
            grade: '',
            cgpaMax: 0,
            cgpaScored: 0,
            percentile: 0,
            percentage: 0,
            keyHighlights: '',
        },
    ],
    showcaseDetails: [
        {
            _id: '',
            personId: '',
            url: '',
            type: '',
            fileType: '',
        },
    ],
};


/***/ })

}]);
//# sourceMappingURL=app-profile-profile-module.js.map