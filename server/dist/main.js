(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../app/auth/auth.module": [
		"./src/app/auth/auth.module.ts",
		"default~app-auth-auth-module~app-landing-components-landing-components-module~app-profile-profile-mo~c717f2c6",
		"default~app-auth-auth-module~app-profile-profile-module",
		"app-auth-auth-module"
	],
	"../app/landing-components/landing-components.module": [
		"./src/app/landing-components/landing-components.module.ts",
		"default~app-auth-auth-module~app-landing-components-landing-components-module~app-profile-profile-mo~c717f2c6",
		"app-landing-components-landing-components-module"
	],
	"../app/profile/profile.module": [
		"./src/app/profile/profile.module.ts",
		"default~app-auth-auth-module~app-landing-components-landing-components-module~app-profile-profile-mo~c717f2c6",
		"default~app-auth-auth-module~app-profile-profile-module",
		"app-profile-profile-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/ThemeOptions/store/config.actions.ts":
/*!******************************************************!*\
  !*** ./src/app/ThemeOptions/store/config.actions.ts ***!
  \******************************************************/
/*! exports provided: ConfigActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigActions", function() { return ConfigActions; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_redux_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular-redux/store */ "./node_modules/@angular-redux/store/lib/src/index.js");
/* harmony import */ var _angular_redux_store__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_angular_redux_store__WEBPACK_IMPORTED_MODULE_2__);



var ConfigActions = /** @class */ (function () {
    function ConfigActions(ngRedux) {
        this.ngRedux = ngRedux;
    }
    ConfigActions_1 = ConfigActions;
    ConfigActions.prototype.getConfig = function () {
        this.ngRedux.dispatch({
            type: ConfigActions_1.CONFIG_GET,
        });
    };
    ConfigActions.prototype.updateConfig = function (config) {
        this.ngRedux.dispatch({
            type: ConfigActions_1.CONFIG_UPDATE,
            payload: config
        });
    };
    var ConfigActions_1;
    ConfigActions.CONFIG_GET = 'CONFIG_GET';
    ConfigActions.CONFIG_UPDATE = 'CONFIG_UPDATE';
    ConfigActions = ConfigActions_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_redux_store__WEBPACK_IMPORTED_MODULE_2__["NgRedux"]])
    ], ConfigActions);
    return ConfigActions;
}());



/***/ }),

/***/ "./src/app/ThemeOptions/store/config.reducer.ts":
/*!******************************************************!*\
  !*** ./src/app/ThemeOptions/store/config.reducer.ts ***!
  \******************************************************/
/*! exports provided: ConfigReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigReducer", function() { return ConfigReducer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _config_actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config.actions */ "./src/app/ThemeOptions/store/config.actions.ts");


var INITIAL_STATE = {
    headerTheme: '',
    sidebarTheme: '',
};
function ConfigReducer(state, action) {
    if (state === void 0) { state = INITIAL_STATE; }
    switch (action.type) {
        case _config_actions__WEBPACK_IMPORTED_MODULE_1__["ConfigActions"].CONFIG_GET:
            return Object.assign({}, state);
        case _config_actions__WEBPACK_IMPORTED_MODULE_1__["ConfigActions"].CONFIG_UPDATE:
            return Object.assign({}, state, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, action.payload));
        default:
            return state;
    }
}


/***/ }),

/***/ "./src/app/ThemeOptions/store/index.ts":
/*!*********************************************!*\
  !*** ./src/app/ThemeOptions/store/index.ts ***!
  \*********************************************/
/*! exports provided: ArchitectUIState, rootReducer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArchitectUIState", function() { return ArchitectUIState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rootReducer", function() { return rootReducer; });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");
/* harmony import */ var _config_reducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config.reducer */ "./src/app/ThemeOptions/store/config.reducer.ts");


var ArchitectUIState = /** @class */ (function () {
    function ArchitectUIState() {
    }
    return ArchitectUIState;
}());

;
var rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
    config: _config_reducer__WEBPACK_IMPORTED_MODULE_1__["ConfigReducer"],
});


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    {
        path: "landing",
        loadChildren: "../app/landing-components/landing-components.module#LandingComponentsModule"
    },
    {
        path: "auth",
        loadChildren: "../app/auth/auth.module#AuthModule"
    },
    {
        path: "profile",
        loadChildren: "../app/profile/profile.module#ProfileModule"
    },
    { path: "**", redirectTo: "landing" }
];
var routerOptions = { useHash: true, anchorScrolling: "enabled", scrollPositionRestoration: "enabled" };
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, routerOptions)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "::ng-deep .swal-title {\n  margin: 0px !important;\n  font-size: 18px !important;\n  margin-bottom: 8px !important;\n  position: relative !important;\n  max-width: 100% !important;\n  padding: 0 !important;\n  color: #595959 !important;\n  font-weight: 400 !important;\n  text-align: center !important;\n  text-transform: none !important;\n  word-wrap: break-word !important; }\n\n::ng-deep .swal2-actions {\n  margin: 0 !important; }\n\n::ng-deep .form-group label > span {\n  color: red !important; }\n\n::ng-deep .swal2-confirm {\n  width: 80px !important;\n  height: 40px !important;\n  margin: 0 auto !important;\n  padding: 0 !important; }\n\n::ng-deep .swal2-show {\n  width: 25% !important; }\n\n::ng-deep .swal2-icon {\n  margin: 10px !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRDpcXFdvcmtcXFByb2plY3RzXFxJTlZFTlRGVU5EXFxCUkFOQ0hFU1xcaW52ZW50ZnVuZC10ZXN0L3NyY1xcYXBwXFxhcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxzQkFBc0I7RUFDdEIsMEJBQTBCO0VBRTFCLDZCQUE2QjtFQUM3Qiw2QkFBNkI7RUFDN0IsMEJBQTBCO0VBQzFCLHFCQUFxQjtFQUNyQix5QkFBeUI7RUFFekIsMkJBQTJCO0VBQzNCLDZCQUE2QjtFQUM3QiwrQkFBK0I7RUFDL0IsZ0NBQWdDLEVBQUE7O0FBR2pDO0VBQ0Msb0JBQW9CLEVBQUE7O0FBR3JCO0VBQ0MscUJBQXFCLEVBQUE7O0FBR3RCO0VBQ0Msc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2Qix5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7O0FBR3RCO0VBQ0kscUJBQXFCLEVBQUE7O0FBSXpCO0VBQ0MsdUJBQXVCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLnN3YWwtdGl0bGUge1xyXG5cdG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XHJcblx0Zm9udC1zaXplOiAxOHB4ICFpbXBvcnRhbnQ7XHJcblx0Ly8gYm94LXNoYWRvdzogMHB4IDFweCAxcHggcmdiYSgwLCAwLCAwLCAwLjIxKSAhaW1wb3J0YW50O1xyXG5cdG1hcmdpbi1ib3R0b206IDhweCAhaW1wb3J0YW50O1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZSAhaW1wb3J0YW50O1xyXG5cdG1heC13aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG5cdHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuXHRjb2xvcjogIzU5NTk1OSAhaW1wb3J0YW50O1xyXG5cdC8vIGZvbnQtc2l6ZTogMS44NzVlbSAhaW1wb3J0YW50O1xyXG5cdGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxuXHR0ZXh0LXRyYW5zZm9ybTogbm9uZSAhaW1wb3J0YW50O1xyXG5cdHdvcmQtd3JhcDogYnJlYWstd29yZCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLnN3YWwyLWFjdGlvbnMge1xyXG5cdG1hcmdpbjogMCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmZvcm0tZ3JvdXAgbGFiZWwgPiBzcGFuIHtcclxuXHRjb2xvcjogcmVkICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAuc3dhbDItY29uZmlybSB7XHJcblx0d2lkdGg6IDgwcHggIWltcG9ydGFudDtcclxuXHRoZWlnaHQ6IDQwcHggIWltcG9ydGFudDtcclxuXHRtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xyXG5cdHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5zd2FsMi1zaG93IHtcclxuICAgIHdpZHRoOiAyNSUgIWltcG9ydGFudDtcclxuICAgIC8vIGhlaWdodDogNDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAuc3dhbDItaWNvbiB7XHJcblx0bWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_menu_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/menu.service */ "./src/app/services/menu.service.ts");
/* harmony import */ var _utilities_menu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utilities/menu */ "./src/app/utilities/menu.ts");




var AppComponent = /** @class */ (function () {
    function AppComponent(menuService) {
        this.menuService = menuService;
        this.title = 'Invent Funds';
        menuService.addMenu(_utilities_menu__WEBPACK_IMPORTED_MODULE_3__["menuForTesting"]);
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_menu_service__WEBPACK_IMPORTED_MODULE_2__["MenuService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: provideConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "provideConfig", function() { return provideConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_redux_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular-redux/store */ "./node_modules/@angular-redux/store/lib/src/index.js");
/* harmony import */ var _angular_redux_store__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_angular_redux_store__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ThemeOptions_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ThemeOptions/store */ "./src/app/ThemeOptions/store/index.ts");
/* harmony import */ var _ThemeOptions_store_config_actions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ThemeOptions/store/config.actions */ "./src/app/ThemeOptions/store/config.actions.ts");
/* harmony import */ var _theme_options__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./theme-options */ "./src/app/theme-options.ts");
/* harmony import */ var _ngx_formly_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-formly/core */ "./node_modules/@ngx-formly/core/fesm5/ngx-formly-core.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ng-bootstrap-form-validation */ "./node_modules/ng-bootstrap-form-validation/fesm5/ng-bootstrap-form-validation.js");
/* harmony import */ var _utilities_customErrors__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./utilities/customErrors */ "./src/app/utilities/customErrors.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! angularx-social-login */ "./node_modules/angularx-social-login/angularx-social-login.es5.js");
/* harmony import */ var _utilities_customValidators__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./utilities/customValidators */ "./src/app/utilities/customValidators.ts");
/* harmony import */ var _shared_layouts_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./shared/layouts.module */ "./src/app/shared/layouts.module.ts");



















var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true,
};
var config = new angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["AuthServiceConfig"]([
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["GoogleLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["GoogleLoginProvider"]('518112019515-lk75gpa98pi65el6vqmbtld6ovatgsm5.apps.googleusercontent.com'),
    },
    {
        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["FacebookLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["FacebookLoginProvider"]('561602290896109'),
    },
]);
function provideConfig() {
    return config;
}
var AppModule = /** @class */ (function () {
    function AppModule(ngRedux, devTool) {
        this.ngRedux = ngRedux;
        this.devTool = devTool;
        this.ngRedux.configureStore(_ThemeOptions_store__WEBPACK_IMPORTED_MODULE_6__["rootReducer"], {}, [], [devTool.isEnabled() ? devTool.enhancer() : function (f) { return f; }]);
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_13__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
                ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_11__["NgBootstrapFormValidationModule"].forRoot(),
                _ngx_formly_core__WEBPACK_IMPORTED_MODULE_9__["FormlyModule"].forRoot({
                    validators: _utilities_customValidators__WEBPACK_IMPORTED_MODULE_16__["validators"],
                    validationMessages: _utilities_customValidators__WEBPACK_IMPORTED_MODULE_16__["validationMessages"],
                }),
                _angular_redux_store__WEBPACK_IMPORTED_MODULE_5__["NgReduxModule"],
                _shared_layouts_module__WEBPACK_IMPORTED_MODULE_17__["LayoutsModule"].forRoot(),
            ],
            providers: [
                {
                    provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_10__["PERFECT_SCROLLBAR_CONFIG"],
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
                },
                {
                    provide: ng_bootstrap_form_validation__WEBPACK_IMPORTED_MODULE_11__["CUSTOM_ERROR_MESSAGES"],
                    useValue: _utilities_customErrors__WEBPACK_IMPORTED_MODULE_12__["CUSTOM_ERRORS"],
                    multi: true,
                },
                {
                    provide: angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["AuthServiceConfig"],
                    useFactory: provideConfig,
                },
                _ThemeOptions_store_config_actions__WEBPACK_IMPORTED_MODULE_7__["ConfigActions"],
                _theme_options__WEBPACK_IMPORTED_MODULE_8__["ThemeOptions"],
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["NO_ERRORS_SCHEMA"]],
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_redux_store__WEBPACK_IMPORTED_MODULE_5__["NgRedux"], _angular_redux_store__WEBPACK_IMPORTED_MODULE_5__["DevToolsExtension"]])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/ct-module/carousel/carousel.component.html":
/*!************************************************************!*\
  !*** ./src/app/ct-module/carousel/carousel.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrap\">\r\n\t<div #carousel>\r\n\t\t<div [style.width]=\"3 * slides.length * slideWidth + 'px'\">\r\n\t\t\t<div class=\"carousel-model\">\r\n\t\t\t\t<ng-container\r\n\t\t\t\t\t[ngTemplateOutlet]=\"\r\n\t\t\t\t\t\tslides && slides.first ? slides.first.templateRef : null\r\n\t\t\t\t\t\"\r\n\t\t\t\t>\r\n\t\t\t\t</ng-container>\r\n\t\t\t</div>\r\n\t\t\t<ng-content></ng-content>\r\n\t\t</div>\r\n\t</div>\r\n\t<ng-content *ngIf=\"!noCarousel\" select=\".prev-button\"></ng-content>\r\n\t<ng-content *ngIf=\"!noCarousel\" select=\".next-button\"></ng-content>\r\n\t<ng-container *ngIf=\"showControls && !noCarousel\">\r\n\t\t<div class=\"carousel_prev-button\">\r\n\t\t\t<a class=\"carousel-control-prev-icon\" (click)=\"prev()\"></a>\r\n\t\t</div>\r\n\t\t<div class=\"carousel_next-button\">\r\n\t\t\t<a class=\"carousel-control-next-icon\" (click)=\"next()\"></a>\r\n\t\t</div>\r\n\t</ng-container>\r\n</div>\r\n<ng-container *ngIf=\"showSelectors && !noCarousel\">\r\n\t<div class=\"selectors\">\r\n\t\t<ng-container *ngFor=\"let t of thumbails; let i = index\">\r\n\t\t\t<div\r\n\t\t\t\tclass=\"selector\"\r\n\t\t\t\t(click)=\"setSlide(i * increment)\"\r\n\t\t\t\t[style.background-color]=\"\r\n\t\t\t\t\tcurrentSlide >= t && currentSlide < t + increment\r\n\t\t\t\t\t\t? 'black'\r\n\t\t\t\t\t\t: 'transparent'\r\n\t\t\t\t\"\r\n\t\t\t></div>\r\n\t\t</ng-container>\r\n\t</div>\r\n</ng-container>\r\n<div style=\"width: 72000px; overflow-x: hidden;\"></div>\r\n"

/***/ }),

/***/ "./src/app/ct-module/carousel/carousel.component.scss":
/*!************************************************************!*\
  !*** ./src/app/ct-module/carousel/carousel.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".carousel-model {\n  position: absolute;\n  z-index: -1; }\n\n.wrap {\n  width: 100%;\n  overflow-x: hidden;\n  position: relative; }\n\n::ng-deep .carousel-control-next-icon {\n  background-image: url('right-arrow.svg') !important;\n  background-color: whitesmoke;\n  border-radius: 12px; }\n\n::ng-deep .carousel-control-prev-icon {\n  background-image: url('right-arrow.svg') !important;\n  transform: rotate(180deg);\n  background-color: whitesmoke;\n  border-radius: 12px; }\n\na.carousel-control-next-icon,\na.carousel-control-prev-icon {\n  display: inline-block;\n  width: 20px;\n  height: 20px; }\n\n.carousel_prev-button,\n.carousel_next-button {\n  position: absolute;\n  z-index: 2;\n  opacity: 0.5;\n  margin: auto;\n  background-color: transparent;\n  text-align: center;\n  cursor: pointer; }\n\n.carousel_next-button:hover,\n.carousel_prev-button:hover {\n  opacity: 0.8;\n  transition: opacity 0.25s ease; }\n\n.carousel_prev-button {\n  top: 50%;\n  bottom: 0;\n  left: 0;\n  margin-top: -10px; }\n\n.carousel_next-button {\n  top: 50%;\n  bottom: 0;\n  right: 0;\n  margin-top: -10px; }\n\n.selectors {\n  text-align: center; }\n\n.selector {\n  width: 0.5rem;\n  height: 0.5rem;\n  border: 1px solid #0058b0;\n  border-radius: 50%;\n  margin: 0.5rem 0.25rem;\n  display: inline-block;\n  cursor: pointer; }\n\n.zoomed {\n  height: 97%;\n  width: 97%;\n  transition: all 0.5s ease; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3QtbW9kdWxlL2Nhcm91c2VsL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxcY3QtbW9kdWxlXFxjYXJvdXNlbFxcY2Fyb3VzZWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxrQkFBa0I7RUFDbEIsV0FBVyxFQUFBOztBQUVaO0VBQ0MsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0IsRUFBQTs7QUFHbkI7RUFFQyxtREFBNkU7RUFFN0UsNEJBQTRCO0VBQ3pCLG1CQUFtQixFQUFBOztBQUd2QjtFQUVDLG1EQUE2RTtFQUU3RSx5QkFBeUI7RUFDekIsNEJBQTRCO0VBQ3pCLG1CQUFtQixFQUFBOztBQUd2Qjs7RUFFQyxxQkFBcUI7RUFDckIsV0FBVztFQUNYLFlBQVksRUFBQTs7QUFHYjs7RUFFQyxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFlBQVk7RUFDWixZQUFZO0VBQ1osNkJBQTZCO0VBQzdCLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBR2hCOztFQUVDLFlBQVk7RUFDWiw4QkFBOEIsRUFBQTs7QUFHL0I7RUFDQyxRQUFRO0VBQ1IsU0FBUztFQUNULE9BQU87RUFDUCxpQkFBaUIsRUFBQTs7QUFHbEI7RUFDQyxRQUFRO0VBQ1IsU0FBUztFQUNULFFBQVE7RUFDUixpQkFBaUIsRUFBQTs7QUFHbEI7RUFDQyxrQkFBa0IsRUFBQTs7QUFHbkI7RUFDQyxhQUFhO0VBQ2IsY0FBYztFQUNkLHlCQUF5QjtFQUN6QixrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixlQUFlLEVBQUE7O0FBR2hCO0VBQ0MsV0FBVztFQUNYLFVBQVU7RUFJVix5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2N0LW1vZHVsZS9jYXJvdXNlbC9jYXJvdXNlbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJvdXNlbC1tb2RlbCB7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdHotaW5kZXg6IC0xO1xyXG59XHJcbi53cmFwIHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRvdmVyZmxvdy14OiBoaWRkZW47XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLmNhcm91c2VsLWNvbnRyb2wtbmV4dC1pY29uIHtcclxuXHQvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNjc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZycgZmlsbD0nJTIzZmZmJyB2aWV3Qm94PScwIDAgMzAgMzAnJTNlJTNjcGF0aCBkPSdNNy4zMyAyNGwtMi44My0yLjgyOSA5LjMzOS05LjE3NS05LjMzOS05LjE2NyAyLjgzLTIuODI5IDEyLjE3IDExLjk5NnonLyUzZSUzYy9zdmclM2VcIik7XHJcblx0YmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1hZ2VzLXVpL3JpZ2h0LWFycm93LnN2ZycpICFpbXBvcnRhbnQ7XHJcblx0Ly8gZmlsbDogZ3JlZW47XHJcblx0YmFja2dyb3VuZC1jb2xvcjogd2hpdGVzbW9rZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAuY2Fyb3VzZWwtY29udHJvbC1wcmV2LWljb24ge1xyXG5cdC8vIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM2NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyBmaWxsPSclMjNmZmYnIHZpZXdCb3g9JzAgMCAzMCAzMCclM2UlM2NwYXRoIGQ9J00xNi42NyAwbDIuODMgMi44MjktOS4zMzkgOS4xNzUgOS4zMzkgOS4xNjctMi44MyAyLjgyOS0xMi4xNy0xMS45OTZ6Jy8lM2UlM2Mvc3ZnJTNlXCIpO1xyXG5cdGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy11aS9yaWdodC1hcnJvdy5zdmcnKSAhaW1wb3J0YW50O1xyXG5cdC8vIGZpbGw6IGdyZWVuOyAgICBcclxuXHR0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpOyAgICBcclxuXHRiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZXNtb2tlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxufVxyXG5cclxuYS5jYXJvdXNlbC1jb250cm9sLW5leHQtaWNvbixcclxuYS5jYXJvdXNlbC1jb250cm9sLXByZXYtaWNvbiB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHdpZHRoOiAyMHB4O1xyXG5cdGhlaWdodDogMjBweDtcclxufVxyXG5cclxuLmNhcm91c2VsX3ByZXYtYnV0dG9uLFxyXG4uY2Fyb3VzZWxfbmV4dC1idXR0b24ge1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHR6LWluZGV4OiAyO1xyXG5cdG9wYWNpdHk6IDAuNTtcclxuXHRtYXJnaW46IGF1dG87XHJcblx0YmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmNhcm91c2VsX25leHQtYnV0dG9uOmhvdmVyLFxyXG4uY2Fyb3VzZWxfcHJldi1idXR0b246aG92ZXIge1xyXG5cdG9wYWNpdHk6IDAuODtcclxuXHR0cmFuc2l0aW9uOiBvcGFjaXR5IDAuMjVzIGVhc2U7XHJcbn1cclxuXHJcbi5jYXJvdXNlbF9wcmV2LWJ1dHRvbiB7XHJcblx0dG9wOiA1MCU7XHJcblx0Ym90dG9tOiAwO1xyXG5cdGxlZnQ6IDA7XHJcblx0bWFyZ2luLXRvcDogLTEwcHg7XHJcbn1cclxuXHJcbi5jYXJvdXNlbF9uZXh0LWJ1dHRvbiB7XHJcblx0dG9wOiA1MCU7XHJcblx0Ym90dG9tOiAwO1xyXG5cdHJpZ2h0OiAwO1xyXG5cdG1hcmdpbi10b3A6IC0xMHB4O1xyXG59XHJcblxyXG4uc2VsZWN0b3JzIHtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zZWxlY3RvciB7XHJcblx0d2lkdGg6IDAuNXJlbTtcclxuXHRoZWlnaHQ6IDAuNXJlbTtcclxuXHRib3JkZXI6IDFweCBzb2xpZCAjMDA1OGIwO1xyXG5cdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHRtYXJnaW46IDAuNXJlbSAwLjI1cmVtO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi56b29tZWQge1xyXG5cdGhlaWdodDogOTclO1xyXG5cdHdpZHRoOiA5NyU7XHJcblx0LXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xyXG5cdC1tb3otdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZTtcclxuXHQtby10cmFuc2l0aW9uOiBhbGwgMC41cyBlYXNlO1xyXG5cdHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2U7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/ct-module/carousel/carousel.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/ct-module/carousel/carousel.component.ts ***!
  \**********************************************************/
/*! exports provided: CarouselSlideElement, CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselSlideElement", function() { return CarouselSlideElement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _carousel_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./carousel.directive */ "./src/app/ct-module/carousel/carousel.directive.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");






var CarouselSlideElement = /** @class */ (function () {
    function CarouselSlideElement() {
    }
    CarouselSlideElement = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Directive"])({
            selector: '.carousel-model'
        })
    ], CarouselSlideElement);
    return CarouselSlideElement;
}());

var CarouselComponent = /** @class */ (function () {
    /**
     *
     * @param builder animation builder for carousel
     * @param viewContainer conatiner for carousel
     */
    function CarouselComponent(builder, viewContainer) {
        this.builder = builder;
        this.viewContainer = viewContainer;
        this.currentSlide = 0;
        this.increment = 2;
        this.offset = 2;
        this.thumbails = [];
        this.noCarousel = false;
        this.onTranslation = false;
        // Input and Outputs for component
        this.showControls = true;
        this.showSelectors = true;
        this.timing = '500ms ease-in';
    }
    CarouselComponent.prototype.onResize = function (event) {
        if (this.slideWidth && this.slides && this.slides.first)
            this.resizeCarousel();
    };
    CarouselComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["timer"])(0, 200).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeWhile"])(function () { return !_this.slideWidth || !_this.slides || !_this.slides.first; })).subscribe(function () {
            // console.log(this.slideElement)
            // if(this.slideElement != undefined) {
            var square = _this.slideElement.nativeElement.getBoundingClientRect();
            _this.slideWidth = square.width;
            if (_this.slideWidth && _this.slides && _this.slides.first)
                _this.resizeCarousel();
            // }
        });
    };
    // previous
    CarouselComponent.prototype.prev = function () {
        if (!this.noCarousel)
            this.transitionCarousel(null, this.currentSlide - this.increment);
    };
    // next
    CarouselComponent.prototype.next = function () {
        if (!this.noCarousel)
            this.transitionCarousel(null, this.currentSlide + this.increment);
    };
    // to set slider
    CarouselComponent.prototype.setSlide = function (slide, force) {
        if (force === void 0) { force = false; }
        if (this.noCarousel)
            return;
        if (!force)
            slide = slide - slide % this.increment;
        this.transitionCarousel(null, slide);
    };
    // resize carousel on screen change
    CarouselComponent.prototype.resizeCarousel = function () {
        var _this = this;
        if (this.carousel) {
            var totalWidth = this.carousel.nativeElement.getBoundingClientRect().width;
            if (totalWidth > this.slides.length * this.slideWidth) {
                this.thumbails = [];
                this.thumbails[0] = 0;
                this.offset = (totalWidth - this.slides.length * this.slideWidth) / 2;
                this.transitionCarousel(0, this.currentSlide);
                this.noCarousel = true;
                return;
            }
            this.noCarousel = false;
            this.increment = Math.floor(totalWidth / this.slideWidth);
            if (this.increment == 0)
                this.increment = 1;
            this.thumbails = [];
            for (var i = 0; i < this.slides.length / this.increment; i++)
                this.thumbails[i] = i * this.increment;
            var count_1 = (this.increment * this.slideWidth) < totalWidth ? 1 : 0;
            this.offset = (totalWidth - 3 * (this.increment) * this.slideWidth) / 2 - this.slideWidth * count_1;
            this.slides.first.viewContainer.clear();
            this.slides.last.viewContainer.clear();
            this.slides.last.viewContainer.createEmbeddedView(this.slides.last.templateRef);
            var addedleft_1 = 0;
            var addedrigth_1 = 0;
            this.slides.forEach(function (x, index) {
                if (index && index >= (_this.slides.length - _this.increment - count_1)) {
                    _this.slides.first.viewContainer.createEmbeddedView(x.templateRef);
                    addedleft_1++;
                }
                if (index < _this.increment + count_1) {
                    _this.slides.last.viewContainer.createEmbeddedView(x.templateRef);
                    addedrigth_1++;
                }
            });
            if (this.increment + 1 >= this.slides.length)
                this.slides.first.viewContainer.createEmbeddedView(this.slides.first.templateRef, null, 0);
            this.slides.first.viewContainer.createEmbeddedView(this.slides.first.templateRef);
            this.currentSlide = 0;
            this.transitionCarousel(0, this.currentSlide);
        }
    };
    // transition of carousel
    CarouselComponent.prototype.transitionCarousel = function (time, slide) {
        var myAnimation = this.buildAnimation(time, slide);
        this.player = myAnimation.create(this.carousel.nativeElement);
        this.currentSlide = (slide >= this.slides.length) ? slide - this.slides.length :
            (slide < 0) ? this.currentSlide = slide + this.slides.length :
                slide;
        this.player.play();
    };
    // animation for carousel
    CarouselComponent.prototype.buildAnimation = function (time, slide) {
        var animation = (slide >= this.slides.length) ? 1 : (slide < 0) ? 2 : 0;
        var offsetInitial = (slide >= this.slides.length) ?
            this.offset - this.slideWidth * (this.currentSlide - this.slides.length) :
            0;
        var offsetFinal = (slide < 0) ?
            this.offset - this.slideWidth * (slide + this.slides.length) :
            0;
        var offset = (slide >= this.slides.length) ?
            this.offset - this.slideWidth * (slide - this.slides.length) :
            this.offset - this.slideWidth * slide;
        return animation == 1 ? this.builder.build([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: "translateX(" + offsetInitial + "px)" }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])(time == null ? this.timing : 0, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: "translateX(" + offset + "px)" }))
        ]) : animation == 2 ? this.builder.build(Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["sequence"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])(time == null ? this.timing : 0, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: "translateX(" + offset + "px)" })),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: "translateX(" + offsetFinal + "px" })
        ]))
            : this.builder.build([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])(time == null ? this.timing : 0, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: "translateX(" + offset + "px)" }))
            ]);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CarouselComponent.prototype, "showControls", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CarouselComponent.prototype, "showSelectors", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CarouselComponent.prototype, "timing", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ContentChildren"])(_carousel_directive__WEBPACK_IMPORTED_MODULE_1__["CarouselDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["QueryList"])
    ], CarouselComponent.prototype, "slides", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])('carousel'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], CarouselComponent.prototype, "carousel", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])(CarouselSlideElement, { read: _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"] }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ElementRef"])
    ], CarouselComponent.prototype, "slideElement", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["HostListener"])('window:resize', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], CarouselComponent.prototype, "onResize", null);
    CarouselComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
            selector: 'app-carousel',
            template: __webpack_require__(/*! ./carousel.component.html */ "./src/app/ct-module/carousel/carousel.component.html"),
            styles: [__webpack_require__(/*! ./carousel.component.scss */ "./src/app/ct-module/carousel/carousel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_animations__WEBPACK_IMPORTED_MODULE_3__["AnimationBuilder"], _angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewContainerRef"]])
    ], CarouselComponent);
    return CarouselComponent;
}());



/***/ }),

/***/ "./src/app/ct-module/carousel/carousel.directive.ts":
/*!**********************************************************!*\
  !*** ./src/app/ct-module/carousel/carousel.directive.ts ***!
  \**********************************************************/
/*! exports provided: CarouselDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselDirective", function() { return CarouselDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CarouselDirective = /** @class */ (function () {
    function CarouselDirective(templateRef, viewContainer) {
        this.templateRef = templateRef;
        this.viewContainer = viewContainer;
    }
    CarouselDirective.prototype.ngOnInit = function () {
        this.viewContainer.createEmbeddedView(this.templateRef);
    };
    CarouselDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[carouselSlide]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"]])
    ], CarouselDirective);
    return CarouselDirective;
}());



/***/ }),

/***/ "./src/app/layout/landing-footer/landing-footer.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/layout/landing-footer/landing-footer.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer style=\"background-color: #e8e8e8; padding-top: 15px;\">\r\n  <div class=\"col-sm-12\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-7 seven-three\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-4\">\r\n              <h6 class=\"footer-heading\">COMPANY</h6>\r\n              <ul class=\"ul-footer\">\r\n                <li class=\"footer-links\">About Us</li>\r\n                <li class=\"footer-links\">Meet the Team</li>\r\n                <li class=\"footer-links\">Case studies</li>\r\n                <li class=\"footer-links\">Press and Media Kit</li>\r\n                <li class=\"footer-links\">Jobs</li>\r\n                <li class=\"footer-links\">FAQs</li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <h6 class=\"footer-heading\">JOIN</h6>\r\n              <ul class=\"ul-footer\">\r\n                <li class=\"footer-links\">Investors</li>\r\n                <li class=\"footer-links\">Entrepreneurs</li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"col-sm-4\">\r\n              <h6 class=\"footer-heading\">LEARN</h6>\r\n              <ul class=\"ul-footer\">\r\n                <li class=\"footer-links\">Equity Crowdfunding</li>\r\n                <li class=\"footer-links\">Academy</li>\r\n                <li class=\"footer-links\">Resources</li>\r\n              </ul>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-5 five-two\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <h6 class=\"footer-heading\">LEGAL</h6>\r\n              <ul class=\"ul-footer\">\r\n                <li class=\"footer-links\">Terms of Use</li>\r\n                <li class=\"footer-links\">Privacy Policy</li>\r\n                <li class=\"footer-links\">Cookie Notice</li>\r\n                <li class=\"footer-links\">Legal Documents</li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <h6 class=\"footer-heading\">HOW IT WORKS</h6>\r\n              <ul class=\"ul-footer\">\r\n                <li class=\"footer-links\">Invest</li>\r\n                <li class=\"footer-links\">Raised</li>\r\n                <li class=\"footer-links\">Regulate</li>\r\n              </ul>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br /><br /><br />\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6\">\r\n        <h2>About Invent Fund</h2>\r\n        <p>\r\n          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do\r\n          eiusmod tempor incididunt ut labore et dolore magna aliqua\r\n        </p>\r\n      </div>\r\n      <div class=\"col-md-6\">\r\n        <div class=\"contacts\">\r\n          <p class=\"mb-0\">Get in Touch With Us:&nbsp; help@inventfunds.com</p>\r\n          <p class=\"mb-0\">\r\n            No.135, 7th Main Road, 4th Block, Jayanagar, Bangalore-560011\r\n          </p>\r\n          <p>You can also find us on:</p>\r\n\r\n          <div>\r\n            <i style=\"color: #0e76a8;\" class=\"mr-2 fab fa-linkedin-in\"></i>\r\n            <i style=\"color: #38a1f3;\" class=\"mx-2 fab fa-twitter\"></i>\r\n            <i style=\"color: #3b5998;\" class=\"mx-2 fab fa-facebook-f\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <br /><br />\r\n</footer>\r\n"

/***/ }),

/***/ "./src/app/layout/landing-footer/landing-footer.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/landing-footer/landing-footer.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@media (min-width: 768px) {\n  div.col-sm-7.seven-three {\n    width: 60% !important; }\n  div.col-sm-5.five-two {\n    width: 40% !important; } }\n\n.footer-heading {\n  font-size: medium;\n  font-weight: 500;\n  color: #0058B0;\n  margin-left: 25px; }\n\n.about-invent {\n  margin-left: 35px;\n  color: #0058B0;\n  font-weight: 500; }\n\n.about-paragraph {\n  margin-left: auto;\n  margin-right: auto;\n  padding-left: 35px;\n  padding-right: 50px; }\n\n.contacts {\n  padding-left: 125px !important; }\n\n@media (max-width: 425px) {\n  .contacts {\n    padding-left: 0px !important; } }\n\n.footer-links {\n  font-weight: 500; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2xhbmRpbmctZm9vdGVyL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxcbGF5b3V0XFxsYW5kaW5nLWZvb3RlclxcbGFuZGluZy1mb290ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSTtJQUF5QixxQkFBbUIsRUFBQTtFQUM1QztJQUFzQixxQkFBbUIsRUFBQSxFQUFHOztBQUdoRDtFQUNFLGlCQUFpQjtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsaUJBQWlCLEVBQUE7O0FBRXJCO0VBQ0UsaUJBQWlCO0VBQ2YsY0FBZ0I7RUFDaEIsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0UsOEJBQThCLEVBQUE7O0FBRWhDO0VBQ0U7SUFDRSw0QkFBNEIsRUFBQSxFQUM3Qjs7QUFHSDtFQUNJLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2xhbmRpbmctZm9vdGVyL2xhbmRpbmctZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQG1lZGlhKG1pbi13aWR0aDogNzY4cHgpe1xyXG4gICAgZGl2LmNvbC1zbS03LnNldmVuLXRocmVle3dpZHRoOjYwJSFpbXBvcnRhbnQ7fVxyXG4gICAgZGl2LmNvbC1zbS01LmZpdmUtdHdve3dpZHRoOjQwJSFpbXBvcnRhbnQ7fVxyXG59XHJcblxyXG4uZm9vdGVyLWhlYWRpbmd7XHJcbiAgZm9udC1zaXplOiBtZWRpdW07XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMwMDU4QjA7XHJcbiAgICBtYXJnaW4tbGVmdDogMjVweDtcclxufVxyXG4uYWJvdXQtaW52ZW50e1xyXG4gIG1hcmdpbi1sZWZ0OiAzNXB4O1xyXG4gICAgY29sb3I6ICAgIzAwNThCMDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuLmFib3V0LXBhcmFncmFwaHtcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgcGFkZGluZy1sZWZ0OiAzNXB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDUwcHg7XHJcbn1cclxuLmNvbnRhY3Rze1xyXG4gIHBhZGRpbmctbGVmdDogMTI1cHggIWltcG9ydGFudDtcclxufVxyXG5AbWVkaWEobWF4LXdpZHRoOjQyNXB4KXtcclxuICAuY29udGFjdHN7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweCAhaW1wb3J0YW50O1xyXG4gIH1cclxufVxyXG5cclxuLmZvb3Rlci1saW5rc3tcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/layout/landing-footer/landing-footer.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/landing-footer/landing-footer.component.ts ***!
  \*******************************************************************/
/*! exports provided: LandingFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingFooterComponent", function() { return LandingFooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LandingFooterComponent = /** @class */ (function () {
    function LandingFooterComponent() {
    }
    LandingFooterComponent.prototype.ngOnInit = function () {
    };
    LandingFooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing-footer',
            template: __webpack_require__(/*! ./landing-footer.component.html */ "./src/app/layout/landing-footer/landing-footer.component.html"),
            styles: [__webpack_require__(/*! ./landing-footer.component.scss */ "./src/app/layout/landing-footer/landing-footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LandingFooterComponent);
    return LandingFooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/landing-header/landing-header.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/layout/landing-header/landing-header.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav\r\n  class=\"navbar navbar-expand-md navbar-dark bg-fade fixed-top\"\r\n  style=\"background-color: #0058b0;\"\r\n>\r\n  <a class=\"navbar-brand pointer-ref\" (click)=\"routerNavigation('landing')\">\r\n    <img\r\n      width=\"100\"\r\n      height=\"45\"\r\n      src=\"../../../assets/images/logo-transparent.svg\"\r\n      alt=\"logo\"\r\n    />\r\n  </a>\r\n  <button\r\n    type=\"button\"\r\n    class=\"hamburger-button hamburger close-sidebar-btn hamburger--elastic\"\r\n    [ngClass]=\"{ 'is-active': globals.toggleSidebarMobile }\"\r\n    (click)=\"toggleSidebarMobile()\"\r\n  >\r\n    <span class=\"hamburger-box\">\r\n      <span class=\"hamburger-inner\"></span>\r\n    </span>\r\n  </button>\r\n  <button\r\n    class=\"navbar-toggler\"\r\n    type=\"button\"\r\n    data-toggle=\"collapse\"\r\n    data-target=\"#navbarsExampleDefault\"\r\n    aria-controls=\"navbarsExampleDefault\"\r\n    aria-expanded=\"false\"\r\n    aria-label=\"Toggle navigation\"\r\n  >\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarsExampleDefault\">\r\n    <form class=\"form-inline ml-0 my-2 my-lg-0 margin-left-50\">\r\n      <div class=\"search-bar-padding input-group\">\r\n        <input\r\n          class=\"form-control mr-sm-2\"\r\n          type=\"text\"\r\n          class=\"form-control\"\r\n          placeholder=\"Search\"\r\n        />\r\n        <button class=\"btn\" type=\"submit\">\r\n          <div class=\"input-group-append\">\r\n            <i style=\"color: white;\" class=\"fa fa-search\"></i>\r\n          </div>\r\n        </button>\r\n      </div>\r\n    </form>\r\n    <ul class=\"navbar-nav ml-auto\">\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link font-wt-600\" style=\"color: white; margin-top: 10px;\"\r\n          >How it works</a\r\n        >\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link font-wt-600\" style=\"color: white; margin-top: 10px;\"\r\n          >Contact</a\r\n        >\r\n      </li>\r\n      <li class=\"nav-item\" *ngIf=\"hideSign\">\r\n        <a\r\n          class=\"nav-link font-wt-600\"\r\n          style=\"color: white; margin-top: 10px;\"\r\n          href=\"/login\"\r\n          >Login</a\r\n        >\r\n      </li>\r\n      <li class=\"nav-item\" *ngIf=\"hideSign\">\r\n        <a\r\n          class=\"nav-link font-wt-600\"\r\n          style=\"color: white; margin-top: 10px;\"\r\n          href=\"/sign-up\"\r\n          >Sign Up</a\r\n        >\r\n      </li>\r\n      <li class=\"nav-item dropdown\">\r\n        <a\r\n          class=\"nav-link dropdown-toggle\"\r\n          href=\"#\"\r\n          id=\"navbarDropdownMenuLink\"\r\n          role=\"button\"\r\n          data-toggle=\"dropdown\"\r\n          aria-haspopup=\"true\"\r\n          aria-expanded=\"false\"\r\n        >\r\n          <img\r\n            src=\"{{ avatar$ | async }}\"\r\n            width=\"40\"\r\n            height=\"40\"\r\n            class=\"rounded-circle\"\r\n          />\r\n        </a>\r\n        <div\r\n          class=\"dropdown-menu position-right\"\r\n          aria-labelledby=\"navbarDropdownMenuLink\"\r\n        >\r\n          <h2 class=\"dropdown-item bold-18px\">Account</h2>\r\n          <a\r\n            class=\"dropdown-item padding-top-0px\"\r\n            (click)=\"routerNavigation('profileSettings')\"\r\n            >Settings And Privacy</a\r\n          >\r\n          <!-- <a class=\"dropdown-item padding-top-0px\">Help</a>\r\n          <a class=\"dropdown-item padding-top-0px\">Language</a>\r\n          \r\n          <h2 class=\"dropdown-item bold-18px\">Manage</h2>\r\n          <a class=\"dropdown-item padding-top-0px\">Profile</a>\r\n          <a class=\"dropdown-item padding-top-0px\">Funder Account</a> -->\r\n          <h2\r\n            class=\"dropdown-item bold-18px\"\r\n            (click)=\"routerNavigation('profile')\"\r\n          >\r\n            Profile\r\n          </h2>\r\n          <!-- <a class=\"dropdown-item padding-top-0px\">Profile</a> -->\r\n          <h2\r\n            class=\"dropdown-item bold-18px\"\r\n            *ngIf=\"(currentRouter | async) != 'dashboard'\"\r\n            (click)=\"routerNavigation('dashboard')\"\r\n          >\r\n            Dashboard\r\n          </h2>\r\n          <h2\r\n            class=\"dropdown-item bold-18px\"\r\n            (click)=\"routerNavigation('logOut')\"\r\n          >\r\n            Logout\r\n          </h2>\r\n        </div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/layout/landing-header/landing-header.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/landing-header/landing-header.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".font-wt-600 {\n  font-weight: 600; }\n\n.navbar-nav > li {\n  margin-left: 20px;\n  margin-right: 20px; }\n\n.bg-custom-1 {\n  background-color: #85144b; }\n\n.bg-custom-2 {\n  background-image: linear-gradient(15deg, #13547a 0%, #80d0c7 100%); }\n\n.position-right {\n  right: 10px !important; }\n\n.bold-18px {\n  font-weight: 500;\n  font-size: 16px;\n  margin: 0px; }\n\n.padding-top-0px {\n  padding-top: 0px; }\n\n.dropdown-menu {\n  left: -150px !important; }\n\n.hamburger-button {\n  margin-left: 30px; }\n\n.search-bar-padding {\n  margin-left: 30px; }\n\n@media only screen and (min-width: 320px) and (max-width: 768px) {\n  .dropdown-menu {\n    left: 0 !important;\n    position: fixed !important;\n    /* z-index: 50; */\n    left: 68% !important;\n    right: 0px;\n    float: right;\n    top: 21% !important;\n    width: 30% !important;\n    transform: translateY(-50%) !important;\n    min-width: 10px !important; } }\n\n@media only screen and (max-width: 768px) {\n  .hamburger-button {\n    margin-left: -8px; }\n  .search-bar-padding {\n    margin-left: 6px; }\n  .navbar-nav > li {\n    margin-left: 0px;\n    margin-right: 20px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2xhbmRpbmctaGVhZGVyL0Q6XFxXb3JrXFxQcm9qZWN0c1xcSU5WRU5URlVORFxcQlJBTkNIRVNcXGludmVudGZ1bmQtdGVzdC9zcmNcXGFwcFxcbGF5b3V0XFxsYW5kaW5nLWhlYWRlclxcbGFuZGluZy1oZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBZ0IsRUFBQTs7QUFHcEI7RUFDSSxpQkFBZ0I7RUFDaEIsa0JBQWlCLEVBQUE7O0FBR25CO0VBQ0UseUJBQXlCLEVBQUE7O0FBRzNCO0VBQ0Esa0VBQWtFLEVBQUE7O0FBR2xFO0VBQ0Usc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixXQUFXLEVBQUE7O0FBR2I7RUFDRSxnQkFBZ0IsRUFBQTs7QUFHbEI7RUFDRSx1QkFBdUIsRUFBQTs7QUFFekI7RUFDRSxpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxpQkFBaUIsRUFBQTs7QUFFbkI7RUFDRTtJQUNFLGtCQUFrQjtJQUNsQiwwQkFBMEI7SUFDMUIsaUJBQUE7SUFDQSxvQkFBb0I7SUFDcEIsVUFBVTtJQUNWLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIscUJBQXFCO0lBQ3JCLHNDQUFzQztJQUN0QywwQkFBMEIsRUFBQSxFQUMzQjs7QUFHSDtFQUNFO0lBQ0UsaUJBQWlCLEVBQUE7RUFHbkI7SUFDRSxnQkFBZ0IsRUFBQTtFQUdsQjtJQUNFLGdCQUFlO0lBQ2Ysa0JBQWlCLEVBQUEsRUFDbEIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvbGFuZGluZy1oZWFkZXIvbGFuZGluZy1oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9udC13dC02MDB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4ubmF2YmFyLW5hdiA+IGxpe1xyXG4gICAgbWFyZ2luLWxlZnQ6MjBweDtcclxuICAgIG1hcmdpbi1yaWdodDoyMHB4O1xyXG4gIH1cclxuXHJcbiAgLmJnLWN1c3RvbS0xIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM4NTE0NGI7XHJcbiAgfVxyXG4gIFxyXG4gIC5iZy1jdXN0b20tMiB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDE1ZGVnLCAjMTM1NDdhIDAlLCAjODBkMGM3IDEwMCUpO1xyXG4gIH1cclxuXHJcbiAgLnBvc2l0aW9uLXJpZ2h0IHtcclxuICAgIHJpZ2h0OiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICAuYm9sZC0xOHB4IHtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBtYXJnaW46IDBweDtcclxuICB9XHJcblxyXG4gIC5wYWRkaW5nLXRvcC0wcHgge1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICB9XHJcblxyXG4gIC5kcm9wZG93bi1tZW51IHtcclxuICAgIGxlZnQ6IC0xNTBweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAuaGFtYnVyZ2VyLWJ1dHRvbntcclxuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG4gIH1cclxuXHJcbiAgLnNlYXJjaC1iYXItcGFkZGluZ3tcclxuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG4gIH1cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kKG1pbi13aWR0aDozMjBweCkgYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgICAuZHJvcGRvd24tbWVudSB7XHJcbiAgICAgIGxlZnQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgcG9zaXRpb246IGZpeGVkICFpbXBvcnRhbnQ7XHJcbiAgICAgIC8qIHotaW5kZXg6IDUwOyAqL1xyXG4gICAgICBsZWZ0OiA2OCUgIWltcG9ydGFudDtcclxuICAgICAgcmlnaHQ6IDBweDtcclxuICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICB0b3A6IDIxJSAhaW1wb3J0YW50O1xyXG4gICAgICB3aWR0aDogMzAlICFpbXBvcnRhbnQ7XHJcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKSAhaW1wb3J0YW50O1xyXG4gICAgICBtaW4td2lkdGg6IDEwcHggIWltcG9ydGFudDtcclxuICAgIH1cclxuICB9IFxyXG5cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kKG1heC13aWR0aDo3NjhweCl7XHJcbiAgICAuaGFtYnVyZ2VyLWJ1dHRvbntcclxuICAgICAgbWFyZ2luLWxlZnQ6IC04cHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuc2VhcmNoLWJhci1wYWRkaW5ne1xyXG4gICAgICBtYXJnaW4tbGVmdDogNnB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItbmF2ID4gbGl7XHJcbiAgICAgIG1hcmdpbi1sZWZ0OjBweDtcclxuICAgICAgbWFyZ2luLXJpZ2h0OjIwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgfVxyXG5cclxuIFxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/layout/landing-header/landing-header.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/landing-header/landing-header.component.ts ***!
  \*******************************************************************/
/*! exports provided: LandingHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LandingHeaderComponent", function() { return LandingHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_app_state_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/app-state.service */ "./src/app/services/app-state.service.ts");
/* harmony import */ var src_app_theme_options__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/theme-options */ "./src/app/theme-options.ts");





// import * as profileImage from 'src/assets/images/avatars'
var LandingHeaderComponent = /** @class */ (function () {
    function LandingHeaderComponent(router, appState, globals) {
        this.router = router;
        this.appState = appState;
        this.globals = globals;
        this.imageProfile = '../../../assets/images/avatars/profile.jpg';
        this.hideSign = true;
        this.currentRouter = this.router.url;
        // console.log('current router : ' + this.appState.currentRouter);
        // console.log('get boolean value : ' + JSON.stringify(this.appState.getsessionStorageData()));
        // console.log(
        //   'get boolean value : ' + JSON.stringify(this.appState.getProfileImage('landing-header'))
        // );
        this.imageProfile = this.appState.getProfileImage();
        if (sessionStorage.getItem('profileImage')) {
            console.log('get profile image data : ' + JSON.stringify(sessionStorage.getItem('profileImage')));
            this.appState.setAvatar(sessionStorage.getItem('profileImage'));
        }
        else {
            this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
        }
        // this.appState.getProfileImage('landing-header');
        this.hideSign = this.appState.getsessionStorageData();
    }
    LandingHeaderComponent.prototype.ngOnInit = function () {
        // console.log('get boolean value : ' + JSON.stringify(this.appState.getAvatar()));
        // console.log('get currentRouter value : ' + JSON.stringify(this.appState.getRouter()));
        this.avatar$ = this.appState.getAvatar();
        this.currentRouter = this.appState.getRouter();
    };
    LandingHeaderComponent.prototype.routerNavigation = function (data) {
        if (data == 'dashboard') {
            this.router.navigate(['/profile/dashboard']);
        }
        else if (data == 'profileSettings') {
            this.router.navigate(['/profile/settings']);
        }
        else if (data == 'landing') {
            this.router.navigate(['landing/landing-page']);
        }
        else if (data == 'profile') {
            this.router.navigate(['profile/editProfile']);
        }
        else {
            this.router.navigate(['landing/landing-page']);
            sessionStorage.clear();
            // window.location.reload();
        }
    };
    LandingHeaderComponent.prototype.toggleSidebarMobile = function () {
        this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
    };
    LandingHeaderComponent.prototype.toggleHeaderMobile = function () {
        this.globals.toggleHeaderMobile = !this.globals.toggleHeaderMobile;
    };
    LandingHeaderComponent.prototype.toggleSidebar = function () {
        this.globals.toggleSidebar = !this.globals.toggleSidebar;
    };
    LandingHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-landing-header',
            template: __webpack_require__(/*! ./landing-header.component.html */ "./src/app/layout/landing-header/landing-header.component.html"),
            styles: [__webpack_require__(/*! ./landing-header.component.scss */ "./src/app/layout/landing-header/landing-header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_app_state_service__WEBPACK_IMPORTED_MODULE_3__["AppStateService"],
            src_app_theme_options__WEBPACK_IMPORTED_MODULE_4__["ThemeOptions"]])
    ], LandingHeaderComponent);
    return LandingHeaderComponent;
}());



/***/ }),

/***/ "./src/app/services/app-state.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/app-state.service.ts ***!
  \***********************************************/
/*! exports provided: AppStateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppStateService", function() { return AppStateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _services_roleaccessjson__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/roleaccessjson */ "./src/app/services/roleaccessjson.ts");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
/* harmony import */ var _invent_funds_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./invent-funds-api-service */ "./src/app/services/invent-funds-api-service.ts");






var AppStateService = /** @class */ (function () {
    /**
     * constructor
     */
    function AppStateService(inventFundsApiService) {
        this.inventFundsApiService = inventFundsApiService;
        this.showSideBar = true;
        this.profileImage = '';
        this.settingPageClass = true;
        this.globalData = {
            roleId: '',
            roleCode: '',
            partyId: '',
            personId: '',
            personEmail: '',
            personType: '',
            orgId: '',
            partyCode: '',
            token: '',
        };
        this.encryptSecretKey = 'KA0f2100e34b54d5VA0fd013df8f300d3497H579O';
        this.encryptOptions = { mode: crypto_js__WEBPACK_IMPORTED_MODULE_2__["mode"].CBC, padding: crypto_js__WEBPACK_IMPORTED_MODULE_2__["pad"].Pkcs7 };
        this.roleAccess = _services_roleaccessjson__WEBPACK_IMPORTED_MODULE_3__["roleAccessJson"];
        this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
        this.avatar$ = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        this.currentRouter = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        this.profileSummary$ = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        this.educationDetails$ = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        this.professionalDetails$ = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        this.achievementsDetails$ = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        this.skillsDetails$ = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](null);
        // console.log('app state');
        if (sessionStorage.getItem('profile') != null) {
            this.encryptProfileJson();
        }
    }
    // encrypr data using the crypto
    AppStateService.prototype.encryptProfileJson = function () {
        // console.log('profile data : ' + JSON.stringify(sessionStorage.getItem('profile')));
        var decrypted = crypto_js__WEBPACK_IMPORTED_MODULE_2__["AES"].decrypt(sessionStorage.getItem('profile'), this.encryptSecretKey, this.encryptOptions);
        if (decrypted.toString()) {
            var profileJson = JSON.parse(decrypted.toString(crypto_js__WEBPACK_IMPORTED_MODULE_2__["enc"].Utf8));
            this.encryptedData = profileJson;
            // console.log('profile data : ' + JSON.stringify(this.encryptedData));
        }
    };
    // to get encrypted data values dippends on key
    AppStateService.prototype.encryptedDataValue = function (value) {
        if (this.encryptedData) {
            return this.encryptedData[value];
        }
    };
    // to check weather session storage consists any data or not
    AppStateService.prototype.getsessionStorageData = function () {
        if (sessionStorage.getItem('profile')) {
            return false;
        }
        else {
            return true;
        }
    };
    // get profile image async
    // getProfileImage(from) {
    //   const params = {
    //     personId: this.encryptedDataValue('personId'),
    //     type: 'PROFILE',
    //   };
    //   return this.inventFundsApiService
    //     .getImageDataWithId(params)
    //     .then((result) => {
    //       // console.log(
    //       //   'result data of signed with id : ' + JSON.stringify(result) + ' ' + JSON.stringify(from)
    //       // );
    //       if (result.code === 200) {
    //         if (result.data !== null) {
    //           this.profileImageAfterSet = result.data;
    //           this.setAvatar(result.data);
    //         } else {
    //           this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
    //           this.setAvatar(this.profileImageAfterSet);
    //         }
    //         sessionStorage.setItem('profileImage', 'profile');
    //         return this.profileImage;
    //       } else {
    //         this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
    //         return this.profileImage;
    //       }
    //     })
    //     .catch((err) => {
    //       console.log('error : ' + JSON.stringify(err));
    //       this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
    //       return this.profileImage;
    //     });
    // }
    AppStateService.prototype.getProfileImage = function () {
        if (sessionStorage.getItem('profileImage')) {
            this.profileImageAfterSet = sessionStorage.getItem('profileImage');
            return this.profileImage;
        }
        else {
            this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
            return this.profileImage;
        }
    };
    // get an profile avatar after uploading in profile settings
    AppStateService.prototype.getAvatar = function () {
        return this.avatar$.asObservable();
    };
    // to set the avatar after changing the profile image
    AppStateService.prototype.setAvatar = function (value) {
        this.setProfileImage(value);
        this.avatar$.next(value);
    };
    // to get the current route async
    AppStateService.prototype.getRouter = function () {
        return this.currentRouter.asObservable();
    };
    // setting current route async
    AppStateService.prototype.setRouter = function (data) {
        this.currentRouter.next(data);
    };
    // set profile image on chage async
    AppStateService.prototype.setProfileImage = function (data) {
        // console.log('data profile image : ' + JSON.stringify(data));
        this.profileImageAfterSet = data;
    };
    // set profile side bar after filling data true or fase
    /**
     *
     * @param profileSummary to set profile summary variable to show done or cancel images
     * @param educationDetails to set educationDetails variable to show done or cancel images
     * @param professionalDetails to set professionalDetails variable to show done or cancel images
     * @param achievementsDetails to set achievementsDetails variable to show done or cancel images
     * @param skillsDetails to set skillsDetails variable to show done or cancel images
     */
    AppStateService.prototype.setProfileStatus = function (profileSummary, educationDetails, professionalDetails, achievementsDetails, skillsDetails) {
        this.profileSummary$.next(profileSummary);
        this.educationDetails$.next(educationDetails);
        this.professionalDetails$.next(professionalDetails);
        this.achievementsDetails$.next(achievementsDetails);
        this.skillsDetails$.next(skillsDetails);
    };
    // getting profile side bar data variables in async
    AppStateService.prototype.getProfileStatus = function () {
        var obj = {
            profileSummary: this.profileSummary$.asObservable(),
            educationDetails: this.educationDetails$.asObservable(),
            professionalDetails: this.professionalDetails$.asObservable(),
            achievementsDetails: this.achievementsDetails$.asObservable(),
            skillsDetails: this.skillsDetails$.asObservable(),
        };
        return obj;
    };
    // setting profile data in an array of object
    AppStateService.prototype.setDataProfile = function (data1, data2, data3, data4, data5) {
        // console.log("data 1 : " + JSON.stringify(data1))
        // console.log("data 2 : " + JSON.stringify(data2.filter(item => item._id != "").length))
        // console.log("data 3 : " + JSON.stringify(data3))
        // console.log("data 4 : " + JSON.stringify(data4))
        // console.log("data 5 : " + JSON.stringify(data5))
        var data1Set = null;
        var data2Set = null;
        var data3Set = null;
        var data4Set = null;
        var data5Set = null;
        if (Object.keys(data1).length) {
            data1Set = data1.profileSummary == '' ? null : true;
        }
        if (data2.length) {
            data2Set = data2.filter(function (item) { return item._id != ''; }).length ? true : null;
        }
        if (data3.length) {
            data3Set = data3.filter(function (item) { return item._id != ''; }).length ? true : null;
        }
        if (data4.length) {
            data4Set = data4.filter(function (item) { return item._id != ''; }).length ? true : null;
        }
        if (data5.length) {
            data5Set = data5.length ? true : null;
        }
        this.setProfileStatus(data1Set, data2Set, data3Set, data4Set, data5Set);
    };
    AppStateService.prototype.getSignedUrl = function (path, key) {
        var _this = this;
        var param = {
            path: path,
            key: key,
        };
        // console.log('response data : ' + JSON.stringify(param));
        this.inventFundsApiService
            .getImageData(param)
            .then(function (result) {
            // console.log('result data : ' + JSON.stringify(result));
            if (result.status == 200) {
                _this.profileImage = result.data;
                _this.setProfileImage(result.data);
                _this.setAvatar(result.data);
                // sessionStorage.removeItem('profileImage');
                sessionStorage.setItem('profileImage', 'profile');
            }
        })
            .catch(function (err) {
            console.log('err data : ' + JSON.stringify(err));
        });
    };
    AppStateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_invent_funds_api_service__WEBPACK_IMPORTED_MODULE_5__["InventFundsApiService"]])
    ], AppStateService);
    return AppStateService;
}());



/***/ }),

/***/ "./src/app/services/invent-funds-api-service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/invent-funds-api-service.ts ***!
  \******************************************************/
/*! exports provided: InventFundsApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventFundsApiService", function() { return InventFundsApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _providers_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/config */ "./src/providers/config.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);





var InventFundsApiService = /** @class */ (function () {
    /**
     *
     * @param _httpClient to access the post , get for api end points
     */
    function InventFundsApiService(_httpClient) {
        this._httpClient = _httpClient;
        // setting headers for api end points
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'text/html',
        });
    }
    // get end point from the provider config by passing that end point variable
    InventFundsApiService.prototype.getEndPoint = function (endPoint) {
        return _providers_config__WEBPACK_IMPORTED_MODULE_3__[endPoint];
    };
    InventFundsApiService.prototype.signIn = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["SIGNIN"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.signUp = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["SIGNUP"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.getToken = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["GETTOKEN"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.linkedInSignIn = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.get(_providers_config__WEBPACK_IMPORTED_MODULE_3__["LINKEDINSIGNIN"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.appendSectionUrlandId = function (url, params) {
        var urlAppended;
        // console.log(
        //   "data+'?params='+params : " + JSON.stringify(JSON.stringify(url) + '?params=' + params)
        // );
        return (urlAppended = url + '/' + params);
    };
    InventFundsApiService.prototype.linkedInRequestToken = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .get(_providers_config__WEBPACK_IMPORTED_MODULE_3__["LINKEDINREQUESTTOKEN"], { headers: _this.headers, params: params })
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.google = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .get(_providers_config__WEBPACK_IMPORTED_MODULE_3__["GOOGLE"], { headers: _this.headers, params: params })
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.googleCallback = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .get(_providers_config__WEBPACK_IMPORTED_MODULE_3__["GOOGLECALLBACK"], { headers: _this.headers, params: params })
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.changePassword = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["CHANGEPASSWORD"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.resetPassword = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["RESETPASSWORD"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.updateRoleAndOrgDetails = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["UPDATEORGANDROLE"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.retrieveMongoDBAll = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["RETRIEVEMONGOALL"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.retrieveMongoDBOne = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["RETRIEVEMONGOONE"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.createMongodbData = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["CREATEMONGODATA"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.updateMongodbData = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["UPDATEMONGODATA"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.deleteMongodbData = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["DELETEMONGODATA"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.retrieveScreenbuilderAll = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["RETRIEVESCREENBUILDERALL"], params).subscribe(function (response) {
                // console.log('response : ' + JSON.stringify(response));
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.retrieveScreenbuilderLookUp = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_this.getEndPoint(params.endPoint), params)
                .subscribe(function (response) {
                // console.log('response : ' + JSON.stringify(response));
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.attachmentSingleFile = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["SINGLEATTACHMENT"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.attachmentUpdateSingleFile = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["UPDATEATTACHMENT"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.retrieveGeneric = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_this.getEndPoint(params.endPoint), params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.createGeneric = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_this.getEndPoint(params.endPoint), params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.updateGeneric = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_this.getEndPoint(params.endPoint), params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.deleteGeneric = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_this.getEndPoint(params.endPoint), params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.retrieveScreenDetailsGrouped = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["RETRIEVESCREENBUILDERGROUPED"], params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.getImageData = function (params) {
        var _this = this;
        // console.log('api hit : ' + JSON.stringify(params));
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["GETATTACHMENTDATA"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.getImageDataWithId = function (params) {
        var _this = this;
        // console.log('api hit : ' + JSON.stringify(params));
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["RETRIEVEATTACHMENTDATASIGNEDWITHID"], params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.deleatAttachmentFile = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["DELETEATTACHMENT"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.deleatAttachmentFileSingle = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient
                .post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["DELETEATTACHMENTDATARECORD"], params)
                .subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService.prototype.showAlertCreateMessage = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Successfully Created',
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertCreateErrorMessage = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Create Unsuccessful',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#FF0000',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertUpdateMessage = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Successfully Updated',
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertUpdateMessageNoReload = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Successfully Updated',
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertUpdateErrorMessage = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Update Unsuccessful',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#FF0000',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertUpdateErrorMessageNoReload = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Update Unsuccessful',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#FF0000',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertDeleteMessage = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Successfully deleted',
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.showAlertDeleteErrorMessage = function () {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default.a.fire({
            title: 'Delete Unsuccessful',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#FF0000',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
                // container: 'sweet_containerImportant',
                title: 'swal-title',
                actions: 'swal2-actions',
                confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
                cancelButton: 'swal2-confirm',
            },
        }).then(function () {
            // window.location.reload();
        });
    };
    InventFundsApiService.prototype.verifyToken = function (params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._httpClient.post(_providers_config__WEBPACK_IMPORTED_MODULE_3__["VERIFYTOKEN"], params).subscribe(function (response) {
                resolve(response);
            }, reject);
        });
    };
    InventFundsApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root',
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], InventFundsApiService);
    return InventFundsApiService;
}());



/***/ }),

/***/ "./src/app/services/menu.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/menu.service.ts ***!
  \******************************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MenuService = /** @class */ (function () {
    function MenuService() {
        this.menuItems = [];
    }
    MenuService.prototype.addMenu = function (items) {
        var _this = this;
        items.forEach(function (item) {
            _this.menuItems.push(item);
        });
    };
    MenuService.prototype.getMenu = function () {
        return this.menuItems;
    };
    MenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MenuService);
    return MenuService;
}());



/***/ }),

/***/ "./src/app/services/roleaccessjson.ts":
/*!********************************************!*\
  !*** ./src/app/services/roleaccessjson.ts ***!
  \********************************************/
/*! exports provided: roleAccessJson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "roleAccessJson", function() { return roleAccessJson; });
/**
 * define all the routing end points so that we can give
 * access to each route dippends on role code
 */
var roleAccessJson = [{
        "moduleName": "dashboard",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "dashboard"
    },
    {
        "moduleName": "login",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "login"
    },
    {
        "moduleName": "linkedInLogin",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "linkedInLogin"
    },
    {
        "moduleName": "sign-up",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "sign-up"
    },
    {
        "moduleName": "sign-up-details",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "sign-up-details"
    },
    {
        "moduleName": "sign-up-roles",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "sign-up-roles"
    },
    {
        "moduleName": "help-inovator-page",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "help-inovator-page"
    },
    {
        "moduleName": "email-sent",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "email-sent"
    },
    {
        "moduleName": "thankyou",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "thankyou"
    },
    {
        "moduleName": "gotAnIdea",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "gotAnIdea"
    },
    {
        "moduleName": "forgot-password",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "forgot-password"
    },
    {
        "moduleName": "invest-in-ideas",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "invest-in-ideas"
    },
    {
        "moduleName": "landing-page",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "landing-page"
    },
    {
        "moduleName": "settings",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "settings"
    },
    {
        "moduleName": "editProfile",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "editProfile"
    },
    {
        "moduleName": "messages",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "messages"
    },
    {
        "moduleName": "connections",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "connections"
    },
    {
        "moduleName": "jobs",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "jobs"
    },
    {
        "moduleName": "settings/:data",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "settings/:data"
    },
    {
        "moduleName": "sign-up-details/:data",
        "roleCode": [
            "100",
            "101",
            "103",
            "104"
        ],
        "routeName": "sign-up-details/:data"
    }];


/***/ }),

/***/ "./src/app/shared/layouts.module.ts":
/*!******************************************!*\
  !*** ./src/app/shared/layouts.module.ts ***!
  \******************************************/
/*! exports provided: LayoutsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutsModule", function() { return LayoutsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _layout_landing_header_landing_header_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../layout/landing-header/landing-header.component */ "./src/app/layout/landing-header/landing-header.component.ts");
/* harmony import */ var _layout_landing_footer_landing_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../layout/landing-footer/landing-footer.component */ "./src/app/layout/landing-footer/landing-footer.component.ts");
/* harmony import */ var _ct_module_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../ct-module/carousel/carousel.component */ "./src/app/ct-module/carousel/carousel.component.ts");
/* harmony import */ var _ct_module_carousel_carousel_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ct-module/carousel/carousel.directive */ "./src/app/ct-module/carousel/carousel.directive.ts");







var LayoutsModule = /** @class */ (function () {
    function LayoutsModule() {
    }
    LayoutsModule_1 = LayoutsModule;
    LayoutsModule.forRoot = function () {
        return {
            ngModule: LayoutsModule_1
        };
    };
    var LayoutsModule_1;
    LayoutsModule = LayoutsModule_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _layout_landing_header_landing_header_component__WEBPACK_IMPORTED_MODULE_3__["LandingHeaderComponent"],
                _layout_landing_footer_landing_footer_component__WEBPACK_IMPORTED_MODULE_4__["LandingFooterComponent"],
                _ct_module_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__["CarouselComponent"],
                _ct_module_carousel_carousel_directive__WEBPACK_IMPORTED_MODULE_6__["CarouselDirective"],
                _ct_module_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__["CarouselSlideElement"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]
            ],
            exports: [
                _layout_landing_header_landing_header_component__WEBPACK_IMPORTED_MODULE_3__["LandingHeaderComponent"],
                _layout_landing_footer_landing_footer_component__WEBPACK_IMPORTED_MODULE_4__["LandingFooterComponent"],
                _ct_module_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__["CarouselComponent"],
                _ct_module_carousel_carousel_directive__WEBPACK_IMPORTED_MODULE_6__["CarouselDirective"],
                _ct_module_carousel_carousel_component__WEBPACK_IMPORTED_MODULE_5__["CarouselSlideElement"],
            ]
        })
    ], LayoutsModule);
    return LayoutsModule;
}());



/***/ }),

/***/ "./src/app/theme-options.ts":
/*!**********************************!*\
  !*** ./src/app/theme-options.ts ***!
  \**********************************/
/*! exports provided: ThemeOptions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeOptions", function() { return ThemeOptions; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ThemeOptions = /** @class */ (function () {
    function ThemeOptions() {
        this.sidebarHover = false;
        this.toggleSidebar = false;
        this.toggleSidebarMobile = false;
        this.toggleHeaderMobile = false;
        this.toggleThemeOptions = false;
        this.toggleDrawer = false;
        this.toggleFixedFooter = false;
    }
    ThemeOptions = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], ThemeOptions);
    return ThemeOptions;
}());



/***/ }),

/***/ "./src/app/utilities/customErrors.ts":
/*!*******************************************!*\
  !*** ./src/app/utilities/customErrors.ts ***!
  \*******************************************/
/*! exports provided: CUSTOM_ERRORS, alphaValidation, alphaNumericValidation, duplicateEmailFormat, passwordMismatchFormat, invalidEmailFormat, invalidPasswordFormat, passwordDosentMatchFormat, samePassword, equalToFormat, patternFormat, phoneNumberValidation, minDatevalidation, maxDatevalidation, fromDateToValidation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOM_ERRORS", function() { return CUSTOM_ERRORS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaValidation", function() { return alphaValidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaNumericValidation", function() { return alphaNumericValidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "duplicateEmailFormat", function() { return duplicateEmailFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "passwordMismatchFormat", function() { return passwordMismatchFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "invalidEmailFormat", function() { return invalidEmailFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "invalidPasswordFormat", function() { return invalidPasswordFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "passwordDosentMatchFormat", function() { return passwordDosentMatchFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "samePassword", function() { return samePassword; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "equalToFormat", function() { return equalToFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "patternFormat", function() { return patternFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "phoneNumberValidation", function() { return phoneNumberValidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minDatevalidation", function() { return minDatevalidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maxDatevalidation", function() { return maxDatevalidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fromDateToValidation", function() { return fromDateToValidation; });
var CUSTOM_ERRORS = [
    {
        error: "duplicateEmail",
        format: duplicateEmailFormat
    },
    {
        error: "passwordMismatch",
        format: passwordMismatchFormat
    },
    {
        error: "invalidEmail",
        format: invalidEmailFormat
    },
    {
        error: "invalidPassword",
        format: invalidPasswordFormat
    },
    {
        error: "passwordDosentMatch",
        format: passwordDosentMatchFormat
    },
    {
        error: "newPasswordCantBeSame",
        format: samePassword
    },
    {
        error: "equalTo",
        format: equalToFormat
    },
    {
        error: "pattern",
        format: patternFormat
    },
    {
        error: "minDate",
        format: minDatevalidation
    },
    {
        error: "maxDate",
        format: maxDatevalidation
    },
    {
        error: "phoneNumber",
        format: phoneNumberValidation
    },
    {
        error: "fromDateTo",
        format: fromDateToValidation
    },
    {
        error: "alpha",
        format: alphaValidation
    },
    {
        error: "alphaNumeric",
        format: alphaNumericValidation
    }
];
function alphaValidation(label, error) {
    return "Please enter valid data!";
}
function alphaNumericValidation(label, error) {
    return "Please enter valid data!";
}
function duplicateEmailFormat(label, error) {
    return " Email already exists in the system!";
}
function passwordMismatchFormat(label, error) {
    return " Passwords do not match!";
}
function invalidEmailFormat(label, error) {
    return " Email does not exist in the system!Please Sign Up";
}
function invalidPasswordFormat(label, error) {
    return " Invalid Password!";
}
function passwordDosentMatchFormat(label, error) {
    return " Please enter current password!";
}
function samePassword(label, error) {
    return " New password can't be same as current password!";
}
function equalToFormat(label, error) {
    return " Password doesn't match!";
}
function patternFormat(label, error) {
    // console.log("label : " + JSON.stringify(label));
    // console.log("error : " + JSON.stringify(error));
    if (error.requiredPattern == '^(?!^ +$)^.+$') {
        return "Not a valid text!";
    }
    else {
        return "Password must have a minimum length of 7 characters\n out of which one number and one upper case\n letter are mandatory!";
    }
}
function phoneNumberValidation(label, error) {
    // console.log("label : " + JSON.stringify(label));
    // console.log("error : " + JSON.stringify(error));
    return "please enter valid phone number!";
}
function minDatevalidation(label, error) {
    // console.log({ label });
    // console.log({ error });
    return "Please enter valid date!";
}
function maxDatevalidation(label, error) {
    // console.log({ label });
    // console.log({ error });
    return "Please enter valid date!";
}
function fromDateToValidation(label, error) {
    // console.log({ label });
    // console.log({ error });
    return "Please enter date greater than from date!";
}


/***/ }),

/***/ "./src/app/utilities/customValidators.ts":
/*!***********************************************!*\
  !*** ./src/app/utilities/customValidators.ts ***!
  \***********************************************/
/*! exports provided: alphaValidator, alphaNumericValidator, alphaValidatorMessage, alphaNumericMessage, charactersNumbersValidator, charactersNumbersValidatorMessage, EmailValidator, EmailValidatorMessage, maxValidationMessage, minValidationMessage, maxLengthValidationMessage, minLengthValidationMessage, phoneNumberValidator, phoneNumberValidatorMessage, doorNumbersValidator, doorNumbersMessage, zipCodeValidator, zipCodeMessage, cityValidator, cityMessage, poBoxValidator, poBoxMessage, addressValidator, addressMessage, identificationValidator, IFSCValidator, IFSCMessage, bankBranchValidator, bankBranchMessage, micrValidator, micrMessage, identificationMessage, MaxDateValidator, minDateValidator, MaxDateMessage, minDateMessage, vinValidator, vinMessage, engineValidator, engineMessage, emptySpacesValidator, emptySpacesMessage, fromDateToMessage, ValidateAlphaNumeric, ValidateAlphaNumericMessage, validators, validationMessages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaValidator", function() { return alphaValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaNumericValidator", function() { return alphaNumericValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaValidatorMessage", function() { return alphaValidatorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaNumericMessage", function() { return alphaNumericMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "charactersNumbersValidator", function() { return charactersNumbersValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "charactersNumbersValidatorMessage", function() { return charactersNumbersValidatorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailValidator", function() { return EmailValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailValidatorMessage", function() { return EmailValidatorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maxValidationMessage", function() { return maxValidationMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minValidationMessage", function() { return minValidationMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maxLengthValidationMessage", function() { return maxLengthValidationMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minLengthValidationMessage", function() { return minLengthValidationMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "phoneNumberValidator", function() { return phoneNumberValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "phoneNumberValidatorMessage", function() { return phoneNumberValidatorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doorNumbersValidator", function() { return doorNumbersValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doorNumbersMessage", function() { return doorNumbersMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "zipCodeValidator", function() { return zipCodeValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "zipCodeMessage", function() { return zipCodeMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cityValidator", function() { return cityValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cityMessage", function() { return cityMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "poBoxValidator", function() { return poBoxValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "poBoxMessage", function() { return poBoxMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addressValidator", function() { return addressValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addressMessage", function() { return addressMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "identificationValidator", function() { return identificationValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IFSCValidator", function() { return IFSCValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IFSCMessage", function() { return IFSCMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bankBranchValidator", function() { return bankBranchValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bankBranchMessage", function() { return bankBranchMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "micrValidator", function() { return micrValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "micrMessage", function() { return micrMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "identificationMessage", function() { return identificationMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaxDateValidator", function() { return MaxDateValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minDateValidator", function() { return minDateValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaxDateMessage", function() { return MaxDateMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minDateMessage", function() { return minDateMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vinValidator", function() { return vinValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "vinMessage", function() { return vinMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "engineValidator", function() { return engineValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "engineMessage", function() { return engineMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "emptySpacesValidator", function() { return emptySpacesValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "emptySpacesMessage", function() { return emptySpacesMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fromDateToMessage", function() { return fromDateToMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidateAlphaNumeric", function() { return ValidateAlphaNumeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidateAlphaNumericMessage", function() { return ValidateAlphaNumericMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validators", function() { return validators; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validationMessages", function() { return validationMessages; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

function alphaValidator(control) {
    return !control.value || /^[a-z ,.'-]+$/i.test(control.value) ? null : { 'alpha': true };
}
function alphaNumericValidator(control) {
    return !control.value || /^[a-zA-Z0-9?=.,*!@#$%^&*_\-/\s]+$/i.test(control.value) ? null : { 'alphaNumeric': true };
}
function alphaValidatorMessage(err, field) {
    return "This field cannot contain numbers or special characters!";
}
function alphaNumericMessage(err, field) {
    return "This field can be alpha numeric!";
}
function charactersNumbersValidator(control) {
    return !control.value || /^[a-zA-Z0-9_./-]*$/i.test(control.value) ? null : { 'charactersNumbers': true };
}
function charactersNumbersValidatorMessage(err, field) {
    return "This field cannot contain special characters!";
}
function EmailValidator(control) {
    return !control.value || /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(control.value) ? null : { 'email': true };
}
function EmailValidatorMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Email id";
}
function maxValidationMessage(err, field) {
    return "Value cannot be greater than " + field.templateOptions.max + " ";
}
function minValidationMessage(err, field) {
    return "Value cannot be less than " + field.templateOptions.min + " ";
}
function maxLengthValidationMessage(err, field) {
    return "Value cannot be greater than " + field.templateOptions.maxLength + " ";
}
function minLengthValidationMessage(err, field) {
    return "Value cannot be less than " + field.templateOptions.minLength + " ";
}
function phoneNumberValidator(control) {
    if (control.value == 0) {
        return !control.value ? null : { 'phoneNumber': true };
    }
    else {
        return !control.value || /^\s*(?:\+?(\d{1,3}))?[- (]*(\d{3})[- )]*(\d{3})[- ]*(\d{4})(?: *[x/#]{1}(\d+))?\s*$/.test(control.value) ? null : { 'phoneNumber': true };
    }
}
function phoneNumberValidatorMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Phone Number";
}
function doorNumbersValidator(control) {
    return !control.value || /((?:[a-zA-Z]+\s)*)([\d]+)(?:(?:\/)(\d+))?/.test(control.value) ? null : { 'doorNumbers': true };
}
function doorNumbersMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Numbers";
}
function zipCodeValidator(control) {
    return !control.value || /\b\d{5}(?:-\d{4})?\b/.test(control.value) ? null : { 'zipCode': true };
}
function zipCodeMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Zipcode";
}
function cityValidator(control) {
    return !control.value || /\b\d{5}(?:-\d{4})?\b/.test(control.value) ? null : { 'city': true };
}
function cityMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Zipcode";
}
function poBoxValidator(control) {
    return !control.value || /^(?:(?<numberstreet>\d+(?:\ |\w|\.)+(?=$|\n|\,))|(?<pobox>(?:PO|P\.O\.)[\ ](?:BOX)[\ ](?:\d|\-)+(?=$|\n|\,)))?(?:\ *\,\ *)?(?<suite>(?:SUITE|STE)\ [\#]?(?:\w|\d|\-)+)?(?:\ *\,\ *)?(?<citystatezip>(?<=^|,|\ )(?<city>[\w\ ]+)(?:\ *\,\ *)(?<stateabrv>[A-Z]{1,3})(?:\ +)(?<zipcode>\d{5}(?:\-\d*)?))?$/.test(control.value) ? null : { 'poBox': true };
}
function poBoxMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Po Box Number";
}
function addressValidator(control) {
    return !control.value || /\w+\s*(road|street|square|rd|st|sq)\W/.test(control.value) ? null : { 'address': true };
}
function addressMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Address";
}
function identificationValidator(control) {
    return !control.value || /^((?!666|000)[0-8][0-9\_]{2}\-(?!00)[0-9\_]{2}\-(?!0000)[0-9\_]{4})*$/.test(control.value) ? null : { 'identification': true };
}
function IFSCValidator(control) {
    return !control.value || /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/.test(control.value) ? null : { 'IFSC': true };
}
function IFSCMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid IFSC Code";
}
function bankBranchValidator(control) {
    return !control.value || /^0[A-Z0-9a-z]{6}$/.test(control.value) ? null : { 'Branch': true };
}
function bankBranchMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Branch Code";
}
function micrValidator(control) {
    return !control.value || /^[0-9]{9}$/.test(control.value) ? null : { 'MICR': true };
}
function micrMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid MICR Number";
}
function identificationMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Identification Number";
}
function MaxDateValidator(control) {
    var controlDate = moment__WEBPACK_IMPORTED_MODULE_0__(control.value);
    var currentDate = moment__WEBPACK_IMPORTED_MODULE_0__();
    if (controlDate.isAfter(currentDate)) {
        return { 'maxDate': true };
    }
    return null;
}
function minDateValidator(control) {
    var controlDate = moment__WEBPACK_IMPORTED_MODULE_0__(control.value);
    var currentDate = moment__WEBPACK_IMPORTED_MODULE_0__().subtract(1, 'd');
    if (controlDate.isBefore(currentDate)) {
        return { 'minDate': true };
    }
    // else {
    //     return {'minDate':true}
    // }
    return null;
}
function MaxDateMessage(err, field) {
    return "Date cannot be greater than today's date!";
}
function minDateMessage(err, field) {
    return "Date cannot be less than today's date!";
}
function vinValidator(control) {
    return !control.value || /^[a-zA-Z0-9]{9}[a-zA-Z0-9-]{2}[0-9]{6}$/.test(control.value) ? null : { 'vinNumber': true };
}
function vinMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid VIN Number";
}
function engineValidator(control) {
    return !control.value || /^([A-z]{2}[A-z0-9]{5,16})$/.test(control.value) ? null : { 'engineNumber': true };
}
function engineMessage(err, field) {
    return "\"" + field.formControl.value + "\" is not a valid Engine Number";
}
function emptySpacesValidator(control) {
    return !control.value || /^$|^\S+.*/i.test(control.value) ? null : { 'emptySpaces': true };
    // return !control.value
}
function emptySpacesMessage(err, field) {
    return "Not a valid text!";
}
function fromDateToMessage(err, field) {
    return "Please enter date greater than from date!";
}
function ValidateAlphaNumeric(control) {
    // ^[a-zA-Z]+[a-zA-Z .]*$
    // ^[a-zA-Z .]*$
    if (control.value == null || control.value == "") {
        return null;
    }
    else if (!/^[(a-zA-Z)]+[a-zA-Z0-9\s]*$/.test(control.value)) {
        return { validAlphaNumeric: true };
    }
    return null;
}
function ValidateAlphaNumericMessage(control) {
    return "This field can be alpha numeric!";
}
var validators = [
    { name: 'alpha', validation: alphaValidator },
    { name: 'alphaNumeric', validation: alphaNumericValidator },
    { name: 'alphaNumericVal', validation: ValidateAlphaNumeric },
    { name: 'charactersNumbers', validation: charactersNumbersValidator },
    { name: 'email', validation: EmailValidator },
    { name: 'phoneNumber', validation: phoneNumberValidator },
    { name: 'doorNumbers', validation: doorNumbersValidator },
    { name: 'zipCode', validation: zipCodeValidator },
    { name: 'city', validation: cityValidator },
    { name: 'poBox', validation: poBoxValidator },
    { name: 'address', validation: addressValidator },
    { name: 'identification', validation: identificationValidator },
    { name: 'vinNumber', validation: vinValidator },
    { name: 'engineNumber', validation: vinValidator },
    { name: 'maxDate', validation: MaxDateValidator },
    { name: 'minDate', validation: minDateValidator },
    { name: 'IFSC', validation: IFSCValidator },
    { name: 'Branch', validation: bankBranchValidator },
    { name: 'MICR', validation: micrValidator },
    { name: 'emptySpaces', validation: emptySpacesValidator },
];
var validationMessages = [
    { name: 'required', message: 'This field is required' },
    { name: 'alpha', message: alphaValidatorMessage },
    { name: 'charactersNumbers', message: charactersNumbersValidatorMessage },
    { name: 'alphaNumeric', message: alphaNumericMessage },
    { name: 'alphaNumericVal', message: ValidateAlphaNumericMessage },
    { name: 'email', message: EmailValidatorMessage },
    { name: 'phoneNumber', message: phoneNumberValidatorMessage },
    { name: 'max', message: maxValidationMessage },
    { name: 'min', message: minValidationMessage },
    { name: 'maxLength', message: maxLengthValidationMessage },
    { name: 'minLength', message: minLengthValidationMessage },
    { name: 'doorNumbers', message: doorNumbersMessage },
    { name: 'zipCode', message: zipCodeMessage },
    { name: 'city', message: cityMessage },
    { name: 'poBox', message: poBoxMessage },
    { name: 'address', message: addressMessage },
    { name: 'identification', message: identificationMessage },
    { name: 'vinNumber', message: vinMessage },
    { name: 'engineNumber', message: vinMessage },
    { name: 'maxDate', message: MaxDateMessage },
    { name: 'minDate', message: minDateMessage },
    { name: 'IFSC', message: IFSCMessage },
    { name: 'Branch', message: bankBranchMessage },
    { name: 'MICR', message: micrMessage },
    { name: 'emptySpaces', message: emptySpacesMessage },
    { name: 'fromDateTo', message: fromDateToMessage },
];


/***/ }),

/***/ "./src/app/utilities/menu.ts":
/*!***********************************!*\
  !*** ./src/app/utilities/menu.ts ***!
  \***********************************/
/*! exports provided: testMenu1, testMenu2, testMenu3, testMenu4, testMenu5, menuForTesting */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testMenu1", function() { return testMenu1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testMenu2", function() { return testMenu2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testMenu3", function() { return testMenu3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testMenu4", function() { return testMenu4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testMenu5", function() { return testMenu5; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "menuForTesting", function() { return menuForTesting; });
//Testing Purpose Only
// const testMenu1 = 
//       {
//         text: 'Menu 1',
//         link: '/dashboard',
//         icon: 'icon-speedometer',
//         submenu: [
//             {
//                 text: 'level 1',
//                 link: '/test',
//                 submenu: [
//                   {
//                       text: 'level 1 subitem 1',
//                       link: '/test'
//                   },
//                   {
//                       text: 'level 2',
//                       link: '/test',
//                       submenu: [
//                         {
//                             text: 'level 2 sub 1',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 2',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 3',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 4',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 5',
//                             link: '/test'
//                         }
//                     ]
//                   }
//               ]
//             },
//             {
//                 text: 'Level v2',
//                 link: '/test'
//             },
//             {
//                 text: 'Level v3',
//                 link: '/test'
//             }
//         ]
//     }
//  const testMenu2 =  {
//     text: 'Menu 2',
//     link: '/test',
//     icon: 'icon-speedometer',
//     submenu: [
//         {
//             text: 'Dashbord v1',
//             link: '/test',
//             submenu: [
//               {
//                   text: 'Dashbord v1',
//                   link: '/test'
//               },
//               {
//                   text: 'Dashbord v2',
//                   link: '/test',
//               }
//           ]
//         },
//         {
//             text: 'Dashbord v2',
//             link: '/test'
//         },
//         {
//             text: 'Dashbord v3',
//             link: '/test'
//         }
//     ]
// }
// export const testMenu3 =  {
//     text: 'Menu 3',
//     link: '/test',
//     icon: 'icon-speedometer',
// }
var testMenu1 = {
    text: "Home",
    link: "/landing",
    icon: "vsm-icon pe-7s-home pe-3x pe-va"
    // icon:"vsm-icon home-icon pe-3x pe-va"
};
var testMenu2 = {
    text: "Jobs",
    link: "/profile/jobs",
    icon: "vsm-icon pe-7s-tools pe-3x pe-va"
    // icon: "vsm-icon jobs-icon pe-3x pe-va"
};
var testMenu3 = {
    text: "Connections",
    link: "/profile/connections",
    icon: "vsm-icon pe-7s-network pe-3x pe-va"
    // icon: "vsm-icon connections-icon pe-3x pe-va"
};
var testMenu4 = {
    text: "Messages",
    link: "/profile/messages",
    icon: "vsm-icon pe-7s-chat pe-3x pe-va"
    // icon: "vsm-icon messages-icon pe-3x pe-va"
};
var testMenu5 = {
    text: "Profile",
    link: "dprofile/editProfile",
    icon: "vsm-icon pe-7s-user pe-3x pe-va"
    // icon:"vsm-icon profile-icon pe-3x pe-va"
    //   icon: "vsm-icon pe-7s-car"
};
var menuForTesting = [testMenu1, testMenu2, testMenu3, testMenu4, testMenu5];
// export const commissionerMenu = [
//     testMenu1,
//     testMenu2,
//     testMenu3,
// ]


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ "./src/providers/config.ts":
/*!*********************************!*\
  !*** ./src/providers/config.ts ***!
  \*********************************/
/*! exports provided: SERVERTYPE, SERVERURLDEV, SERVERURLTEST, SERVERREDRECTURLDEV, SERVERREDRECTURLTESTIN, SERVERREDRECTURLUAT, SERVERREDRECTURLPROD, SERVERREDRECTURLCHANGEPASSWORDDEV, SERVERREDRECTURLCHANGEPASSWORDTESTIN, SERVERREDRECTURLCHANGEPASSWORDUAT, SERVERREDRECTURLCHANGEPASSWORDPROD, SIGNIN, SIGNUP, CHANGEPASSWORD, RESETPASSWORD, VERIFYTOKEN, UPDATEORGANDROLE, COMPARECURRENTPASSWORD, LINKEDINSIGNIN, LINKEDINREQUESTTOKEN, GOOGLE, GOOGLECALLBACK, GETTOKEN, SINGLEATTACHMENT, UPDATEATTACHMENT, DELETEATTACHMENT, DELETEATTACHMENTDATARECORD, GETATTACHMENTDATA, RETRIEVEATTACHMENTDATASIGNED, RETRIEVEATTACHMENTDATASIGNEDWITHID, CREATEPROFILE, UPDATEPROFILE, RETRIVEPROFILE, CREATEPROFILESUMMARY, RETRIEVEPROFILESUMMARY, RETRIEVEPROFILESUMMARYONE, UPDATEPROFILESUMMARY, RETRIEVEATTACHMENTDATA, CREATEPROFESSIONALDETAILS, RETRIEVEPROFESSIONALDETAILS, RETRIEVEPROFESSIONALDETAILSONE, UPDATEPROFESSIONALDETAILS, RETRIEVELOOKUPDATA, RETRIEVESCREENBUILDERGROUPED, DELETEPROFESSIONALDETAILS, CREATEACHIDETAILS, RETRIEVEACHIDETAILS, RETRIEVEACHIDETAILSONE, UPDATEACHIDETAILS, DELETEACHIDETAILS, RETRIEVELOOKUPDATAACHI, RETRIEVESCREENBUILDERGROUPEDACHI, CREATESKILLSDETAILS, RETRIEVESKILLSDETAILS, RETRIEVESKILLSDETAILSONE, UPDATESKILLSDETAILS, DELETESKILLSDETAILS, RETRIEVELOOKUPDATASKILLS, RETRIEVESCREENBUILDERGROUPEDSKILLS, RETRIEVESCREENBUILDERLOOKUPSKILLS, CREATEEDUCATIONDETAILS, RETRIEVEEDUCATIONDETAILS, RETRIEVEEDUCATIONDETAILSONE, UPDATEEDUCATIONDETAILS, DELETEEDUCATIONDETAILS, RETRIEVEMONGOALL, RETRIEVESCREENBUILDERALL, CREATEMONGODATA, UPDATEMONGODATA, DELETEMONGODATA, RETRIEVEMONGOONE, CREATECHATDATA, CREATECHANNEL, GETCONTACTS, RETRIVECHATS, UPDATEMESSAGESTATUS, UPDATECHANNELID, GETACTIVECHANNELCOUNT */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERTYPE", function() { return SERVERTYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERURLDEV", function() { return SERVERURLDEV; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERURLTEST", function() { return SERVERURLTEST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLDEV", function() { return SERVERREDRECTURLDEV; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLTESTIN", function() { return SERVERREDRECTURLTESTIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLUAT", function() { return SERVERREDRECTURLUAT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLPROD", function() { return SERVERREDRECTURLPROD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLCHANGEPASSWORDDEV", function() { return SERVERREDRECTURLCHANGEPASSWORDDEV; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLCHANGEPASSWORDTESTIN", function() { return SERVERREDRECTURLCHANGEPASSWORDTESTIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLCHANGEPASSWORDUAT", function() { return SERVERREDRECTURLCHANGEPASSWORDUAT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERVERREDRECTURLCHANGEPASSWORDPROD", function() { return SERVERREDRECTURLCHANGEPASSWORDPROD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SIGNIN", function() { return SIGNIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SIGNUP", function() { return SIGNUP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANGEPASSWORD", function() { return CHANGEPASSWORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESETPASSWORD", function() { return RESETPASSWORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VERIFYTOKEN", function() { return VERIFYTOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEORGANDROLE", function() { return UPDATEORGANDROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMPARECURRENTPASSWORD", function() { return COMPARECURRENTPASSWORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LINKEDINSIGNIN", function() { return LINKEDINSIGNIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LINKEDINREQUESTTOKEN", function() { return LINKEDINREQUESTTOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GOOGLE", function() { return GOOGLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GOOGLECALLBACK", function() { return GOOGLECALLBACK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GETTOKEN", function() { return GETTOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SINGLEATTACHMENT", function() { return SINGLEATTACHMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEATTACHMENT", function() { return UPDATEATTACHMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETEATTACHMENT", function() { return DELETEATTACHMENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETEATTACHMENTDATARECORD", function() { return DELETEATTACHMENTDATARECORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GETATTACHMENTDATA", function() { return GETATTACHMENTDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEATTACHMENTDATASIGNED", function() { return RETRIEVEATTACHMENTDATASIGNED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEATTACHMENTDATASIGNEDWITHID", function() { return RETRIEVEATTACHMENTDATASIGNEDWITHID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATEPROFILE", function() { return CREATEPROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEPROFILE", function() { return UPDATEPROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIVEPROFILE", function() { return RETRIVEPROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATEPROFILESUMMARY", function() { return CREATEPROFILESUMMARY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEPROFILESUMMARY", function() { return RETRIEVEPROFILESUMMARY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEPROFILESUMMARYONE", function() { return RETRIEVEPROFILESUMMARYONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEPROFILESUMMARY", function() { return UPDATEPROFILESUMMARY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEATTACHMENTDATA", function() { return RETRIEVEATTACHMENTDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATEPROFESSIONALDETAILS", function() { return CREATEPROFESSIONALDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEPROFESSIONALDETAILS", function() { return RETRIEVEPROFESSIONALDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEPROFESSIONALDETAILSONE", function() { return RETRIEVEPROFESSIONALDETAILSONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEPROFESSIONALDETAILS", function() { return UPDATEPROFESSIONALDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVELOOKUPDATA", function() { return RETRIEVELOOKUPDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESCREENBUILDERGROUPED", function() { return RETRIEVESCREENBUILDERGROUPED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETEPROFESSIONALDETAILS", function() { return DELETEPROFESSIONALDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATEACHIDETAILS", function() { return CREATEACHIDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEACHIDETAILS", function() { return RETRIEVEACHIDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEACHIDETAILSONE", function() { return RETRIEVEACHIDETAILSONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEACHIDETAILS", function() { return UPDATEACHIDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETEACHIDETAILS", function() { return DELETEACHIDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVELOOKUPDATAACHI", function() { return RETRIEVELOOKUPDATAACHI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESCREENBUILDERGROUPEDACHI", function() { return RETRIEVESCREENBUILDERGROUPEDACHI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATESKILLSDETAILS", function() { return CREATESKILLSDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESKILLSDETAILS", function() { return RETRIEVESKILLSDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESKILLSDETAILSONE", function() { return RETRIEVESKILLSDETAILSONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATESKILLSDETAILS", function() { return UPDATESKILLSDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETESKILLSDETAILS", function() { return DELETESKILLSDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVELOOKUPDATASKILLS", function() { return RETRIEVELOOKUPDATASKILLS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESCREENBUILDERGROUPEDSKILLS", function() { return RETRIEVESCREENBUILDERGROUPEDSKILLS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESCREENBUILDERLOOKUPSKILLS", function() { return RETRIEVESCREENBUILDERLOOKUPSKILLS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATEEDUCATIONDETAILS", function() { return CREATEEDUCATIONDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEEDUCATIONDETAILS", function() { return RETRIEVEEDUCATIONDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEEDUCATIONDETAILSONE", function() { return RETRIEVEEDUCATIONDETAILSONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEEDUCATIONDETAILS", function() { return UPDATEEDUCATIONDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETEEDUCATIONDETAILS", function() { return DELETEEDUCATIONDETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEMONGOALL", function() { return RETRIEVEMONGOALL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVESCREENBUILDERALL", function() { return RETRIEVESCREENBUILDERALL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATEMONGODATA", function() { return CREATEMONGODATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEMONGODATA", function() { return UPDATEMONGODATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETEMONGODATA", function() { return DELETEMONGODATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIEVEMONGOONE", function() { return RETRIEVEMONGOONE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATECHATDATA", function() { return CREATECHATDATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CREATECHANNEL", function() { return CREATECHANNEL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GETCONTACTS", function() { return GETCONTACTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RETRIVECHATS", function() { return RETRIVECHATS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATEMESSAGESTATUS", function() { return UPDATEMESSAGESTATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATECHANNELID", function() { return UPDATECHANNELID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GETACTIVECHANNELCOUNT", function() { return GETACTIVECHANNELCOUNT; });
// export const SERVERTYPE = "DEVELOPMENT";
// export const SERVER_URL_DEV = "http://localhost:4000";
// set type DEVELOPMENT , TESTING , UAT , PRODUCTION
var SERVERTYPE = 'DEVELOPMENT';
var SERVERURLDEV = 'http://inventfunds-dev.crecientech.com/';
var SERVERURLTEST = 'https://inventfunds-demo.crecientech.com/';
var SERVERREDRECTURLDEV = 'http://inventfunds-dev.crecientech.com/api/auth/getToken';
var SERVERREDRECTURLTESTIN = 'http://inventfunds-dev.crecientech.com/api/auth/getToken';
var SERVERREDRECTURLUAT = '';
var SERVERREDRECTURLPROD = '';
var SERVERREDRECTURLCHANGEPASSWORDDEV = 'http://inventfunds-dev.crecientech.com/api/auth/getTokenChangePassword';
var SERVERREDRECTURLCHANGEPASSWORDTESTIN = 'https://inventfunds-demo.crecientech.com/api/auth/getTokenChangePassword';
var SERVERREDRECTURLCHANGEPASSWORDUAT = '';
var SERVERREDRECTURLCHANGEPASSWORDPROD = '';
// Sign-Up ,Sign-In  and Change Password
var SIGNIN = './api/user/mongoSignIn';
var SIGNUP = './api/user/mongoSignUp';
var CHANGEPASSWORD = './api/user/mongoChangePassword';
var RESETPASSWORD = './api/user/mongoResetPassword';
var VERIFYTOKEN = '/api/user/verifyToken';
var UPDATEORGANDROLE = './api/user/updateOrgAndRole';
var COMPARECURRENTPASSWORD = './api/user/comparePassword';
var LINKEDINSIGNIN = './api/auth/linkedin/callback';
var LINKEDINREQUESTTOKEN = './api/auth/requestToken';
var GOOGLE = './auth/google';
var GOOGLECALLBACK = './auth/google/callback';
var GETTOKEN = './api/user/getToken';
var SINGLEATTACHMENT = './api/attachment/singleAttachment';
var UPDATEATTACHMENT = './api/attachment/updateAttachment';
var DELETEATTACHMENT = './api/attachment/deleteAttachment';
var DELETEATTACHMENTDATARECORD = './api/attachment/deleteAttachmentSingle';
var GETATTACHMENTDATA = './api/attachment/getSignedData';
var RETRIEVEATTACHMENTDATASIGNED = './api/attachment/retrieveAttachmentDataWithSignedUrl';
var RETRIEVEATTACHMENTDATASIGNEDWITHID = './api/attachment/getSignedUrlWithId';
var CREATEPROFILE = './api/profileData/createProfile';
var UPDATEPROFILE = './api/profileData/updateprofile';
var RETRIVEPROFILE = './api/profileData/retrieveProfileData';
var CREATEPROFILESUMMARY = './api/profileSummary/createProfileSummary';
var RETRIEVEPROFILESUMMARY = './api/profileSummary/retrieveProfileSummary';
var RETRIEVEPROFILESUMMARYONE = './api/profileSummary/retrieveProfileSummaryOne';
var UPDATEPROFILESUMMARY = './api/profileSummary/updateProfileSummary';
var RETRIEVEATTACHMENTDATA = './api/profileSummary/retrieveAttachmentData';
var CREATEPROFESSIONALDETAILS = './api/professionalDetails/createProfessionalDetails';
var RETRIEVEPROFESSIONALDETAILS = './api/professionalDetails/retrieveProfessionalDetails';
var RETRIEVEPROFESSIONALDETAILSONE = './api/professionalDetails/retrieveProfessionalDetailsOne';
var UPDATEPROFESSIONALDETAILS = './api/professionalDetails/updateProfessionalDetails';
var RETRIEVELOOKUPDATA = './api/professionalDetails/retrieveLookupData';
var RETRIEVESCREENBUILDERGROUPED = './api/professionalDetails/getScreenBuildergrouped';
var DELETEPROFESSIONALDETAILS = './api/professionalDetails/deleteProfessionalDetails';
var CREATEACHIDETAILS = './api/achievementsDetails/createAchievementsDetails';
var RETRIEVEACHIDETAILS = './api/achievementsDetails/retrieveAchievementsDetails';
var RETRIEVEACHIDETAILSONE = './api/achievementsDetails/retrieveAchievementsDetailsOne';
var UPDATEACHIDETAILS = './api/achievementsDetails/updateAchievementsDetails';
var DELETEACHIDETAILS = './api/achievementsDetails/deleteAchiDetails';
var RETRIEVELOOKUPDATAACHI = './api/achievementsDetails/retrieveLookupData';
var RETRIEVESCREENBUILDERGROUPEDACHI = './api/achievementsDetails/getScreenBuildergrouped';
var CREATESKILLSDETAILS = './api/skillsDetails/createSkillsDetails';
var RETRIEVESKILLSDETAILS = './api/skillsDetails/retrieveSkillsDetails';
var RETRIEVESKILLSDETAILSONE = './api/skillsDetails/retrieveSkillsDetailsOne';
var UPDATESKILLSDETAILS = './api/skillsDetails/updateSkillsDetails';
var DELETESKILLSDETAILS = './api/skillsDetails/deleteAchiDetails';
var RETRIEVELOOKUPDATASKILLS = './api/skillsDetails/retrieveLookupData';
var RETRIEVESCREENBUILDERGROUPEDSKILLS = './api/skillsDetails/getScreenBuildergrouped';
var RETRIEVESCREENBUILDERLOOKUPSKILLS = './api/skillsDetails/retrieveLookUpDetails';
var CREATEEDUCATIONDETAILS = './api/educationDetails/createEducationDetails';
var RETRIEVEEDUCATIONDETAILS = './api/educationDetails/retrieveEducationDetails';
var RETRIEVEEDUCATIONDETAILSONE = './api/educationDetails/retrieveEducationDetailsOne';
var UPDATEEDUCATIONDETAILS = './api/educationDetails/updateEducationDetails';
var DELETEEDUCATIONDETAILS = './api/educationDetails/deleteEducationDetails';
//Mongo DB Related End points - for CRUD
var RETRIEVEMONGOALL = './api/mongoDb/retrieveAll';
var RETRIEVESCREENBUILDERALL = './api/mongoDb/retrieveAllScreenbuilder';
var CREATEMONGODATA = './api/mongoDb/createData';
var UPDATEMONGODATA = './api/mongoDb/updateData';
var DELETEMONGODATA = './api/mongoDb/deleteData';
var RETRIEVEMONGOONE = './api/mongoDb/retrieveOne';
// chat panel
var CREATECHATDATA = './api/chatPusher/createChat';
var CREATECHANNEL = './api/chatPusher/createChannel';
var GETCONTACTS = './api/chatPusher/retriveContacts';
var RETRIVECHATS = './api/chatPusher/retriveAllMessages';
var UPDATEMESSAGESTATUS = './api/chatPusher/updateMessageStatus';
var UPDATECHANNELID = './api/chatPusher/updateChannelId';
var GETACTIVECHANNELCOUNT = './api/chatPusher/activeChannelCount';


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Work\Projects\INVENTFUND\BRANCHES\inventfund-test\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map