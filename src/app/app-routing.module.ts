import { NgModule } from "@angular/core";
import { Routes, RouterModule, ExtraOptions } from "@angular/router";
import { RolesAuthGuardService } from './services/roles-auth-guard.service';

const routes: Routes = [
  {
    path: "landing",
    loadChildren: "../app/landing-components/landing-components.module#LandingComponentsModule"
  },
  {
    path: "auth",
    loadChildren: "../app/auth/auth.module#AuthModule"
  },
  {
    path: "profile",
    loadChildren: "../app/profile/profile.module#ProfileModule"
  },
  { path: "**", redirectTo: "landing" }
];

const routerOptions: ExtraOptions = { useHash: true, anchorScrolling: "enabled", scrollPositionRestoration: "enabled" };

@NgModule({
  imports: [
    RouterModule.forRoot(routes, routerOptions)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
