import { Component } from '@angular/core';
import { MenuService } from './services/menu.service';
import { menuForTesting } from './utilities/menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Invent Funds';

  ngOnInit() { }

  constructor(public menuService: MenuService) {
    menuService.addMenu(menuForTesting);
  }
}
