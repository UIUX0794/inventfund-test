import { Component, OnInit } from "@angular/core";
import { InventFundsApiService } from "../../services/invent-funds-api-service";
import { ActivatedRoute, Router } from "@angular/router";
import Swal from "sweetalert2";
import { AppStateService } from "../../services/app-state.service";

@Component({
  selector: "app-auth-callback",
  templateUrl: "./auth-callback.component.html",
  styleUrls: ["./auth-callback.component.scss"]
})
export class AuthCallbackComponent implements OnInit {

  // global variables

  routerData: any;
  code: string;
  state: string;
  page: string;

  /**
   * 
   * @param inventFundsApiService apo service to get data from database
   * @param activatedRoute to get the current activated route
   * @param router navigate between diffrent routes
   * @param appState get required data from appstate
   */

  constructor(
    private inventFundsApiService: InventFundsApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private appState: AppStateService
  ) {
    this.page = sessionStorage.getItem("page");
    // console.log("page type from session storage : " + JSON.stringify(this.page));
    this.activatedRoute.queryParams.subscribe(res => {
      // console.log("queryParams:" + JSON.stringify(res));
      if (res) {
        this.navigate(res, this.page);
      }
    });
  }

  // ng onInit

  ngOnInit() { }

  // dippends on the page type navigate to login or signup

  /**
   * 
   * @param queryData response from query params
   * @param pageType login or signup from session storage
   */

  navigate(queryData, pageType) {
    // console.log("query data : " + JSON.stringify(queryData));
    // console.log("page type : " + JSON.stringify(pageType));
    if (pageType == "login") {
      this.navigateToProfile(queryData);
    } else if (pageType == "signup") {
      this.navigateToSignupRole(queryData);
    } else if (pageType == "resetPassword") {
      console.log("reset password : " + JSON.stringify(queryData));
      this.navigateToresetPassword(queryData);
    }
  }

  // if page type login navigate to login

  navigateToProfile(email) {
    if (email) {
      this.router.navigate(["auth/login"], {
        queryParams: { email: email.email }
      });
    }
  }

  // if page type signup navigate to signup

  navigateToSignupRole(email) {
    console.log("data for signup : " + JSON.stringify(email));
    if (email) {
      this.router.navigate(["auth/sign-up"], {
        queryParams: { email: email.email }
      });
    }
  }

  // if page type signup navigate to signup

  navigateToresetPassword(email) {
    console.log("data for signup : " + JSON.stringify(email));
    if (email) {
      var enc = window.btoa(email.email);
      this.router.navigate(['/profile/settings'], {
        queryParams: { email: enc, setActive: 2 }
      });
    }
  }

}
