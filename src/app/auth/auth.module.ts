import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from "./login/login.component";
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignUpRolesComponent } from './sign-up-roles/sign-up-roles.component';
import { EmailsentpageComponent } from './emailsentpage/emailsentpage.component';
import { SignUpDetailsComponent } from './sign-up-details/sign-up-details.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { AuthCallbackComponent } from "./auth-callback/auth-callback.component";
import { RolesAuthGuardService } from '../services/roles-auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [RolesAuthGuardService],
    data: ['login']
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    canActivate: [RolesAuthGuardService],
    data: ['sign-up']
  },
  {
    path: "sign-up-roles",
    component: SignUpRolesComponent,
    canActivate: [RolesAuthGuardService],
    data: ['sign-up-roles']
  },
  {
    path: "email-sent",
    component: EmailsentpageComponent,
    canActivate: [RolesAuthGuardService],
    data: ['email-sent']
  },
  {
    path: "sign-up-details",
    component: SignUpDetailsComponent,
    canActivate: [RolesAuthGuardService],
    data: ['sign-up-details']
  },
  {
    path: "sign-up-details/:data",
    component: SignUpDetailsComponent,
    canActivate: [RolesAuthGuardService],
    data: ['sign-up-details/:data']
  },
  {
    path: "linkedInLogin",
    component: AuthCallbackComponent
  },
  {
    path: "un-authorized",
    component: UnauthorizedComponent
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  declarations: [
    LoginComponent,
    SignUpComponent,
    SignUpRolesComponent,
    EmailsentpageComponent,
    SignUpDetailsComponent,
    UnauthorizedComponent,
    AuthCallbackComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    NgBootstrapFormValidationModule.forRoot(),
  ],
  exports: [RouterModule]
})
export class AuthModule { }
