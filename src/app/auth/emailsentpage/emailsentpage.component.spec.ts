import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailsentpageComponent } from './emailsentpage.component';

describe('EmailsentpageComponent', () => {
  let component: EmailsentpageComponent;
  let fixture: ComponentFixture<EmailsentpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailsentpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailsentpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
