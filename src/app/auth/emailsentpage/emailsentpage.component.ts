import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'emailsentpage',
  templateUrl: './emailsentpage.component.html',
  styleUrls: ['./emailsentpage.component.scss']
})
export class EmailsentpageComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() { }

  routerNavigation(data) {
    this.router.navigate(['landing/landing-page']);
  }

}
