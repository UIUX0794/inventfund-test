import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import Swal from 'sweetalert2';
import { AppStateService } from '../../services/app-state.service';
import * as crypto from 'crypto-js';
import { AuthService, SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import * as serverConfig from '../../../providers/config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  // global variables

  loginForm: FormGroup;
  roleCode: any;
  private user: SocialUser;
  private loggedIn: boolean;
  redirectUrl;

  /**
   *
   * @param fb reactive form builder
   * @param router navigate to different routes
   * @param inventFundsApiService services to get data from database using endpoint apis
   * @param appState get variables from appsate or methods
   * @param authService to authenticate the data
   * @param activatedRoute get current route and data from that url
   */

  constructor(
    fb: FormBuilder,
    private router: Router,
    private inventFundsApiService: InventFundsApiService,
    private appState: AppStateService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute
  ) {
    // get data from activated route params

    this.activatedRoute.queryParams.subscribe((res) => {
      // console.log('queryParams login linked in :' + JSON.stringify(res.email));
      if (res.email) {
        this.linkedIn(res.email);
      }
    });

    // login from for inputes

    this.loginForm = fb.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
      password: [null, Validators.compose([Validators.required])],
    });
  }

  // ng onInit

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = user != null;
    });
  }

  // mehod to save and validate data onclick ob submit button

  /**
   *
   * @param $ev event from the form
   * @param value input values from the form
   */

  submit($ev, value: any) {
    // console.log('form ' + JSON.stringify(value));
    for (let c in this.loginForm.controls) {
      this.loginForm.controls[c].markAsTouched();
    }
    if (this.loginForm.valid) {
      // console.log("Valid!");
      this.login(value);
    } else {
      // console.log('Form invalid!');
    }
  }

  routerNavigation(data) {
    this.router.navigate(['landing/landing-page']);
  }

  // if form validate take form data and login

  login(formValue) {
    var params = {
      collectionName: 'user',
      userName: formValue.email,
      password: formValue.password,
    };

    this.inventFundsApiService.signIn(params).then((res) => {
      // console.log("login response-->" + JSON.stringify(res));

      if (res.code == 200 && res.signUpType == 'Custom') {
        this.setApplicationData(res);
        // this.router.navigate(['/profile/dashboard'])
        this.router.navigate(['profile/editProfile']);
      } else if (res.code == 400 && res.message == 'Invalid password!') {
        //   return this.invalidPassword = true;
        this.loginForm.controls['password'].setErrors({
          invalidPassword: true,
        });
      } else if (res.code == 400 && res.message == 'Invalid Email!') {
        //    return this.invalidEmail = true;
        this.loginForm.controls['email'].setErrors({ invalidEmail: true });
      } else if (res.code == 200 && res.data.signUpType == 'Social') {
        this.swalMessage(
          'You have signed up using ' +
            res.data.signUpProvider +
            '! Please login via ' +
            res.data.signUpProvider
        ).then((result) => {
          this.loginForm.reset();
        });
      }
    });
  }

  // set appsate data and session storage data in terms of to access user data

  setApplicationData(data) {
    console.log('token set local : ', JSON.stringify(data));
    this.appState.globalData.token = data.token;
    this.appState.globalData.roleId = data.roleId;
    this.appState.globalData.roleCode = data.roleCode;
    this.appState.globalData.partyId = data.partyId;
    this.appState.globalData.personId = data.personId;
    this.appState.globalData.personEmail = data.personEmail;
    this.appState.globalData.personType = data.personType;
    this.appState.globalData.orgId = data.orgId;
    this.appState.globalData.partyCode = data.partyCode;
    this.appState.globalData.signUpProvider = data.signUpProvider;
    this.appState.globalData.name = data.name;
    sessionStorage.setItem('passwordType', data.passWordType);
    var profile = crypto.AES.encrypt(
      JSON.stringify(this.appState.globalData),
      this.appState.encryptSecretKey,
      this.appState.encryptOptions
    ).toString();
    sessionStorage.setItem('profile', profile);
    console.log('data for profile : ' + JSON.stringify(data));
    if (data.url != null) {
      this.appState.setProfileImage(data.url);
      // sessionStorage.setItem('profileImage', data.url);
      sessionStorage.setItem('profileImage', data.url);
      this.appState.getProfileImage();
      this.appState.setAvatar(data.url);
    }
    // else if ('path' in data && 'key' in data) {
    //   if (data.path !== null && data.key !== null) {
    //     this.appState.getSignedUrl(data.path, data.key);
    //   } else {
    //     this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
    //   }
    // }
    else {
      this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
    }
    this.appState.encryptProfileJson();
  }

  // signin with goole authentication

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      // console.log('res:' + JSON.stringify(res));
      var params = {
        collectionName: 'user',
        userName: res.email,
        password: '',
      };
      this.inventFundsApiService.signIn(params).then((res) => {
        // console.log('Google login :' + JSON.stringify(res))
        if (res.code == 200 && res.signUpProvider == 'Google') {
          this.setApplicationData(res);
          // this.router.navigate(['/profile/dashboard'])
          this.router.navigate(['profile/editProfile']);
        } else if (res.code == 400 && res.provider == '') {
          this.swalMessage(
            'You have registered using Custom Sign Up.Please use the login form to login'
          ).then((result) => {
            this.loginForm.reset();
          });
        }
      });
    });
  }

  // login with linkedin authentication from data base

  linkedIn(data): void {
    var params = {
      collectionName: 'user',
      userName: data,
      password: '',
    };
    this.inventFundsApiService.signIn(params).then((res) => {
      // console.log('LinkedIn login :' + JSON.stringify(res))
      if (res.code == 200 && res.signUpProvider == 'LinkedIn') {
        this.setApplicationData(res);
        // this.router.navigate(['/profile/dashboard'])
        this.router.navigate(['profile/editProfile']);
      } else if (res.code == 400) {
        this.swalMessage(
          'You have registered using Custom Sign Up.Please use the login form to login'
        ).then((result) => {
          this.loginForm.reset();
        });
      }
    });
  }

  // logout from the application

  signOut(): void {
    this.authService.signOut();
  }

  // to authenticate from linkedin

  signInWithLinkedIn() {
    if (serverConfig.SERVERTYPE == 'DEVELOPMENT') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLDEV;
    } else if (serverConfig.SERVERTYPE == 'TESTING') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLTESTIN;
    } else if (serverConfig.SERVERTYPE == 'UAT') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLUAT;
    } else if (serverConfig.SERVERTYPE == 'PRODUCTION') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLPROD;
    }
    sessionStorage.setItem('page', 'login');
    window.location.href =
      'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ctgyj14vau28&redirect_uri=' +
      this.redirectUrl +
      '&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social';
  }

  /**
   *
   * @param content text to show in swal message and returns the tru or false
   */

  swalMessage(content) {
    return Swal.fire({
      title: content,
      icon: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    })
      .then((result) => {
        return result;
      })
      .catch((err) => {
        return err;
      });
  }
}
