import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import { CustomValidators } from 'ng2-validation';
import { phoneNumberValidator } from '../../utilities/customValidators';

@Component({
  selector: 'app-sign-up-details',
  templateUrl: './sign-up-details.component.html',
  styleUrls: ['./sign-up-details.component.scss']
})
export class SignUpDetailsComponent implements OnInit {

  // global variables

  signUpDetailsForm: FormGroup;
  routerData: any;
  emailId: any;
  userId: any;

  /**
   * 
   * @param fb reactive form builder
   * @param router navigate to different routes
   * @param activatedRoute get activated route for query params
   * @param inventFundsApiService api service to call endpoints
   */

  constructor(
    fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private inventFundsApiService: InventFundsApiService
  ) {

    //Query params of the route:
    this.routerData = activatedRoute.snapshot.params.data;
    if (this.routerData) {
      this.retrieveData(this.routerData);
    }

    // to compare the password and retype password

    let newPasswordFormControl =
      new FormControl('', Validators.compose([Validators.required,
      Validators.pattern("^(?=.*?[A-Z])(?=.*?[A-Za-z])(?=.*?[0-9]).{7,}$")]));
    let confirmPasswordFromControl =
      new FormControl('', [Validators.required,
      CustomValidators.equalTo(newPasswordFormControl)]);
    this.signUpDetailsForm = fb.group({
      'password': newPasswordFormControl,
      'retypePassword': confirmPasswordFromControl,
      'phoneNumber': ['', Validators.compose([Validators.required, phoneNumberValidator])],
    });
  }

  ngOnInit() { }

  /**
   * 
   * @param personId person id to retrive data from database
   */
  // retrive person data from use collection

  retrieveData(personId) {
    var params = {
      "collectionName": "user",
      "queryStr": { "personId": { "$eq": personId } },
    };
    this.inventFundsApiService.retrieveMongoDBAll(params)
      .then(
        (res: any) => {
          if (res.status == 200) {
            console.log("user response : " + JSON.stringify(res));
            this.emailId = res.response[0].userName;
            // this.userId = res.response[0]._id;
          }
        })
      .catch(err => {
        console.log(err);
      });
  }

  // validate and submit form

  submit($ev, value: any) {
    // console.log('form ' + JSON.stringify(value))
    for (let c in this.signUpDetailsForm.controls) {
      this.signUpDetailsForm.controls[c].markAsTouched();
    }
    if (this.signUpDetailsForm.valid) {
      // console.log('Valid!');
      this.updatePasswordDetails(value);
    }
    else {
      // console.log('Form invalid!');
    }
  }

  // reset or update password of user

  updatePasswordDetails(value) {
    var params = {
      "userName": this.emailId,
      "oldPassword": "Forgot Password",
      "newPassword": value.password,
      "confirmPassword": value.retypePassword,
      "phoneNumber": value.phoneNumber
    };

    this.inventFundsApiService.changePassword(params)
      .then(res => {
        // console.log('update response->' + JSON.stringify(res))
        if (res.code == 200) {
          sessionStorage.setItem('personId', res.personId);
          this.router.navigate(['auth/sign-up-roles']);
        } else {
          //error handling for failure to update password
        }
      });
  }

  // to compare password 

  checkPasswords(event) {
    console.log(`hello one
    two
    three`);
    // console.log('pass:' + this.signUpDetailsForm.controls['password'].value + ' re type:' + event)
    if ((this.signUpDetailsForm.controls['password'].value) != (event)) {
      // alert('mismatch');
      this.signUpDetailsForm.controls['retypePassword'].setErrors({ passwordMismatch: true });
    }
  }

  routerNavigation(data) {
    this.router.navigate(['landing/landing-page']);
  }
}
