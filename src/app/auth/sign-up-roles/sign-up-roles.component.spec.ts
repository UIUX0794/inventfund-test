import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpRolesComponent } from './sign-up-roles.component';

describe('SignUpRolesComponent', () => {
  let component: SignUpRolesComponent;
  let fixture: ComponentFixture<SignUpRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
