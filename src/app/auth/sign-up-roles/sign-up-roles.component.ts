import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AppStateService } from '../../services/app-state.service';
declare var $: any;
import * as crypto from 'crypto-js';

@Component({
  selector: 'sign-up-roles',
  templateUrl: './sign-up-roles.component.html',
  styleUrls: ['./sign-up-roles.component.scss'],
})
export class SignUpRolesComponent implements OnInit {
  // global variables

  enable = false;
  selected;
  changeBtn;
  roleChosen: boolean = false;
  roleName;
  founder: boolean;
  funder: boolean;
  fixer: boolean;
  personId: string;
  id: any;
  id1: any;

  /**
   *
   * @param router navigate to different routes
   * @param appState get variables or methods from appstate
   * @param activatedRoute get activated routes and query params
   * @param inventFundsApiService api services to call end points
   */

  constructor(
    private router: Router,
    public appState: AppStateService,
    private activatedRoute: ActivatedRoute,
    private inventFundsApiService: InventFundsApiService
  ) {
    this.personId = sessionStorage.getItem('personId');
  }

  ngOnInit() {}

  // to change css of active block

  Active(event, id) {
    this.enable = true;
    this.id = id;
    this.changeBtn = 'add';
    // console.log(id + "=>id")
    if (this.enable == true && this.id == 0) {
      $('.one').addClass('transition');
      $('.two').removeClass('transition');
      $('.three').removeClass('transition');
      $('.bt').addClass('add');
      $('.bt').removeClass('add');
      $('.bt').removeClass('add');
      $('.founder').addClass('founder1');
      $('.fixer').removeClass('fixer1');
      $('.funder').removeClass('funder1');
      $('.oneb').addClass('btntrans');
      $('.twob').removeClass('btntrans');
      $('.threeb').removeClass('btntrans');
      $('.cd1').addClass('col-centered1');
      $('.cd2').removeClass('col-centered1');
      $('.cd3').removeClass('col-centered1');
      $('.twob').addClass('btt');
      $('.oneb').removeClass('btt');
      $('.threeb').addClass('btt');
      $('.bt').addClass('add');
      this.roleName = 'Invent Fund Founder';
      this.roleChosen = true;
      this.founder = true;
      this.funder = false;
      this.fixer = false;
    }

    if (this.enable == true && this.id == 1) {
      $('.two').addClass('transition');
      $('.one').removeClass('transition');
      $('.three').removeClass('transition');
      $('.fixer').addClass('fixer1');
      $('.founder').removeClass('founder1');
      $('.funder').removeClass('funder1');
      $('.twob').addClass('btntrans');
      $('.oneb').removeClass('btntrans');
      $('.threeb').removeClass('btntrans');
      $('.twob').removeClass('btt');
      $('.oneb').addClass('btt');
      $('.threeb').addClass('btt');
      $('.cd2').addClass('col-centered1');
      $('.cd1').removeClass('col-centered1');
      $('.cd3').removeClass('col-centered1');
      $('.bt').addClass('add');
      this.roleName = 'Invent Fund Fixer';
      this.roleChosen = true;
      this.founder = false;
      this.funder = false;
      this.fixer = true;
    }

    if (this.enable == true && this.id == 2) {
      $('.three').addClass('transition');
      $('.two').removeClass('transition');
      $('.one').removeClass('transition');
      $('.funder').addClass('funder1');
      $('.founder').removeClass('founder1');
      $('.fixer').removeClass('fixer1');
      $('.threeb').addClass('btntrans');
      $('.twob').removeClass('btntrans');
      $('.oneb').removeClass('btntrans');
      $('.cd3').addClass('col-centered1');
      $('.cd2').removeClass('col-centered1');
      $('.cd1').removeClass('col-centered1');
      $('.twob').addClass('btt');
      $('.oneb').addClass('btt');
      $('.threeb').removeClass('btt');
      $('.bt').addClass('add');
      this.roleName = 'Invent Fund Funder';
      this.roleChosen = true;
      this.founder = false;
      this.funder = true;
      this.fixer = false;
    }
  }

  // deactivate card css

  Deactive(event) {
    this.enable = false;
    if (this.id == 0) {
      $('.cd1').removeClass('col-centered1');
      $('.cd1').addClass('col-centered');
      $('.founder').removeClass('founder1');
      $('.founder').addClass('founder');
      $('.one').removeClass('transition');
      $('.one').addClass('one');
      $('.oneb').removeClass('btntrans');
      $('.oneb').addClass('btt');
      $('.bt').removeClass('add');
    }

    if (this.id == 1) {
      $('.cd2').removeClass('col-centered1');
      $('.cd2').addClass('col-centered');
      $('.fixer').removeClass('fixer1');
      $('.fixer').addClass('fixer');
      $('.two').removeClass('transition');
      $('.two').addClass('two');
      $('.twob').removeClass('btntrans');
      $('.twob').addClass('btt');
      $('.bt').removeClass('add');
    }

    if (this.id == 2) {
      $('.cd3').removeClass('col-centered1');
      $('.cd3').addClass('col-centered');
      $('.funder').removeClass('funder1');
      $('.funder').addClass('funder');
      $('.three').removeClass('transition');
      $('.three').addClass('three');
      $('.threeb').removeClass('btntrans');
      $('.threeb').addClass('btt');
      $('.bt').removeClass('add');
    }
  }

  // select generic role

  genericRole() {
    //alert('generic selected')
    this.roleName = 'Generic';
    this.founder = false;
    this.funder = false;
    this.fixer = false;
    this.roleChosen = false;
    var params = {
      personId: this.personId,
      roleName: this.roleName,
    };
    this.inventFundsApiService.updateRoleAndOrgDetails(params).then((res) => {
      // console.log('res of generic role update-->' + JSON.stringify(res))
      if (res.code == 200) {
        this.router.navigate(['/dashboard']);
      }
    });
  }

  // specific roles

  specificRole() {
    //alert('specific selected:'+this.roleName)
    var params = {
      personId: this.personId,
      roleName: this.roleName,
    };
    this.inventFundsApiService.updateRoleAndOrgDetails(params).then((res) => {
      // console.log('res of specific role update-->' + JSON.stringify(res));
      // console.log("get personId from local storage : " + JSON.stringify(sessionStorage.getItem('personId')))
      var params = {
        personId: sessionStorage.getItem('personId'),
      };
      this.inventFundsApiService.getToken(params).then((res) => {
        // console.log("get token : " + JSON.stringify(res))
        if (res.status == 200) {
          this.setApplicationData(res.response);
        }
      });
    });
  }

  // after role setup application data in session storage

  setApplicationData(data) {
    // console.log('data for set password : ' + JSON.stringify(data));
    sessionStorage.setItem('token', data.token);
    this.appState.globalData.token = data.token;
    this.appState.globalData.roleId = data.roleId;
    this.appState.globalData.roleCode = data.roleCode;
    this.appState.globalData.partyId = data.partyId;
    this.appState.globalData.personId = data.personId;
    this.appState.globalData.personEmail = data.personEmail;
    this.appState.globalData.personType = data.personType;
    this.appState.globalData.orgId = data.orgId;
    this.appState.globalData.partyCode = data.partyCode;
    this.appState.globalData.signUpProvider = data.signUpProvider;
    this.appState.globalData.name = data.name;
    sessionStorage.setItem('passwordType', data.passWordType);
    var profile = crypto.AES.encrypt(
      JSON.stringify(this.appState.globalData),
      this.appState.encryptSecretKey,
      this.appState.encryptOptions
    ).toString();
    sessionStorage.setItem('profile', profile);

    if (data.url != null) {
      this.appState.setProfileImage(data.url);
      sessionStorage.setItem('profileImage', data.url);
      // this.appState.getProfileImage('signup-role');
      this.appState.getProfileImage();
      this.appState.setAvatar(data.url);
    }
    // else if ('path' in data && 'key' in data) {
    //   if (data.path !== null && data.key !== null) {
    //     this.appState.getSignedUrl(data.path, data.key);
    //   } else {
    //     this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
    //   }
    // }
    else {
      this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
    }
    // this.router.navigate(['/profile/dashboard'])
    this.router.navigate(['profile/editProfile']);
    this.appState.encryptProfileJson();
  }
}
