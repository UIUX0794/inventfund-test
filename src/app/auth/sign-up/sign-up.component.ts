import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import Swal from 'sweetalert2';
import { AuthService, SocialUser, FacebookLoginProvider } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import * as serverConfig from '../../../providers/config';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  // global variables

  signUpForm: FormGroup;
  private user: SocialUser;
  private loggedIn: boolean;
  socialUserEmail: any;
  redirectUrl;

  /**
   *
   * @param fb formbuilder for reactive forms
   * @param router naviagte to different routes
   * @param inventFundsApiService api services for endpoints to get data
   * @param authService to login or signup using google or linkedin
   * @param activatedRoute ativated routes to get query params
   */

  constructor(
    fb: FormBuilder,
    private router: Router,
    private inventFundsApiService: InventFundsApiService,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute
  ) {
    // query params data from router

    this.activatedRoute.queryParams.subscribe((res) => {
      // console.log("queryParams :" + JSON.stringify(res));
      if (res.email) {
        // this.requestToken();
        this.linkedInSignUp(res.email);
      }
    });

    this.signUpForm = fb.group({
      email: [null, Validators.compose([Validators.required, Validators.email])],
    });
  }

  // ng onInit

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = user != null;
    });
  }

  routerNavigation(data) {
    this.router.navigate(['landing/landing-page']);
  }

  signInWithFB(): void {
    this.authService
      .signIn(FacebookLoginProvider.PROVIDER_ID)
      .then((result) => {
        console.log('facebook login : ' + JSON.stringify(result));
      })
      .catch((err) => {
        console.log('err : ' + JSON.stringify(err));
      });
  }

  // validate and signup user

  submit($ev, value: any) {
    // console.log("form " + JSON.stringify(value));
    for (let c in this.signUpForm.controls) {
      this.signUpForm.controls[c].markAsTouched();
    }
    if (this.signUpForm.valid) {
      // console.log("Valid!");
      this.sendActivationEmail(value);
    } else {
      console.log('Form invalid!');
    }
  }

  // to send activation mail if custom signup

  sendActivationEmail(formValue) {
    var params = {
      roleName: '',
      type: 'Custom',
      provider: '',
      partyCode: '',
      partyType: '',
      partyName: '',
      partyDescription: '',
      orgId: '',
      emailId: formValue.email,
      passwordRequired: 'N',
      personType: '',
      name: '',
      designation: '',
      logIn: 'true',
    };

    this.inventFundsApiService.signUp(params).then((res) => {
      // console.log("sign up response-->" + JSON.stringify(res));
      if (res.code == 200) {
        this.router.navigate(['auth/email-sent']);
      } else {
        this.signUpForm.controls['email'].setErrors({ duplicateEmail: true });
      }
    });
  }

  // signup with google

  signUpWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res) => {
      // console.log("res:" + JSON.stringify(res));
      var dataToCreate = res;
      this.socialUserEmail = res.email;
      var params = {
        collectionName: 'user',
        queryStr: { userName: { $eq: this.socialUserEmail } },
      };
      this.inventFundsApiService.retrieveMongoDBAll(params).then((res) => {
        // console.log("resp-->" + JSON.stringify(res));
        if (res.response.length > 0) {
          Swal.fire({
            text: 'You are already registered with us! Please Login',
            icon: 'warning',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
          }).then((result) => {});
        } else {
          //create new user
          var params = {
            roleName: '',
            type: 'Social',
            provider: 'Google',
            partyCode: '',
            partyType: '',
            partyName: '',
            partyDescription: '',
            orgId: '',
            emailId: this.socialUserEmail,
            passwordRequired: 'N',
            personType: '',
            name: dataToCreate.name,
            photoUrl: dataToCreate.photoUrl,
            firstName: dataToCreate.firstName,
            lastName: dataToCreate.lastName,
            designation: '',
            logIn: 'true',
          };
          // console.log("res params :" + JSON.stringify(params));
          this.inventFundsApiService.signUp(params).then((res) => {
            // console.log("res of google sign up--->" + JSON.stringify(res));
            // console.log("res code--->" + JSON.stringify(res.code));
            // console.log("res personId--->" + JSON.stringify(res.personId));
            if (res.code == 200) {
              sessionStorage.setItem('personId', res.personId);
              sessionStorage.setItem('passwordType', res.passWordType);
              this.router.navigate(['auth/sign-up-roles']);
            }
          });
        }
      });
    });
  }

  // signup with linkedin

  signUpWithLinkedIn(): void {
    if (serverConfig.SERVERTYPE == 'DEVELOPMENT') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLDEV;
    } else if (serverConfig.SERVERTYPE == 'TESTING') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLTESTIN;
    } else if (serverConfig.SERVERTYPE == 'UAT') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLUAT;
    } else if (serverConfig.SERVERTYPE == 'PRODUCTION') {
      this.redirectUrl = serverConfig.SERVERREDRECTURLPROD;
    }
    sessionStorage.setItem('page', 'signup');
    window.location.href =
      'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ctgyj14vau28&redirect_uri=' +
      this.redirectUrl +
      '&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social';
  }

  // create data for user if linkedin signup true

  linkedInSignUp(email) {
    var params = {
      collectionName: 'user',
      queryStr: { userName: { $eq: email } },
    };
    this.inventFundsApiService.retrieveMongoDBAll(params).then((res) => {
      // console.log("resp of user table-->" + JSON.stringify(res));
      if (res.response.length > 0) {
        Swal.fire({
          text: 'You are already registered with us! Please Login',
          icon: 'warning',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
        }).then((result) => {});
      } else {
        //create new user
        var params = {
          roleName: '',
          type: 'Social',
          provider: 'LinkedIn',
          partyCode: '',
          partyType: '',
          partyName: '',
          partyDescription: '',
          orgId: '',
          emailId: email,
          passwordRequired: 'N',
          personType: '',
          photoUrl: null,
          firstName: '',
          lastName: '',
          name: '',
          designation: '',
          logIn: 'true',
        };
        // console.log('params:' + JSON.stringify(params))
        this.inventFundsApiService.signUp(params).then((res) => {
          // console.log("res of Linkedin sign up--->" + JSON.stringify(res));
          // console.log("res code--->" + JSON.stringify(res.code));
          // console.log("res personId--->" + JSON.stringify(res.personId));
          if (res.code == 200) {
            sessionStorage.setItem('personId', res.personId);
            sessionStorage.setItem('passwordType', res.passWordType);
            this.router.navigate(['auth/sign-up-roles']);
          }
        });
      }
    });
  }
}
