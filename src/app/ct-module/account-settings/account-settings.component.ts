import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InventFundsApiService } from 'src/app/services/invent-funds-api-service';
import { JsonPipe } from '@angular/common';
import { ConfigActions } from '../../ThemeOptions/store/config.actions';
import { ThemeOptions } from '../../theme-options';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { AttachmentService } from 'src/app/services/attachment.service';
import { AppStateService } from 'src/app/services/app-state.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent {

  // global variables

  settingActive = 1;
  valForm: FormGroup;
  registeredData;
  passwordDataReset;
  optionsPassword;
  optionsProfile;
  imagePathParams = { "type": "" };

  // Input and outputs for component

  @Input('profile') profileJson;
  @Input('profileImage') profileImage;
  @Input('password') passwordJson;
  @Input('refMapProfile') refMapProfile;
  @Input('refMapPassword') refMapPassword;
  @Output() imageBase64Data: EventEmitter<any> = new EventEmitter<any>();
  @Output() formlyData: EventEmitter<any> = new EventEmitter<any>();
  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();
  @Output() updatePasswordData: EventEmitter<any> = new EventEmitter<any>();
  imageRes: any;
  options;
  setActiveTab = 1;
  dataReset;

  @Input('profileData') set modelData(data) {
    console.log("input profile data : " + JSON.stringify(data));
    this.registeredData = data;
    this.dataReset = { ...data };
  };

  /**
   * 
   * @param fb reactive form builder
   * @param appState get variables or methods from app state
   * @param inventFundsApiService services to call end api's 
   * @param attachmentService returns s3 bucket folder path
   * @param globals theme options
   * @param configActions project config
   */

  constructor(private fb: FormBuilder, public appState: AppStateService,
    private inventFundsApiService: InventFundsApiService,
    public attachmentService: AttachmentService, public activatedRoute: ActivatedRoute,
    public globals: ThemeOptions, public configActions: ConfigActions) {
    this.imagePathParams.type = this.attachmentService.getImagePath("LOGO",
      this.appState.encryptedDataValue('personId'));
    this.activatedRoute.queryParams.subscribe(res => {
      console.log("queryParams:" + JSON.stringify(res.email));
      if (res.setActive) {
        this.setActiveTab = res.setActive;
      }
    });
  }

  ngOnInit() { }

  // cancel profile

  cancelProfile($event) {
    // this.registeredData = null;
    // console.log("data : " + JSON.stringify($event));
    // console.log('registerd data : ' + JSON.stringify(this.registeredData));
    // console.log("reset data : " + JSON.stringify(this.dataReset));
    if ($event == "true") {
      let rest = { ...this.dataReset };
      this.registeredData = rest;
      // console.log("data registeredData : " + JSON.stringify(this.registeredData))
    }
  }

  // cancel password

  cancelPassword() {
    this.passwordDataReset = null;
  }

  // emit image data url

  imageData(event) {
    // this.imageRes=event;
    this.imageBase64Data.emit(event);
  }

  // emit user profile data

  createData(event, type) {
    var profileParams = {
      "data": event,
      "type": type,
      "imageData": this.imageRes
    };
    var passwordChangeParams = {
      "data": event,
      "type": type,
    };

    if (type == 'profile') {
      this.formlyData.emit(profileParams);
    }
    else {
      this.formlyData.emit(passwordChangeParams);
    }
    // console.log("event" + JSON.stringify(event) + "type " + type)
  }

  // update user address

  updateAddressData($event, type) {
    // console.log("data for update : " + JSON.stringify($event))
    var profileParams = {
      "data": $event,
      "type": type,
      "imageData": this.imageRes
    };
    var passwordChangeParams = {
      "data": $event,
      "type": type,
    };

    if (type == 'profile') {
      this.updateData.emit(profileParams);
    }
    else {
      this.updateData.emit(passwordChangeParams);
    }
  }

  // emit updated password

  emitedPasswordUpdate($event) {
    console.log("emitted password for update : " + JSON.stringify($event));
    this.updatePasswordData.emit($event);
  }
}
