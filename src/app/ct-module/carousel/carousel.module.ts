import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent, CarouselSlideElement } from './carousel.component';
import { CarouselDirective } from './carousel.directive';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgbCarouselModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [CarouselComponent, CarouselDirective, CarouselSlideElement],
  imports: [
    CommonModule,
    SlickCarouselModule,
    NgbCarouselModule,
    NgbModule
  ],
  exports: [CarouselComponent, CarouselDirective]
})
export class CarouselModule { }
