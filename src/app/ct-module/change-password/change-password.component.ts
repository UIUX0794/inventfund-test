import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CUSTOM_ERRORS } from '../../utilities/customErrors';
import { InventFundsApiService } from 'src/app/services/invent-funds-api-service';
import { AppStateService } from 'src/app/services/app-state.service';
import { CustomValidators } from 'ng2-validation';
import { AuthService, GoogleLoginProvider } from 'angularx-social-login';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import * as serverConfig from '../../../providers/config';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  // global variables

  changePasswordForm: FormGroup;
  hideCurrentpasswordInput = true;
  buttonEnable = true;
  redirectUrl;

  // emit data password

  @Output() passwordUpdate: EventEmitter<any> = new EventEmitter<any>();

  /**
   * 
   * @param fb reactive form builder
   * @param inventFundApiSer service to call api end points
   * @param appState get variables or methods from app state
   * @param authService to use google and linked in signin
   * @param activatedRoute 
   */

  constructor(public fb: FormBuilder, private inventFundApiSer: InventFundsApiService,
    public appState: AppStateService, private authService: AuthService,
    public activatedRoute: ActivatedRoute) {
    this.typeOfProvider();
    this.activatedRoute.queryParams.subscribe(res => {
      console.log("queryParams:" + JSON.stringify(res.email));
      if (res.email) {
        var dec = res.email;
        if (dec) {
          // this.requestToken();
          this.linked(dec);
        }
      }
    });
  }

  // type of provider ex.LinkedIn / google / custom

  typeOfProvider() {
    // console.log("data for current password : " + JSON.stringify(this.appState.encryptedDataValue('signUpProvider')));
    var params = {
      collectionName: "user",
      queryStr: { userName: { $eq: this.appState.encryptedDataValue('personEmail') } }
    };
    // this.inventFundApiSer.retrieveMongoDBAll(params).then(res => {
    // console.log("response from user table : " + sessionStorage.getItem('passwordType'))
    // console.log("response from user table : " + JSON.stringify(typeProvider))
    //   if (res.status == 200 && res.response.length != 0) {
    var typeProvider = this.appState.encryptedDataValue('signUpProvider');
    if ((typeProvider == 'Google' || typeProvider == 'LinkedIn') &&
      sessionStorage.getItem('passwordType') == 'false') {
      this.hideCurrentpasswordInput = false;
      let newPasswordFormControl = new FormControl('',
        Validators.compose([Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[A-Za-z])(?=.*?[0-9]).{7,}$")]));
      let confirmPasswordFromControl = new FormControl('',
        [Validators.required, CustomValidators.equalTo(newPasswordFormControl)]);
      this.changePasswordForm = this.fb.group({
        // currentPassword: [null, Validators.compose([Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[A-Za-z])(?=.*?[0-9]).{7,}$")])],
        newPassword: newPasswordFormControl,
        confirmPassword: confirmPasswordFromControl
      });
    } else {
      this.hideCurrentpasswordInput = true;
      let newPasswordFormControl = new FormControl('',
        Validators.compose([Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[A-Za-z])(?=.*?[0-9]).{7,}$")]));
      let confirmPasswordFromControl = new FormControl('',
        [Validators.required, CustomValidators.equalTo(newPasswordFormControl)]);
      this.changePasswordForm = this.fb.group({
        currentPassword: [null, Validators.compose([Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[A-Za-z])(?=.*?[0-9]).{7,}$")])],
        newPassword: newPasswordFormControl,
        confirmPassword: confirmPasswordFromControl
      });
      this.changePasswordForm.controls['newPassword'].disable();
      this.changePasswordForm.controls['confirmPassword'].disable();
    }
  }

  ngOnInit() { }

  // compare current password with new password

  compareCurrentPassword() {
    // console.log("val form data : "+JSON.stringify(this.changePasswordForm.value))
    // this.changePasswordForm.controls['confirmPassword'].setErrors({invalidPassword: true})
    var params = {
      "userName": this.appState.encryptedDataValue('personEmail'),
      "currentPassword": this.changePasswordForm.controls['currentPassword'].value,
      "module": "user",
      "endPoint": 'COMPARECURRENTPASSWORD'
    };
    this.inventFundApiSer.retrieveGeneric(params).then((res) => {
      console.log("response current password : " + JSON.stringify(res));
      if (res.code == 200) {
        // this.changePasswordForm.controls['currentPassword'].setErrors({invalidPassword: true})
        this.changePasswordForm.controls['newPassword'].enable();
      } else {
        this.changePasswordForm.controls['currentPassword'].setErrors({ invalidPassword: true });
        this.changePasswordForm.controls['newPassword'].disable();
      }
    });
  }

  // cancel change password

  cancel(form: FormGroup) {
    this.changePasswordForm.reset({});
    // this.changePasswordForm.setValidators(null);
    this.changePasswordForm.setValidators([]);
    this.changePasswordForm.updateValueAndValidity();
    // this.changePasswordForm.controls['currentPassword'].setValue('');
    // this.changePasswordForm.controls['currentPassword'].validator(null);
    this.changePasswordForm.controls['newPassword'].disable();
    this.changePasswordForm.controls['confirmPassword'].disable();

  }

  // compare current and new password

  compareCurrentAndNewPassword($event) {
    console.log('change charater : ' + JSON.stringify($event));
    // console.log("data compare : " + JSON.stringify(this.changePasswordForm.controls['currentPassword'].value));
    var dataValForm = this.changePasswordForm.value;
    // console.log("data compare : " + JSON.stringify(dataValForm['currentPassword']));
    if (dataValForm['currentPassword'] != undefined) {
      if (this.changePasswordForm.controls['newPassword'].value ==
        this.changePasswordForm.controls['currentPassword'].value) {
        this.changePasswordForm.controls['newPassword'].setErrors({ newPasswordCantBeSame: true });
      } else {
        this.changePasswordForm.controls['confirmPassword'].enable();
      }
    }
    this.setButtonEnable();
  }

  // set save button enable if data valid

  setButtonEnable() {
    if (this.changePasswordForm.valid) {
      this.buttonEnable = false;
    } else {
      this.buttonEnable = true;
    }
  }

  // validate and save password

  submit($ev, value: any) {
    // console.log("form " + JSON.stringify(value));
    for (let c in this.changePasswordForm.controls) {
      this.changePasswordForm.controls[c].markAsTouched();
    }
    if (this.changePasswordForm.valid) {
      console.log("Valid!");
      var typeProvider = this.appState.encryptedDataValue('signUpProvider');
      if (typeProvider == 'Google' && sessionStorage.getItem('passwordType') == 'false') {
        // this.hideCurrentpasswordInput = false;

        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
          .then(res => {
            // console.log("res data : " + JSON.stringify(res));
            var params = {
              collectionName: "user",
              userName: res.email,
              password: ''
            };
            this.inventFundApiSer.signIn(params)
              .then(res => {
                console.log('Google login :' + JSON.stringify(res));
                if (res.code == 200 && res.signUpProvider == 'Google') {
                  this.passwordUpdate.emit(this.changePasswordForm.value);
                  // this.login(value);
                } else {
                  Swal.fire({
                    title: 'Please select proper account!',
                    icon: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Okay',
                    customClass: {
                      // container: 'sweet_containerImportant',
                      title: 'swal-title',
                      actions: 'swal2-actions',
                      confirmButton: 'font-large btn btn-wide mb-2 mr-2 btn-primary',
                      // cancelButton: 'sweet_cancelbuttonImportant',
                    }
                  });
                }
              });
          });
      } else if (typeProvider == 'LinkedIn' && sessionStorage.getItem('passwordType') == 'false') {
        // this.inventFundsApiService.linkedInRequestToken({})
        // console.log(sessionStorage.getItem('passwordType') + " data fr ")
        if (serverConfig.SERVERTYPE == 'DEVELOPMENT') {
          this.redirectUrl = serverConfig.SERVERREDRECTURLCHANGEPASSWORDDEV;
        } else if (serverConfig.SERVERTYPE == 'TESTING') {
          this.redirectUrl = serverConfig.SERVERREDRECTURLCHANGEPASSWORDTESTIN;
        } else if (serverConfig.SERVERTYPE == 'UAT') {
          this.redirectUrl = serverConfig.SERVERREDRECTURLCHANGEPASSWORDUAT;
        } else if (serverConfig.SERVERTYPE == 'PRODUCTION') {
          this.redirectUrl = serverConfig.SERVERREDRECTURLCHANGEPASSWORDPROD;
        }
        sessionStorage.setItem("page", "resetPassword");
        const password = this.changePasswordForm.controls['confirmPassword'].value;
        console.log({ password });
        sessionStorage.setItem("tempPassword", window.btoa(password));
        window.location.href = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=81ctgyj14vau28&redirect_uri=' + this.redirectUrl + '&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social';

      } else {
        this.passwordUpdate.emit(this.changePasswordForm.value);
      }
    } else {
      console.log("Form invalid!");
    }
  }

  // linkedin authentication

  linked(email) {
    console.log({ email });
    if (email != 'settings') {
      var params = {
        collectionName: "user",
        userName: email,
        password: ''
      };
      this.inventFundApiSer.signIn(params)
        .then(res => {
          console.log('LinkedIn login :' + JSON.stringify(res));
          if (res.code == 200 && res.signUpProvider == 'LinkedIn') {
            const password = sessionStorage.getItem('tempPassword');
            const convertAtob = window.atob(password);
            this.passwordUpdate.emit({ newPassword: convertAtob });
            sessionStorage.removeItem('tempPassword');
            // this.login(value);
          }
        });
    }
  }

}
