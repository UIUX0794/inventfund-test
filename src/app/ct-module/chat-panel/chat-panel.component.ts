import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ThemeOptions } from 'src/app/theme-options';
import { ChatPanelService } from './services/chat-panel.service';
import { NgForm } from '@angular/forms';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { Message } from './inerfaces/message';
import { AppStateService } from 'src/app/services/app-state.service';

@Component({
  selector: 'app-chat-panel',
  templateUrl: './chat-panel.component.html',
  styleUrls: ['./chat-panel.component.scss'],
})
export class ChatPanelComponent implements OnInit {
  // global variables

  selectedContact: any;
  contacts: any[];
  user: any;
  chat: any;
  sidebarFolded: boolean;
  userId: any;
  private _replyForm: NgForm;
  private _replyInput: ElementRef;
  @ViewChild('replyForm')
  set replyForm(content: NgForm) {
    this._replyForm = content;
  }
  @ViewChild('replyInput')
  set replyInput(content: ElementRef) {
    this._replyInput = content;
  }
  @ViewChild('perfectScroll') perfectScroll: PerfectScrollbarComponent;
  messages: Array<Message>;
  userData: string;
  message: string;
  counter = false;
  activeId = null;
  channelId = null;
  activeChannelId = null;
  avatar$;

  constructor(
    public globals: ThemeOptions,
    private chatPanelService: ChatPanelService,
    public appState: AppStateService
  ) {
    this.resetChat();
    this.getContacts();
    this.messages = [];
    this.avatar$ = this.appState.getAvatar();
  }

  // ng oninit

  ngOnInit() {}

  newMessage(message: any): void {
    this.chatPanelService.send({
      text: message.message,
      user: message.me,
      id: message.me,
      who: message.who,
      time: message.time,
      channelId: this.channelId,
    });
    this.message = '';
  }

  private newMessageEventHandler(event: Message): void {
    // console.log('new message : ' + JSON.stringify(event));
    this.messages.push(event);
  }

  // toggle the chat drawwer

  toggleDrawer() {
    this.resetChat();
    this.globals.toggleDrawer = !this.globals.toggleDrawer;
  }

  // get contacts data for chat notification

  getContacts() {
    var params = {
      senderId: this.appState.encryptedDataValue('personId'),
      reciverId: this.appState.encryptedDataValue('personId'),
      personId: this.appState.encryptedDataValue('personId'),
    };
    this.chatPanelService
      .getContacts(params)
      .then((result) => {
        console.log('contacts data : ' + JSON.stringify(result));
        if (result.status === '200' && result.response.length !== 0) {
          var responseData = result.response;
          var mappingData = responseData.map((value) => {
            var obj = {
              id:
                value.senderId === this.appState.encryptedDataValue('personId')
                  ? value.reciverId === this.appState.encryptedDataValue('personId')
                    ? value.senderId
                    : value.reciverId
                  : value.senderId,
              name: value.personData.length !== 0 ? value.personData[0].firstName : null,
              avatar:
                // value.attachmentData.length !== 0
                //   ? value.attachmentData[0].path
                //   : 'assets/chat-panel/avatars/alice.jpg',

                value.attachmentData.length !== 0
                  ? 'assets/chat-panel/avatars/Arnold.jpg'
                  : 'assets/chat-panel/avatars/Arnold.jpg',
              status: value.personData.length !== 0 ? value.personData[0].status : null,
              channelId: value.chanelId,
              unread:
                value.chatMessagesData.length !== 0 ? value.chatMessagesData[0].readStatus : null,
            };
            return obj;
          });
          this.contacts = mappingData;
        }
      })
      .catch((err) => {
        console.log('err : ' + JSON.stringify(err));
      });
  }

  toggleChat(contact, activeId, channelId): void {
    if (this.counter === false) {
      this.channelId = channelId;
      this.counter = true;
      this.chatPanelService.initialize(channelId);
      this.chatPanelService.messagesStream.subscribe(this.newMessageEventHandler.bind(this));
    }
    if (this.selectedContact && contact.id === this.selectedContact.id) {
      this.resetChat();
    } else {
      this.selectedContact = contact;
      var params = {
        senderId: this.appState.encryptedDataValue('personId'),
        reciverId: activeId,
      };
      var channelIdUpdateParams = {
        senderId: this.appState.encryptedDataValue('personId'),
        reciverId: activeId,
        channelId: channelId,
      };
      this.chatPanelService
        .updateChannelId(channelIdUpdateParams)
        .then((result) => {
          console.log('stats : ' + JSON.stringify(result));
        })
        .catch((err) => {
          console.log('err : ' + JSON.stringify(err));
        });
      this.chatPanelService.getChatData(params).then((chats) => {
        // console.log('chats data : ' + JSON.stringify(chats));
        if (chats.status === '200' && chats.data.length !== 0) {
          this.chatPanelService
            .updateMessageStatus(activeId, this.appState.encryptedDataValue('personId'))
            .then((result) => {
              if (result.status === '200') {
                var contactsArray = [...this.contacts];
                var objIndex = contactsArray.findIndex((obj) => obj.id === activeId);
                contactsArray[objIndex].unread = null;
                this.contacts = contactsArray;
              }
            })
            .catch((err) => {
              console.log('err : ' + JSON.stringify(err));
            });

          var chatsMapping = chats.data;
          this.messages = chatsMapping.map((value) => {
            var obj = {
              text: value.chatMessage,
              user: value.senderId,
              id: value.senderId,
              who: value.reciverId,
              time: value.date,
              channelId: channelId,
            };
            return obj;
          });
        }
        this.userId = this.appState.encryptedDataValue('personId');
        this.activeId = activeId;
        this.activeChannelId = channelId;
        setTimeout(() => {
          this.perfectScroll.directiveRef.scrollToBottom(0); //for update scroll
        });
      });
    }
  }

  resetChat(): void {
    this.selectedContact = null;
    this.chat = null;
    this.messages = [];
    this.counter = false;
    this.channelId = null;
  }

  private _prepareChatForReplies(): void {
    setTimeout(() => {
      this._replyInput.nativeElement.focus();
    });
  }

  unfoldSidebarTemporarily() {}

  isFirstMessageOfGroup(message, i): boolean {
    return i === 0 || (this.messages[i - 1] && this.messages[i - 1].id !== message.id);
  }

  isLastMessageOfGroup(message, i): boolean {
    return (
      i === this.messages.length - 1 ||
      (this.messages[i + 1] && this.messages[i + 1].id !== message.id)
    );
  }

  shouldShowContactAvatar(message, i): boolean {
    return (
      message.id === this.selectedContact.id &&
      ((this.messages[i + 1] && this.messages[i + 1].id !== this.selectedContact.id) ||
        !this.messages[i + 1])
    );
  }

  reply(event): void {
    event.preventDefault();
    if (!this._replyForm.form.value.message) {
      return;
    }
    const message = {
      who: this.activeId,
      me: this.userId,
      message: this._replyForm.form.value.message,
      time: new Date().toISOString(),
    };
    var paramsActiveChannelCount = {
      senderId: this.userId,
      reciverId: this.activeId,
      channelId: this.activeChannelId,
    };
    console.log('params active channel count : ' + JSON.stringify(paramsActiveChannelCount));
    this.chatPanelService
      .getActiveChannelCount(paramsActiveChannelCount)
      .then((result) => {
        console.log('active channel count : ' + JSON.stringify(result));
        if (result.status === '200') {
          var params = {
            collectionName: 'chatMessages',
            updateData: {
              senderId: this.userId,
              reciverId: this.activeId,
              chatMessage: this._replyForm.form.value.message,
              date: new Date().toISOString(),
              readStatus:
                result.response === undefined || result.response === null
                  ? 'unread'
                  : result.response,
              createdDate: new Date().toISOString(),
              createdBy: this.userId,
              updatedDate: new Date().toISOString(),
              updatedBy: this.userId,
            },
          };
          this.chatPanelService
            .createChatData(params)
            .then((result) => {
              this.newMessage(message);
              this._replyForm.reset();

              this._prepareChatForReplies();
              setTimeout(() => {
                this.perfectScroll.directiveRef.scrollToBottom(0); //for update scroll
              });
            })
            .catch((err) => {
              console.log(err);
            });
        }
        this.getContacts();
      })
      .catch((err) => {
        console.log('active channel err : ' + JSON.stringify(err));
      });
  }
}
