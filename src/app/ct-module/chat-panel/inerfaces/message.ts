export interface Message {
  text: string;
  user: string;
  id: string;
  who: string;
  time: Date;
  channelId: string;
}
