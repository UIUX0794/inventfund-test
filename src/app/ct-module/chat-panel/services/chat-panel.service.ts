import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChatUtils } from '../utilities/utils';
import { ReplaySubject } from 'rxjs/Rx';
import { Message } from '../inerfaces/message';
import { PusherService } from './pusher.service';
import * as config from '../../../../providers/config';

@Injectable()
export class ChatPanelService {
  contacts: any[];
  chats: any[];
  user: any;
  messagesStream = new ReplaySubject<Message>(1);
  contactsDataUrl = '../../../assets/mockup/contacts.json';
  userDataUrl = '../../../assets/mockup/users.json';
  chatsDataUrl = '../../../assets/mockup/chats.json';

  constructor(private _httpClient: HttpClient, private pusherService: PusherService) {}

  initialize(id) {
    this.pusherService.messagesChannel = this.pusherService.pusher.subscribe(
      'private-crecientech-chat' + id
    );
    this.pusherService.messagesChannel.bind('client-new-message' + id, (message) => {
      this.emitNewMessage(message);
    });
  }

  send(message: Message) {
    this.pusherService.messagesChannel.trigger('client-new-message' + message.channelId, message);
    this.emitNewMessage(message);
  }

  emitNewMessage(message: Message) {
    this.messagesStream.next(message);
  }

  updateMessageStatus(senderId, reciverId): Promise<any> {
    var params = {
      senderId: senderId,
      reciverId: reciverId,
    };
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.UPDATEMESSAGESTATUS, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  getContacts(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.GETCONTACTS, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  getChatData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.RETRIVECHATS, params).subscribe((response: any) => {
        console.log(response);
        resolve(response);
      }, reject);
    });
  }

  createChatData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.CREATECHATDATA, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  updateChannelId(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.UPDATECHANNELID, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  getActiveChannelCount(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.GETACTIVECHANNELCOUNT, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }
}
