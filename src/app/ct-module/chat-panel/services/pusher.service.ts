import { Injectable } from '@angular/core';
import { environment } from '../enveronment/environment';
declare const Pusher: any;
import * as serverConfig from '../../../../providers/config';

@Injectable({
  providedIn: 'root',
})
export class PusherService {
  pusher: any;
  messagesChannel: any;

  constructor() {
    // var serverConfig;
    if (serverConfig.SERVERTYPE == 'DEVELOPMENT') {
      var redirectUrl = serverConfig.SERVERURLDEV;
    } else if (serverConfig.SERVERTYPE == 'TESTING') {
      var redirectUrl = serverConfig.SERVERURLTEST;
    } else if (serverConfig.SERVERTYPE == 'UAT') {
      var redirectUrl = serverConfig.SERVERREDRECTURLUAT;
    } else if (serverConfig.SERVERTYPE == 'PRODUCTION') {
      var redirectUrl = serverConfig.SERVERREDRECTURLPROD;
    }
    this.pusher = new Pusher(environment.pusher.key, {
      cluster: 'ap2',
      authEndpoint: redirectUrl + 'api/chatPusher/pusher', // 'http://localhost:4000/api/chatPusher/pusher',
    });
  }

  pusherMessage(id: any): any {
    this.messagesChannel = this.pusher.subscribe(id);
  }
}
