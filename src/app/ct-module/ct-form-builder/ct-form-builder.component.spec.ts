import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CtFormBuilderComponent } from './ct-form-builder.component';

describe('CtFormBuilderComponent', () => {
  let component: CtFormBuilderComponent;
  let fixture: ComponentFixture<CtFormBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CtFormBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CtFormBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
