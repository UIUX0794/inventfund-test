import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Router, ActivatedRoute } from '@angular/router';
// import { JwtHelper } from 'angular2-jwt';
// import * as crypto from 'crypto-js';

@Component({
  selector: 'app-ct-form-builder',
  templateUrl: './ct-form-builder.component.html',
  styleUrls: ['./ct-form-builder.component.scss']
})
export class CtFormBuilderComponent implements OnInit {

  // input data for formly

  @Input('formFieldsJson') formFieldsJson;
  encryptedData: any;
  @Input('modelData') set modelData(data) {


    this.dataForUpdate = data;
    this.model = data;
    if (this.dataForUpdate != undefined || this.dataForUpdate != null) {
      this.updateDataFlag = true;
    }
  };

  @Input('refMap') refMap;
  // event emiter for data to save in particular component

  @Output() saveDataEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() updateDataEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Output() cancelDataEmitter: EventEmitter<any> = new EventEmitter<any>();

  form = new FormGroup({});
  @Input('model') model: any = {};
  options: FormlyFormOptions = {};
  @Input('options') set data(val) {
    this.options = val;
  }
  routerData; // data passed in router
  dataForUpdate; // After a to b data in router
  updateDataFlag = false;  // To update data 

  @Input('isDisabled') isDisabled;

  constructor() { }

  ngOnInit() { }

  // --------------------------------------------------------------------
  // onClick of save emit data to save in perticular component
  // --------------------------------------------------------------------

  emitFormlyDataSave(event, type) {
    // alert('Model -->'+JSON.stringify(this.model))
    for (let c in this.form.controls) {
      this.form.controls[c].markAsTouched();
    }
    if (this.form.valid) {
      if (this.updateDataFlag == false) {
        // alert('create'+this.updateDataFlag)
        if (this.refMap) {
          Object.assign(this.form.value, this.refMap);
        }
        this.saveDataEmitter.emit({ response: this.form.value });
      } else {
        // alert('update:'+this.updateDataFlag)
        this.updateDataEmitter.emit({
          response: this.form.value,
          id: this.dataForUpdate._id
        });
      }

    }
  }

  // --------------------------------------------------------------------
  // onClick of cancel emit data
  // --------------------------------------------------------------------

  emitFormlyCancel() {
    this.cancelDataEmitter.emit('true');
  }

}
