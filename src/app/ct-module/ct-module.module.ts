import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { CtFormBuilderComponent } from './ct-form-builder/ct-form-builder.component';
import { validationMessages, validators } from '../utilities/customValidators';
import { FormlyModule } from '@ngx-formly/core';
import { FileUploadModule } from 'ng2-file-upload';
import { SingleAttachmentComponent } from './single-attachment/single-attachment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {
  NgBootstrapFormValidationModule,
  CUSTOM_ERROR_MESSAGES,
} from 'ng-bootstrap-form-validation';
import { NgReduxModule } from '@angular-redux/store';
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';
import { CUSTOM_ERRORS } from '../utilities/customErrors';
import { SocialLoginModule } from 'angularx-social-login';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CountUpModule } from 'ngx-countup';
import CountoModule from 'angular2-counto';
import { MatCheckboxModule } from '@angular/material';
import { ProfileSectionViewComponent } from './profile-section-view/profile-section-view.component';
import { DatepickerTypeComponent } from '../type';
import { ChatPanelComponent } from './chat-panel/chat-panel.component';
import { ChatPanelService } from './chat-panel/services/chat-panel.service';
import { PusherService } from './chat-panel/services/pusher.service';
// import { FlexLayoutModule } from '@angular/flex-layout';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

@NgModule({
  declarations: [
    SingleAttachmentComponent,
    AccountSettingsComponent,
    CtFormBuilderComponent,
    ChangePasswordComponent,
    ProfileSectionViewComponent,
    DatepickerTypeComponent,
    ChatPanelComponent,
  ],
  imports: [
    CommonModule,
    FileUploadModule,

    FormlyModule.forRoot({
      validators: validators,
      validationMessages: validationMessages,
      types: [
        {
          name: 'datepicker',
          component: DatepickerTypeComponent,
          wrappers: ['form-field'],
          defaultOptions: {
            defaultValue: new Date(),
            templateOptions: {
              datepickerOptions: {},
            },
          },
        },
      ],
    }),
    FormsModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgReduxModule,
    SlickCarouselModule,
    NgReduxModule,
    FormlyBootstrapModule,
    AngularFontAwesomeModule,
    PerfectScrollbarModule,
    NgbModule,
    RoundProgressModule,
    CountUpModule,
    CountoModule,
    MatCheckboxModule,
    SocialLoginModule,
    // FlexLayoutModule,
  ],
  exports: [
    SingleAttachmentComponent,
    AccountSettingsComponent,
    CtFormBuilderComponent,
    ChatPanelComponent,
  ],
  providers: [
    ChatPanelService,
    PusherService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    {
      provide: CUSTOM_ERROR_MESSAGES,
      useValue: CUSTOM_ERRORS,
      multi: true,
    },
  ],
})
export class CtModule {}
