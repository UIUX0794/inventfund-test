import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSectionViewComponent } from './profile-section-view.component';

describe('ProfileSectionViewComponent', () => {
  let component: ProfileSectionViewComponent;
  let fixture: ComponentFixture<ProfileSectionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileSectionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSectionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
