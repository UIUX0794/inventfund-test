import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-section-view',
  templateUrl: './profile-section-view.component.html',
  styleUrls: ['./profile-section-view.component.scss']
})
export class ProfileSectionViewComponent implements OnInit {

  title: any;

  @Input('cardTitle') set cardTitle(data: any) {
    if (data) {
      this.title = data;
    }
  };

  constructor() { }

  ngOnInit() { }

}
