import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleAttachmentComponent } from './single-attachment.component';

describe('SingleAttachmentComponent', () => {
  let component: SingleAttachmentComponent;
  let fixture: ComponentFixture<SingleAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
