import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { AppStateService } from '../../services/app-state.service';
const URL = 'api/attachment/uploadDirectToS3';

@Component({
  selector: 'single-attachment',
  templateUrl: './single-attachment.component.html',
  styleUrls: ['./single-attachment.component.scss'],
})
export class SingleAttachmentComponent implements OnInit {
  // global variables

  imageDataAssign;
  image = 'file_upload_label';

  // input and output

  @Output() imageUrl: EventEmitter<any> = new EventEmitter<any>();
  @Input('imageData') imageData;
  @Input() uImagePathParam: any;

  // fileupload required data

  public uUploader: FileUploader = new FileUploader({
    url: URL,
    additionalParameter: { type: 'inventFund' },
    headers: [
      {
        name: 'Accept',
        value: 'application/json',
      },
    ],
    allowedFileType: ['image', 'video', 'pdf', 'doc'],
    maxFileSize: 20 * 1024 * 1024,
  });

  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  /**
   *
   * @param appState get variables and methods from app state
   */

  constructor(public appState: AppStateService) {
    // console.log('each image local : ' + this.appState.profileImageSigned);
    this.setAvatar();
    // if (sessionStorage.getItem('profileImage')) {
    //   this.image = "file_upload_label_img";
    //   this.imageDataAssign = sessionStorage.getItem('profileImage');
    // }
  }

  // ngOninit

  ngOnInit() {
    // set folder path for an attachment URL

    this.uUploader.onBuildItemForm = (item, form) => {
      // console.log("append type : ")
      form.append('TYPE', this.uImagePathParam.type);
    };

    // get response oncomplete of attachment

    this.uUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // console.log('response data : ' + JSON.stringify(response));
      var data: any = response;
      var image = JSON.parse(data).signedData;
      var emitImage = JSON.parse(data).data;
      this.image = 'file_upload_label_img';
      this.appState.setAvatar(image);
      // this.imageDataAssign = this.appState.getAvatar();
      this.imageUrl.emit(emitImage);
    };
  }

  // on file select complete upload in to s3 bucket

  onFileSelected(event) {
    // const json = JSON.stringify(event);
    // console.log("original image : " + json)
    this.uUploader.uploadAll();
  }

  setAvatar() {
    this.appState.getProfileImage();
    // this.appState.getProfileImage('single-attachment');
    // console.log('image data with id : ' + this.appState.getProfileImage('single-attachment'));
    this.image = 'file_upload_label_img';
    this.imageDataAssign = this.appState.getAvatar();
  }
}
