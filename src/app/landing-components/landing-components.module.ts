import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LandingHeaderComponent } from '../layout/landing-header/landing-header.component';
import { SidebarComponent } from '../layout/layout-components/sidebar/sidebar.component';
import { OptionsDrawerComponent } from '../ThemeOptions/options-drawer/options-drawer.component';
import { DrawerComponent } from '../layout/layout-components/header/elements/drawer/drawer.component';
import { CarouselComponent, CarouselSlideElement } from '../ct-module/carousel/carousel.component';
import { CarouselDirective } from '../ct-module/carousel/carousel.directive';
import { GotAnIdeaPageComponent } from '../unused-components/got-an-idea-page/got-an-idea-page.component';
import { RolesAuthGuardService } from '../services/roles-auth-guard.service';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { SocialLoginModule } from 'angularx-social-login';
import { LayoutsModule } from '../shared/layouts.module';

const routes: Routes = [
  { path: '', redirectTo: 'landing-page', pathMatch: 'full' },
  {
    path: 'landing-page',
    component: LandingPageComponent,
    // canActivate: [RolesAuthGuardService],
    // data: ['landing-page']
  },
  { path: '**', redirectTo: 'landing-page' }
];

@NgModule({
  declarations: [
    LandingPageComponent,
    GotAnIdeaPageComponent,
  ],
  imports: [
    CommonModule,
    SlickCarouselModule,
    SocialLoginModule,
    LayoutsModule,
    RouterModule.forChild(routes),
    NgBootstrapFormValidationModule.forRoot(),
  ],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class LandingComponentsModule { }
