import { Component, OnInit, HostListener } from '@angular/core';
// declare var $:any;
import * as $ from 'jquery';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import { HttpClient } from '@angular/common/http';
import { AppStateService } from '../../services/app-state.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent implements OnInit {
  // global variables

  step = 50;
  hideSign = true;
  imageProfile = '../../../assets/images/avatars/profile.jpg';
  avatar$;
  currentRouter;

  /**
   *
   * @param inventFundApiService service to call api end points
   * @param http htpp request to get data from end points
   * @param router navigate to different routes
   * @param appState get variables and methods from appstate
   */

  constructor(
    private inventFundApiService: InventFundsApiService,
    private http: HttpClient,
    private router: Router,
    public appState: AppStateService
  ) {
    // console.log("current router : " + this.appState.currentRouter)
    // console.log("get boolean value : " + JSON.stringify(this.appState.getAvatar()))
    // if (sessionStorage.getItem('profileImage')) {
    //   this.imageProfile = sessionStorage.getItem('profileImage');
    // }
    // if (sessionStorage.getItem('profileImage')) {
    //   this.appState.setAvatar(sessionStorage.getItem('profileImage'));
    // } else {
    //   this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
    // }

    this.appState.getProfileImage();
    this.hideSign = this.appState.getsessionStorageData();
  }

  // get update profile picture

  ngOnInit() {
    // console.log("get currentRouter value landing page : " +
    //   JSON.stringify(this.appState.getRouter()))
    this.avatar$ = this.appState.getAvatar();
    this.currentRouter = this.appState.getRouter();
    this.parallax();
  }

  parallax() {
    $(window).scroll(function () {
      var scroll = $(window).scrollTop();
      if (scroll > 0) {
        $('.navbar').addClass('navbar-scroll');
      } else {
        $('.navbar').removeClass('navbar-scroll');
      }
      if (scroll > 700) {
      } else {
      }
    });
  }

  // routing to different routes dippends on value

  routerNavigation(data) {
    if (data == 'dashboard') {
      this.router.navigate(['/profile/dashboard']);
    } else if (data == 'profileSettings') {
      this.router.navigate(['/profile/settings']);
    } else if (data == 'landing') {
      this.router.navigate(['landing/landing-page']);
    } else if (data == 'profile') {
      this.router.navigate(['/profile/editProfile']);
    } else {
      this.router.navigate(['landing/landing-page']);
      sessionStorage.clear();
      window.location.reload();
    }
  }

  // local images

  items = [
    { img: '../../assets/images/Google.svg' },
    { img: '../../assets/images/Tata_logo.svg' },
    { img: '../../assets/images/Google.svg' },
    { img: '../../assets/images/Tata_logo.svg' },
    { img: '../../assets/images/Google.svg' },
    { img: '../../assets/images/Tata_logo.svg' },
  ];

  // slide config

  slideConfig3 = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
}
