import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppStateService } from 'src/app/services/app-state.service';
import { ThemeOptions } from 'src/app/theme-options';
// import * as profileImage from 'src/assets/images/avatars'

@Component({
  selector: 'app-landing-header',
  templateUrl: './landing-header.component.html',
  styleUrls: ['./landing-header.component.scss'],
})
export class LandingHeaderComponent implements OnInit {
  imageProfile = '../../../assets/images/avatars/profile.jpg';
  hideSign = true;
  avatar$;
  currentRouter;

  constructor(
    private router: Router,
    public appState: AppStateService,
    public globals: ThemeOptions
  ) {
    this.currentRouter = this.router.url;
    // console.log('current router : ' + this.appState.currentRouter);
    // console.log('get boolean value : ' + JSON.stringify(this.appState.getsessionStorageData()));
    // console.log(
    //   'get boolean value : ' + JSON.stringify(this.appState.getProfileImage('landing-header'))
    // );
    this.imageProfile = this.appState.getProfileImage();
    if (sessionStorage.getItem('profileImage')) {
      console.log(
        'get profile image data : ' + JSON.stringify(sessionStorage.getItem('profileImage'))
      );
      this.appState.setAvatar(sessionStorage.getItem('profileImage'));
    } else {
      this.appState.setAvatar('../../../assets/images/avatars/profile.jpg');
    }
    // this.appState.getProfileImage('landing-header');
    this.hideSign = this.appState.getsessionStorageData();
  }

  ngOnInit() {
    // console.log('get boolean value : ' + JSON.stringify(this.appState.getAvatar()));
    // console.log('get currentRouter value : ' + JSON.stringify(this.appState.getRouter()));
    this.avatar$ = this.appState.getAvatar();
    this.currentRouter = this.appState.getRouter();
  }

  routerNavigation(data) {
    if (data == 'dashboard') {
      this.router.navigate(['/profile/dashboard']);
    } else if (data == 'profileSettings') {
      this.router.navigate(['/profile/settings']);
    } else if (data == 'landing') {
      this.router.navigate(['landing/landing-page']);
    } else if (data == 'profile') {
      this.router.navigate(['profile/editProfile']);
    } else {
      this.router.navigate(['landing/landing-page']);
      sessionStorage.clear();
      // window.location.reload();
    }
  }

  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }

  toggleHeaderMobile() {
    this.globals.toggleHeaderMobile = !this.globals.toggleHeaderMobile;
  }
  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }
}
