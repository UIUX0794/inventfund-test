import { Component, OnInit } from '@angular/core';
import { ThemeOptions } from '../../../../../theme-options';
import { AppStateService } from 'src/app/services/app-state.service';
import { Router } from '@angular/router';
import { BaseLayoutComponent } from 'src/app/layout/base-layout/base-layout.component';
import { ConfigActions } from 'src/app/ThemeOptions/store/config.actions';

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
})
export class UserBoxComponent  {
  email: any;
  personType: any;
  baseLayoutComponentObj: BaseLayoutComponent;
  toggleDrawer() {
    this.globals.toggleDrawer = !this.globals.toggleDrawer;
  }

  constructor(

    private appState: AppStateService,
    public router: Router,
    public globals: ThemeOptions, public configActions: ConfigActions
  ) {
   
    this.getUserInfo();
  }

  ngOnInit() {

  }

  getUserInfo() {
    this.email = this.appState.encryptedDataValue('personEmail')
    this.personType = this.appState.encryptedDataValue('personType');

    console.log('email:' + this.email)
  }
  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }

  routeToSettingPage() {
    this.appState.showSideBar=false;
    this.globals.toggleSidebar = !this.globals.toggleSidebar
    this.router.navigate(['settings']);
    this.appState.settingPageClass=true
    this.globals.toggleSidebar = !this.globals.toggleSidebar
    // window.location.reload();
  }

}

