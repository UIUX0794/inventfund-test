import {Component, HostBinding} from '@angular/core';
import {select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {ThemeOptions} from '../../../theme-options';
import { AppStateService } from 'src/app/services/app-state.service';
import { Router } from '@angular/router';
import { ConfigActions } from 'src/app/ThemeOptions/store/config.actions';
import { BaseLayoutComponent } from '../../base-layout/base-layout.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  email: any;


  constructor(public router:Router,private appState:AppStateService,
    public globals: ThemeOptions, public configActions: ConfigActions) {
  
      console.log("header")
    this.checkLoginStatus();
    
    
  }

  isLoggedIn:boolean=false;

  @HostBinding('class.isActive')
  get isActiveAsGetter() {
    return this.isActive;
  }

  isActive: boolean;

  @select('config') public config$: Observable<any>;

  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }

  toggleHeaderMobile() {
    this.globals.toggleHeaderMobile = !this.globals.toggleHeaderMobile;
  }

  checkLoginStatus(){
    //Yet to be implemented-- 

    this.email = this.appState.encryptedDataValue('personEmail')  
    console.log('person email:'+this.email)
    if(this.email){
      this.isLoggedIn=true;
    }
    else{
      this.isLoggedIn=false;
    }
  }



}
