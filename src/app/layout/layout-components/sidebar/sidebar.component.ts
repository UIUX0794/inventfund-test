import { Component, HostListener, OnInit } from '@angular/core';
import { ThemeOptions } from '../../../theme-options';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: ['.active-link-background{background: #e0f3ff;color: #3f6ad8;font-weight: bold;}'],
})
export class SidebarComponent implements OnInit {
  public extraParameter: any;

  //Testing
  menuItems: Array<any>;
  router: Router;
  clicked: boolean = false;
  constructor(
    public globals: ThemeOptions,
    private activatedRoute: ActivatedRoute,
    public menu: MenuService
  ) {
    this.menuItems = menu.getMenu();
  }

  @select('config') public config$: Observable<any>;

  private newInnerWidth: number;
  private innerWidth: number;
  // activeId = 'dashboardsMenu';

  toggleSidebar() {
    this.globals.toggleSidebar = !this.globals.toggleSidebar;
  }

  sidebarHover() {
    // this.globals.sidebarHover = !this.globals.sidebarHover;
  }

  //

  toggleSidebarMobile() {
    this.globals.toggleSidebarMobile = !this.globals.toggleSidebarMobile;
  }

  toggleHeaderMobile() {
    this.globals.toggleHeaderMobile = !this.globals.toggleHeaderMobile;
  }

  ngOnInit() {
    setTimeout(() => {
      this.innerWidth = window.innerWidth;
      if (this.innerWidth < 1200) {
        this.globals.toggleSidebar = true;
      }
    });

    // this.extraParameter = this.activatedRoute.snapshot.firstChild.data.extraParameter;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.newInnerWidth = event.target.innerWidth;

    if (this.newInnerWidth < 1200) {
      this.globals.toggleSidebar = true;
    } else {
      this.globals.toggleSidebar = false;
    }
  }

  Clicked() {
    this.clicked = true;
  }

  toggleItems(item) {
    console.log('toogle message : ' + JSON.stringify(item));
    if (item.text === 'Messages') {
      this.globals.toggleDrawer = !this.globals.toggleDrawer;
    }
  }
}
