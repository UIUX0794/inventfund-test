import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../../services/app-state.service';
import { ThemeOptions } from '../../theme-options';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  /**
   * 
   * @param appState get variables and methods from app state
   * @param globals global theme options
   */

  constructor(private appState: AppStateService, public globals: ThemeOptions) {
    appState.showSideBar = true
  }

  ngOnInit() { }

}
