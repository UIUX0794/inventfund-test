import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import { IDropdownSettings } from 'ng-multiselect-dropdown/multiselect.model';
import { FileUploader } from 'ng2-file-upload';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import { AttachmentService } from '../../services/attachment.service';
import { AppStateService } from '../../services/app-state.service';
import { CustomValidators } from 'ng2-validation';
import { alphaNumericValidator, alphaValidator } from '../../utilities/customValidators';
import * as moment from 'moment';
const URL = 'api/attachment/uploadDirectToS3';

@Component({
  selector: 'app-profile-modal',
  templateUrl: './profile-modal.component.html',
  styleUrls: ['./profile-modal.component.scss'],
})
export class ProfileModalComponent implements OnInit {
  // global variables

  // attachment

  public uUploader: FileUploader = new FileUploader({
    url: URL,
    additionalParameter: { type: 'inventFund' },
    headers: [
      // {
      //   name: 'Accept',
      //   value: 'application/pdf',
      // },
    ],
    allowedFileType: ['image', 'video', 'pdf', 'doc'],
    maxFileSize: 20 * 1024 * 1024,
    queueLimit: 1,
  });

  public hasBaseDropZoneOver: boolean = false;
  uImagePathParam;

  uiImageInObj = {
    locationLocal: 'assets/img/icons/plain-white-background.jpg',
    locationS3Url: '',
    newLocation: '',
    certificateType: '',
    awsS3FileLocation: '',
    type: 'showcase',
    fileName: '',
    size: '',
    data: '',
  };
  arrayToEmit = [];
  disableUpload = false;
  iconList = [
    // array of icon class list based on type
    { type: 'xlsx', icon: 'fa fa-file-excel fa-2x' },
    { type: 'pdf', icon: 'fa fa-file-pdf fa-2x' },
    { type: 'jpg', icon: 'fa fa-file-image fa-2x' },
    { type: 'mp4', icon: 'fa fa-file-image fa-2x' },
    { type: 'jpeg', icon: 'fa fa-file-image fa-2x' },
  ];

  @Input() public data;
  @Output() passEntry: EventEmitter<any> = new EventEmitter();
  @Output() formlyData: EventEmitter<any> = new EventEmitter<any>();

  heading: any;
  profileSummaryFlag: boolean;
  formEdu: FormGroup;
  profDetIndividual: boolean;
  profDetailsIndJson: any;
  formly: boolean;
  profDetailsIndModelData: any;
  locationConfig = {
    geoCountryRestriction: ['in'],
    geoTypes: ['establishment'],
  };
  country = [];
  typeOfOperation: any;
  skillsHeading;
  skillsFlag = false;
  skills: Array<any> = [];
  dropdownSettings: IDropdownSettings = {};
  disabled = false;
  ShowFilter = true;
  showAll = true;
  limitSelection = false;
  selectedItems: Array<any> = [];
  skillsCat: Array<any> = [];
  skillsSet: Array<any> = [];
  educationFlag = false;
  setImage;
  showcaseFlag: boolean;
  isHovering: boolean;
  form: FormGroup;
  updateId;
  selfAssessmentArray = [];

  /**
   *
   * @param activeModal to get active model window for pop up
   * @param formBuilder reactive form builder
   * @param inventFundApi service to get data from end points
   * @param attachmentService attachment service returns the s3 folder structure
   * @param appState get variables and methods from app state
   */

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    public inventFundApi: InventFundsApiService,
    public attachmentService: AttachmentService,
    public appState: AppStateService
  ) {}

  // ngOnInit

  ngOnInit() {
    // console.log("data received:" + JSON.stringify(this.data));
    this.heading = this.data.heading;
    if (this.data.hasOwnProperty('profileSummary')) {
      this.setImage = 'summary';
      this.form = this.formBuilder.group({
        profileSummary: [
          null,
          Validators.compose([
            Validators.required,
            Validators.maxLength(1000),
            Validators.pattern('(?!^ +$)^.+$'),
          ]),
        ],
      });
      this.profileSummaryFlag = true;
      var pfSummary = this.data.profileSummary;
      // this.form.controls['profileSummary'].setValue[this.data.profileSummary]
      this.form.controls['profileSummary'].setValue(pfSummary);
    } else if (this.data.hasOwnProperty('educationalDetails')) {
      this.setImage = 'education';
      // console.log('Educational details Data:' + JSON.stringify(this.data))
      this.heading = this.data.headline;
      this.country = this.data.educationalDetails;
      this.educationFlag = true;
      this.typeOfOperation = this.data.operation;
      var date = new Date();
      // console.log('locationConfig===>'+JSON.stringify(this.locationConfig))
      this.form = this.formBuilder.group({
        country: [null, Validators.required],
        address: [null],
        college: [null],
        education: [
          null,
          Validators.compose([
            Validators.required,
            alphaValidator,
            Validators.pattern('(?!^ +$)^.+$'),
          ]),
        ],
        specialization: [
          null,
          Validators.compose([
            Validators.required,
            alphaValidator,
            Validators.pattern('(?!^ +$)^.+$'),
          ]),
        ],
        from: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.minDate('1980-12-12'),
            CustomValidators.maxDate(date),
          ]),
        ],
        to: [null, Validators.compose([Validators.required])],
        grade: [
          null,
          Validators.compose([
            Validators.required,
            alphaNumericValidator,
            Validators.pattern('(?!^ +$)^.+$'),
          ]),
        ],
        cgpaMax: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.min(1),
            CustomValidators.max(100),
          ]),
        ],
        cgpaScored: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.min(1),
            CustomValidators.max(100),
          ]),
        ],
        percentile: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.min(1),
            CustomValidators.max(100),
          ]),
        ],
        percentage: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.min(1),
            CustomValidators.max(100),
          ]),
        ],
        keyHighlights: [
          null,
          Validators.compose([
            Validators.required,
            alphaNumericValidator,
            Validators.pattern('(?!^ +$)^.+$'),
          ]),
        ],
      });

      if (this.typeOfOperation == 'update') {
        // console.log('Update data->' + JSON.stringify(this.data.updateData))
        for (let c in this.form.controls) {
          // console.log('C--->' + JSON.stringify(c))
          this.form.controls[c].setValue(this.data.updateData[c]);
          this.updateId = this.data.updateData._id;
        }
      }
    } else if (this.data.hasOwnProperty('profDetailsJson')) {
      this.setImage = 'professional';
      // console.log('data received in modal:' + JSON.stringify(this.data))
      this.profDetIndividual = true;
      this.formly = true;
      this.profDetailsIndJson = this.data.profDetailsJson;
      this.heading = this.data.headline;
      this.profDetailsIndModelData = this.data.model[0];
    } else if (this.data.hasOwnProperty('achievementsDetails')) {
      this.setImage = 'achievements';
      // console.log('data received in modal:' + JSON.stringify(this.data))
      this.profDetIndividual = true;
      this.formly = true;
      this.profDetailsIndJson = this.data.achievementsDetails;
      this.heading = this.data.headline;
      this.typeOfOperation = this.data.operation;
      if (this.typeOfOperation == 'update') {
        // console.log('Update data->' + JSON.stringify(this.data.updateData))
        this.profDetailsIndModelData = this.data.updateData;
        this.updateId = this.data.updateData._id;
      }
    } else if (this.data.hasOwnProperty('skillsJson')) {
      this.setImage = 'skills';
      // console.log('Educational details Data:' + JSON.stringify(this.data))
      this.heading = this.data.headline;
      var modelData = this.data.updateData;
      this.skillsFlag = true;
      this.skillsSet = this.data.skillSet;
      this.skillsCat = this.data.skillCategory;
      this.selfAssessmentArray = this.data.selfAssessment;
      // console.log('model details Data:' + JSON.stringify(modelData.rating))
      // console.log('Educational details Data:' + JSON.stringify(this.skillsCat))
      this.typeOfOperation = this.data.operation;
      this.dropdownSettings = {
        singleSelection: false,
        defaultOpen: false,
        idField: 'value',
        textField: 'label',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableCheckAll: false,
        itemsShowLimit: 3,
        allowSearchFilter: true,
      };
      this.form = this.formBuilder.group({
        skillCategory: [null, Validators.required],
        skillSet: [null, Validators.required],
        selfAssessmentScore: [null, Validators.required],
        rating: [10],
      });
      if (this.typeOfOperation == 'update') {
        this.form.controls['skillCategory'].setValue(modelData.skillCategory);
        this.form.controls['skillSet'].setValue(modelData.skillSet);
        this.form.controls['selfAssessmentScore'].setValue(modelData.selfAssessmentScore);
        if (modelData.rating) {
          this.form.controls['rating'].setValue(modelData.rating);
        }
        this.updateId = this.data.updateData._id;
      }
    } else if (this.data.hasOwnProperty('showcase')) {
      this.heading = this.data.headline;
      this.setImage = 'showcase';
      this.showcaseFlag = true;
      // console.log('In showcase:' + JSON.stringify(this.data))
      this.form = this.formBuilder.group({
        description: [
          null,
          Validators.compose([Validators.required, Validators.pattern('(?!^ +$)^.+$')]),
        ],
      });
      this.form.controls['description'].disable();
      this.uUploader.onBuildItemForm = (item, form) => {
        // console.log("append type : " + JSON.stringify(this.attachmentService.getImagePath('SHOWCASE', this.appState.encryptedDataValue('personId'))))
        form.append(
          'TYPE',
          this.attachmentService.getImagePath(
            'SHOWCASE',
            this.appState.encryptedDataValue('personId')
          )
        );
        // form.append("TYPE", 'ShowCase');
      };

      this.uUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        var data: any = response;
        // var image = JSON.parse(data);var data: any = response;
        // var image = JSON.parse(data);
        // console.log("response data : " + JSON.parse(data));
        console.log('response data : ' + response);

        this.uiImageInObj.awsS3FileLocation = JSON.parse(response).signedData;
        this.uiImageInObj.locationLocal = JSON.parse(response).signedData;
        this.uiImageInObj.locationS3Url = JSON.parse(response).signedData;
        this.uiImageInObj.newLocation = JSON.parse(response).signedData;
        this.uiImageInObj.data = JSON.parse(response).data;
        // this.uiImageInObj.certificateType = this.form.controls['description'].value
        var data: any = this.uiImageInObj;
        this.disableUpload = true;
        const json = JSON.stringify(this.uiImageInObj);
        // console.log("final data : " + json)

        this.arrayToEmit.push(this.uiImageInObj);
        this.form.controls['description'].enable();
      };
    }
  }

  // validate and save data

  save(value) {
    // this.activeModal.close({ value, 'action': this.typeOfOperation, 'id': this.updateId });
    if (this.data.hasOwnProperty('showcase')) {
      this.showCaseDataSend(value);
    } else {
      for (let c in this.form.controls) {
        this.form.controls[c].markAsTouched();
      }
      if (this.form.valid) {
        // console.log("Valid! ");
        // console.log("data for attachment : " + JSON.stringify(this.arrayToEmit))
        this.activeModal.close({
          value,
          action: this.typeOfOperation,
          id: this.updateId,
        });
      } else {
        // console.log("Form invalid!");
      }
    }
  }

  // emit show case data

  showCaseDataSend(data) {
    console.log('data for des : ' + JSON.stringify(data));
    console.log('data for url save : ' + JSON.stringify(this.arrayToEmit));
    if (data && this.arrayToEmit.length) {
      var obj = {
        imageUrl: this.arrayToEmit[0].awsS3FileLocation,
        imageType: this.arrayToEmit[0].type,
        description: data.description,
        data: this.arrayToEmit[0].data,
      };
      console.log('data for obj : ' + JSON.stringify(obj));
      this.activeModal.close({ obj });
    }
  }

  // ng4geo auto complete callback

  autoCompleteCallback1(selectedData: any) {
    // console.log('data in auto complete:' + JSON.stringify(selectedData))
    this.form.controls['address'].setValue(selectedData.data);
    this.form.controls['college'].setValue(selectedData.data.name);
  }

  // cancel button to navigate to component

  cancel() {
    this.activeModal.close({ action: 'cancelled' });
  }

  // create data in the component

  createData(event, type) {
    // console.log('Create data event and type:' + JSON.stringify(event) + ' ' + type)
    var profileIndParams = {
      data: event,
      type: type,
      action: 'create',
    };
    this.activeModal.close(profileIndParams);
  }

  // update data in the component

  updateData(event, type) {
    // console.log('Update data event and type:' + JSON.stringify(event) + ' ' + type)
    var profileIndParams = {
      data: event,
      type: type,
      action: 'update',
    };
    this.activeModal.close(profileIndParams);
  }

  // close active model

  cancelData(event) {
    this.activeModal.close({ action: 'cancelled' });
  }

  // ng4geo country change

  countryChange(event) {
    // alert('country change event')
    this.locationConfig.geoCountryRestriction = [];
    // console.log('locationConfig:' + JSON.stringify(this.locationConfig))
    this.locationConfig.geoCountryRestriction.push(event);
  }

  // toggle hover

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  // file upload hover

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  // on file select emit url

  public onFileSelected(event: EventEmitter<File[]>) {
    console.log(event);
    const file: File = event[0];
    this.uUploader.uploadAll();
    for (var i = 0; i < this.uUploader.queue.length; i++) {
      // const json = JSON.stringify(this.uUploader.queue[i].file.name);
      // console.log("original : " + json)
      this.uiImageInObj.type = this.uUploader.queue[i].file.type;
    }
  }

  // remove image from upload array queue

  removeFromQue(item, index) {
    // console.log("remove from array : " + JSON.stringify(this.arrayToEmit))
    this.deleteAttIns3(this.arrayToEmit[index], item);
  }

  // delete attachment from s3 bucket

  deleteAttIns3(array, item) {
    // console.log("image array for delete : " + JSON.stringify(array))
    var arrayDeleteImage = [array];
    var fileData = {
      imageArray: arrayDeleteImage,
    };
    // console.log(">>>>" + JSON.stringify(fileData))
    this.inventFundApi
      .deleatAttachmentFile(fileData)
      .then((res: any) => {
        // console.log("delete res s3 : " + JSON.stringify(res))
        if (res.code == 200) {
          item.remove();
          this.form.reset();
          this.form.controls['description'].disable();
          this.disableUpload = false;
        }
      })
      .catch((err) => {
        console.log('Error:');
      });
  }

  // get type of attachment

  getFileExtension(filename) {
    // this will give you icon class name
    let ext = filename.split('/').pop();
    // console.log("file name : "+JSON.stringify(ext))
    let obj = this.iconList.filter((row) => {
      if (row.type === ext) {
        return true;
      }
    });
    // console.log({obj})
    if (obj.length > 0) {
      let icon = obj[0].icon;
      return icon;
    } else {
      return '';
    }
  }

  toDateValidation(dataType) {
    var to = new Date(this.form.controls['to'].value); //Year, Month, Date
    var from = new Date(this.form.controls['from'].value); //Year, Month, Date
    console.log('to date validation : ' + JSON.stringify(to));
    console.log('to date validation : ' + JSON.stringify(from));
    if (to < from) {
      this.form.controls['to'].setErrors({ fromDateTo: true });
    }
  }
}
