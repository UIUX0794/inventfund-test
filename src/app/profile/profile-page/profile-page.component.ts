import { fixerProfileSchema } from '../../utilities/profileJson';
import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ThemeOptions } from '../../theme-options';
import { AppStateService } from '../../services/app-state.service';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProfileModalComponent } from '../profile-modal/profile-modal.component';
import Swal from 'sweetalert2';
import { forkJoin, Observable } from 'rxjs';
import * as profileJson from '../../utilities/profileJson';
import { MatSelect } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit {
  // global variables

  profileData: any;
  profileSummaryJson: any;
  modelData: any;
  currentRouter;
  type: any = 'Individual/Freelancer';
  form: FormGroup;
  closeResult: string;
  profileSummaryExists: boolean;
  existingProfileSummaryId: any;
  employmentType: any;
  lookupArray = [];
  individualProfessionalDetails = [];
  achievementsDetails = [];
  educationDetails = [];
  fullProfileInfo;
  fixerProfileSchema = profileJson.fixerProfileSchema;
  founderProfileSchema = profileJson.founderProfileSchema;
  funderProfileSchema = profileJson.funderProfileSchema;
  show = false;
  skillSet = [];
  skillCategory = [];
  skillsDetails = [];
  editmainsection = false;
  showcaseArray = [];
  slideConfig4 = {
    slidesToShow: 3,
    dots: true,
  };
  profileStatus;
  typeOfRole;
  types: Object[] = [
    { value: 'value-0', viewValue: 'Individual' },
    { value: 'value-1', viewValue: 'Seed Funder' },
    { value: 'value-2', viewValue: 'Organization' },
    { value: 'value-3', viewValue: 'Venture Capital' },
  ];
  selectedValue;
  @ViewChild('matSelect') matSelect: MatSelect;
  loading = true;
  profileImg;
  icon: boolean = false;
  sampleImageArray;
  updateSummary;

  /**
   *
   * @param globals setting global options
   * @param appState get variables and methods from app state
   * @param inventFundsApiService services to call api end points
   * @param router navigate to different routes
   * @param formBuilder reactive form builder
   * @param sanitizer
   * @param modalService open model service for popup
   */

  constructor(
    public globals: ThemeOptions,
    public appState: AppStateService,
    private inventFundsApiService: InventFundsApiService,
    private router: Router,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef
  ) {
    this.globals.toggleSidebar = true;
    this.currentRouter = this.router.url;
    this.appState.showSideBar = true;
  }

  // ng onInit

  async ngOnInit() {
    this.typeOfRole = this.appState.encryptedDataValue('personType');
    // console.log('this.typeOfRole: ' + this.typeOfRole)

    this.form = this.formBuilder.group({
      roleType: [null],
    });
    this.currentRouter = this.appState.getRouter();

    this.retrieveProfileInfo();
    this.getProfileSummaryJson();
    this.getProfileSummary();
    this.getSkillsDetails(null);
    this.getEducationData();

    // console.log('profile img:' + localStorage.getItem('profileImage'));
    this.profileImg = this.appState.getAvatar();

    this.fullProfileInfo = await this.retrieveAllProfileInfo().toPromise();
    //  this.fullProfileInfo = profileJson.fixerProfileSchema
    // console.log('fullProfileInfo:' + JSON.stringify(this.fullProfileInfo))

    if (this.typeOfRole == 'Invent Fund Fixer') {
      // console.log('Fixer data : ' + JSON.stringify(this.fullProfileInfo));
      Object.assign(
        this.fixerProfileSchema.fixerPersonDetails,
        this.fullProfileInfo[0].response[0]
      );
      Object.assign(
        this.fixerProfileSchema.fixerProfileSummary,
        this.fullProfileInfo[1].response[0]
      );
      Object.assign(
        this.fixerProfileSchema.fixerProfessionalDetails,
        this.fullProfileInfo[2].response
      );
      Object.assign(
        this.fixerProfileSchema.fixerEducationalDetails,
        this.fullProfileInfo[3].response
      );
      Object.assign(
        this.fixerProfileSchema.fixerAchievementDetails,
        this.fullProfileInfo[4].response
      );
      Object.assign(this.fixerProfileSchema.fixerSkillDetails, this.fullProfileInfo[5].response);
      // console.log('show case data : ' + JSON.stringify(this.fullProfileInfo[6].response));
      const showCaseData = [...this.fullProfileInfo[6].response];
      // Object.assign(this.fixerProfileSchema.showcaseDetails, showCaseData);

      this.fixerProfileSchema.showcaseDetails = showCaseData;
      Object.assign(this.showcaseArray, this.fullProfileInfo[6].response);
      // console.log('show case array : ' + JSON.stringify(this.fullProfileInfo));
      this.recomputeAttachmentArray();
      // this.showcaseArray.map((item) => {
      //   console.log('item-->' + JSON.stringify(item));
      //   item.url = this.sanitizeUrl(item.url);
      //   item.render = item.url;
      // });
      // this.reload();

      // console.log('sanitized->' + JSON.stringify(this.showcaseArray))

      this.setDataProfileStatus(
        this.fixerProfileSchema.fixerProfileSummary,
        this.fixerProfileSchema.fixerEducationalDetails,
        this.fixerProfileSchema.fixerProfessionalDetails,
        this.fixerProfileSchema.fixerAchievementDetails,
        this.fixerProfileSchema.fixerSkillDetails
      );
      this.profileStatus = this.appState.getProfileStatus();
      // console.log('fixer profile data:' + JSON.stringify(this.fixerProfileSchema))

      // console.log('Showcase details:' + JSON.stringify(this.fixerProfileSchema.showcaseDetails))
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      // console.log('founder data : ' + JSON.stringify(this.fullProfileInfo));
      Object.assign(
        this.founderProfileSchema.founderPersonDetails,
        this.fullProfileInfo[0].response[0]
      );
      Object.assign(
        this.founderProfileSchema.founderProfileSummary,
        this.fullProfileInfo[1].response[0]
      );
      Object.assign(
        this.founderProfileSchema.founderEducationalDetails,
        this.fullProfileInfo[2].response
      );
      Object.assign(this.showcaseArray, this.fullProfileInfo[3].response);
      this.recomputeAttachmentArray();
      // this.showcaseArray.map((item) => {
      //   console.log('item-->' + JSON.stringify(item));
      //   item.url = this.sanitizeUrl(item.url);
      // });
      // this.reload();
      this.setDataProfileStatus(
        this.founderProfileSchema.founderProfileSummary,
        this.founderProfileSchema.founderEducationalDetails,
        [],
        [],
        []
      );
      this.profileStatus = this.appState.getProfileStatus();
      // console.log('founder profile data:' + JSON.stringify(this.founderProfileSchema))
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      // console.log('Funder data : ' + JSON.stringify(this.fullProfileInfo));
      Object.assign(
        this.funderProfileSchema.funderPersonDetails,
        this.fullProfileInfo[0].response[0]
      );
      Object.assign(
        this.funderProfileSchema.funderProfileSummary,
        this.fullProfileInfo[1].response[0]
      );
      Object.assign(
        this.funderProfileSchema.funderEducationalDetails,
        this.fullProfileInfo[2].response
      );
      Object.assign(this.showcaseArray, this.fullProfileInfo[3].response);
      this.recomputeAttachmentArray();
      // this.showcaseArray.map((item) => {
      //   console.log('item-->' + JSON.stringify(item));
      //   item.url = this.sanitizeUrl(item.url);
      // });
      // this.reload();
      this.setDataProfileStatus(
        this.funderProfileSchema.funderProfileSummary,
        this.funderProfileSchema.funderEducationalDetails,
        [],
        [],
        []
      );
      this.profileStatus = this.appState.getProfileStatus();
      // console.log('funder profile data:' + JSON.stringify(this.funderProfileSchema))
    }

    if (this.funderProfileSchema.funderProfileSummary.personType) {
      this.types.map((item) => {
        // console.log("item:" + JSON.stringify(item) + ', ' + this.funderProfileSchema.funderProfileSummary
        //   .personType);
        if (
          this.funderProfileSchema.funderProfileSummary.personType == item['value'] ||
          this.funderProfileSchema.funderProfileSummary.personType == item['viewValue']
        ) {
          // alert('item["value"]:'+item["value"])
          this.selectedValue = item['value'];
          this.form.controls['roleType'].setValue(item['value']);
          // this.form.controls['roleType'].markAsUntouched
        }
      });
      // this.selectedValue = this.funderProfileSchema.funderProfileSummary.personType
    } else {
      this.selectedValue = 'value-0';
    }
  }

  // set data for profile status

  setDataProfileStatus(data1, data2, data3, data4, data5) {
    // console.log("data 1 : " + Object.keys(data1).length)
    // console.log("data 2 : " + data2.length)
    // console.log("data 3 : " + data3.length)
    // console.log("data 4 : " + data4.length)
    // console.log("data 5 : " + data5.length)
    this.appState.setDataProfile(data1, data2, data3, data4, data5);
  }

  //Fork Join to retrieve all profile related details

  retrieveAllProfileInfo(): Observable<any> {
    var profileCol, professionCol, educationCol, achievementCol, skillCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      profileCol = 'fixerProfileSummary';
      professionCol = 'fixerProfessionalDetails';
      educationCol = 'fixerEducationDetails';
      (achievementCol = 'fixerAchievementDetails'), (skillCol = 'fixerSkillDetails');
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      profileCol = 'founderProfileSummary';
      professionCol = '';
      educationCol = 'founderEducationDetails';
      (achievementCol = ''), (skillCol = '');
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      profileCol = 'funderProfileSummary';
      professionCol = '';
      educationCol = 'funderEducationDetails';
      (achievementCol = ''), (skillCol = '');
    }

    var person = {
      collectionName: 'person',
      _id: this.appState.encryptedDataValue('personId'),
      endPoint: 'RETRIVEPROFILE',
    };
    var attachment = {
      collectionName: 'attachment',
      queryStr: {
        $and: [{ personId: this.appState.encryptedDataValue('personId') }, { type: 'SHOWCASE' }],
      },
      endPoint: 'RETRIEVEATTACHMENTDATA',
    };
    var profileSummary = {
      collectionName: profileCol,
      endPoint: 'RETRIEVEPROFILESUMMARY',
      queryStr: { personId: this.appState.encryptedDataValue('personId') },
    };
    var professionalDetails = {
      collectionName: professionCol,
      endPoint: 'RETRIEVEPROFESSIONALDETAILS',
      queryStr: { personId: this.appState.encryptedDataValue('personId') },
    };
    var educationDetails = {
      collectionName: educationCol,
      endPoint: 'RETRIEVEEDUCATIONDETAILS',
      queryStr: { personId: this.appState.encryptedDataValue('personId') },
    };
    var achievementDetails = {
      collectionName: achievementCol,
      endPoint: 'RETRIEVEACHIDETAILS',
      queryStr: { personId: this.appState.encryptedDataValue('personId') },
    };
    var skillDetails = {
      collectionName: skillCol,
      endPoint: 'RETRIEVESKILLSDETAILS',
      queryStr: {
        personId: this.appState.encryptedDataValue('personId'),
      },
    };
    if (this.typeOfRole == 'Invent Fund Fixer') {
      return forkJoin([
        this.inventFundsApiService.retrieveGeneric(person),
        this.inventFundsApiService.retrieveGeneric(profileSummary),
        this.inventFundsApiService.retrieveGeneric(professionalDetails),
        this.inventFundsApiService.retrieveGeneric(educationDetails),
        this.inventFundsApiService.retrieveGeneric(achievementDetails),
        this.inventFundsApiService.retrieveGeneric(skillDetails),
        this.inventFundsApiService.retrieveGeneric(attachment),
      ]);
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      return forkJoin([
        this.inventFundsApiService.retrieveGeneric(person),
        this.inventFundsApiService.retrieveGeneric(profileSummary),
        this.inventFundsApiService.retrieveGeneric(educationDetails),
        this.inventFundsApiService.retrieveGeneric(attachment),
      ]);
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      return forkJoin([
        this.inventFundsApiService.retrieveGeneric(person),
        this.inventFundsApiService.retrieveGeneric(profileSummary),
        this.inventFundsApiService.retrieveGeneric(educationDetails),
        this.inventFundsApiService.retrieveGeneric(attachment),
      ]);
    }
  }

  // edit main section

  onclick() {
    var data = { ...this.modelData };
    this.updateSummary = data;
    // console.log('update modelData : ' + JSON.stringify(this.modelData));
    this.editmainsection = true;
  }

  // change or toggle icon

  click() {
    this.icon = !this.icon;
  }

  // retrive profile info

  retrieveProfileInfo() {
    var params = {
      collectionName: 'person',
      _id: this.appState.encryptedDataValue('personId'),
      endPoint: 'RETRIVEPROFILE',
    };
    this.inventFundsApiService.retrieveGeneric(params).then((res: any) => {
      // console.log('resp:' + JSON.stringify(res.response))
      this.profileData = res.response[0].personType;
    });
  }

  // get skill details

  getSkillsDetails(data) {
    var params = {
      collectionName: 'fixerSkillDetails',
      endPoint: 'RETRIEVESKILLSDETAILS',
      queryStr: { personId: this.appState.encryptedDataValue('personId') },
    };

    this.inventFundsApiService
      .retrieveGeneric(params)
      .then((res) => {
        this.skillsDetails = [];
        this.fixerProfileSchema.fixerSkillDetails = [];
        // console.log('achi response!' + JSON.stringify(res))
        if (res.status == 200 && res.response.length != 0) {
          res.response.map((item) => {
            this.skillsDetails.push(item);
            this.fixerProfileSchema.fixerSkillDetails.push(item);
          });
          this.setDataProfileStatus(
            this.fixerProfileSchema.fixerProfileSummary,
            this.fixerProfileSchema.fixerEducationalDetails,
            this.fixerProfileSchema.fixerProfessionalDetails,
            this.fixerProfileSchema.fixerAchievementDetails,
            this.fixerProfileSchema.fixerSkillDetails
          );
        } else if (res.status == 200) {
          this.setDataProfileStatus(
            this.fixerProfileSchema.fixerProfileSummary,
            this.fixerProfileSchema.fixerEducationalDetails,
            this.fixerProfileSchema.fixerProfessionalDetails,
            this.fixerProfileSchema.fixerAchievementDetails,
            this.fixerProfileSchema.fixerSkillDetails
          );
        }
        if (res.status == 200) {
          if (data == 'create') {
            this.inventFundsApiService.showAlertCreateMessage();
          } else if (data == 'update') {
            this.inventFundsApiService.showAlertUpdateMessage();
          } else if (data == 'delete') {
            this.inventFundsApiService.showAlertDeleteMessage();
          }
        } else {
          if (data == 'create') {
            this.inventFundsApiService.showAlertCreateErrorMessage();
          } else if (data == 'update') {
            this.inventFundsApiService.showAlertUpdateErrorMessage();
          } else if (data == 'delete') {
            this.inventFundsApiService.showAlertDeleteErrorMessage();
          }
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // profileSummary screen builder data

  getProfileSummaryJson() {
    var params = {
      collectionName: 'profileSummary',
    };
    this.inventFundsApiService.retrieveScreenbuilderAll(params).then((res) => {
      this.profileSummaryJson = res.response;
      // console.log("profileSummary res " + JSON.stringify(res.response))
    });
  }

  // get profile summary

  getProfileSummary() {
    var profileCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      profileCol = 'fixerProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      profileCol = 'founderProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      profileCol = 'funderProfileSummary';
    }
    var params = {
      collectionName: profileCol,
      endPoint: 'RETRIEVEPROFILESUMMARY',
    };
    this.inventFundsApiService
      .retrieveGeneric(params)
      .then((res) => {
        // console.log('Profile summary response!' + JSON.stringify(res));
        res.response.map((item) => {
          if (item.personId == this.appState.encryptedDataValue('personId')) {
            // alert('id already exists')
            this.profileSummaryExists = true;
            this.existingProfileSummaryId = item._id;
            this.type = item.type;
            // this.updateSummary = item;
            this.modelData = item;
          }
        });
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // get achivements details

  getAchiDetails(data) {
    var params = {
      collectionName: 'fixerAchievementDetails',
      endPoint: 'RETRIEVEACHIDETAILS',
      queryStr: { personId: this.appState.encryptedDataValue('personId') },
    };

    this.inventFundsApiService
      .retrieveGeneric(params)
      .then((res) => {
        this.achievementsDetails = [];
        this.fixerProfileSchema.fixerAchievementDetails = [];
        var achievement;
        // console.log('achi response!' + JSON.stringify(res))
        if (res.status == 200 && res.response.length != 0) {
          res.response.map((item) => {
            // console.log('ITEM:' + JSON.stringify(item))
            this.achievementsDetails.push(item);
            this.fixerProfileSchema.fixerAchievementDetails.push(item);
          });
          this.setDataProfileStatus(
            this.fixerProfileSchema.fixerProfileSummary,
            this.fixerProfileSchema.fixerEducationalDetails,
            this.fixerProfileSchema.fixerProfessionalDetails,
            this.fixerProfileSchema.fixerAchievementDetails,
            this.fixerProfileSchema.fixerSkillDetails
          );

          // var index = this.fixerProfileSchema.fixerAchievementDetails.findIndex(p => p._id == achievement._id)
          // this.fixerProfileSchema.fixerAchievementDetails.splice(index, 1);
        } else if (res.status == 200) {
          this.setDataProfileStatus(
            this.fixerProfileSchema.fixerProfileSummary,
            this.fixerProfileSchema.fixerEducationalDetails,
            this.fixerProfileSchema.fixerProfessionalDetails,
            this.fixerProfileSchema.fixerAchievementDetails,
            this.fixerProfileSchema.fixerSkillDetails
          );
        }
        if (res.status == 200) {
          if (data == 'create') {
            this.inventFundsApiService.showAlertCreateMessage();
          } else if (data == 'update') {
            this.inventFundsApiService.showAlertUpdateMessage();
          } else if (data == 'delete') {
            this.inventFundsApiService.showAlertDeleteMessage();
          }
        } else {
          if (data == 'create') {
            this.inventFundsApiService.showAlertCreateErrorMessage();
          } else if (data == 'update') {
            this.inventFundsApiService.showAlertUpdateErrorMessage();
          } else if (data == 'delete') {
            this.inventFundsApiService.showAlertDeleteErrorMessage();
          }
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // create profile summary

  createProfileSummaryData(event) {
    var profileCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      profileCol = 'fixerProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      profileCol = 'founderProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      profileCol = 'funderProfileSummary';
    }
    // console.log("create profile summary:" + JSON.stringify(event))
    var personSummaryData = event.response;
    Object.assign(personSummaryData, { personId: this.appState.encryptedDataValue('personId') });
    Object.assign(personSummaryData, { personType: 'Individual' });

    if (this.typeOfRole == 'Invent Fund Funder') {
      // alert(this.selectedValue);
      var personType;
      this.types.map((item) => {
        // console.log('item:' + JSON.stringify(item));
        if (this.selectedValue == item['value']) {
          personType = item['viewValue'];
        }
      });
      Object.assign(personSummaryData, { personType: personType });
    }

    var profile = {
      collectionName: profileCol,
      updateData: personSummaryData,
      endPoint: 'CREATEPROFILESUMMARY',
    };
    if (!this.profileSummaryExists) {
      this.inventFundsApiService
        .createGeneric(profile)
        .then((res: any) => {
          // this.getProfile();

          // console.log('create response-->' + JSON.stringify(res));
          if (res.data) {
            // alert('create successful');
            this.modelData = res.data[0];
            // this.updateSummary = res.data[0];
            this.type = res.data[0].type;
            this.profileSummaryExists = true;
            this.existingProfileSummaryId = res.data[0]._id;

            if (this.typeOfRole == 'Invent Fund Fixer') {
              this.fixerProfileSchema.fixerProfileSummary = res.data[0];
              this.setDataProfileStatus(
                this.fixerProfileSchema.fixerProfileSummary,
                this.fixerProfileSchema.fixerEducationalDetails,
                this.fixerProfileSchema.fixerProfessionalDetails,
                this.fixerProfileSchema.fixerAchievementDetails,
                this.fixerProfileSchema.fixerSkillDetails
              );
            } else if (this.typeOfRole == 'Invent Fund Founder') {
              this.founderProfileSchema.founderProfileSummary = res.data[0];
              this.setDataProfileStatus(
                this.founderProfileSchema.founderProfileSummary,
                this.founderProfileSchema.founderEducationalDetails,
                [],
                [],
                []
              );
            } else if (this.typeOfRole == 'Invent Fund Funder') {
              this.funderProfileSchema.funderProfileSummary = res.data[0];
              this.setDataProfileStatus(
                this.funderProfileSchema.funderProfileSummary,
                this.funderProfileSchema.funderEducationalDetails,
                [],
                [],
                []
              );
            }

            this.inventFundsApiService.showAlertCreateMessage();
          }
        })
        .catch((err) => {
          console.log('Error:' + err);
        });
    } else {
      this.updateProfileSummarySection(personSummaryData);
    }
    this.editmainsection = false;
  }

  // update profile summary

  updateProfileSummaryData(event) {
    // console.log("update profile summary:" + JSON.stringify(event))
    var profileCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      profileCol = 'fixerProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      profileCol = 'founderProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      profileCol = 'funderProfileSummary';
    }
    var emittedData = event.response;
    var profileSummary = {
      collectionName: profileCol,
      pkId: event.id,
      updateData: emittedData,
      endPoint: 'UPDATEPROFILESUMMARY',
    };
    this.inventFundsApiService
      .updateGeneric(profileSummary)
      .then((res: any) => {
        // console.log('update response-->' + JSON.stringify(res));
        // this.type = res.data.type;
        var params = {
          collectionName: profileCol,
          _id: res.data._id,
          endPoint: 'RETRIEVEPROFILESUMMARYONE',
        };
        this.inventFundsApiService.retrieveGeneric(params).then((res2) => {
          // console.log('retrieve one response-->' + JSON.stringify(res2));
          // this.type = res2.response[0].type;
          if (this.typeOfRole == 'Invent Fund Fixer') {
            this.fixerProfileSchema.fixerProfileSummary = res2.response[0];
            this.setDataProfileStatus(
              this.fixerProfileSchema.fixerProfileSummary,
              this.fixerProfileSchema.fixerEducationalDetails,
              this.fixerProfileSchema.fixerProfessionalDetails,
              this.fixerProfileSchema.fixerAchievementDetails,
              this.fixerProfileSchema.fixerSkillDetails
            );
          } else if (this.typeOfRole == 'Invent Fund Founder') {
            this.founderProfileSchema.founderProfileSummary = res2.response[0];
            this.setDataProfileStatus(
              this.founderProfileSchema.founderProfileSummary,
              this.founderProfileSchema.founderEducationalDetails,
              [],
              [],
              []
            );
          } else if (this.typeOfRole == 'Invent Fund Funder') {
            this.funderProfileSchema.funderProfileSummary = res2.response[0];
            this.setDataProfileStatus(
              this.funderProfileSchema.funderProfileSummary,
              this.funderProfileSchema.funderEducationalDetails,
              [],
              [],
              []
            );
          }
        });

        this.inventFundsApiService.showAlertUpdateMessageNoReload();
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
    this.editmainsection = false;
  }

  // update profile summary section

  updateProfileSummarySection(data) {
    var profileCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      profileCol = 'fixerProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      profileCol = 'founderProfileSummary';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      profileCol = 'funderProfileSummary';
    }
    // console.log('Profile summary section data:' + JSON.stringify(data))
    if (this.profileSummaryExists) {
      //update
      // existingProfileSummaryId
      var profileSummary = {
        collectionName: profileCol,
        pkId: this.existingProfileSummaryId,
        updateData: data,
        endPoint: 'UPDATEPROFILESUMMARY',
      };
      this.inventFundsApiService
        .updateGeneric(profileSummary)
        .then((res) => {
          // console.log('updated profile sumamry section response->' + JSON.stringify(res))
          this.modelData = res.data;
          // this.updateSummary = res.data;
          //f this.type = res.data[0].type;
          var params = {
            collectionName: profileCol,
            _id: res.data._id,
            endPoint: 'RETRIEVEPROFILESUMMARYONE',
          };
          this.inventFundsApiService.retrieveGeneric(params).then((res2) => {
            // console.log('retrieve one response-->' + JSON.stringify(res2));
            // this.type = res2.response[0].type;
            this.modelData.profileSummary = res2.response[0].profileSummary;
            if (this.typeOfRole == 'Invent Fund Fixer') {
              this.fixerProfileSchema.fixerProfileSummary.profileSummary =
                res2.response[0].profileSummary;
              this.setDataProfileStatus(
                this.fixerProfileSchema.fixerProfileSummary,
                this.fixerProfileSchema.fixerEducationalDetails,
                this.fixerProfileSchema.fixerProfessionalDetails,
                this.fixerProfileSchema.fixerAchievementDetails,
                this.fixerProfileSchema.fixerSkillDetails
              );
            } else if (this.typeOfRole == 'Invent Fund Founder') {
              this.founderProfileSchema.founderProfileSummary.profileSummary =
                res2.response[0].profileSummary;
              this.setDataProfileStatus(
                this.founderProfileSchema.founderProfileSummary,
                this.founderProfileSchema.founderEducationalDetails,
                [],
                [],
                []
              );
            } else if (this.typeOfRole == 'Invent Fund Funder') {
              this.funderProfileSchema.funderProfileSummary.profileSummary =
                res2.response[0].profileSummary;
              this.setDataProfileStatus(
                this.funderProfileSchema.funderProfileSummary,
                this.funderProfileSchema.funderEducationalDetails,
                [],
                [],
                []
              );
            }

            this.inventFundsApiService.showAlertUpdateMessageNoReload();
          });
        })
        .catch((err) => {
          console.log('Error:' + err);
        });
    } else if (!this.profileSummaryExists) {
      //create
      // alert('create data in table')
      var personSummaryData = data;
      Object.assign(personSummaryData, { personId: this.appState.encryptedDataValue('personId') });
      var profile = {
        collectionName: profileCol,
        updateData: personSummaryData,
        endPoint: 'CREATEPROFILESUMMARY',
      };
      this.inventFundsApiService
        .createGeneric(profile)
        .then((res: any) => {
          // this.getProfile();
          // console.log('create response-->' + JSON.stringify(res));
          if (res.data) {
            // alert('create successful');
            this.modelData = res.data[0];
            // this.updateSummary = res.data[0];
            this.modelData.profileSummary = res.data[0].profileSummary;
            if (this.typeOfRole == 'Invent Fund Fixer') {
              this.fixerProfileSchema.fixerProfileSummary.profileSummary =
                res.data[0].profileSummary;
              this.setDataProfileStatus(
                this.fixerProfileSchema.fixerProfileSummary,
                this.fixerProfileSchema.fixerEducationalDetails,
                this.fixerProfileSchema.fixerProfessionalDetails,
                this.fixerProfileSchema.fixerAchievementDetails,
                this.fixerProfileSchema.fixerSkillDetails
              );
            } else if (this.typeOfRole == 'Invent Fund Founder') {
              this.founderProfileSchema.founderProfileSummary.profileSummary =
                res.data[0].profileSummary;
              this.setDataProfileStatus(
                this.founderProfileSchema.founderProfileSummary,
                this.founderProfileSchema.founderEducationalDetails,
                [],
                [],
                []
              );
            } else if (this.typeOfRole == 'Invent Fund Funder') {
              this.funderProfileSchema.funderProfileSummary.profileSummary =
                res.data[0].profileSummary;
              this.setDataProfileStatus(
                this.funderProfileSchema.funderProfileSummary,
                this.funderProfileSchema.funderEducationalDetails,
                [],
                [],
                []
              );
            }
            // this.type = res.data[0].type;
            this.profileSummaryExists = true;
            this.existingProfileSummaryId = res.data[0]._id;
            this.inventFundsApiService.showAlertCreateMessage();
          }
        })
        .catch((err) => {
          console.log('Error:' + err);
        });
      this.editmainsection = false;
    }
  }

  // cancel profile summary data

  cancelProfileSummary(event) {
    // alert(event)
    // if (event == "true") {
    //   // alert("true")
    //   let { headline, professionalSummary, type, ...rest } = this.modelData;
    //   this.modelData = rest;
    //   console.log("modelData : " + JSON.stringify(this.modelData))
    // }
    // console.log('update summary : ' + JSON.stringify(this.updateSummary));
    this.modelData = this.updateSummary;
    // console.log('update modelData : ' + JSON.stringify(this.modelData));
    // this.modelData = this.updateSummary;
    this.editmainsection = false;
  }

  // open(content) {
  //   this.modalService.open(content, {centered: true});
  // }

  // open model async

  async openModal(content) {
    // alert(JSON.stringify(content))
    // console.log("data for open model  : " + JSON.stringify(content))
    if (content.hasOwnProperty('professionalDetailsIndividual')) {
      var lookupParams = {
        collectionName: 'lookUp',
        endPoint: 'RETRIEVELOOKUPDATA',
      };

      this.inventFundsApiService
        .retrieveScreenbuilderAll(lookupParams)
        .then((res: any) => {
          if (res.status == 200) {
            // console.log('Lookup data:' + JSON.stringify(res));
            if (res.status == 200) {
              res.response.filter((item) => {
                if (item.lookupKey == 'employmentType') {
                  this.lookupArray.push({
                    label: item.label,
                    value: item.value,
                  });
                }
              });
            }
          }
        })
        .catch((err) => {
          console.log('error:' + err);
        });

      var params = {
        collectionName: 'professionalDetailsIndividual',
      };
      this.inventFundsApiService.retrieveScreenbuilderAll(params).then(async (res) => {
        // this.profileSummaryJson = res.response;
        // console.log('professionalDetailsIndividual res ' + JSON.stringify(res.response));
        var profDetailsJson = res.response;
        profDetailsJson.map((item) => {
          item.fieldGroup.map((item2) => {
            if (item2.key == 'employmentType') {
              item2.templateOptions.options = this.lookupArray;
              this.lookupArray = [];
            }
          });
        });
        var params2 = {
          _id: content.model,
          collectionName: 'fixerProfessionalDetails',
          endPoint: 'RETRIEVEPROFESSIONALDETAILSONE',
        };
        var modelData = await this.inventFundsApiService.retrieveGeneric(params2);

        // console.log('modelData:' + JSON.stringify(modelData.response))

        // console.log('profDetailsJson:' + JSON.stringify(profDetailsJson))
        const modalRef = this.modalService.open(ProfileModalComponent, {
          size: 'lg',
          centered: true,
          backdrop: 'static',
          keyboard: false,
        });
        modalRef.componentInstance.data = {
          profDetailsJson: profDetailsJson,
          headline: 'Edit Professional Details',
          model: modelData.response,
        };

        modalRef.result
          .then((result) => {
            if (result) {
              // console.log("receivedEntry:" + JSON.stringify(result));
              if (result.action == 'create') {
                this.createDataFormly(result.data, result.type);
              } else if (result.action == 'cancelled') {
                // console.log('cancelled:' + JSON.stringify(this.fixerProfileSchema.fixerProfessionalDetails) + ' item:' + JSON.stringify(content.model))
                // content.model
              } else {
                this.updateDataFormly(result.data, result.type);
              }
            }
          })
          .catch((cancel) => console.log(cancel));
      });
    } else if (content.hasOwnProperty('educationalDetails')) {
      var lookupParams = {
        collectionName: 'lookUp',
        endPoint: 'RETRIEVELOOKUPDATA',
      };

      this.inventFundsApiService
        .retrieveScreenbuilderAll(lookupParams)
        .then((res: any) => {
          if (res.status == 200) {
            this.lookupArray = [];
            // console.log('Lookup data:' + JSON.stringify(res))
            if (res.status == 200) {
              res.response.filter((item) => {
                if (item.lookupKey == 'country') {
                  this.lookupArray.push({
                    label: item.label,
                    value: item.value,
                  });
                }
              });
              const modalRef = this.modalService.open(ProfileModalComponent, {
                size: 'lg',
                centered: true,
              });
              if (content.hasOwnProperty('updateData')) {
                modalRef.componentInstance.data = {
                  educationalDetails: this.lookupArray,
                  updateData: content.updateData,
                  headline: content.heading,
                  operation: 'update',
                };
              } else {
                modalRef.componentInstance.data = {
                  educationalDetails: this.lookupArray,
                  headline: content.heading,
                  operation: 'create',
                };
              }
              this.lookupArray = [];
              modalRef.result
                .then((result) => {
                  if (result) {
                    // console.log("receivedEntry in education details:" + JSON.stringify(result));
                    if (result.action == 'create') {
                      this.createEducationData(result.value);
                    } else if (result.action == 'cancelled') {
                      //cancelled
                    } else {
                      this.updateEducationData(result.value, result.id);
                    }
                  }
                })
                .catch((cancel) => console.log(cancel));
            }
          }
        })
        .catch((err) => {
          console.log('error:' + err);
        });
    } else if (content.hasOwnProperty('achievementsDetails')) {
      // console.log('achivenments:' + JSON.stringify(content))
      var lookupParams = {
        collectionName: 'lookUp',
        endPoint: 'RETRIEVELOOKUPDATA',
      };

      var achiLookup = await this.inventFundsApiService.retrieveScreenbuilderAll(lookupParams);
      // console.log('achievements lookup-->' + JSON.stringify(achiLookup))

      achiLookup.response.filter((item) => {
        if (item.lookupKey == 'achievementType') {
          this.lookupArray.push({
            label: item.label,
            value: item.value,
          });
        }
      });

      // console.log('Lookup array in achievements-->' + JSON.stringify(this.lookupArray))

      var params = {
        collectionName: 'achievements',
      };
      this.inventFundsApiService.retrieveScreenbuilderAll(params).then(async (res) => {
        // this.profileSummaryJson = res.response;
        // console.log("achievements res " + JSON.stringify(res.response))
        // console.log("achievements lookupArray " + JSON.stringify(this.lookupArray))
        var achievementsJson = res.response;
        achievementsJson.map((item) => {
          // console.log("achievements item1 " + JSON.stringify(item))
          item.fieldGroup.map((item2) => {
            // console.log("achievements item2 " + JSON.stringify(item2))
            // console.log(item2.key === 'achievementsCategory')
            if (item2.key == 'achievementsCategory') {
              item2.templateOptions.options = this.lookupArray;
              this.lookupArray = [];
            }
          });
        });
        var params2 = {
          _id: content.updateData,
          collectionName: 'fixerAchievementDetails',
          endPoint: 'RETRIEVEACHIDETAILSONE',
        };
        var achiModelData = await this.inventFundsApiService.retrieveGeneric(params2);
        // console.log('achiModelData:' + JSON.stringify(achiModelData))
        // console.log('achievements content:' + JSON.stringify(achievementsJson))
        // const modalRef = this.modalService.open(ProfileModalComponent, { size: 'lg', centered: true });
        // modalRef.componentInstance.data = { "achievementsJson": achievementsJson, "headline": "Edit Achievements Details", model: content.model };
        const modalRef = this.modalService.open(ProfileModalComponent, {
          size: 'lg',
          centered: true,
        });
        if (content.hasOwnProperty('updateData')) {
          // modalRef.componentInstance.data = { "achievementsDetails": achievementsJson, "updateData": content.updateData, "headline": content.heading, "operation": "update" };
          modalRef.componentInstance.data = {
            achievementsDetails: achievementsJson,
            updateData: achiModelData.response[0],
            headline: content.heading,
            operation: 'update',
          };
        } else {
          modalRef.componentInstance.data = {
            achievementsDetails: achievementsJson,
            headline: content.heading,
            operation: 'create',
          };
        }

        modalRef.result
          .then((result) => {
            if (result) {
              // console.log("achievements results:" + JSON.stringify(result));
              if (result.action != 'cancelled') {
                if (result.action == 'create') {
                  this.createDataAchi(result.data, result.type);
                } else {
                  this.updateDataAchi(result.data, result.type);
                }
              }
            }
          })
          .catch((cancel) => console.log(cancel));
      });
    } else if (content.hasOwnProperty('skillsDetails')) {
      this.skillsData(content);
    } else if (content.hasOwnProperty('showcase')) {
      // console.log('show case');
      const modalRef = this.modalService.open(ProfileModalComponent, {
        size: 'lg',
        centered: true,
      });
      modalRef.componentInstance.data = {
        showcase: '',
        headline: content.heading,
        operation: 'create',
      };

      modalRef.result
        .then((result) => {
          // console.log(result.action)
          if (result.obj) {
            console.log('receivedEntry:' + JSON.stringify(result));
            // {"obj":
            // {"imageUrl":
            // "https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFund/HOST/100/IFFIXER/SHOWCASE/5ed8eb4ff2ae0d4f70cbf102/download.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA6CMG2RRKEAAYQ65F%2F20200616%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20200616T102257Z&X-Amz-Expires=30&X-Amz-Signature=8538e119d3cf4ea4b3b97de42e7eacca9eadd2f1d19a98991951c4c943f2d9fd&X-Amz-SignedHeaders=host",
            // "imageType":"image/jpeg","description":"hello!",
            // "data":"https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFund/HOST/100/IFFIXER/SHOWCASE/5ed8eb4ff2ae0d4f70cbf102/download.jpg"}}
            // var n = result.obj.data.lastIndexOf('/');
            // var key = result.obj.data.substring(n + 1);
            // var path = 'inventFund' + result.obj.data.split('inventFund')[1].replace(key, '');
            var imageParams = {
              collectionName: 'attachment',
              createData: {
                id: this.appState.encryptedDataValue('personId'),
                // "url": result.obj.imageUrl,
                fileType: result.obj.imageType,
                description: result.obj.description,
                type: 'SHOWCASE',
                // path: path,
                // key: key,
                url: result.obj.data,
              },
            };
            // console.log("imageParams : " + JSON.stringify(imageParams))
            this.inventFundsApiService.attachmentSingleFile(imageParams).then((res) => {
              // console.log('Image upload res-->' + JSON.stringify(res));
              if (res.code == 200) {
                this.recomputeAttachmentArray();
                // this.fixerProfileSchema.showcaseDetails.push(res.data[0])
                // this.showcaseArray.push(res.data[0])
              }
            });
          }
        })
        .catch((cancel) => console.log(cancel));
    } else {
      const modalRef = this.modalService.open(ProfileModalComponent, { size: 'lg' });
      modalRef.componentInstance.data = content;

      modalRef.result
        .then((result) => {
          if (result) {
            // console.log('receivedEntry:' + JSON.stringify(result));
            if (result.action != 'cancelled') {
              if (result.value.hasOwnProperty('profileSummary')) {
                this.updateProfileSummarySection(result.value);
              }
            }
          }
        })
        .catch((cancel) => console.log(cancel));
    }
  }

  // get skills data for pop up

  async skillsData(data) {
    var skillSet = await this.getSkillSet();
    var skillCat = await this.getSkillCat();
    var selfAssessment = await this.getSelfAsesment();
    await this.openSkillsModel(data, skillCat, skillSet, selfAssessment);
  }

  // get skills set from collection

  getSkillSet() {
    var lookupParamsKey = {
      collectionName: 'lookUp',
      endPoint: 'RETRIEVESCREENBUILDERLOOKUPSKILLS',
      queryStr: { lookupCode: 'skillSet' },
      sortQuery: { order: 1 },
    };
    return this.inventFundsApiService
      .retrieveScreenbuilderLookUp(lookupParamsKey)
      .then((res: any) => {
        if (res.status == 200) {
          // console.log('Lookup data:' + JSON.stringify(res));
          if (res.status == 200) {
            return res.response;
          } else {
            return null;
          }
        }
      })
      .catch((err) => {
        console.log('error:' + err);
      });
  }

  // get skill category

  getSkillCat() {
    var lookupParamsKey = {
      collectionName: 'lookUp',
      endPoint: 'RETRIEVESCREENBUILDERLOOKUPSKILLS',
      queryStr: { lookupCode: 'skillCategory' },
      sortQuery: { order: 1 },
    };
    return this.inventFundsApiService
      .retrieveScreenbuilderLookUp(lookupParamsKey)
      .then((res: any) => {
        if (res.status == 200) {
          // console.log('Lookup data:' + JSON.stringify(res));
          if (res.status == 200) {
            return res.response;
          } else {
            return null;
          }
        }
      })
      .catch((err) => {
        console.log('error:' + err);
      });
  }

  // get self assment

  getSelfAsesment() {
    var lookupParamsKey = {
      collectionName: 'lookUp',
      endPoint: 'RETRIEVESCREENBUILDERLOOKUPSKILLS',
      queryStr: { lookupCode: 'selfAssessment' },
      sortQuery: { order: 1 },
    };
    return this.inventFundsApiService
      .retrieveScreenbuilderLookUp(lookupParamsKey)
      .then((res: any) => {
        if (res.status == 200) {
          // console.log('Lookup data:' + JSON.stringify(res));
          if (res.status == 200) {
            return res.response;
          } else {
            return null;
          }
        }
      })
      .catch((err) => {
        console.log('error:' + err);
      });
  }

  // open skill model

  openSkillsModel(data, skillCat, skillSet, selfAssessment) {
    // console.log("data for skills : " + JSON.stringify(data))
    const modalRef = this.modalService.open(ProfileModalComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.data = {
      skillsJson: '',
      headline: data.heading,
      model: data.model,
    };
    if (data.hasOwnProperty('updateData')) {
      modalRef.componentInstance.data = {
        skillsJson: '',
        selfAssessment: selfAssessment,
        skillSet: skillSet,
        skillCategory: skillCat,
        headline: data.heading,
        updateData: data.updateData,
        model: data.model,
        operation: 'update',
      };
    } else {
      modalRef.componentInstance.data = {
        skillsJson: '',
        selfAssessment: selfAssessment,
        skillSet: skillSet,
        skillCategory: skillCat,
        headline: data.heading,
        model: data.model,
        operation: 'create',
      };
    }
    modalRef.result
      .then((result) => {
        if (result) {
          // console.log('receivedEntry:' + JSON.stringify(result));
          if (result.action != 'cancelled') {
            if (result.action == 'create') {
              this.createDataSkills(result, result.type);
            } else {
              this.updateDataSkills(result, result.type);
            }
          }
        }
      })
      .catch((cancel) => console.log(cancel));
  }

  // create data for achivements

  createDataAchi(data, type) {
    // console.log('formly create data:' + JSON.stringify(data) + '' + type)
    Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
    var params = {
      updateData: data.response,
      collectionName: 'fixerAchievementDetails',
      endPoint: 'CREATEACHIDETAILS',
    };
    this.inventFundsApiService.createGeneric(params).then((res) => {
      if (res.status == 200) {
        this.getAchiDetails('create');
      }
    });
  }

  // update achivements

  updateDataAchi(data, type) {
    // console.log('formly update data:' + JSON.stringify(data) + '' + type)
    Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
    var params = {
      updateData: data.response,
      pkId: data.id,
      collectionName: 'fixerAchievementDetails',
      endPoint: 'UPDATEACHIDETAILS',
    };
    this.inventFundsApiService.updateGeneric(params).then((res) => {
      if (res.status == 200) {
        this.getAchiDetails('update');
      }
    });
  }

  // create data from formly

  createDataFormly(data, type) {
    // console.log('formly create data:' + JSON.stringify(data) + '' + type)

    if (type == 'professionalDetailsIndividual') {
      Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
      var params = {
        updateData: data.response,
        collectionName: 'fixerProfessionalDetails',
        endPoint: 'CREATEPROFESSIONALDETAILS',
      };
      this.inventFundsApiService.createGeneric(params).then((res) => {
        // console.log('data created : ' + JSON.stringify(res));
        this.individualProfessionalDetails.push(res.data[0]);
        // console.log('ind prof det:' + JSON.stringify(this.individualProfessionalDetails));
        this.fixerProfileSchema.fixerProfessionalDetails.push(res.data[0]);
        this.setDataProfileStatus(
          this.fixerProfileSchema.fixerProfileSummary,
          this.fixerProfileSchema.fixerEducationalDetails,
          this.fixerProfileSchema.fixerProfessionalDetails,
          this.fixerProfileSchema.fixerAchievementDetails,
          this.fixerProfileSchema.fixerSkillDetails
        );
        this.inventFundsApiService.showAlertCreateMessage();
      });
    }
  }

  // update data from formly

  updateDataFormly(data, type) {
    // console.log('formly update data:' + JSON.stringify(data) + '' + type)

    if (type == 'professionalDetailsIndividual') {
      Object.assign(data.response, { personId: this.appState.encryptedDataValue('personId') });
      var params = {
        updateData: data.response,
        pkId: data.id,
        collectionName: 'fixerProfessionalDetails',
        endPoint: 'UPDATEPROFESSIONALDETAILS',
      };
      this.inventFundsApiService.updateGeneric(params).then((res) => {
        // console.log("data updated : " + JSON.stringify(res));
        if (res.status == 200) {
          var params = {
            collectionName: 'fixerProfessionalDetails',
            endPoint: 'RETRIEVEPROFESSIONALDETAILSONE',
            _id: res.data._id,
          };
          this.inventFundsApiService
            .retrieveGeneric(params)
            .then((res2) => {
              // console.log('retrieve fixerProfessionalDetails one response-->' + JSON.stringify(res2))
              // this.educationDetails = res
              var index = this.fixerProfileSchema.fixerProfessionalDetails.findIndex(
                (p) => p._id == res2.response[0]._id
              );
              this.fixerProfileSchema.fixerProfessionalDetails[index] = res2.response[0];
              this.setDataProfileStatus(
                this.fixerProfileSchema.fixerProfileSummary,
                this.fixerProfileSchema.fixerEducationalDetails,
                this.fixerProfileSchema.fixerProfessionalDetails,
                this.fixerProfileSchema.fixerAchievementDetails,
                this.fixerProfileSchema.fixerSkillDetails
              );
              this.inventFundsApiService.showAlertUpdateMessage();
            })
            .catch((err) => {
              console.log('Error:' + err);
            });
        }
      });
    }
  }

  // get education data

  getEducationData() {
    var params = {
      collectionName: 'fixerEducationDetails',
      endPoint: 'RETRIEVEEDUCATIONDETAILS',
    };

    this.inventFundsApiService
      .retrieveGeneric(params)
      .then((res) => {
        // console.log('educationDetails response!' + JSON.stringify(res))
        res.response.map((item) => {
          if (item.personId == this.appState.encryptedDataValue('personId')) {
            // alert('professionalDetails already exists')
            this.educationDetails.push(item);
          }
        });
        // console.log('edu details array:' + JSON.stringify(this.educationDetails))
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // create education data

  createEducationData(value) {
    var eduCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      eduCol = 'fixerEducationDetails';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      eduCol = 'founderEducationDetails';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      eduCol = 'funderEducationDetails';
    }
    var educationData = value;
    Object.assign(educationData, { personId: this.appState.encryptedDataValue('personId') });
    var eduDetails = {
      collectionName: eduCol,
      updateData: educationData,
      endPoint: 'CREATEEDUCATIONDETAILS',
    };
    this.inventFundsApiService
      .createGeneric(eduDetails)
      .then((res) => {
        // console.log('education create response==>' + JSON.stringify(res));
        this.educationDetails.push(res.data[0]);
        if (this.typeOfRole == 'Invent Fund Fixer') {
          this.fixerProfileSchema.fixerEducationalDetails.push(res.data[0]);
          this.setDataProfileStatus(
            this.fixerProfileSchema.fixerProfileSummary,
            this.fixerProfileSchema.fixerEducationalDetails,
            this.fixerProfileSchema.fixerProfessionalDetails,
            this.fixerProfileSchema.fixerAchievementDetails,
            this.fixerProfileSchema.fixerSkillDetails
          );
        } else if (this.typeOfRole == 'Invent Fund Founder') {
          this.founderProfileSchema.founderEducationalDetails.push(res.data[0]);
          this.setDataProfileStatus(
            this.founderProfileSchema.founderProfileSummary,
            this.founderProfileSchema.founderEducationalDetails,
            [],
            [],
            []
          );
        } else if (this.typeOfRole == 'Invent Fund Funder') {
          this.funderProfileSchema.funderEducationalDetails.push(res.data[0]);
          this.setDataProfileStatus(
            this.funderProfileSchema.funderProfileSummary,
            this.funderProfileSchema.funderEducationalDetails,
            [],
            [],
            []
          );
        }

        this.inventFundsApiService.showAlertCreateMessage();
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // update education data

  updateEducationData(value, id) {
    var eduCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      eduCol = 'fixerEducationDetails';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      eduCol = 'founderEducationDetails';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      eduCol = 'funderEducationDetails';
    }
    var eduDetails = {
      collectionName: eduCol,
      pkId: id,
      updateData: value,
      endPoint: 'UPDATEEDUCATIONDETAILS',
    };
    this.inventFundsApiService
      .updateGeneric(eduDetails)
      .then((res) => {
        // console.log('Update education response->' + JSON.stringify(res))
        if (res.status == 200) {
          var params = {
            collectionName: eduCol,
            _id: res.data._id,
            endPoint: 'RETRIEVEEDUCATIONDETAILSONE',
          };
          this.inventFundsApiService
            .retrieveGeneric(params)
            .then((res2) => {
              // console.log('retrieve educationDetails one response-->' + JSON.stringify(res2));
              // this.educationDetails = res2
              var index = this.educationDetails.findIndex((p) => p._id == res2.response[0]._id);
              this.educationDetails[index] = res2.response[0];
              if (this.typeOfRole == 'Invent Fund Fixer') {
                var index = this.fixerProfileSchema.fixerEducationalDetails.findIndex(
                  (p) => p._id == res2.response[0]._id
                );
                this.fixerProfileSchema.fixerEducationalDetails[index] = res2.response[0];
                this.setDataProfileStatus(
                  this.fixerProfileSchema.fixerProfileSummary,
                  this.fixerProfileSchema.fixerEducationalDetails,
                  this.fixerProfileSchema.fixerProfessionalDetails,
                  this.fixerProfileSchema.fixerAchievementDetails,
                  this.fixerProfileSchema.fixerSkillDetails
                );
              } else if (this.typeOfRole == 'Invent Fund Founder') {
                // alert('in founder edu update')
                var index = this.founderProfileSchema.founderEducationalDetails.findIndex(
                  (p) => p._id == res2.response[0]._id
                );
                this.founderProfileSchema.founderEducationalDetails[index] = res2.response[0];
                // console.log(
                //   'edu details updated:' +
                //     JSON.stringify(this.founderProfileSchema.founderEducationalDetails)
                // );
                this.setDataProfileStatus(
                  this.founderProfileSchema.founderProfileSummary,
                  this.founderProfileSchema.founderEducationalDetails,
                  [],
                  [],
                  []
                );
              } else if (this.typeOfRole == 'Invent Fund Funder') {
                // alert('in funder edu update')
                var index = this.funderProfileSchema.funderEducationalDetails.findIndex(
                  (p) => p._id == res2.response[0]._id
                );
                this.funderProfileSchema.funderEducationalDetails[index] = res2.response[0];
                // console.log(
                //   'edu details updated:' +
                //     JSON.stringify(this.funderProfileSchema.funderEducationalDetails)
                // );
                this.setDataProfileStatus(
                  this.funderProfileSchema.funderProfileSummary,
                  this.funderProfileSchema.funderEducationalDetails,
                  [],
                  [],
                  []
                );
              }

              this.inventFundsApiService.showAlertUpdateMessage();
            })
            .catch((err) => {
              console.log('Error:' + err);
            });
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // delete education

  deleteEducationData(id) {
    var eduCol;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      eduCol = 'fixerEducationDetails';
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      eduCol = 'founderEducationDetails';
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      eduCol = 'funderEducationDetails';
    }
    this.swalMessage('Are you sure you want to delete this entry?')
      .then((res) => {
        const variable = Object.keys(res).includes('value');
        // alert(JSON.stringify(res) + JSON.stringify(id))
        if (variable) {
          var params = {
            pkId: id,
            collectionName: eduCol,
            endPoint: 'DELETEEDUCATIONDETAILS',
          };

          this.inventFundsApiService
            .deleteGeneric(params)
            .then((res2) => {
              // console.log('delete response->' + JSON.stringify(res2))
              if (res2.status == 200) {
                // var index = this.educationDetails.findIndex(p => p._id == id)
                // this.educationDetails.splice(index, 1);
                if (this.typeOfRole == 'Invent Fund Fixer') {
                  var index = this.fixerProfileSchema.fixerEducationalDetails.findIndex(
                    (p) => p._id == id
                  );
                  this.fixerProfileSchema.fixerEducationalDetails.splice(index, 1);
                  this.setDataProfileStatus(
                    this.fixerProfileSchema.fixerProfileSummary,
                    this.fixerProfileSchema.fixerEducationalDetails,
                    this.fixerProfileSchema.fixerProfessionalDetails,
                    this.fixerProfileSchema.fixerAchievementDetails,
                    this.fixerProfileSchema.fixerSkillDetails
                  );
                } else if (this.typeOfRole == 'Invent Fund Founder') {
                  var index = this.founderProfileSchema.founderEducationalDetails.findIndex(
                    (p) => p._id == id
                  );
                  this.founderProfileSchema.founderEducationalDetails.splice(index, 1);
                  this.setDataProfileStatus(
                    this.founderProfileSchema.founderProfileSummary,
                    this.founderProfileSchema.founderEducationalDetails,
                    [],
                    [],
                    []
                  );
                } else if (this.typeOfRole == 'Invent Fund Funder') {
                  var index = this.funderProfileSchema.funderEducationalDetails.findIndex(
                    (p) => p._id == id
                  );
                  this.funderProfileSchema.funderEducationalDetails.splice(index, 1);
                  this.setDataProfileStatus(
                    this.funderProfileSchema.funderProfileSummary,
                    this.funderProfileSchema.funderEducationalDetails,
                    [],
                    [],
                    []
                  );
                }

                this.inventFundsApiService.showAlertDeleteMessage();
              }
            })
            .catch((err) => {
              console.log('Error:' + err);
            });
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // delete profession

  deleteProfessionData(id) {
    this.swalMessage('Are you sure you want to delete this entry?')
      .then((res) => {
        const variable = Object.keys(res).includes('value');
        // alert(JSON.stringify(res) + JSON.stringify(id))
        if (variable) {
          var params = {
            pkId: id,
            endPoint: 'DELETEPROFESSIONALDETAILS',
          };

          this.inventFundsApiService
            .deleteGeneric(params)
            .then((res2) => {
              // console.log('delete response->' + JSON.stringify(res2))
              if (res2.status == 200) {
                var index = this.fixerProfileSchema.fixerProfessionalDetails.findIndex(
                  (p) => p._id == id
                );
                this.fixerProfileSchema.fixerProfessionalDetails.splice(index, 1);
                this.setDataProfileStatus(
                  this.fixerProfileSchema.fixerProfileSummary,
                  this.fixerProfileSchema.fixerEducationalDetails,
                  this.fixerProfileSchema.fixerProfessionalDetails,
                  this.fixerProfileSchema.fixerAchievementDetails,
                  this.fixerProfileSchema.fixerSkillDetails
                );
                this.inventFundsApiService.showAlertDeleteMessage();
              }
            })
            .catch((err) => {
              console.log('Error:' + err);
            });
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // delete achivements

  deleteAchi(id) {
    this.swalMessage('Are you sure you want to delete this entry?')
      .then((res) => {
        const variable = Object.keys(res).includes('value');
        // alert(JSON.stringify(res) + JSON.stringify(id))
        if (variable) {
          var params = {
            pkId: id,
            endPoint: 'DELETEACHIDETAILS',
          };

          this.inventFundsApiService
            .deleteGeneric(params)
            .then((res2) => {
              // console.log('delete response->' + JSON.stringify(res2));
              if (res2.status == 200) {
                // var index = this.educationDetails.findIndex(p => p._id == id)
                // this.educationDetails.splice(index, 1);
                this.getAchiDetails('delete');
              }
            })
            .catch((err) => {
              console.log('Error:' + err);
            });
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // create skills data

  createDataSkills(data, type) {
    // console.log('formly create data:' + JSON.stringify(data) + '' + type)
    var skillsCat = [];
    var skillSet = [];
    if (data.value.skillCategory) {
      data.value.skillCategory.filter((item) => {
        skillsCat.push(item.value);
      });
      // console.log("skills cat : " + JSON.stringify(skillsCat))
    }
    if (data.value.skillSet) {
      data.value.skillSet.filter((item) => {
        skillSet.push(item.value);
      });
      // console.log("skills set : " + JSON.stringify(skillSet))
    }
    var obj = {
      skillCategory: skillsCat,
      skillSet: skillSet,
      selfAssessmentScore: data.value.selfAssessmentScore,
      personId: this.appState.encryptedDataValue('personId'),
    };
    var params = {
      updateData: obj,
      collectionName: 'fixerSkillDetails',
      endPoint: 'CREATESKILLSDETAILS',
    };
    this.inventFundsApiService.createGeneric(params).then((res) => {
      // console.log("data created : " + JSON.stringify(res));
      this.getSkillsDetails('create');
      // this.individualProfessionalDetails.push(res.data[0])
      // console.log('ind prof det:' + JSON.stringify(this.individualProfessionalDetails))
    });
  }

  // update skills

  updateDataSkills(data, type) {
    // console.log('formly update data:' + JSON.stringify(data) + '' + type)
    var skillsCat = [];
    var skillSet = [];
    if (data.value.skillCategory) {
      data.value.skillCategory.filter((item) => {
        if (item.value) {
          skillsCat.push(item.value);
        } else {
          skillsCat.push(item);
        }
      });
    }
    if (data.value.skillSet) {
      data.value.skillSet.filter((item) => {
        // skillSet.push(item.value);
        if (item.value) {
          skillSet.push(item.value);
        } else {
          skillSet.push(item);
        }
      });
    }
    var obj = {
      skillCategory: skillsCat,
      skillSet: skillSet,
      selfAssessmentScore: data.value.selfAssessmentScore,
      personId: this.appState.encryptedDataValue('personId'),
    };
    var params = {
      updateData: obj,
      pkId: data.id,
      collectionName: 'fixerSkillDetails',
      endPoint: 'UPDATESKILLSDETAILS',
    };
    this.inventFundsApiService.updateGeneric(params).then((res) => {
      // console.log("data updated : " + JSON.stringify(res));
      this.getSkillsDetails('update');
      // this.individualProfessionalDetails.push(res.data[0])
      // console.log('ind prof det:'+JSON.stringify(this.individualProfessionalDetails))
    });
  }

  // delete skills

  deleteSkills(id) {
    this.swalMessage('Are you sure you want to delete this entry?')
      .then((res) => {
        const variable = Object.keys(res).includes('value');
        // alert(JSON.stringify(res) + JSON.stringify(id))
        if (variable) {
          var params = {
            pkId: id,
            endPoint: 'DELETESKILLSDETAILS',
          };
          this.inventFundsApiService
            .deleteGeneric(params)
            .then((res2) => {
              // console.log('delete response->' + JSON.stringify(res2))
              if (res2.status == 200) {
                // var index = this.educationDetails.findIndex(p => p._id == id)
                // this.educationDetails.splice(index, 1);
                this.getSkillsDetails('delete');
              }
            })
            .catch((err) => {
              console.log('Error:' + err);
            });
        }
      })
      .catch((err) => {
        console.log('Error:' + err);
      });
  }

  // recompute attachmant array

  recomputeAttachmentArray() {
    var attachment = {
      collectionName: 'attachment',
      queryStr: {
        $and: [{ personId: this.appState.encryptedDataValue('personId') }, { type: 'SHOWCASE' }],
      },
      endPoint: 'RETRIEVEATTACHMENTDATASIGNED',
    };

    this.inventFundsApiService.retrieveGeneric(attachment).then((res) => {
      // console.log('recomputeAttachmentArray res :' + JSON.stringify(res));
      if (res.status == 200 && res.data.length != 0) {
        // this.fixerProfileSchema.showcaseDetails.push(res.data[0])
        // this.showcaseArray.push(res.data[0])
        this.fixerProfileSchema.showcaseDetails = res.response;
        var sanitizedData = res.data.map((item) => ({
          _id: item._id,
          personId: item.personId,
          url: this.sanitizeUrl(item.url),
          type: item.type,
          // path: item.path,
          // key: item.key,
          fileType: item.fileType,
          description: item.description,
          createdDate: item.description,
          createdBy: item.createdBy,
          updatedDate: item.updatedDate,
          updatedBy: item.updatedBy,
        }));
        var changedArrayAttachment = [...sanitizedData];
        this.showcaseArray = changedArrayAttachment;
        this.reload();
        // this.cdr.detectChanges();
        // console.log('after recompute : ' + JSON.stringify(this.showcaseArray));
        this.loading = false;
      } else {
        this.showcaseArray = [];
      }
    });
  }

  // change type of profession

  changeInType(event, previous) {
    // alert('change in type:'+JSON.stringify(event))
    var profileCol, updateId;
    if (this.typeOfRole == 'Invent Fund Fixer') {
      profileCol = 'fixerProfileSummary';
      updateId = this.fixerProfileSchema.fixerProfileSummary._id;
    } else if (this.typeOfRole == 'Invent Fund Founder') {
      profileCol = 'founderProfileSummary';
      updateId = this.founderProfileSchema.founderProfileSummary._id;
    } else if (this.typeOfRole == 'Invent Fund Funder') {
      profileCol = 'funderProfileSummary';
      updateId = this.funderProfileSchema.funderProfileSummary._id;
    }
    this.selectedValue = event;
    var updateValue;

    this.types.map((item) => {
      // console.log('item:' + JSON.stringify(item))
      if (event == item['value']) {
        updateValue = item['viewValue'];
      }
    });

    // alert('updateValue:'+updateValue+'pk id:'+updateId)
    if (updateId) {
      this.swalMessage('Are you sure you want to change your role?').then((res) => {
        const variable = Object.keys(res).includes('value');
        if (variable) {
          var profileSummary = {
            collectionName: profileCol,
            pkId: updateId,
            updateData: { personType: updateValue },
            endPoint: 'UPDATEPROFILESUMMARY',
          };
          this.inventFundsApiService
            .updateGeneric(profileSummary)
            .then((res) => {
              // console.log('personType update resp:' + JSON.stringify(res))
              this.types.map((item) => {
                // console.log('item:' + JSON.stringify(item))
                if (res.data.personType == item['value']) {
                  this.selectedValue = item['viewValue'];
                }
              });
              this.inventFundsApiService.showAlertUpdateMessage();
            })
            .catch((err) => {
              console.log('error:' + err);
            });
        } else {
          // console.log("selected value : " + JSON.stringify(this.selectedValue), { previous });
          this.selectedValue = previous;
          this.form.controls['roleType'].setValue(previous);
        }
      });
    }
  }

  /**
   *
   * @param url it helps preventing Cross Site Scripting Security bugs (XSS) by
   *            sanitizing values to be safe to use in the different DOM contexts
   */

  sanitizeUrl(url) {
    // return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    return url;
  }

  /**
   *
   * @param $event object which consits url and type of file to render in new window if type is pdf
   */

  emitImageObject($event) {
    console.log('render obj : ' + JSON.stringify($event));
    if ($event.fileType == 'application/pdf') {
      // var data = {
      //   path: $event.path,
      //   key: $event.key,
      // };
      // this.inventFundsApiService
      //   .getImageData(data)
      //   .then((result) => {
      //     if (result.status === 200) {
      // console.log(result);
      var w = window.open($event['url']);
      w.focus();
      // }
      // })
      // .catch((err) => {
      //   console.log('err data : ' + JSON.stringify(err));
      // });
    }
  }

  /**
   *
   * @param obj contains url and id to delete image or doc
   */

  deleteCarosulDoc(obj) {
    // console.log('delete object : ' + JSON.stringify(obj));
    this.swalMessage('Are you sure you want to delete?').then((response) => {
      const variable = Object.keys(response).includes('value');
      // console.log(JSON.stringify(response));
      if (variable) {
        var params = {
          collectionName: 'attachment',
          data: {
            url: obj.url,
            _id: obj._id,
          },
          endPoint: 'DELETEATTACHMENTDATARECORD',
        };
        this.inventFundsApiService
          .deleatAttachmentFileSingle(params)
          .then((result) => {
            // console.log(result);
            if (result.code == 200) {
              this.recomputeAttachmentArray();
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    });
  }

  /**
   *
   * @param content text to show in swal message and returns the tru or false
   */

  swalMessage(content) {
    return Swal.fire({
      title: content,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      customClass: {
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    })
      .then((result) => {
        return result;
      })
      .catch((err) => {
        return err;
      });
  }

  // reload showcase data dippends on change to sync data

  reload() {
    this.show = false;
    setTimeout(() => (this.show = true));
  }
}
