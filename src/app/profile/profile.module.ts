import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BaseLayoutComponent } from '../layout/base-layout/base-layout.component';
import { SharedModule } from '../shared/shared.module';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ProfileModalComponent } from './profile-modal/profile-modal.component';
import { SettingsComponent } from './settings/settings.component';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import { RolesAuthGuardService } from '../services/roles-auth-guard.service';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { LayoutsModule } from '../shared/layouts.module';

const routes: Routes = [
  {
    path: "",
    component: BaseLayoutComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'dashboard', component: DashboardComponent,
        canActivate: [RolesAuthGuardService],
        data: ['dashboard']
      },
      {
        path: "editProfile",
        component: ProfilePageComponent,
        canActivate: [RolesAuthGuardService],
        data: ['editProfile']
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [RolesAuthGuardService],
        data: ['settings']
      },
      {
        path: 'settings/:data',
        component: SettingsComponent,
        canActivate: [RolesAuthGuardService],
        data: ['settings/:data']
      },
    ]
  },
  { path: '**', redirectTo: 'dashboard' }
]
@NgModule({
  declarations: [
    BaseLayoutComponent,
    DashboardComponent,
    ProfilePageComponent,
    ProfileModalComponent,
    SettingsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    SlickCarouselModule,
    RouterModule.forChild(routes),
    NgBootstrapFormValidationModule.forRoot(),
    Ng4GeoautocompleteModule.forRoot(),
    LayoutsModule
  ],
  entryComponents: [ProfileModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class ProfileModule { }
