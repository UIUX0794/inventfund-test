import { Component, OnInit } from '@angular/core';
import { InventFundsApiService } from '../../services/invent-funds-api-service';
import { AppStateService } from '../../services/app-state.service';
import { ThemeOptions } from '../../theme-options';
const URL = 'api/attachment/uploadDirectToS3';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  // global variables

  profileJson = [];
  passwordJson = [];
  refMapPasswordChange = { sourceId: '', sourceType: '' };
  refMapProfileChange = { sourceId: '', sourceType: '' };
  profileData;
  passwordData;

  /**
   *
   * @param globals set theme options
   * @param appState get appstate variable and methods
   * @param inventFundsApiService call apis from services
   * @param router navigate to different components
   */

  constructor(
    public globals: ThemeOptions,
    public appState: AppStateService,
    private inventFundsApiService: InventFundsApiService,
    private router: Router
  ) {
    this.getPasswordChangeJson();
    this.getProfileJson();
    this.globals.toggleSidebar = true;
  }

  // ng onInit

  ngOnInit() {
    this.getProfileData();
  }

  // password change screen builder data

  getPasswordChangeJson() {
    var params = {
      collectionName: 'passwordChange',
    };
    this.inventFundsApiService.retrieveScreenbuilderAll(params).then((res) => {
      // console.log("passwordChange res " + JSON.stringify(res.response))
      // this.passwordJson = res.response;
      this.passwordJson = [
        {
          key: 'password',
          validators: {
            fieldMatch: {
              expression: (control) => {
                const value = control.value;
                return (
                  value.confirmNewPassword === value.newPassword ||
                  // avoid displaying the message error when values are empty
                  !value.confirmNewPassword ||
                  !value.newPassword
                );
              },
              message: 'Password Not Matching',
              errorPath: 'confirmNewPassword',
            },
          },
          fieldGroup: res.response[0].fieldGroup,
        },
      ];
    });
  }

  // profile screen builder data

  getProfileJson() {
    var params = {
      collectionName: 'profile',
    };
    this.inventFundsApiService.retrieveScreenbuilderAll(params).then((res) => {
      this.profileJson = res.response;
      // console.log("profile res " + JSON.stringify(res.response))
    });
  }

  // To get image base64 data

  getImageBase64Dat(event) {
    console.log('settings profile imge>>>> : ' + JSON.stringify(event));
    // https://invent-fund-dev.s3.ap-south-1.amazonaws.com/inventFundMulter/HOST/100/IFFIXER/LOGO/5ecf7f4f01207222c87f5054/0266554465.jpeg
    // var n = event.lastIndexOf('/');
    // var key = event.substring(n + 1);
    // var path = 'inventFund' + event.split('inventFund')[1].replace(key, '');
    // // console.log('after data split : ' + JSON.stringify(path));
    // // console.log('key data : ' + JSON.stringify(key));
    // // console.log("person id : " + JSON.stringify(this.appState.encryptedDataValue('profile')))
    if (sessionStorage.getItem('profileImage')) {
      // console.log('object if');
      var imageParams = {
        collectionName: 'attachment',
        createData: {
          id: this.appState.encryptedDataValue('personId'),
          // path: path,
          // key: key,
          url: event,
          type: 'PROFILE',
        },
      };
      // console.log('settings profile imge>>>> : ' + JSON.stringify(imageParams));
      this.inventFundsApiService
        .attachmentUpdateSingleFile(imageParams)
        .then((res: any) => {
          if (res.code == 200) {
            console.log('each image attache : ' + JSON.stringify(res));
            // var param = {
            //   path: res.data[0].path,
            //   key: res.data[0].key,
            // };
            // // console.log('response data : ' + JSON.stringify(param));
            // this.inventFundsApiService
            //   .getImageData(param)
            //   .then((result) => {
            //     // console.log('result data : ' + JSON.stringify(result));
            //     if (result.status == 200) {

            // this.appState.profileImage = res.data[0].url;
            // this.appState.setProfileImage(res.data[0].url);
            // this.appState.setAvatar(res.data[0].url);
            // this.appState.profileImageSigned = res.data[0].url;
            // sessionStorage.removeItem('profileImage');
            // sessionStorage.setItem('profileImage', res.data[0].url);
            //     }
            //   })
            //   .catch((err) => {
            //     console.log('err data : ' + JSON.stringify(err));
            //   });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      // console.log('object else');
      var imageParams = {
        collectionName: 'attachment',
        createData: {
          id: this.appState.encryptedDataValue('personId'),
          // path: path,
          // key: key,
          url: event,
          type: 'PROFILE',
        },
      };
      // console.log('settings profile imge>>>> : ' + JSON.stringify(imageParams));
      this.inventFundsApiService
        .attachmentSingleFile(imageParams)
        .then((res: any) => {
          if (res.code == 200 && res.data.length != 0) {
            console.log('data response for image else : ' + JSON.stringify(res.data));
            //   var param = {
            //     path: res.data[0].path,
            //     key: res.data[0].key,
            //   };
            //   // console.log('response data : ' + JSON.stringify(param));
            //   this.inventFundsApiService
            //     .getImageData(param)
            //     .then((result) => {
            //       // console.log('result data : ' + JSON.stringify(result));
            //       if (result.status == 200) {
            this.appState.profileImage = res.data[0].url;
            this.appState.setProfileImage(res.data[0].url);
            this.appState.setAvatar(res.data[0].url);
            this.appState.profileImageSigned = res.data[0].url;
            //         // console.log(
            //         //   'app state profile image : ' + JSON.stringify(this.appState.profileImageSigned)
            //         // );
            sessionStorage.removeItem('profileImage');
            sessionStorage.setItem('profileImage', res.data[0].url);
            this.appState.getProfileImage();
            //       }
            //     })
            //     .catch((err) => {
            //       console.log('err data : ' + JSON.stringify(err));
            //     });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  // on sucess

  onSuccess(response) {
    this.appState.setAvatar(response.newAvatar);
  }

  // Tp get formly data

  getFormlyData(event) {
    // console.log("getFormlyData : " + JSON.stringify(event));
    if (event.type == 'profile') {
      Object.assign(event.data, {
        personId: this.appState.encryptedDataValue('personId'),
        sourceType: 'Profile',
      });
      var params = {
        updateData: event.data,
        module: 'user',
        endPoint: 'CREATEPROFILE',
      };
      this.inventFundsApiService
        .createGeneric(params)
        .then((res) => {
          // console.log('data created : ' + JSON.stringify(res));
          this.inventFundsApiService.showAlertCreateMessage();
        })
        .catch((err) => {
          this.inventFundsApiService.showAlertCreateErrorMessage();
        });
    }
  }

  // get formly update data

  getFormlyUpdateData($event) {
    // console.log('data to update : ' + JSON.stringify($event));
    if ($event.type == 'profile') {
      this.updateProfileData($event);
    }
  }

  // update profile data

  updateProfileData($event) {
    // console.log('update : ' + JSON.stringify($event));
    var emittedData = $event['data'].response;
    // Object.assign(emittedData,{partyId:sessionStorage.getItem('partyId')});
    var profile = {
      collectionName: 'person',
      pkId: $event['data'].id,
      updateData: emittedData,
      endPoint: 'UPDATEPROFILE',
    };
    this.inventFundsApiService
      .updateGeneric(profile)
      .then((res: any) => {
        this.inventFundsApiService.showAlertUpdateMessageNoReload();
        this.getProfile();
      })
      .catch((err) => {
        console.log('Error:' + JSON.stringify(err));
        this.inventFundsApiService.showAlertUpdateErrorMessageNoReload();
      });
  }

  // get profile data

  getProfileData() {
    this.getProfile();
  }

  // get profile data

  getProfile() {
    // console.log('fetchAdd:' + JSON.stringify(this.appState.encryptedDataValue('personId')));
    // this.dataEmitted = eventArgs;
    var params = {
      collectionName: 'person',
      _id: this.appState.encryptedDataValue('personId'),
      endPoint: 'RETRIVEPROFILE',
    };
    this.inventFundsApiService
      .retrieveGeneric(params)
      .then((res: any) => {
        // console.log("data : " + JSON.stringify(res))
        if (res.status == 200) {
          if (res.response.length != 0) {
            // console.log("profileData " + JSON.stringify(res.response[0]))
            //   // this.selectedIndex = 1;
            var objectData = res.response[0];
            Object.assign(objectData, {
              userName: this.appState.encryptedDataValue('personEmail'),
            });
            this.profileData = res.response[0];
            this.refMapProfileChange = {
              sourceId: res.response[0]._id,
              sourceType: 'Profile',
            };
          } else {
            // console.log("else : " + JSON.stringify(this.refMapProfileChange))
            this.refMapProfileChange = {
              sourceId: this.appState.encryptedDataValue('personId'),
              sourceType: 'Profile',
            };
          }
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  // update password emited from settings compnent

  updatePasswordEmit($event) {
    // console.log("data for update password new : " + JSON.stringify($event))
    // var emittedData = $event['data'].response;
    // Object.assign(emittedData,{partyId:sessionStorage.getItem('partyId')});
    // console.log("update password : " + JSON.stringify($event))
    if ($event) {
      var params = {
        module: 'user',
        userName: this.appState.encryptedDataValue('personEmail'),
        newPassword: $event.newPassword,
        endPoint: 'RESETPASSWORD',
      };
      this.inventFundsApiService.resetPassword(params).then((res) => {
        // console.log("data created : " + JSON.stringify(res))
        if (res.code == 200) {
          this.passwordData = null;
          // this.inventFundsApiService.showAlertUpdateMessageNoReload()
          Swal.fire({
            title:
              'Your password as been changed sucessffully, please Login with new password to access your application!',
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Okay',
            customClass: {
              // container: 'sweet_containerImportant',
              title: 'swal-title',
              actions: 'swal2-actions',
              confirmButton: 'font-large btn btn-wide mb-2 mr-2 btn-primary',
              // cancelButton: 'sweet_cancelbuttonImportant',
            },
          }).then(() => {
            sessionStorage.clear();
            this.router.navigate(['/login']);
          });
        } else {
          this.inventFundsApiService.showAlertUpdateErrorMessageNoReload();
        }
      });
    }
  }
}
