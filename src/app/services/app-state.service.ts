import { Injectable } from '@angular/core';
import * as crypto from 'crypto-js';
import { roleAccessJson } from '../services/roleaccessjson';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { InventFundsApiService } from './invent-funds-api-service';

@Injectable({
  providedIn: 'root',
})
export class AppStateService {
  // global variables

  avatar$: BehaviorSubject<any>;
  profileSummary$: BehaviorSubject<any>;
  educationDetails$: BehaviorSubject<any>;
  professionalDetails$: BehaviorSubject<any>;
  achievementsDetails$: BehaviorSubject<any>;
  skillsDetails$: BehaviorSubject<any>;
  showSideBar: boolean = true;
  profileImage = '';
  settingPageClass: boolean = true;
  globalData: any = {
    roleId: '',
    roleCode: '',
    partyId: '',
    personId: '',
    personEmail: '',
    personType: '',
    orgId: '',
    partyCode: '',
    token: '',
  };
  encryptSecretKey: any = 'KA0f2100e34b54d5VA0fd013df8f300d3497H579O';
  encryptOptions = { mode: crypto.mode.CBC, padding: crypto.pad.Pkcs7 };
  encryptedData: any;
  storedRoute;
  // currentRouter;
  currentRouter: BehaviorSubject<any>;
  roleAccess = roleAccessJson;
  profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
  profileImageSigned: any;

  /**
   * constructor
   */

  constructor(private inventFundsApiService: InventFundsApiService) {
    this.avatar$ = new BehaviorSubject(null);
    this.currentRouter = new BehaviorSubject(null);
    this.profileSummary$ = new BehaviorSubject(null);
    this.educationDetails$ = new BehaviorSubject(null);
    this.professionalDetails$ = new BehaviorSubject(null);
    this.achievementsDetails$ = new BehaviorSubject(null);
    this.skillsDetails$ = new BehaviorSubject(null);
    // console.log('app state');
    if (sessionStorage.getItem('profile') != null) {
      this.encryptProfileJson();
    }
  }

  // encrypr data using the crypto

  encryptProfileJson() {
    // console.log('profile data : ' + JSON.stringify(sessionStorage.getItem('profile')));
    var decrypted = crypto.AES.decrypt(
      sessionStorage.getItem('profile'),
      this.encryptSecretKey,
      this.encryptOptions
    );
    if (decrypted.toString()) {
      var profileJson = JSON.parse(decrypted.toString(crypto.enc.Utf8));
      this.encryptedData = profileJson;
      // console.log('profile data : ' + JSON.stringify(this.encryptedData));
    }
  }

  // to get encrypted data values dippends on key

  encryptedDataValue(value) {
    if (this.encryptedData) {
      return this.encryptedData[value];
    }
  }

  // to check weather session storage consists any data or not

  getsessionStorageData() {
    if (sessionStorage.getItem('profile')) {
      return false;
    } else {
      return true;
    }
  }

  // get profile image async

  // getProfileImage(from) {
  //   const params = {
  //     personId: this.encryptedDataValue('personId'),
  //     type: 'PROFILE',
  //   };

  //   return this.inventFundsApiService
  //     .getImageDataWithId(params)
  //     .then((result) => {
  //       // console.log(
  //       //   'result data of signed with id : ' + JSON.stringify(result) + ' ' + JSON.stringify(from)
  //       // );
  //       if (result.code === 200) {
  //         if (result.data !== null) {
  //           this.profileImageAfterSet = result.data;
  //           this.setAvatar(result.data);
  //         } else {
  //           this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
  //           this.setAvatar(this.profileImageAfterSet);
  //         }
  //         sessionStorage.setItem('profileImage', 'profile');
  //         return this.profileImage;
  //       } else {
  //         this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
  //         return this.profileImage;
  //       }
  //     })
  //     .catch((err) => {
  //       console.log('error : ' + JSON.stringify(err));
  //       this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
  //       return this.profileImage;
  //     });
  // }

  getProfileImage() {
    if (sessionStorage.getItem('profileImage')) {
      this.profileImageAfterSet = sessionStorage.getItem('profileImage');
      return this.profileImage;
    } else {
      this.profileImageAfterSet = '../../../assets/images/avatars/profile.jpg';
      return this.profileImage;
    }
  }

  // get an profile avatar after uploading in profile settings

  getAvatar() {
    return this.avatar$.asObservable();
  }

  // to set the avatar after changing the profile image

  setAvatar(value: any) {
    this.setProfileImage(value);
    this.avatar$.next(value);
  }

  // to get the current route async

  getRouter() {
    return this.currentRouter.asObservable();
  }

  // setting current route async

  setRouter(data) {
    this.currentRouter.next(data);
  }

  // set profile image on chage async

  setProfileImage(data) {
    // console.log('data profile image : ' + JSON.stringify(data));
    this.profileImageAfterSet = data;
  }

  // set profile side bar after filling data true or fase

  /**
   *
   * @param profileSummary to set profile summary variable to show done or cancel images
   * @param educationDetails to set educationDetails variable to show done or cancel images
   * @param professionalDetails to set professionalDetails variable to show done or cancel images
   * @param achievementsDetails to set achievementsDetails variable to show done or cancel images
   * @param skillsDetails to set skillsDetails variable to show done or cancel images
   */

  setProfileStatus(
    profileSummary,
    educationDetails,
    professionalDetails,
    achievementsDetails,
    skillsDetails
  ) {
    this.profileSummary$.next(profileSummary);
    this.educationDetails$.next(educationDetails);
    this.professionalDetails$.next(professionalDetails);
    this.achievementsDetails$.next(achievementsDetails);
    this.skillsDetails$.next(skillsDetails);
  }

  // getting profile side bar data variables in async

  getProfileStatus() {
    var obj = {
      profileSummary: this.profileSummary$.asObservable(),
      educationDetails: this.educationDetails$.asObservable(),
      professionalDetails: this.professionalDetails$.asObservable(),
      achievementsDetails: this.achievementsDetails$.asObservable(),
      skillsDetails: this.skillsDetails$.asObservable(),
    };
    return obj;
  }

  // setting profile data in an array of object

  setDataProfile(data1, data2, data3, data4, data5) {
    // console.log("data 1 : " + JSON.stringify(data1))
    // console.log("data 2 : " + JSON.stringify(data2.filter(item => item._id != "").length))
    // console.log("data 3 : " + JSON.stringify(data3))
    // console.log("data 4 : " + JSON.stringify(data4))
    // console.log("data 5 : " + JSON.stringify(data5))
    var data1Set = null;
    var data2Set = null;
    var data3Set = null;
    var data4Set = null;
    var data5Set = null;

    if (Object.keys(data1).length) {
      data1Set = data1.profileSummary == '' ? null : true;
    }
    if (data2.length) {
      data2Set = data2.filter((item) => item._id != '').length ? true : null;
    }
    if (data3.length) {
      data3Set = data3.filter((item) => item._id != '').length ? true : null;
    }
    if (data4.length) {
      data4Set = data4.filter((item) => item._id != '').length ? true : null;
    }
    if (data5.length) {
      data5Set = data5.length ? true : null;
    }
    this.setProfileStatus(data1Set, data2Set, data3Set, data4Set, data5Set);
  }

  getSignedUrl(path, key) {
    var param = {
      path: path,
      key: key,
    };
    // console.log('response data : ' + JSON.stringify(param));
    this.inventFundsApiService
      .getImageData(param)
      .then((result) => {
        // console.log('result data : ' + JSON.stringify(result));
        if (result.status == 200) {
          this.profileImage = result.data;
          this.setProfileImage(result.data);
          this.setAvatar(result.data);
          // sessionStorage.removeItem('profileImage');
          sessionStorage.setItem('profileImage', 'profile');
        }
      })
      .catch((err) => {
        console.log('err data : ' + JSON.stringify(err));
      });
  }
}
