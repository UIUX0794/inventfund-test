import { Injectable } from '@angular/core';
import { AppStateService } from './app-state.service';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {

  // global variables

  encryptedData: any;

  /**
   * 
   * @param appStateService to get appstate data
   */

  constructor(private appStateService: AppStateService) { }

  // get image path to set attchments folder in s3 bucket

  getImagePath(type: any, id: any): any {

    var orgCode = this.appStateService.encryptedDataValue('roleCode');
    var partyCode = this.appStateService.encryptedDataValue('partyCode');
    var orgId = this.appStateService.encryptedDataValue('orgId');

    var defaultPath = 'HOST' + '/' + orgCode;

    var folderPath: any;
    if (type == "LOGO") {
      return folderPath = defaultPath + '/' + partyCode + '/' + 'LOGO' + '/' + id;
    }
    if (type == "SHOWCASE") {
      return folderPath = defaultPath + '/' + partyCode + '/' + 'SHOWCASE' + '/' + id;
    }
  }
}
