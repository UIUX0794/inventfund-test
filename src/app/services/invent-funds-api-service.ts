import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as config from '../../providers/config';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class InventFundsApiService {
  /**
   *
   * @param _httpClient to access the post , get for api end points
   */

  constructor(private _httpClient: HttpClient) {}

  // setting headers for api end points

  private headers = new HttpHeaders({
    'Content-Type': 'text/html',
  });

  // get end point from the provider config by passing that end point variable

  getEndPoint(endPoint) {
    return config[endPoint];
  }

  signIn(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.SIGNIN, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  signUp(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.SIGNUP, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  getToken(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.GETTOKEN, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  linkedInSignIn(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get(config.LINKEDINSIGNIN, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  appendSectionUrlandId(url, params) {
    var urlAppended;
    // console.log(
    //   "data+'?params='+params : " + JSON.stringify(JSON.stringify(url) + '?params=' + params)
    // );
    return (urlAppended = url + '/' + params);
  }

  linkedInRequestToken(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .get(config.LINKEDINREQUESTTOKEN, { headers: this.headers, params: params })
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  google(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .get(config.GOOGLE, { headers: this.headers, params: params })
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  googleCallback(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .get(config.GOOGLECALLBACK, { headers: this.headers, params: params })
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  changePassword(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.CHANGEPASSWORD, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  resetPassword(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.RESETPASSWORD, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  updateRoleAndOrgDetails(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.UPDATEORGANDROLE, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  retrieveMongoDBAll(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.RETRIEVEMONGOALL, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  retrieveMongoDBOne(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.RETRIEVEMONGOONE, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  createMongodbData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.CREATEMONGODATA, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  updateMongodbData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.UPDATEMONGODATA, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  deleteMongodbData(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.DELETEMONGODATA, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  retrieveScreenbuilderAll(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.RETRIEVESCREENBUILDERALL, params).subscribe((response: any) => {
        // console.log('response : ' + JSON.stringify(response));
        resolve(response);
      }, reject);
    });
  }

  retrieveScreenbuilderLookUp(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(this.getEndPoint(params.endPoint), params)
        .subscribe((response: any) => {
          // console.log('response : ' + JSON.stringify(response));
          resolve(response);
        }, reject);
    });
  }

  attachmentSingleFile(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.SINGLEATTACHMENT, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  attachmentUpdateSingleFile(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.UPDATEATTACHMENT, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  retrieveGeneric(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(this.getEndPoint(params.endPoint), params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  createGeneric(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(this.getEndPoint(params.endPoint), params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  updateGeneric(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(this.getEndPoint(params.endPoint), params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }
  deleteGeneric(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(this.getEndPoint(params.endPoint), params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  retrieveScreenDetailsGrouped(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(config.RETRIEVESCREENBUILDERGROUPED, params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  getImageData(params): Promise<any> {
    // console.log('api hit : ' + JSON.stringify(params));
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.GETATTACHMENTDATA, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  getImageDataWithId(params): Promise<any> {
    // console.log('api hit : ' + JSON.stringify(params));
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(config.RETRIEVEATTACHMENTDATASIGNEDWITHID, params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  deleatAttachmentFile(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.DELETEATTACHMENT, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }

  deleatAttachmentFileSingle(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient
        .post(config.DELETEATTACHMENTDATARECORD, params)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  showAlertCreateMessage() {
    Swal.fire({
      title: 'Successfully Created',
      icon: 'success',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertCreateErrorMessage() {
    Swal.fire({
      title: 'Create Unsuccessful',
      icon: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#FF0000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertUpdateMessage() {
    Swal.fire({
      title: 'Successfully Updated',
      icon: 'success',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertUpdateMessageNoReload() {
    Swal.fire({
      title: 'Successfully Updated',
      icon: 'success',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertUpdateErrorMessage() {
    Swal.fire({
      title: 'Update Unsuccessful',
      icon: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#FF0000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertUpdateErrorMessageNoReload() {
    Swal.fire({
      title: 'Update Unsuccessful',
      icon: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#FF0000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertDeleteMessage() {
    Swal.fire({
      title: 'Successfully deleted',
      icon: 'success',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  showAlertDeleteErrorMessage() {
    Swal.fire({
      title: 'Delete Unsuccessful',
      icon: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#FF0000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Okay',
      customClass: {
        // container: 'sweet_containerImportant',
        title: 'swal-title',
        actions: 'swal2-actions',
        confirmButton: 'swal2-confirm font-large btn btn-wide mb-2 mr-2 btn-primary',
        cancelButton: 'swal2-confirm',
      },
    }).then(() => {
      // window.location.reload();
    });
  }

  verifyToken(params): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.post(config.VERIFYTOKEN, params).subscribe((response: any) => {
        resolve(response);
      }, reject);
    });
  }
}
