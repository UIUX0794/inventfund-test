/**
 * define all the routing end points so that we can give 
 * access to each route dippends on role code
 */

export const roleAccessJson = [{
  "moduleName": "dashboard",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "dashboard"
},
{
  "moduleName": "login",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "login"
},
{
  "moduleName": "linkedInLogin",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "linkedInLogin"
},
{
  "moduleName": "sign-up",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "sign-up"
},
{
  "moduleName": "sign-up-details",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "sign-up-details"
},
{
  "moduleName": "sign-up-roles",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "sign-up-roles"
},
{
  "moduleName": "help-inovator-page",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "help-inovator-page"
},
{
  "moduleName": "email-sent",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "email-sent"
},
{
  "moduleName": "thankyou",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "thankyou"
},
{
  "moduleName": "gotAnIdea",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "gotAnIdea"
},
{
  "moduleName": "forgot-password",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "forgot-password"
},
{
  "moduleName": "invest-in-ideas",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "invest-in-ideas"
},
{
  "moduleName": "landing-page",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "landing-page"
},
{
  "moduleName": "settings",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "settings"
},
{
  "moduleName": "editProfile",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "editProfile"
},
{
  "moduleName": "messages",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "messages"
},
{
  "moduleName": "connections",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "connections"
},
{
  "moduleName": "jobs",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "jobs"
},
{
  "moduleName": "settings/:data",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "settings/:data"
},
{
  "moduleName": "sign-up-details/:data",
  "roleCode": [
    "100",
    "101",
    "103",
    "104"
  ],
  "routeName": "sign-up-details/:data"
}]