import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AppStateService } from './app-state.service';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import * as crypto from 'crypto-js';
import { Router } from '@angular/router';
import { InventFundsApiService } from './invent-funds-api-service';

@Injectable({
  providedIn: 'root'
})

export class RolesAuthGuardService implements CanActivate {

  /**
   * define which are the restricted routes so that after or befor 
   * login or signup they can't access
   */

  restrictedRoutes = [
    "login", "sign-up", "sign-up-roles",
    "email-sent", "linkedInLogin",
    "sign-up-details/:data", "sign-up-details"
  ];

  /**
   * 
   * @param appState to get variables or data from appstate    
   * @param router to navigate between different routes
   * @param inventFundApi to access the api from server
   */

  constructor(public appState: AppStateService, public router: Router,
    public inventFundApi: InventFundsApiService) { }

  // to get activated router path and the route name

  path: import("@angular/router").ActivatedRouteSnapshot[];
  route: import("@angular/router").ActivatedRouteSnapshot;

  canActivate(route): any {
    let roles = route.data[0] as string;
    if (sessionStorage.getItem('profile') == null) {
      console.log(sessionStorage.getItem('personId'))
      const restrictedRouteCheck = this.restrictedRoutes.includes(roles)
      if(restrictedRouteCheck && roles != 'sign-up-roles') {
        return true;
      } else if(restrictedRouteCheck && roles == 'sign-up-roles' && sessionStorage.getItem('personId')) {
        return true;
      } else {
        this.router.navigate(['landing/landing-page']);
        return false;
      }
    } else if (this.appState.roleAccess != undefined) {
      var routeData = this.appState.roleAccess.filter((item) => roles == item.routeName);
      // console.log("route data : " + JSON.stringify(routeData));
      var rolesAccess: any[] = routeData[0].roleCode;
      var routeName = routeData[0].routeName;
      // console.log("token data : " + JSON.stringify(this.appState.encryptedDataValue('token')))
      console.log("data router : " + JSON.stringify(this.restrictedRoutes.includes(routeName)))
      console.log("data router : " + JSON.stringify(routeName))
      if (this.appState.encryptedDataValue('token') != undefined) {
        var params = {
          "token": this.appState.encryptedDataValue('token')
        }
        return this.inventFundApi.verifyToken(params).then(
          (res) => {
            console.log("res toke : " + JSON.stringify(res))
            if (res.code == '200') {
              if (rolesAccess) {
                var jwtHelper = new JwtHelper()
                  .decodeToken(this.appState.encryptedDataValue('token'));
                // var isExp = jwtHelper.isTokenExpired(this.appState.encryptedDataValue('token'))
                // console.log("decoded toke : " + JSON.stringify(jwtHelper))
                // console.log("token expired : "+JSON.stringify(isExp))
                // console.log("route name includes : " + this.restrictedRoutes.includes(routeName));
                // console.log("routeName : " + JSON.stringify(routeName))
                this.appState.setRouter(routeName);
                if (Object.keys(jwtHelper).length &&
                  this.restrictedRoutes.includes(routeName) == false) {
                  // console.log("jwtHelper data : " + JSON.stringify(jwtHelper))
                  var decrypted = crypto.AES.decrypt(sessionStorage.getItem('profile'),
                    this.appState.encryptSecretKey,
                    this.appState.encryptOptions);
                  if (decrypted.toString()) {
                    var profileJson = JSON.parse(decrypted.toString(crypto.enc.Utf8));
                    if (profileJson.roleCode) {
                      var curentRoleAccess = rolesAccess.includes(profileJson.roleCode);
                      if (curentRoleAccess) {
                        return true;
                      } else {
                        this.router.navigate(["auth/un-authorized"]);
                        return false;
                      }
                    }
                  }
                } else if (sessionStorage.getItem('profile') == null) {
                  // console.log("restricted route");
                  // console.log("local storage data : " + JSON.stringify(sessionStorage.getItem('profile')))
                  // this.router.navigate(["/landing"]);
                  return true;
                } else {
                  this.router.navigate(["auth/un-authorized"]);
                  return false;
                }
              }
            } else {
              sessionStorage.clear();
              this.router.navigate(['']);
              return false;
            }
          })
      } else if (this.restrictedRoutes.includes(routeName)) {
        return true;
      } else {
        return true;
      }
    } else {
      this.router.navigate(["auth/un-authorized"]);
      return false;
    }
  }

}
