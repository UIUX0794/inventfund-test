import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingHeaderComponent } from '../layout/landing-header/landing-header.component';
import { LandingFooterComponent } from '../layout/landing-footer/landing-footer.component';
import { CarouselComponent, CarouselSlideElement } from '../ct-module/carousel/carousel.component';
import { CarouselDirective } from '../ct-module/carousel/carousel.directive';

@NgModule({
  declarations: [
    LandingHeaderComponent,
    LandingFooterComponent,
    CarouselComponent, 
    CarouselDirective, 
    CarouselSlideElement, 
  ],
  imports: [
    CommonModule
  ],
  exports:[
    LandingHeaderComponent,
    LandingFooterComponent,
    CarouselComponent, 
    CarouselDirective, 
    CarouselSlideElement, 
  ]
})
export class LayoutsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LayoutsModule
    };
  }
 }
