import { NgModule, ModuleWithProviders, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { NgReduxModule } from '@angular-redux/store';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgbModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { CountUpModule } from 'ngx-countup';
import CountoModule from 'angular2-counto';
import { MatCheckboxModule, MatExpansionModule, MatSelectModule, MatOptionModule, MatIconModule, MatCardModule, MatDividerModule, MatInputModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { CtModule } from '../ct-module/ct-module.module';
import { SocialLoginModule } from 'angularx-social-login';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { LandingHeaderComponent } from '../layout/landing-header/landing-header.component';
import { SidebarComponent } from '../layout/layout-components/sidebar/sidebar.component';
import { OptionsDrawerComponent } from '../ThemeOptions/options-drawer/options-drawer.component';
import { DrawerComponent } from '../layout/layout-components/header/elements/drawer/drawer.component';

@NgModule({
  declarations: [
    SidebarComponent,
    OptionsDrawerComponent,
    DrawerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgReduxModule,
    FormlyBootstrapModule,
    AngularFontAwesomeModule,
    PerfectScrollbarModule,
    NgbModule,
    NgbCarouselModule,
    RoundProgressModule,
    SlickCarouselModule,
    CountUpModule,
    CountoModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    CtModule,
    MatExpansionModule,
    MatCardModule,
    MatDividerModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgMultiSelectDropDownModule,
    NgxDocViewerModule,
    NgBootstrapFormValidationModule.forRoot()
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgReduxModule,
    AngularFontAwesomeModule,
    PerfectScrollbarModule,
    NgbModule,
    NgbCarouselModule,
    RoundProgressModule,
    SlickCarouselModule,
    CountUpModule,
    CountoModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    CtModule,
    MatExpansionModule,
    MatCardModule,
    MatDividerModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgMultiSelectDropDownModule,
    NgxDocViewerModule,
    SidebarComponent,
    OptionsDrawerComponent,
    DrawerComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }
}
