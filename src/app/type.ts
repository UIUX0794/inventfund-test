import { Component, ViewChild } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { MatInput } from '@angular/material';

@Component({
  selector: 'app-form-datepicker-type',
  template:
    `
    <input
      type="date"
      class="form-control calendar"
      placement="bottom"
      [formControl]="formControl"
      [formlyAttributes]="field"
      placeholder="DD-MM-YYYY"
      >`
  //   `
  //   <input matInput
  //   [errorStateMatcher]="errorStateMatcher"
  //   [formControl]="formControl"
  //   [matDatepicker]="picker"
  //   [matDatepickerFilter]="to.datepickerOptions.filter"
  //   [formlyAttributes]="field">
  // <ng-template #matSuffix>
  //   <mat-datepicker-toggle [for]="picker"></mat-datepicker-toggle>
  // </ng-template>
  // <mat-datepicker #picker></mat-datepicker>
  //   `

  ,
})
export class DatepickerTypeComponent extends FieldType { }
