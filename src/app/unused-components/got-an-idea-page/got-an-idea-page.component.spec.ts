import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GotAnIdeaPageComponent } from './got-an-idea-page.component';

describe('GotAnIdeaPageComponent', () => {
  let component: GotAnIdeaPageComponent;
  let fixture: ComponentFixture<GotAnIdeaPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GotAnIdeaPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GotAnIdeaPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
