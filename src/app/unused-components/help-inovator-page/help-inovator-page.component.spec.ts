import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpInovatorPageComponent } from './help-inovator-page.component';

describe('HelpInovatorPageComponent', () => {
  let component: HelpInovatorPageComponent;
  let fixture: ComponentFixture<HelpInovatorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpInovatorPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpInovatorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
