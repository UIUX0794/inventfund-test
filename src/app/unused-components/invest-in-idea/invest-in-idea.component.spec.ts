import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestInIdeaComponent } from './invest-in-idea.component';

describe('InvestInIdeaComponent', () => {
  let component: InvestInIdeaComponent;
  let fixture: ComponentFixture<InvestInIdeaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestInIdeaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestInIdeaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
