import { ErrorMessage } from "ng-bootstrap-form-validation";

export const CUSTOM_ERRORS: ErrorMessage[] = [
  {
    error: "duplicateEmail",
    format: duplicateEmailFormat
  },
  {
    error: "passwordMismatch",
    format: passwordMismatchFormat
  },
  {
    error: "invalidEmail",
    format: invalidEmailFormat
  },
  {
    error: "invalidPassword",
    format: invalidPasswordFormat
  },
  {
    error: "passwordDosentMatch",
    format: passwordDosentMatchFormat
  },
  {
    error: "newPasswordCantBeSame",
    format: samePassword
  },
  {
    error: "equalTo",
    format: equalToFormat
  },
  {
    error: "pattern",
    format: patternFormat
  },
  {
    error: "minDate",
    format: minDatevalidation
  },
  {
    error: "maxDate",
    format: maxDatevalidation
  },
  {
    error: "phoneNumber",
    format: phoneNumberValidation
  },
  {
    error: "fromDateTo",
    format: fromDateToValidation
  },
  {
    error: "alpha",
    format: alphaValidation
  },
  {
    error: "alphaNumeric",
    format: alphaNumericValidation
  }
];

export function alphaValidation(label: string, error: any): string {
  return `Please enter valid data!`;
}

export function alphaNumericValidation(label: string, error: any): string {
  return `Please enter valid data!`;
}

export function duplicateEmailFormat(label: string, error: any): string {
  return ` Email already exists in the system!`;
}
export function passwordMismatchFormat(label: string, error: any): string {
  return ` Passwords do not match!`;
}
export function invalidEmailFormat(label: string, error: any): string {
  return ` Email does not exist in the system!Please Sign Up`;
}
export function invalidPasswordFormat(label: string, error: any): string {
  return ` Invalid Password!`;
}

export function passwordDosentMatchFormat(label: string, error: any): string {
  return ` Please enter current password!`;
}

export function samePassword(label: string, error: any): string {
  return ` New password can't be same as current password!`;
}

export function equalToFormat(label: string, error: any): string {
  return ` Password doesn't match!`;
}

export function patternFormat(label: string, error: any): string {
  // console.log("label : " + JSON.stringify(label));
  // console.log("error : " + JSON.stringify(error));
  if (error.requiredPattern == '^(?!^ +$)^.+$') {
    return `Not a valid text!`;
  } else {
    return `Password must have a minimum length of 7 characters\n out of which one number and one upper case\n letter are mandatory!`;
  }
}

export function phoneNumberValidation(label: string, error: any): string {
  // console.log("label : " + JSON.stringify(label));
  // console.log("error : " + JSON.stringify(error));
  return `please enter valid phone number!`;
}

export function minDatevalidation(label: string, error: any): string {
  // console.log({ label });
  // console.log({ error });
  return `Please enter valid date!`;
}

export function maxDatevalidation(label: string, error: any): string {
  // console.log({ label });
  // console.log({ error });
  return `Please enter valid date!`;
}

export function fromDateToValidation(label: string, error: any): string {
  // console.log({ label });
  // console.log({ error });
  return `Please enter date greater than from date!`;
}
