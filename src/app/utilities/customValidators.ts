import { AbstractControl } from '@angular/forms';
import { FormControl, ValidationErrors } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import * as moment from 'moment';

export function alphaValidator(control: FormControl): ValidationErrors {
    return !control.value || /^[a-z ,.'-]+$/i.test(control.value) ? null : { 'alpha': true };
}

export function alphaNumericValidator(control: FormControl): ValidationErrors {
    return !control.value || /^[a-zA-Z0-9?=.,*!@#$%^&*_\-/\s]+$/i.test(control.value) ? null : { 'alphaNumeric': true };
}

export function alphaValidatorMessage(err, field: FormlyFieldConfig) {
    return `This field cannot contain numbers or special characters!`;
}

export function alphaNumericMessage(err, field: FormlyFieldConfig) {
    return `This field can be alpha numeric!`;
}

export function charactersNumbersValidator(control: FormControl): ValidationErrors {
    return !control.value || /^[a-zA-Z0-9_./-]*$/i.test(control.value) ? null : { 'charactersNumbers': true };
}

export function charactersNumbersValidatorMessage(err, field: FormlyFieldConfig) {
    return `This field cannot contain special characters!`;
}

export function EmailValidator(control: FormControl): ValidationErrors {
    return !control.value || /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(control.value) ? null : { 'email': true };
}

export function EmailValidatorMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Email id`;
}

export function maxValidationMessage(err, field: FormlyFieldConfig) {
    return `Value cannot be greater than ${field.templateOptions.max} `;
}

export function minValidationMessage(err, field: FormlyFieldConfig) {
    return `Value cannot be less than ${field.templateOptions.min} `;
}

export function maxLengthValidationMessage(err, field: FormlyFieldConfig) {
    return `Value cannot be greater than ${field.templateOptions.maxLength} `;
}

export function minLengthValidationMessage(err, field: FormlyFieldConfig) {
    return `Value cannot be less than ${field.templateOptions.minLength} `;
}

export function phoneNumberValidator(control: FormControl): ValidationErrors {
    if (control.value == 0) {
        return !control.value ? null : { 'phoneNumber': true };
    } else {
        return !control.value || /^\s*(?:\+?(\d{1,3}))?[- (]*(\d{3})[- )]*(\d{3})[- ]*(\d{4})(?: *[x/#]{1}(\d+))?\s*$/.test(control.value) ? null : { 'phoneNumber': true };
    }
}

export function phoneNumberValidatorMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Phone Number`;
}

export function doorNumbersValidator(control: FormControl): ValidationErrors {
    return !control.value || /((?:[a-zA-Z]+\s)*)([\d]+)(?:(?:\/)(\d+))?/.test(control.value) ? null : { 'doorNumbers': true };
}

export function doorNumbersMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Numbers`;
}

export function zipCodeValidator(control: FormControl): ValidationErrors {
    return !control.value || /\b\d{5}(?:-\d{4})?\b/.test(control.value) ? null : { 'zipCode': true };
}

export function zipCodeMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Zipcode`;
}

export function cityValidator(control: FormControl): ValidationErrors {
    return !control.value || /\b\d{5}(?:-\d{4})?\b/.test(control.value) ? null : { 'city': true };
}

export function cityMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Zipcode`;
}

export function poBoxValidator(control: FormControl): ValidationErrors {
    return !control.value || /^(?:(?<numberstreet>\d+(?:\ |\w|\.)+(?=$|\n|\,))|(?<pobox>(?:PO|P\.O\.)[\ ](?:BOX)[\ ](?:\d|\-)+(?=$|\n|\,)))?(?:\ *\,\ *)?(?<suite>(?:SUITE|STE)\ [\#]?(?:\w|\d|\-)+)?(?:\ *\,\ *)?(?<citystatezip>(?<=^|,|\ )(?<city>[\w\ ]+)(?:\ *\,\ *)(?<stateabrv>[A-Z]{1,3})(?:\ +)(?<zipcode>\d{5}(?:\-\d*)?))?$/.test(control.value) ? null : { 'poBox': true };
}

export function poBoxMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Po Box Number`;
}

export function addressValidator(control: FormControl): ValidationErrors {
    return !control.value || /\w+\s*(road|street|square|rd|st|sq)\W/.test(control.value) ? null : { 'address': true };
}

export function addressMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Address`;
}

export function identificationValidator(control: FormControl): ValidationErrors {
    return !control.value || /^((?!666|000)[0-8][0-9\_]{2}\-(?!00)[0-9\_]{2}\-(?!0000)[0-9\_]{4})*$/.test(control.value) ? null : { 'identification': true };
}

export function IFSCValidator(control: FormControl): ValidationErrors {
    return !control.value || /^[A-Za-z]{4}0[A-Z0-9a-z]{6}$/.test(control.value) ? null : { 'IFSC': true };
}

export function IFSCMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid IFSC Code`;
}

export function bankBranchValidator(control: FormControl): ValidationErrors {
    return !control.value || /^0[A-Z0-9a-z]{6}$/.test(control.value) ? null : { 'Branch': true };
}

export function bankBranchMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Branch Code`;
}

export function micrValidator(control: FormControl): ValidationErrors {
    return !control.value || /^[0-9]{9}$/.test(control.value) ? null : { 'MICR': true };
}

export function micrMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid MICR Number`;
}

export function identificationMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Identification Number`;
}


export function MaxDateValidator(control: FormControl): ValidationErrors {
    const controlDate = moment(control.value);
    const currentDate = moment();

    if (controlDate.isAfter(currentDate)) {
        return { 'maxDate': true };
    }
    return null;
}

export function minDateValidator(control: FormControl): ValidationErrors {
    const controlDate = moment(control.value);
    const currentDate = moment().subtract(1, 'd');

    if (controlDate.isBefore(currentDate)) {
        return { 'minDate': true };
    }
    // else {
    //     return {'minDate':true}
    // }
    return null;
}

export function MaxDateMessage(err, field: FormlyFieldConfig) {
    return `Date cannot be greater than today's date!`;
}

export function minDateMessage(err, field: FormlyFieldConfig) {
    return `Date cannot be less than today's date!`;
}

export function vinValidator(control: FormControl): ValidationErrors {
    return !control.value || /^[a-zA-Z0-9]{9}[a-zA-Z0-9-]{2}[0-9]{6}$/.test(control.value) ? null : { 'vinNumber': true };
}

export function vinMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid VIN Number`;
}

export function engineValidator(control: FormControl): ValidationErrors {
    return !control.value || /^([A-z]{2}[A-z0-9]{5,16})$/.test(control.value) ? null : { 'engineNumber': true };
}

export function engineMessage(err, field: FormlyFieldConfig) {
    return `"${field.formControl.value}" is not a valid Engine Number`;
}

export function emptySpacesValidator(control: FormControl): ValidationErrors {
    return !control.value || /^$|^\S+.*/i.test(control.value) ? null : { 'emptySpaces': true };
    // return !control.value
}

export function emptySpacesMessage(err, field: FormlyFieldConfig) {
    return `Not a valid text!`;
}

export function fromDateToMessage(err, field: FormlyFieldConfig) {
    return `Please enter date greater than from date!`;
}

export function ValidateAlphaNumeric(control: AbstractControl) {
    // ^[a-zA-Z]+[a-zA-Z .]*$
    // ^[a-zA-Z .]*$
    if (control.value == null || control.value == "") {
        return null;
    }
    else if (!/^[(a-zA-Z)]+[a-zA-Z0-9\s]*$/.test(control.value)) {
        return { validAlphaNumeric: true };
    }
    return null;
}

export function ValidateAlphaNumericMessage(control: AbstractControl) {
    return `This field can be alpha numeric!`;
}

export const validators = [
    { name: 'alpha', validation: alphaValidator },
    { name: 'alphaNumeric', validation: alphaNumericValidator },
    { name: 'alphaNumericVal', validation: ValidateAlphaNumeric },
    { name: 'charactersNumbers', validation: charactersNumbersValidator },
    { name: 'email', validation: EmailValidator },
    { name: 'phoneNumber', validation: phoneNumberValidator },
    { name: 'doorNumbers', validation: doorNumbersValidator },
    { name: 'zipCode', validation: zipCodeValidator },
    { name: 'city', validation: cityValidator },
    { name: 'poBox', validation: poBoxValidator },
    { name: 'address', validation: addressValidator },
    { name: 'identification', validation: identificationValidator },
    { name: 'vinNumber', validation: vinValidator },
    { name: 'engineNumber', validation: vinValidator },
    { name: 'maxDate', validation: MaxDateValidator },
    { name: 'minDate', validation: minDateValidator },
    { name: 'IFSC', validation: IFSCValidator },
    { name: 'Branch', validation: bankBranchValidator },
    { name: 'MICR', validation: micrValidator },
    { name: 'emptySpaces', validation: emptySpacesValidator },
];
export const validationMessages = [
    { name: 'required', message: 'This field is required' },
    { name: 'alpha', message: alphaValidatorMessage },
    { name: 'charactersNumbers', message: charactersNumbersValidatorMessage },
    { name: 'alphaNumeric', message: alphaNumericMessage },
    { name: 'alphaNumericVal', message: ValidateAlphaNumericMessage },
    { name: 'email', message: EmailValidatorMessage },
    { name: 'phoneNumber', message: phoneNumberValidatorMessage },
    { name: 'max', message: maxValidationMessage },
    { name: 'min', message: minValidationMessage },
    { name: 'maxLength', message: maxLengthValidationMessage },
    { name: 'minLength', message: minLengthValidationMessage },
    { name: 'doorNumbers', message: doorNumbersMessage },
    { name: 'zipCode', message: zipCodeMessage },
    { name: 'city', message: cityMessage },
    { name: 'poBox', message: poBoxMessage },
    { name: 'address', message: addressMessage },
    { name: 'identification', message: identificationMessage },
    { name: 'vinNumber', message: vinMessage },
    { name: 'engineNumber', message: vinMessage },
    { name: 'maxDate', message: MaxDateMessage },
    { name: 'minDate', message: minDateMessage },
    { name: 'IFSC', message: IFSCMessage },
    { name: 'Branch', message: bankBranchMessage },
    { name: 'MICR', message: micrMessage },
    { name: 'emptySpaces', message: emptySpacesMessage },
    { name: 'fromDateTo', message: fromDateToMessage },
];