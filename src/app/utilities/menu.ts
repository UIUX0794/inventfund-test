

//Testing Purpose Only

// const testMenu1 = 
//       {
//         text: 'Menu 1',
//         link: '/dashboard',
//         icon: 'icon-speedometer',
//         submenu: [
//             {
//                 text: 'level 1',
//                 link: '/test',
//                 submenu: [
//                   {
//                       text: 'level 1 subitem 1',
//                       link: '/test'
//                   },
//                   {
//                       text: 'level 2',
//                       link: '/test',
//                       submenu: [
//                         {
//                             text: 'level 2 sub 1',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 2',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 3',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 4',
//                             link: '/test'
//                         },
//                         {
//                             text: 'level 2 sub 5',
//                             link: '/test'
//                         }
//                     ]
//                   }
//               ]
//             },
//             {
//                 text: 'Level v2',
//                 link: '/test'
//             },
//             {
//                 text: 'Level v3',
//                 link: '/test'
//             }
//         ]
//     }


//  const testMenu2 =  {
//     text: 'Menu 2',
//     link: '/test',
//     icon: 'icon-speedometer',
//     submenu: [
//         {
//             text: 'Dashbord v1',
//             link: '/test',
//             submenu: [
//               {
//                   text: 'Dashbord v1',
//                   link: '/test'
//               },
//               {
//                   text: 'Dashbord v2',
//                   link: '/test',

//               }
//           ]
//         },
//         {
//             text: 'Dashbord v2',
//             link: '/test'
//         },
//         {
//             text: 'Dashbord v3',
//             link: '/test'
//         }
//     ]
// }

// export const testMenu3 =  {
//     text: 'Menu 3',
//     link: '/test',
//     icon: 'icon-speedometer',

// }


export const testMenu1 = {
  text: "Home",
  link: "/landing",
  icon: "vsm-icon pe-7s-home pe-3x pe-va"
  // icon:"vsm-icon home-icon pe-3x pe-va"
};

export const testMenu2 = {
  text: "Jobs",
  link: "/profile/jobs",
  icon: "vsm-icon pe-7s-tools pe-3x pe-va"
  // icon: "vsm-icon jobs-icon pe-3x pe-va"
};

export const testMenu3 = {
  text: "Connections",
  link: "/profile/connections",
  icon: "vsm-icon pe-7s-network pe-3x pe-va"
  // icon: "vsm-icon connections-icon pe-3x pe-va"
};

export const testMenu4 = {
  text: "Messages",
  link: "/profile/messages",
  icon: "vsm-icon pe-7s-chat pe-3x pe-va"
  // icon: "vsm-icon messages-icon pe-3x pe-va"
};

export const testMenu5 = {
  text: "Profile",
  link: "dprofile/editProfile",
  icon: "vsm-icon pe-7s-user pe-3x pe-va"
  // icon:"vsm-icon profile-icon pe-3x pe-va"
  //   icon: "vsm-icon pe-7s-car"
};

export const menuForTesting = [testMenu1, testMenu2, testMenu3, testMenu4, testMenu5];

// export const commissionerMenu = [
//     testMenu1,
//     testMenu2,
//     testMenu3,

// ]

