// export const SERVERTYPE = "DEVELOPMENT";
// export const SERVER_URL_DEV = "http://localhost:4000";

// set type DEVELOPMENT , TESTING , UAT , PRODUCTION

export const SERVERTYPE: string = 'DEVELOPMENT';
export const SERVERURLDEV = 'http://inventfunds-dev.crecientech.com/';
export const SERVERURLTEST = 'https://inventfunds-demo.crecientech.com/';

export const SERVERREDRECTURLDEV = 'http://inventfunds-dev.crecientech.com/api/auth/getToken';
export const SERVERREDRECTURLTESTIN = 'http://inventfunds-dev.crecientech.com/api/auth/getToken';
export const SERVERREDRECTURLUAT = '';
export const SERVERREDRECTURLPROD = '';

export const SERVERREDRECTURLCHANGEPASSWORDDEV =
  'http://inventfunds-dev.crecientech.com/api/auth/getTokenChangePassword';
export const SERVERREDRECTURLCHANGEPASSWORDTESTIN =
  'https://inventfunds-demo.crecientech.com/api/auth/getTokenChangePassword';
export const SERVERREDRECTURLCHANGEPASSWORDUAT = '';
export const SERVERREDRECTURLCHANGEPASSWORDPROD = '';

// Sign-Up ,Sign-In  and Change Password
export const SIGNIN = './api/user/mongoSignIn';
export const SIGNUP = './api/user/mongoSignUp';
export const CHANGEPASSWORD = './api/user/mongoChangePassword';
export const RESETPASSWORD = './api/user/mongoResetPassword';
export const VERIFYTOKEN = '/api/user/verifyToken';
export const UPDATEORGANDROLE = './api/user/updateOrgAndRole';
export const COMPARECURRENTPASSWORD = './api/user/comparePassword';
export const LINKEDINSIGNIN = './api/auth/linkedin/callback';
export const LINKEDINREQUESTTOKEN = './api/auth/requestToken';
export const GOOGLE = './auth/google';
export const GOOGLECALLBACK = './auth/google/callback';
export const GETTOKEN = './api/user/getToken';

export const SINGLEATTACHMENT = './api/attachment/singleAttachment';
export const UPDATEATTACHMENT = './api/attachment/updateAttachment';
export const DELETEATTACHMENT = './api/attachment/deleteAttachment';
export const DELETEATTACHMENTDATARECORD = './api/attachment/deleteAttachmentSingle';
export const GETATTACHMENTDATA = './api/attachment/getSignedData';
export const RETRIEVEATTACHMENTDATASIGNED = './api/attachment/retrieveAttachmentDataWithSignedUrl';
export const RETRIEVEATTACHMENTDATASIGNEDWITHID = './api/attachment/getSignedUrlWithId';

export const CREATEPROFILE = './api/profileData/createProfile';
export const UPDATEPROFILE = './api/profileData/updateprofile';
export const RETRIVEPROFILE = './api/profileData/retrieveProfileData';

export const CREATEPROFILESUMMARY = './api/profileSummary/createProfileSummary';
export const RETRIEVEPROFILESUMMARY = './api/profileSummary/retrieveProfileSummary';
export const RETRIEVEPROFILESUMMARYONE = './api/profileSummary/retrieveProfileSummaryOne';
export const UPDATEPROFILESUMMARY = './api/profileSummary/updateProfileSummary';
export const RETRIEVEATTACHMENTDATA = './api/profileSummary/retrieveAttachmentData';

export const CREATEPROFESSIONALDETAILS = './api/professionalDetails/createProfessionalDetails';
export const RETRIEVEPROFESSIONALDETAILS = './api/professionalDetails/retrieveProfessionalDetails';
export const RETRIEVEPROFESSIONALDETAILSONE =
  './api/professionalDetails/retrieveProfessionalDetailsOne';
export const UPDATEPROFESSIONALDETAILS = './api/professionalDetails/updateProfessionalDetails';
export const RETRIEVELOOKUPDATA = './api/professionalDetails/retrieveLookupData';
export const RETRIEVESCREENBUILDERGROUPED = './api/professionalDetails/getScreenBuildergrouped';
export const DELETEPROFESSIONALDETAILS = './api/professionalDetails/deleteProfessionalDetails';

export const CREATEACHIDETAILS = './api/achievementsDetails/createAchievementsDetails';
export const RETRIEVEACHIDETAILS = './api/achievementsDetails/retrieveAchievementsDetails';
export const RETRIEVEACHIDETAILSONE = './api/achievementsDetails/retrieveAchievementsDetailsOne';
export const UPDATEACHIDETAILS = './api/achievementsDetails/updateAchievementsDetails';
export const DELETEACHIDETAILS = './api/achievementsDetails/deleteAchiDetails';
export const RETRIEVELOOKUPDATAACHI = './api/achievementsDetails/retrieveLookupData';
export const RETRIEVESCREENBUILDERGROUPEDACHI = './api/achievementsDetails/getScreenBuildergrouped';

export const CREATESKILLSDETAILS = './api/skillsDetails/createSkillsDetails';
export const RETRIEVESKILLSDETAILS = './api/skillsDetails/retrieveSkillsDetails';
export const RETRIEVESKILLSDETAILSONE = './api/skillsDetails/retrieveSkillsDetailsOne';
export const UPDATESKILLSDETAILS = './api/skillsDetails/updateSkillsDetails';
export const DELETESKILLSDETAILS = './api/skillsDetails/deleteAchiDetails';
export const RETRIEVELOOKUPDATASKILLS = './api/skillsDetails/retrieveLookupData';
export const RETRIEVESCREENBUILDERGROUPEDSKILLS = './api/skillsDetails/getScreenBuildergrouped';
export const RETRIEVESCREENBUILDERLOOKUPSKILLS = './api/skillsDetails/retrieveLookUpDetails';

export const CREATEEDUCATIONDETAILS = './api/educationDetails/createEducationDetails';
export const RETRIEVEEDUCATIONDETAILS = './api/educationDetails/retrieveEducationDetails';
export const RETRIEVEEDUCATIONDETAILSONE = './api/educationDetails/retrieveEducationDetailsOne';
export const UPDATEEDUCATIONDETAILS = './api/educationDetails/updateEducationDetails';
export const DELETEEDUCATIONDETAILS = './api/educationDetails/deleteEducationDetails';

//Mongo DB Related End points - for CRUD

export const RETRIEVEMONGOALL = './api/mongoDb/retrieveAll';
export const RETRIEVESCREENBUILDERALL = './api/mongoDb/retrieveAllScreenbuilder';
export const CREATEMONGODATA = './api/mongoDb/createData';
export const UPDATEMONGODATA = './api/mongoDb/updateData';
export const DELETEMONGODATA = './api/mongoDb/deleteData';
export const RETRIEVEMONGOONE = './api/mongoDb/retrieveOne';

// chat panel

export const CREATECHATDATA = './api/chatPusher/createChat';
export const CREATECHANNEL = './api/chatPusher/createChannel';
export const GETCONTACTS = './api/chatPusher/retriveContacts';
export const RETRIVECHATS = './api/chatPusher/retriveAllMessages';
export const UPDATEMESSAGESTATUS = './api/chatPusher/updateMessageStatus';
export const UPDATECHANNELID = './api/chatPusher/updateChannelId';
export const GETACTIVECHANNELCOUNT = './api/chatPusher/activeChannelCount';
